package com.watsco.hvac;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.watsco.hvac.impl.SerialToDiscreteImpl;
//RestModelSerialLookupController/SerialtoDiscreteLookUp?buName=CARRIER ENTERPRISE&userId=0&installId=0&udid=0&reportId=0&serialNo=1111V01292&modelNo=0&latitude=&longitude=40.22
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.URLGenerationUtility;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Path("/RestModelSerialLookupController")
public class RestModelSerialLookupController {

    //MAWB-
    private static Logger logger = Logger.getLogger(RestModelSerialLookupController.class);

    @GET
    @Path("/SerialtoDiscreteLookUp")
    public Response customerAuthenticate(@QueryParam("buName") String buName,
            @QueryParam("userId") int userId,
            @QueryParam("installId") int installId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("serialNo") String serialNo,
            @QueryParam("modelNo") String modelNo,
            @QueryParam("latitude") String latitude,
            @QueryParam("longitude") String longitude, @Context UriInfo uriInfo) throws TransformerException, JSONException {
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "RestModelSerialLookupController", "SerialtoDiscreteLookUp", uriInfo, "");
        SerialToDiscreteImpl STDImpl = new SerialToDiscreteImpl();
        String searchedData = "";

        String response = STDImpl.OAuthAccess(buName, userId, installId, udid, reportId, serialNo, modelNo, latitude, longitude);

        if (response.contains("ResultType")) {
            JSONObject jObject = new JSONObject(response);
            searchedData = jObject.getString("ResultType");
            if (searchedData.length() > 0) {
                searchedData = "Search by " + searchedData;
            }
        }

        UsageTracking usageTracking = new UsageTracking();
        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, serialNo, "");

        return Response.status(200).entity(response).build();
    }

}
