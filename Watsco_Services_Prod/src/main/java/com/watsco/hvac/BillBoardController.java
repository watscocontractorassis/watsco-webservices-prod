package com.watsco.hvac;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;

import com.watsco.hvac.impl.GetBillBoardReport;
import com.watsco.hvac.utils.URLGenerationUtility;

@Path("/BillBoard")
public class BillBoardController {

    private static final Logger logger = Logger.getLogger(BillBoardController.class);
    GetBillBoardReport getBillBoardReport = new GetBillBoardReport();

    @GET
    @Path("/BUImage")
    public Response BillBoardResponse(@QueryParam("buName") String buName, @QueryParam("device") String device,
            @QueryParam("userId") int userId, @QueryParam("udid") String udid,
            @QueryParam("language") String language, @QueryParam("appVersion") String appVersion,
            @QueryParam("branchNumber") String branchNumber, @Context UriInfo uriInfo)
            throws TransformerException {
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "BillBoard", "BUImage", uriInfo, "");
        //		String[] keyVakue 	= CacheManager.getCacheValueUsingQueryParams(uriInfo, "userId");
        String response = null;

        response = getBillBoardReport.getBillBoardImageLink(buName, device, userId, udid, language, appVersion, branchNumber, uriInfo);
        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/FeaturedBillBoard")
    public Response InventoryBillBoardResponse(@QueryParam("buName") String buName,
            @QueryParam("device") String device,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("language") String language,
            @QueryParam("appVersion") String appVersion,
            @QueryParam("branchNumber") String branchNumber,
            @QueryParam("feature") String feature,
            @Context UriInfo uriInfo)
            throws TransformerException {

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "BillBoard", "FeaturedBillBoard", uriInfo, "");
        String response = getBillBoardReport.getFeaturedBillBoardImageLink(buName, device, userId, udid, language, appVersion, branchNumber, feature);

        return Response.status(200).entity(response).build();
    }
}
