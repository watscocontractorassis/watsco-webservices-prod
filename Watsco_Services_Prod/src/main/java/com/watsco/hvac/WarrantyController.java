package com.watsco.hvac;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;

import com.watsco.hvac.impl.WarrantyAPI;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;
import com.watsco.hvac.utils.StringUtils;
import com.watsco.hvac.utils.URLGenerationUtility;

@Path("/Warranty")
public class WarrantyController {

    private static Logger logger = Logger.getLogger(WarrantyController.class);
    String serverUrl = "";

    @GET
    @Path("")
    //	@Produces("text/xml")
    public Response RheemWarrantyVerificationResponse(@QueryParam("manufacturer") String manufacturer,
            @QueryParam("serialNo") String serialNo,
            @QueryParam("postalCode") String postalCode,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("registered") String registered, @Context UriInfo uriInfo) throws TransformerException {
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "Warranty", "", uriInfo, "");

        //		logger.info("=============>WarrantyController RheemWarrantyVerificationResponse method invoked");
        //		RheemWarrantyRead rheemWarrantyRead = new RheemWarrantyRead();
        //		String response = rheemWarrantyRead.getRheemWarrantyVerificationResponse(manufacturer, serialNo, postalCode);
        WarrantyAPI warrantyAPI = new WarrantyAPI();
        String response = warrantyAPI.getRheemWarrantyVerificationResponse(manufacturer, serialNo, postalCode,
                installId, userId, udid, reportId, buName, registered);

        //		RheemUsageTracking rheemUsageTracking = new RheemUsageTracking();
        //		rheemUsageTracking.setReportDataSearched(installId, userId, udid, reportId, buName, serialNo, postalCode);
        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/GM/Nordyne")
    public Response nordyneWarrantyVerificationResponse(@QueryParam("manufacturer") String manufacturer,
            @QueryParam("serialNo") String serialNo,
            @QueryParam("state") String state,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName, @Context UriInfo uriInfo) throws TransformerException {
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "Warranty", "/GM/Nordyne", uriInfo, "");

        //		logger.info("=============>WarrantyController nordyneWarrantyVerificationResponse method invoked");
        WarrantyAPI warrantyAPI = new WarrantyAPI();
        String response = warrantyAPI.getNordyneWarrantyVerificationResponse(manufacturer, serialNo, state,
                installId, userId, udid, reportId, buName);

        return Response.status(200).entity(response).build();
    }

//    /Warranty/CE/EntitlementAndServiceHistory?installId=1&userId=75567&udid=0037D2AA-B8B7-4921-B469-31FA43273B1D&reportId=0&buName=CARRIER%20ENTERPRISE&provider=ServiceBench&manufacturer=CARRIER&serialNumber=1692E44669&modelNumber=&type=User&isDev=Y
    @GET
    @Path("/CE/EntitlementAndServiceHistory")
    public Response ceWarrantyEntitlementVerificationResponse(
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("provider") String provider,
            @QueryParam("manufacturer") String manufacturer,
            @QueryParam("serialNumber") String serialNumber,
            @QueryParam("modelNumber") String modelNumber,
            @QueryParam("type") String type,
            @QueryParam("isDev") String isDev,
            @Context UriInfo uriInfo) throws TransformerException {
        String response = "";
        String ceUrl = "";
        try {
            URLGenerationUtility utility = new URLGenerationUtility();
            utility.printServiceUrl("GET", "Warranty", "/CE/EntitlementAndServiceHistory", uriInfo, "");

            WarrantyAPI warrantyAPI = new WarrantyAPI();

            HashMap<String, String> exthm;

            if (serialNumber.length() >= 10) {
                if (serialNumber.charAt(0) == 'S') {
                    StringBuffer serialNoBuffer = new StringBuffer();
                    serialNoBuffer.append(serialNumber);
                    serialNoBuffer.deleteCharAt(0);
                    serialNumber = serialNoBuffer.toString();
                }
            }

            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                exthm = CommonUtil.getExternalAPIConfigProperties(buName);
            } else {
                exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
            }

            MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();

            String key = "";

            switch (buName) {
                case "CARRIER ENTERPRISE":
                    key = "CE_";
                    break;
                case "BAKER":
                    key = "BK_";
                    break;
                case "CARRIER ENTERPRISE MEXICO":
                    key = "CE_MX_";
                    break;
                case "GEMAIRE":
                    key = "GM_";
                    break;
                case "EAST COAST METALS":
                    key = "ECM_";
                    break;
                default:
                    break;
            }

            if (isDev != null) {
                isDev = isDev.trim();
                if (isDev.equalsIgnoreCase("Y")) {

                    ceUrl = exthm.get(key + "Entitlement_SerivceBench_API_DEV");
                    if (ceUrl == null || ceUrl.isEmpty()) {
                        logger.error("=== USER ID :" + userId + "=====>ServiceBench API not Found");
                        return Response.status(200).entity(response).build();
                    }
//	            ceUrl = "https://test.servicebench.com/servicebenchv5/services/ProductEntitlementService";
                } else {
                    ceUrl = exthm.get(key + "Entitlement_SerivceBench_API_PROD");

                    if (ceUrl == null || ceUrl.isEmpty()) {
                        logger.error("=== USER ID :" + userId + "=====>ServiceBench API not Found");
                        return Response.status(200).entity(response).build();
                    }
//                  ceUrl = "https://www.servicebench.com/servicebenchv5/services/ProductEntitlementService";
                }
            }

            response = warrantyAPI.getCEWarrantyVerificationResponse(installId, userId, udid, reportId, buName, manufacturer, serialNumber, modelNumber, type, ceUrl, isDev);
            return Response.status(200).entity(response).build();
        } catch (Exception e) {
            logger.info("=== USER ID :" + userId + "=====>Error Parsing: - " + e);
        }

        return Response.status(200).entity(response).build();

    }

}
