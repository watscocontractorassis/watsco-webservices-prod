package com.watsco.hvac;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.transform.TransformerException;

import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.URLGenerationUtility;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Path("/Ecommerce")
public class ECommerce
{
    //	protected static Logger logger = Logger.getLogger(ECommerce.class);
    @GET
    @Path("")
    public Response ECommerceUsageTracking(@QueryParam("manufacturer") String manufacturer,
                                           @QueryParam("productNo") String productNo,
                                           @QueryParam("installId") int installId,
                                           @QueryParam("userId") int userId, @QueryParam("udid") String udid,
                                           @QueryParam("reportId") int reportId, @QueryParam("buName") String buName,@Context UriInfo uriInfo) throws TransformerException
    {

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "Ecommerce", "", uriInfo, "");
        //		logger.info("ECommerce ECommerceUsageTracking method invoked");
        UsageTracking usageTracking = new UsageTracking();
        String searchedData = "ProductNo : "+ productNo ;
        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "","");
        return Response.status(200).entity(searchedData).build();
        //	return Response.status(200).build();
    }
}
