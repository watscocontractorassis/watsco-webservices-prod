package com.watsco.hvac.usagetracking;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.DBConnection;

public class UsageTracking {

    private static Logger logger = Logger.getLogger(UsageTracking.class);

    enum ProximityBeacon {

        IMMEDIATE, NEAR, FAR, UNKNOWN
    }

    /*public void setReportDataSearched(int installId,int userId,String udid,int reportId,String buName,String searchedData, String productNumber,
     String promptColumn)
     {
     logger.info("====================>setReportDataSearched invoked");
     
     Connection con = null;
     PreparedStatement pstmt = null;
     ResultSet rs = null;
     long executionStart = new Date().getTime();
     try {
     
     con=DBConnection.getConnection(true);
     
     java.sql.Timestamp updatedDate = new java.sql.Timestamp(new java.util.Date().getTime());
     
     
     String query = "Insert into SURFBI_USAGE_TRACKING(INSTALL_ID,USER_ID,UDID,REPORT_ID," +
     "CREATED_BY,UPDATED_BY,BU_NAME,SERVICE_NAME,PAGE_IDS,VIEW_ID,CREATION_DATE,UPDATION_DATE," +
     "PRODUCT_DESCRIPTION, PRODUCT_NUMBER,PROMPT_COLUMN_VALUES) " +
     "values (?,?,?,?,?,?,?,'ReportDataService:watsco',0,0,?,?,?,?,?)";
     
     
     pstmt = con.prepareStatement(query);
     
     pstmt.setInt(1, installId);
     pstmt.setInt(2, userId);
     pstmt.setString(3, udid);
     pstmt.setInt(4, reportId);
     pstmt.setInt(5, userId);
     pstmt.setInt(6, userId);
     pstmt.setString(7, buName);
     pstmt.setTimestamp(8, updatedDate);
     pstmt.setTimestamp(9, updatedDate);
     pstmt.setString(10, searchedData);
     pstmt.setString(11, productNumber);
     pstmt.setString(12, promptColumn);
     
     logger.info("===============>installId:"+installId+";userId:"+userId+";udid:"+udid+";reportId:"+reportId+
     ";userId:"+userId+";buName:"+buName+";updatedDate:"+updatedDate+";searchedData:"+searchedData+";promptColumn:"+promptColumn);
     
     logger.info("====================> pstmt SET");
     
     pstmt.executeUpdate();
     logger.info("====================> executeUpdate");
     
     
     } catch (Exception e) {
     // TODO Auto-generated catch block
     logger.error("=======> Exception is  " + e.getMessage());
     } finally {
     //
     DBConnection.closeConnection(rs, con, pstmt);
     }
     
     long executionEnd = new Date().getTime();
     long executionTime = executionEnd - executionStart;
     logger.info("========>Time taken for generating report for is "
     + (executionTime / 1000) + " Seconds");
     }
     */
    public void usageTrackingSp(int installId, int userId, String udid, int reportId, String buName, String searchedData, String productNumber,
            String promptColumn) {
        //		logger.info("====================>usageTrackingSp invoked");
        Connection con = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        //		long executionStart = new Date().getTime();
        try {
            con = DBConnection.getConnection(true);
            String query = "{call Usage_Tracking_Insert(?,?,?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?)}";
            cs = con.prepareCall(query);
            cs.setInt("@INSTALL_ID", installId);
            cs.setInt("@USER_ID", userId);
            cs.setString("@SERVICE_NAME", "ReportDataService:watsco");
            cs.setString("@UDID", udid);
            cs.setString("@COLUMN_IDS", null);
            cs.setString("@PAGE_IDS", "0");
            cs.setInt("@REPORT_ID", reportId);
            cs.setInt("@VIEW_ID", 0);
            cs.setString("@PROMPT_COLUMN_VALUES", promptColumn);
            cs.setString("@DRILL_REPORT_COLUMNS", null);
            cs.setString("@REPORT_COLUMN_VALUES", null);
            cs.setInt("@CREATED_BY", userId);
            cs.setInt("@UPDATED_BY", userId);
            cs.setString("@COMMENTS", null);
            cs.setString("@EMAIL", null);
            cs.setString("@BU_NAME", buName);
            cs.setString("@PRODUCT_DESCRIPTION", searchedData);
            cs.setString("@PRODUCT_NUMBER", productNumber);
            cs.setInt("@RESULT_RECORD_COUNT", 0);
            cs.setInt("@AVAILABLE_QTY", 0);
            cs.setInt("@ORIGIN_REPORT_ID", -1);
            //			logger.info("===============>installId:"+installId+";userId:"+userId+";udid:"+udid+";reportId:"+reportId+
            //			";userId:"+userId+";buName:"+buName+";searchedData:"+searchedData+";promptColumn:"+promptColumn);

            logger.info("====" + userId + "=====>Query : exec Usage_Tracking_Insert " + installId + "," + userId + ",'ReportDataService:watsco','" + udid + "',null,0," + reportId + ",0,'" + promptColumn
                    + "',null,null," + userId + "," + userId + ",null,null,'" + buName + "','" + searchedData + "','" + productNumber + "',0,0,-1");
            cs.execute();
            //			logger.info("====================> executed Procedure");
//            return cs.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("====" + userId + "=====>Exception : " + e.getMessage());
        } finally {
            //			
            DBConnection.closeConnectionCS(rs, con, cs);
        }
//        String response = cs.toString();
//		return response;

        //		long executionEnd = new Date().getTime();
        //		long executionTime = executionEnd - executionStart;
        //		logger.info("========>Time taken for generating report for is "
        //				+ (executionTime / 1000) + " Seconds");
    }

}
