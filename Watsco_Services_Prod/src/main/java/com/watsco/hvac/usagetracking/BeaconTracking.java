package com.watsco.hvac.usagetracking;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.DBConnection;


public class BeaconTracking {
    private static Logger logger = Logger.getLogger(BeaconTracking.class);
    public void setBeaconData(int userId, String udid, int major, int minor,
                              String beaconUdid, String sessionId, HashMap<String,String> inputMap) {
        //        logger.info("====================>setBeaconData invoked");
        Connection con = null;
        //		PreparedStatement pstmt = null;
        ResultSet rs = null;
        CallableStatement cs = null;
        //		long executionStart = new Date().getTime();
        try {
            
            con=DBConnection.getConnection(true);
            
            //			java.sql.Timestamp updatedDate = new java.sql.Timestamp(new java.util.Date().getTime());
            
            //			String query = "SELECT * FROM SURFBI_BEACON_TRACKING where USER_ID=:USER_ID"+
            //			" AND UDID=':UDID' AND MAJOR=:MAJOR AND MINOR=:MINOR AND BEACON_UDID=':BEACON_UDID' AND SESSION_ID=':SESSION_ID'";
            String query = "exec Beacon_Tracking_Select :USER_ID,':UDID',:MAJOR,:MINOR,':BEACON_UDID',':SESSION_ID'";
            query = setBeaconsSQLValues(userId, udid, major, minor, beaconUdid, sessionId, inputMap, query);
            logger.info("===="+userId+"=====>Query : "+query);
            //			pstmt = con.prepareStatement(query);
            //			rs = pstmt.executeQuery();
            
            cs = con.prepareCall(query);
            rs = cs.executeQuery();
            
            if(rs!=null && rs.next()){
                /*
                 UPDATE SURFBI_BEACON_TRACKING SET EVENT_TIME=?,
                 TEMPERATURE',:ACCELEROMETER,':PROXIMITY',':EVENT_ENTER',EVENT_EXIT,:EVENT_DISTANCE
                 WHERE USER_ID =? AND UDID=? AND
                 SESSION_ID=? AND MAJOR=? AND MINOR=? AND BEACON_UDID=?
                 */
                /*query = "UPDATE SURFBI_BEACON_TRACKING SET EVENT_TIME=?, ";
                 String dynamicQuery = "";
                 String whereQuery = " WHERE USER_ID =? AND UDID=? AND "+
                 "SESSION_ID=? AND MAJOR=? AND MINOR=? AND BEACON_UDID=?";
                 for(String key :inputMap.keySet()){
                 if(!key.equalsIgnoreCase("EVENT_ENTER"))
                 dynamicQuery = dynamicQuery + key+ "=?, ";
                 }
                 query +=dynamicQuery;
                 query = query.substring(0, query.length()-2);
                 
                 query +=whereQuery;
                 logger.info("===="+userId+"=====>Query : "+query);
                 pstmt = con.prepareStatement(query);
                 
                 pstmt.setTimestamp(1, updatedDate);
                 int i=2;
                 for(String key :inputMap.keySet()){
                 if(key.equalsIgnoreCase("EVENT_EXIT"))
                 {
                 if(inputMap.get(key).toString().equalsIgnoreCase("Y"))
                 pstmt.setObject(i++, updatedDate);
                 else
                 pstmt.setObject(i++, null);
                 }
                 else if(key.equalsIgnoreCase("EVENT_DISTANCE"))//float rs(6) min
                 {
                 if(Float.parseFloat(inputMap.get(key)) < rs.getFloat(6))
                 pstmt.setObject(i++, inputMap.get(key));
                 else
                 pstmt.setObject(i++, rs.getObject(6));
                 }
                 else if(key.equalsIgnoreCase("PROXIMITY"))//varchar 11 immediate near far unknown
                 {
                 if(ProximityBeacon.valueOf(rs.getString(11).toUpperCase()).
                 compareTo(ProximityBeacon.valueOf(inputMap.get(key).toString().toUpperCase())) > 0)
                 pstmt.setObject(i++, inputMap.get(key).toString());
                 else
                 pstmt.setObject(i++, rs.getString(11));
                 }
                 else if(!key.equalsIgnoreCase("EVENT_ENTER"))
                 pstmt.setObject(i++, inputMap.get(key));
                 }
                 pstmt.setInt(i++, userId);
                 pstmt.setString(i++, udid);
                 pstmt.setString(i++, sessionId);
                 pstmt.setInt(i++, major);
                 pstmt.setInt(i++, minor);
                 pstmt.setString(i++, beaconUdid);
                 pstmt.executeUpdate();
                 //				logger.info("update Status: "+sta);*/
                query = "exec Beacon_Tracking_Update :USER_ID,':UDID',':SESSION_ID',:MAJOR,:MINOR,':BEACON_UDID',':TEMPERATURE',:ACCELEROMETER,':PROXIMITY',"+
                "':EVENT_EXIT',:EVENT_DISTANCE";
                query = query.replaceAll(":TEMPERATURE",inputMap.get("TEMPERATURE") != null ? inputMap.get("TEMPERATURE") : "null");
                query = query.replaceAll(":ACCELEROMETER",inputMap.get("ACCELEROMETER") != null ? inputMap.get("ACCELEROMETER") : "null");
                query = query.replaceAll(":PROXIMITY",inputMap.get("PROXIMITY") != null ? inputMap.get("PROXIMITY") : "null");
                query = query.replaceAll(":EVENT_EXIT",inputMap.get("EVENT_EXIT") != null ? inputMap.get("EVENT_EXIT") : "null");
                query = query.replaceAll(":EVENT_DISTANCE",inputMap.get("EVENT_DISTANCE") != null ? inputMap.get("EVENT_DISTANCE") : "null");
                query = query.replaceAll(":USER_ID",String.valueOf(userId));
                query = query.replaceAll(":UDID",udid);
                query = query.replaceAll(":SESSION_ID",sessionId);
                query = query.replaceAll(":MAJOR",String.valueOf(major));
                query = query.replaceAll(":MINOR",String.valueOf(minor));
                query = query.replaceAll(":BEACON_UDID",beaconUdid);
                query = query.replaceAll("'null'", "null");
                logger.info("===="+userId+"=====>Query : "+query);
                
                cs = con.prepareCall(query);
                cs.executeUpdate();
            }
            else{
                //				query = "INSERT INTO SURFBI_BEACON_TRACKING(USER_ID,UDID,MAJOR,MINOR,BEACON_UDID,SESSION_ID,TEMPERATURE,ACCELEROMETER,PROXIMITY,EVENT_ENTER,EVENT_DISTANCE,EVENT_TIME) " +
                //						"values (:USER_ID,':UDID',:MAJOR,:MINOR,':BEACON_UDID',':SESSION_ID',':TEMPERATURE',:ACCELEROMETER,':PROXIMITY',':EVENT_ENTER',:EVENT_DISTANCE,':EVENT_TIME')";
                
                query ="exec Beacon_Tracking_Insert :USER_ID,':UDID',:MAJOR," +
                ":MINOR,':BEACON_UDID',':SESSION_ID',':TEMPERATURE'," +
                ":ACCELEROMETER,':PROXIMITY',:EVENT_DISTANCE";
                query = setBeaconsSQLValues(userId, udid, major, minor, beaconUdid, sessionId, inputMap, query);
                logger.info("===="+userId+"=====>Query : "+query);
                //				pstmt = con.prepareStatement(query);
                //				pstmt.executeUpdate();
                
                cs = con.prepareCall(query);
                cs.execute();
                
                //			        	logger.info("sta"+ sta);
            }
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("===="+userId+"=====>Exception : " + e.getMessage());
        } finally {
            //
            DBConnection.closeConnectionCS(rs, con, cs);
            
            /*try {
             if(pstmt!=null)
             pstmt.close();
             } catch (SQLException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
             }*/
        }
        
    }
    
    
    public void updateBeaconInfo(int major, int minor, String beaconUdid,
                                 HashMap<String, String> inputMap, int userId) {
        Connection con 				= null;
        PreparedStatement  pstmt 	= null;
        ResultSet rs 				= null;
        CallableStatement cs = null;
        
        try {
            con=DBConnection.getConnection(true);
            //			String sqlQuery = "SELECT * FROM BEACON_DETAILS where MAJOR=:MAJOR AND MINOR=:MINOR AND PROXIMITY_UUID=':PROXIMITY_UUID'";
            String sqlQuery = "exec Beacon_Details_Select :MAJOR,:MINOR,':PROXIMITY_UUID'";
            sqlQuery = setBeaconDetailsSQLValues(major, minor, beaconUdid, inputMap, sqlQuery);
            logger.info("===="+userId+"=====>Query : "+sqlQuery);
            //			pstmt = con.prepareStatement(sqlQuery);
            //            rs = pstmt.executeQuery();
            
            cs = con.prepareCall(sqlQuery);
            rs = cs.executeQuery();
            
            if(rs!=null && rs.next()){
                /* sqlQuery = "Update BEACON_DETAILS set ";
                 String dynamicQuery = "";
                 for(String key :inputMap.keySet()){
                 dynamicQuery = dynamicQuery + key+ "='"+inputMap.get(key)+ "', ";
                 }
                 sqlQuery +=dynamicQuery;
                 sqlQuery = sqlQuery.substring(0, sqlQuery.length()-2);
                 sqlQuery = sqlQuery+" where major="+major+" and minor="+minor+" and PROXIMITY_UUID='"+beaconUdid+"'";
                 logger.info("===="+userId+"=====>Query : "+sqlQuery);
                 pstmt = con.prepareStatement(sqlQuery);
                 pstmt.executeUpdate();*/
                sqlQuery = "exec Beacon_Details_Update :MAJOR,:MINOR,':PROXIMITY_UUID'," +
                "':MAC_ADDRESS',':RSSI',':FIRMWARE_STATE'," +
                "':MEASURED_POWER',':BATTERY_LEVEL',':ADVERTISING_INTERVAL'," +
                "':HARDWARE_VERSION',':BROADCASTING_POWER',':FIRMWARE_VERSION',':BATTERY_TYPE',':REMAINING_LIFETIME'";
                sqlQuery = setBeaconDetailsSQLValues(major, minor, beaconUdid, inputMap, sqlQuery);
                sqlQuery = sqlQuery.replaceAll("'null'", "null");
                logger.info("===="+userId+"=====>Query : "+sqlQuery);
                cs = con.prepareCall(sqlQuery);
                cs.executeUpdate();
            }else{
                //				sqlQuery = "INSERT INTO BEACON_DETAILS(MAJOR,MINOR,PROXIMITY_UUID,MAC_ADDRESS,RSSI,FIRMWARE_STATE,MEASURED_POWER," +
                //						"BATTERY_LEVEL,ADVERTISING_INTERVAL,HARDWARE_VERSION,BROADCASTING_POWER,FIRMWARE_VERSION) VALUES (:MAJOR,:MINOR,':PROXIMITY_UUID',':MAC_ADDRESS',':RSSI'," +
                //						"':FIRMWARE_STATE',':MEASURED_POWER',':BATTERY_LEVEL',':ADVERTISING_INTERVAL',':HARDWARE_VERSION',':BROADCASTING_POWER',':FIRMWARE_VERSION')";
                
                /*sqlQuery = "Insert into BEACON_DETAILS(MAJOR,MINOR,PROXIMITY_UUID";
                 String dynamicColumn = "";
                 String dynamicValues = "";
                 for(String key :inputMap.keySet()){
                 dynamicColumn = dynamicColumn +","+ key;
                 dynamicValues = dynamicValues +",'"+inputMap.get(key)+"'";
                 }
                 sqlQuery = sqlQuery+dynamicColumn;
                 sqlQuery = sqlQuery+") values ("+major+","+minor+",'"+beaconUdid+"'";
                 sqlQuery = sqlQuery+ dynamicValues +")";
                 
                 logger.info("===="+userId+"=====>Query : "+sqlQuery);
                 pstmt = con.prepareStatement(sqlQuery);
                 pstmt.executeUpdate();*/
                
                sqlQuery ="exec Beacon_Details_Insert :MAJOR,:MINOR,':PROXIMITY_UUID'," +
                "':MAC_ADDRESS',':RSSI',':FIRMWARE_STATE'," +
                "':MEASURED_POWER',':BATTERY_LEVEL',':ADVERTISING_INTERVAL'," +
                "':HARDWARE_VERSION',':BROADCASTING_POWER',':FIRMWARE_VERSION',':BATTERY_TYPE',':REMAINING_LIFETIME'";
                sqlQuery = setBeaconDetailsSQLValues(major, minor, beaconUdid, inputMap, sqlQuery);
                sqlQuery = sqlQuery.replaceAll("'null'", "null");
                logger.info("===="+userId+"=====>Query : "+sqlQuery);
                cs = con.prepareCall(sqlQuery);
                cs.execute();
            }
            
            
        } catch (Exception e) {
            
            logger.error("Exception: " + e);
            
        } finally {
            DBConnection.closeConnectionCS(rs, con, cs);
            
            try {
                if(pstmt!=null)
                    pstmt.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
    }
    
    private String setBeaconDetailsSQLValues(int major,int minor, String beaconUdid, HashMap<String, String> inputMap,String sqlQuery) {
        
        if(sqlQuery.contains(":MAJOR"))
        {
            sqlQuery = sqlQuery.replaceAll(":MAJOR", String.valueOf(major));
        }
        if(sqlQuery.contains(":MINOR"))
        {
            sqlQuery = sqlQuery.replaceAll(":MINOR", String.valueOf(minor));
        }
        if(sqlQuery.contains(":PROXIMITY_UUID"))
        {
            sqlQuery = sqlQuery.replaceAll(":PROXIMITY_UUID", beaconUdid);
        }
        if(sqlQuery.contains(":MAC_ADDRESS"))
        {
            sqlQuery = sqlQuery.replaceAll(":MAC_ADDRESS", inputMap.get("MAC_ADDRESS") != null ? inputMap.get("MAC_ADDRESS") : "null");
        }
        if(sqlQuery.contains(":RSSI"))
        {
            sqlQuery = sqlQuery.replaceAll(":RSSI", inputMap.get("RSSI") != null ? inputMap.get("RSSI") : "null");
        }
        if(sqlQuery.contains(":FIRMWARE_STATE"))
        {
            sqlQuery = sqlQuery.replaceAll(":FIRMWARE_STATE", inputMap.get("FIRMWARE_STATE") != null ? inputMap.get("FIRMWARE_STATE") : "null");
        }
        if(sqlQuery.contains(":MEASURED_POWER"))
        {
            sqlQuery = sqlQuery.replaceAll(":MEASURED_POWER", inputMap.get("MEASURED_POWER") != null ? inputMap.get("MEASURED_POWER") : "null");
        }
        if(sqlQuery.contains(":BATTERY_LEVEL"))
        {
            sqlQuery = sqlQuery.replaceAll(":BATTERY_LEVEL", inputMap.get("BATTERY_LEVEL") != null ? inputMap.get("BATTERY_LEVEL") : "null");
        }
        if(sqlQuery.contains(":ADVERTISING_INTERVAL"))
        {
            sqlQuery = sqlQuery.replaceAll(":ADVERTISING_INTERVAL", inputMap.get("ADVERTISING_INTERVAL") != null ? inputMap.get("ADVERTISING_INTERVAL") : "null");
        }
        
        if(sqlQuery.contains(":HARDWARE_VERSION"))
        {
            sqlQuery = sqlQuery.replaceAll(":HARDWARE_VERSION", inputMap.get("HARDWARE_VERSION") != null ? inputMap.get("HARDWARE_VERSION") : "null");
        }
        if(sqlQuery.contains(":BROADCASTING_POWER"))
        {
            sqlQuery = sqlQuery.replaceAll(":BROADCASTING_POWER", inputMap.get("BROADCASTING_POWER") != null ? inputMap.get("BROADCASTING_POWER") : "null");
        }
        if(sqlQuery.contains(":FIRMWARE_VERSION"))
        {
            sqlQuery = sqlQuery.replaceAll(":FIRMWARE_VERSION", inputMap.get("FIRMWARE_VERSION") != null ? inputMap.get("FIRMWARE_VERSION") : "null");
        }
        if(sqlQuery.contains(":BATTERY_TYPE"))
        {
            sqlQuery = sqlQuery.replaceAll(":BATTERY_TYPE", inputMap.get("BATTERY_TYPE") != null ? inputMap.get("BATTERY_TYPE") : "null");
        }
        if(sqlQuery.contains(":REMAINING_LIFETIME"))
        {
            sqlQuery = sqlQuery.replaceAll(":REMAINING_LIFETIME", inputMap.get("REMAINING_LIFETIME") != null ? inputMap.get("REMAINING_LIFETIME") : "null");
        }
        
        return sqlQuery;
    }
    
    
    private String setBeaconsSQLValues(int userId,String udid,int major,int minor,String beaconUdid,String sessionId,HashMap<String,String> inputMap, String sqlQuery) {
        
        java.sql.Timestamp updatedDate = new java.sql.Timestamp(new java.util.Date().getTime());
        
        if(sqlQuery.contains(":USER_ID"))
        {
            sqlQuery = sqlQuery.replaceAll(":USER_ID", String.valueOf(userId));
        }
        if(sqlQuery.contains(":UDID"))
        {
            sqlQuery = sqlQuery.replaceAll(":UDID", udid);
        }
        if(sqlQuery.contains(":MAJOR"))
        {
            sqlQuery = sqlQuery.replaceAll(":MAJOR", String.valueOf(major));
        }
        if(sqlQuery.contains(":MINOR"))
        {
            sqlQuery = sqlQuery.replaceAll(":MINOR", String.valueOf(minor));
        }
        if(sqlQuery.contains(":BEACON_UDID"))
        {
            sqlQuery = sqlQuery.replaceAll(":BEACON_UDID", beaconUdid);
        }
        if(sqlQuery.contains(":SESSION_ID"))
        {
            sqlQuery = sqlQuery.replaceAll(":SESSION_ID", sessionId);
        }
        if(sqlQuery.contains(":TEMPERATURE"))
        {
            sqlQuery = sqlQuery.replaceAll(":TEMPERATURE", inputMap.get("TEMPERATURE") != null ? inputMap.get("TEMPERATURE") : "");
        }
        if(sqlQuery.contains(":ACCELEROMETER"))
        {
            sqlQuery = sqlQuery.replaceAll(":ACCELEROMETER", inputMap.get("ACCELEROMETER") != null ? inputMap.get("ACCELEROMETER") : "0");
        }
        if(sqlQuery.contains(":PROXIMITY"))
        {
            sqlQuery = sqlQuery.replaceAll(":PROXIMITY", inputMap.get("PROXIMITY") != null ? inputMap.get("PROXIMITY") : "Unknown");
        }
        if(sqlQuery.contains(":EVENT_ENTER"))
        {
            sqlQuery = sqlQuery.replaceAll(":EVENT_ENTER", updatedDate.toString());
        }
        if(sqlQuery.contains(":EVENT_DISTANCE"))
        {
            sqlQuery = sqlQuery.replaceAll(":EVENT_DISTANCE", inputMap.get("EVENT_DISTANCE") != null ? inputMap.get("EVENT_DISTANCE") : "100");
        }
        if(sqlQuery.contains(":EVENT_TIME"))
        {
            sqlQuery = sqlQuery.replaceAll(":EVENT_TIME", updatedDate.toString());
        }
        if(sqlQuery.contains(":EVENT_EXIT"))
        {
            sqlQuery = sqlQuery.replaceAll(":EVENT_EXIT", updatedDate.toString());
        }
        return sqlQuery;
    }
    
}
