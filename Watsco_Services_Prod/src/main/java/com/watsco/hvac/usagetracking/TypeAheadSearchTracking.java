package com.watsco.hvac.usagetracking;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.WebserviceResultXml;

public class TypeAheadSearchTracking {

    private static Logger logger = Logger.getLogger(TypeAheadSearchTracking.class);

    public String typeOverSearch(int userId, String search, String buName, String searchType, String search2, String regionId, int inventoryCount) {
        Connection con = null;
        ResultSet rs = null;
        String response = "";
        CallableStatement cs = null;
        String sqlQuery = "";
        try {
            con = DBConnection.getConnection(false);
            if (searchType.equalsIgnoreCase("warranty")) {
                sqlQuery = "Exec Warranty_Parts_TypeAhead '" + search2 + "','" + buName + "','" + search + "'";
            } else if (searchType.equalsIgnoreCase("Product")) {
                if (regionId != null) {
                    if (buName.equalsIgnoreCase("CARRIER ENTERPRISE") && ((regionId.length() > 0))) {
                        sqlQuery = "Exec PA_Typeahead '" + search + "','" + buName + "','" + regionId + "','" + inventoryCount + "'";
//                                       sqlQuery ="Exec PA_Typeahead '24AN', 'CARRIER ENTERPRISE', 4, '-1'";
                    } else {
                        sqlQuery = "Exec PA_Typeahead '" + search + "','" + buName + "',NULL,'" + inventoryCount + "'";
                    }
                } else {
                    sqlQuery = "Exec PA_Typeahead '" + search + "','" + buName + "',NULL,'" + inventoryCount + "'";
                }
            } else if (searchType.equalsIgnoreCase("EquipBOM")) {
                sqlQuery = "Exec Model_Typeahead '" + search + "','" + buName + "'";
            } else {
                sqlQuery = "EXEC Document_Typeahead '" + search + "','" + buName + "'";
            }

            sqlQuery = sqlQuery.replaceAll("'null'", "null");

            System.out.print(sqlQuery);
            logger.info("====" + userId + "=====>Report Query : " + sqlQuery);
            cs = con.prepareCall(sqlQuery);
            rs = cs.executeQuery();

            response = WebserviceResultXml.getTypeOverSearchXMLDocument(rs);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("====" + userId + "=====>Exeption : " + e);

        } finally {
            DBConnection.closeConnectionCS(rs, con, cs);
        }
        return response;

    }

}
