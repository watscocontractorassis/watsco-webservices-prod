package com.watsco.hvac;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;

import com.sun.jersey.api.client.ClientResponse.Status;
import com.watsco.hvac.impl.ClaimAPI;
import com.watsco.hvac.utils.EncyrptionUtils;
import com.watsco.hvac.utils.StringUtils;
import com.watsco.hvac.utils.URLGenerationUtility;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Path("/Claim")
public class Claims {

    private static Logger logger = Logger.getLogger(Claims.class);

    @GET
    @Path("/Request")
    public Response ClaimRequest(@QueryParam("manufacturer") String manufacturer,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("fields") List<String> fields,
            @Context UriInfo uriInfo) throws TransformerException {

        String url;
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "Claim", "Request", uriInfo, "");
        //		logger.info("=============>Claims ClaimsRequest method invoked");
        HashMap<String, String> hm = new HashMap<String, String>();

        for (String s : fields) {
            int index = s.indexOf("=");
            //			logger.info("key:"+s.substring(0, index)+" value:"+s.substring(index+1,s.length()));

            hm.put(s.substring(0, index), s.substring(index + 1, s.length()));

        }
        ClaimAPI claimAPI = new ClaimAPI();
        String response = claimAPI.getClaimResponse(manufacturer, installId, userId, udid, reportId, buName, hm);

        return Response.status(Status.OK).entity(response).build();

    }

}
