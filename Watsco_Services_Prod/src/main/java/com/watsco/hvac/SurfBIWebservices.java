package com.watsco.hvac;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;

import com.watsco.hvac.cache.CacheManager;
import com.watsco.hvac.common.CommonMethods;
import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.DocumentConvertor;
import com.watsco.hvac.dao.WebserviceResultXml;
import com.watsco.hvac.impl.SurfBIWebservicesImpl;
import com.watsco.hvac.usagetracking.BeaconTracking;
import com.watsco.hvac.usagetracking.TypeAheadSearchTracking;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;
import com.watsco.hvac.utils.StringUtils;
import com.watsco.hvac.utils.URLGenerationUtility;

@Path("/SurfBIWS")
public class SurfBIWebservices {

    private static Logger logger = Logger.getLogger(SurfBIWebservices.class);
    String serverUrl = "";
    URLGenerationUtility utility = new URLGenerationUtility();

    @GET
    @Path("/CheckStoreAvailability")
    public Response CheckStoreAvailability(@QueryParam("buName") String buName,
            @QueryParam("branchWid") int branchWid,
            @QueryParam("userId") int userId, @Context UriInfo uriInfo) throws TransformerException {
        //		logger.info("SurfBIWebservices CheckStoreAvailability method invoked");
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "SurfBIWS", "CheckStoreAvailability", uriInfo, "");

        SurfBIWebservicesImpl surfBIWebservicesImpl = new SurfBIWebservicesImpl();
        String response = surfBIWebservicesImpl.getCheckStoreAvailability(buName, branchWid, userId);

        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/BOMUsingModelNumber")
    public Response BOMUsingModelNumber(@QueryParam("buName") String buName,
            @QueryParam("userId") String userId,
            @QueryParam("modelNumber") String modelNumber,
            @QueryParam("subbu") String subBU,
            @Context UriInfo uriInfo) throws TransformerException {

        String serverUrl = "";
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "SurfBIWS", "BOMUsingModelNumber", uriInfo, "");
        //		logger.info("SurfBIWebservices BOMUsingModelNumber method invoked");
        SurfBIWebservicesImpl surfBIWebservicesImpl = new SurfBIWebservicesImpl();
        String response = "";

        String[] keyVakue = CacheManager.getCacheValueUsingQueryParams(uriInfo, null);

        if (keyVakue != null) {
            response = keyVakue[1];
        }

        if (response == null) {
            if (subBU != null) {
                response = surfBIWebservicesImpl.getBOMUsingModelNumber(buName, modelNumber, subBU);
            } else {
                response = surfBIWebservicesImpl.getBOMUsingModelNumber(buName, modelNumber, "");
            }

            CacheManager.setCache(keyVakue[0], response);
        } else {
            String sqlQuery = "";
            if (buName.equalsIgnoreCase("GEMAIRE")) {
                sqlQuery = "exec model_part_search_website ':MODEL_NUMBER', ':BU_NAME'";
            } else if (buName.equalsIgnoreCase("CARRIER ENTERPRISE")) {
//                sqlQuery = "exec model_part_search_website ':MODEL_NUMBER', ':BU_NAME'";
                sqlQuery = "EXEC model_details_search_CE_BOM_website ':MODEL_NUMBER', ':BU_NAME',:SUB_BU";
            }
            sqlQuery = sqlQuery.replaceAll(":MODEL_NUMBER", modelNumber);
            sqlQuery = sqlQuery.replaceAll(":BU_NAME", buName);
            sqlQuery = sqlQuery.replaceAll(":SUB_BU", subBU == null || subBU.isEmpty() ? "9" : subBU);

            if (CacheManager.isWriteCacheToLogEnable()) {
                logger.info("CACHE===" + userId + "=====>Report Query : " + sqlQuery);
            }
        }

        UsageTracking usageTracking = new UsageTracking();
        usageTracking.usageTrackingSp(1, 67807, null, 30, buName, "Website-ModelNumber: " + modelNumber, modelNumber, "");

        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/getMagentoId")
    public Response getMagentoId(@QueryParam("buName") String buName,
            @QueryParam("mincronNumber") String mincronNumber,
            @QueryParam("userId") int userId, @Context UriInfo uriInfo) throws TransformerException {
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "SurfBIWS", "getMagentoId", uriInfo, "");
        //		logger.info("SurfBIWebservices getMagentoId method invoked");
        SurfBIWebservicesImpl surfBIWebservicesImpl = new SurfBIWebservicesImpl();
        String response = surfBIWebservicesImpl.getMagentoId(buName, mincronNumber, userId);

        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/getPrice")
    public Response getPrice(@QueryParam("buName") String buName,
            @QueryParam("customerNumber") int customerNumber,
            @QueryParam("productNumber") String productNumber,
            @QueryParam("userId") int userId,
            @QueryParam("mincronNumber") String mincronNumber,
            @QueryParam("branchId") int branchId, @Context UriInfo uriInfo) throws TransformerException, ParserConfigurationException {
        //		logger.info("SurfBIWebservices getPrice method invoked");
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "SurfBIWS", "getPrice", uriInfo, "");
        SurfBIWebservicesImpl surfBIWebservicesImpl = new SurfBIWebservicesImpl();
        String response;

        String key1 = "";

        switch (buName) {
            case "CARRIER ENTERPRISE":
                key1 = "CE_";
                break;
            case "BAKER":
                key1 = "BK_";
                break;
            case "CARRIER ENTERPRISE MEXICO":
                key1 = "CE_MX_";
                break;
            case "GEMAIRE":
                key1 = "GM_";
                break;
            case "EAST COAST METALS":
                key1 = "ECM_";
                break;
            default:
                break;
        }
        HashMap<String, String> exthm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }

        String pricingEnable = exthm.get(key1 + "PRICING_API");

        if (!pricingEnable.equalsIgnoreCase("Y")) {
            WebserviceResultXml resultXML = new WebserviceResultXml();
            response = resultXML.getXMLDocumment();
            logger.info("===" + userId + "======>Response :" + response);
        } else {

            response = surfBIWebservicesImpl.getPrice(buName, customerNumber, productNumber, userId, mincronNumber, branchId);
        }

        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/signout")
    public void updateRegistration(@QueryParam("id") int id,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName, @Context UriInfo uriInfo) throws TransformerException {
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "SurfBIWS", "signout", uriInfo, "");
        SurfBIWebservicesImpl surfBIWebservicesImpl = new SurfBIWebservicesImpl();
        surfBIWebservicesImpl.updateRegistration(id, userId);
        UsageTracking usageTracking = new UsageTracking();
        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, "signout", "", "");

    }

    @GET
    @Path("/BeaconInfo")
    public void BeaconInfo(@QueryParam("major") int major,
            @QueryParam("minor") int minor,
            @QueryParam("beaconUdid") String beaconUdid,
            @QueryParam("userId") int userId,
            @QueryParam("fields") List<String> fields, @Context UriInfo uriInfo) throws TransformerException {

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "SurfBIWS", "BeaconInfo", uriInfo, "");
        HashMap<String, String> inputMap = new HashMap<String, String>();
        for (String s : fields) {
            int index = s.indexOf("=");
            //			logger.info("key:"+s.substring(0, index).toUpperCase()+" value:"+s.substring(index+1,s.length()));
            if (index != -1) {
                inputMap.put(s.substring(0, index).toUpperCase(), s.substring(index + 1, s.length()));
            }

        }

        BeaconTracking beaconTracking = new BeaconTracking();
        beaconTracking.updateBeaconInfo(major, minor, beaconUdid, inputMap, userId);
    }

    @GET
    @Path("/ClearCache")
    public Response ClearCache(@QueryParam("pw") String pw, @Context HttpServletRequest request) {
        serverUrl = utility.getServerNameFromExternalFiles();
        String url = serverUrl + "watsco-v4-services/watsco/SurfBIWS/ClearCache?pw=" + pw;
        logger.info("GET Webservice URL :" + url);
        String response = "";
        if (pw.equalsIgnoreCase("951159")) {
            String ip = request.getRemoteAddr();
            logger.info("Cleared Cache from the IP : " + ip);
            response = CacheManager.clearCache();
            return Response.status(200).entity(response).build();
        }
        return Response.status(200).entity("Fail").build();
    }
//    watsco-v4-services/watsco/SurfBIWS/typeOverSearch?search=24ANA124A0030030&installId=1&userId=75401&udid=D0104300-50DF-48AC-871D-BE87034D699D&reportId=36&buName=CARRIER%20ENTERPRISE&&searchType=Warranty&search2=329&regionId=5

    @GET
    @Path("/typeOverSearch")
    public Response typeOverSearch(@QueryParam("search") String search,
            @QueryParam("search2") String search2,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("searchType") String searchType,
            @QueryParam("regionId") String regionId,
            @QueryParam("inventoryCount") int inventoryCount,
            @Context UriInfo uriInfo) throws TransformerException {

        utility.printServiceUrl("GET", "SurfBIWS", "typeOverSearch", uriInfo, "");
        //		SurfBIWebservicesImpl surfBIWebservicesImpl = new SurfBIWebservicesImpl();
        //		String response = surfBIWebservicesImpl.typeOverSearch(userId,search,buName,searchType);

        TypeAheadSearchTracking typeAheadSearchWebservicesImpl = new TypeAheadSearchTracking();
        String response = typeAheadSearchWebservicesImpl.typeOverSearch(userId, search, buName, searchType, search2, regionId, inventoryCount);

        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/marketingChannel")
    public Response getMarketingChannel(@QueryParam("buName") String buName, @Context UriInfo uriInfo) throws TransformerException, ClassNotFoundException, ParserConfigurationException, SQLException, IOException {
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "SurfBIWS", "marketingChannel", uriInfo, "");
        SurfBIWebservicesImpl surfBIWebservicesImpl = new SurfBIWebservicesImpl();
        String response = "";
        response = surfBIWebservicesImpl.getMarketingChannel(buName);
        return Response.status(200).entity(response).build();
    }

//    SurfBIWS/supercedesInfo?buName=CARRIER ENTERPRISE&installId=&userId=1&udid=&reportId=&id=EF17BG277&latitude=28.018627&longitude=-82.530618
    @GET
    @Path("/supercedesInfo")
    public Response getSupercedesInfo(
            @QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @Context UriInfo uriInfo) throws TransformerException {
        Connection con = null;
        String response = "";
        Statement stmt = null;
        ResultSet rs;
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "SurfBIWS", "supercedesInfo", uriInfo, "");
        try {
            con = DBConnection.getConnection(false);

            HashMap<Object, Object> keyValue = CommonMethods.removeNullValues(uriInfo);

            String supercedeInfo = "EXEC Supersede_History '" + keyValue.get("buName") + "','" + keyValue.get("id") + "','" + keyValue.get("latitude") + "','" + keyValue.get("longitude") + "','" + keyValue.get("regionId") + "'";

            supercedeInfo = supercedeInfo.replaceAll("'null'", "null");

            logger.info("=== USER ID :" + userId + "===getSupercedesInfo()===>Report Query : " + supercedeInfo);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(supercedeInfo);

            DocumentConvertor supercedesImpl = new DocumentConvertor();
            response = supercedesImpl.JSONConvertor(rs);
            logger.info("=== USER ID :" + userId + "===getSupercedesInfo()==>Response : " + response);

            if (keyValue.get("recordType") != null) {
                if (keyValue.get("recordType").toString().equalsIgnoreCase("S")) {
                    UsageTracking usageTracking = new UsageTracking();
                    usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, "Supercedes Info - " + keyValue.get("id"), "", "");
                } else if (keyValue.get("recordType").toString().equalsIgnoreCase("A")) {
                    UsageTracking usageTracking = new UsageTracking();
                    usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, "Alternatives Info - " + keyValue.get("id"), "", "");
                }
            }

            return Response.status(200).entity(response).build();
        } catch (Exception e) {
            logger.error("=========>Exeption in getSupercedesInfo() : " + e.toString());
            return Response.status(200).entity(e.toString()).build();
        }

    }

}
