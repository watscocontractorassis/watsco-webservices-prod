package com.watsco.hvac;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import com.watsco.hvac.DBConnection.WarrantyAPIConnection;
import com.watsco.hvac.dto.ClaimDTO;
import com.watsco.hvac.dto.WarrantyResponseDTO;
import com.watsco.hvac.impl.GetClaimInquiryImpl;
import com.watsco.hvac.impl.MailingAPIImpl;
import com.watsco.hvac.impl.SubmitClaimImpl;
import com.watsco.hvac.impl.WarrantyClaimImpl;
import com.watsco.hvac.impl.WatscoCommonImpl;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.JSONExtractor;
import com.watsco.hvac.utils.LocalCommonUtil;
import com.watsco.hvac.utils.StringUtils;
import com.watsco.hvac.utils.URLGenerationUtility;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.DefaultValue;

@Path("/WarrantyProcessing")
public class WarrantyClaims {

    protected static Logger logger = Logger.getLogger(WarrantyClaims.class);
    String serverUrl = "";
    JSONExtractor extractor = new JSONExtractor();
    URLGenerationUtility utility = new URLGenerationUtility();

    @GET
    @Path("/ClaimsType")
    public Response getNewWarrantyClaimsInfo(
            @QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("type") String type,
            @QueryParam("partNumber") String partNumber,
            @Context UriInfo uriInfo) throws TransformerException {

        utility.printServiceUrl("GET", "WarrantyProcessing", "ClaimsType", uriInfo, "");

        WarrantyAPIConnection newWarrantyClaimImpl = new WarrantyAPIConnection();
        String response = newWarrantyClaimImpl.getNewWarrantyClaimAPIInfo(buName, installId, userId, udid, reportId, type, partNumber, uriInfo);
        return Response.status(200).entity(response).build();
    }

    @POST
    @Path("/submitClaimAPI")
//    @Consumes(MediaType.TEXT_XML)
    public Response submitClaimResponse(InputStream incomingData,
            @QueryParam("buWid") int buWid,
            @QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("returnType") String returnType,
            @QueryParam("type") String type,
            @QueryParam("save") String save,
            @QueryParam("isDev") String isDev,
            @Context UriInfo uriInfo) throws TransformerException, ParserConfigurationException, SAXException, IOException, JSONException {
        HashMap<String, String> exthm;

        JSONObject jSONObject = extractor.getJsonObject(incomingData);
        utility.printServiceUrl("POST", "WarrantyProcessing", "submitClaimAPI", uriInfo, jSONObject.toString());

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }

        WarrantyClaimImpl warrantyClaimImpl = new WarrantyClaimImpl();
        String response = "";

        WatscoCommonImpl credentialsDetails = new WatscoCommonImpl();

        String jsonResponse = credentialsDetails.getCredentials("ServiceBench", type, buName, userId, isDev);

        JSONObject jObject = new JSONObject(jsonResponse);
        String SB_ID = jObject.getString("SB_ID");
        String UId = jObject.getString("UId");
        String password = jObject.getString("password");
        String processStatus = "";

        if (save.equalsIgnoreCase("Y")) {
            processStatus = "Save";
            ClaimDTO claimYDTO = JSONExtractor.jsonExtractorClaim(jSONObject);
            response = warrantyClaimImpl.getTransferClaim(claimYDTO, save, SB_ID, installId, udid, userId, UId, reportId, buName, returnType, buWid, processStatus);
        } else {
            processStatus = "Submitted";
            ClaimDTO claimNDTO = JSONExtractor.jsonExtractorClaim(jSONObject);
            SubmitClaimImpl submitClaimImpl = new SubmitClaimImpl();
            response = submitClaimImpl.getSubmitClaimAuthentication(claimNDTO, installId, userId, udid, reportId, buName, returnType, SB_ID, UId, password, save, isDev, buWid, processStatus);
            WarrantyResponseDTO.makeSubmitClaimNULL();
        }

        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/viewClaim")
    public Response getViewClaim(
            @QueryParam("referenceNumber") String referenceNumber,
            @QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId, @Context UriInfo uriInfo
    ) {

        utility.printServiceUrl("GET", "WarrantyProcessing", "viewClaim", uriInfo, "");

        String response = "";
        String searchedData = "";
        if (!(referenceNumber.equals(""))) {
            searchedData = "View Claim API. Reference Number - " + referenceNumber;
        } else {
            searchedData = "View Claim API";
        }

        referenceNumber = referenceNumber.trim();
        WarrantyAPIConnection viewClaimReport = new WarrantyAPIConnection();
        response = viewClaimReport.getViewClaimAuthentication(referenceNumber, buName, installId, userId, udid, reportId);

        UsageTracking usageTracking = new UsageTracking();
        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");
        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/claimInquiry")
    public Response GetClaimInquiryReport(
            @QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("claimNumber") String claimNumber,
            @QueryParam("Claim_WID") int Claim_WID,
            @QueryParam("type") String type,
            @QueryParam("isDev") String isDev, @Context UriInfo uriInfo
    ) {
        utility.printServiceUrl("GET", "WarrantyProcessing", "claimInquiry", uriInfo, "");

        String response = "";

        GetClaimInquiryImpl claiminquiry = new GetClaimInquiryImpl();
        response = claiminquiry.GetClaimInquiryReport(claimNumber, Claim_WID, buName, installId, userId, udid, reportId, type, isDev);
        WarrantyResponseDTO.makeInQuiryClaimNULL();
        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/retrievalClaim")
    public Response getRetreiveClaim(
            @QueryParam("retrievalCode") String retrievalCode,
            @QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId, @Context UriInfo uriInfo) {
        utility.printServiceUrl("GET", "WarrantyProcessing", "retrievalClaim", uriInfo, "");

        String response = "";
        String searchedData = "";
        if (!(retrievalCode.equals(""))) {
            searchedData = "Retreive Claim API. Retrieval Code - " + retrievalCode;
        } else {
            searchedData = "Retreive Claim API";
        }

        WarrantyAPIConnection retreiveClaimReport = new WarrantyAPIConnection();
        response = retreiveClaimReport.getRetreiveClaimAuthentication(retrievalCode, buName, installId, userId, udid, reportId);
        UsageTracking usageTracking = new UsageTracking();
        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");
        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/smartyStreets")
    public Response getSmartyStreets(
            @QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("street") String street,
            @QueryParam("street2") String street2,
            @QueryParam("city") String city,
            @QueryParam("state") String state,
            @QueryParam("zipCode") String zipCode,
            @Context UriInfo uriInfo) {
        utility.printServiceUrl("GET", "WarrantyProcessing", "smartyStreets", uriInfo, "");

        String response = "Null";

        WarrantyAPIConnection smartyStreets = new WarrantyAPIConnection();

//    	MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        String STREET = uriInfo.getQueryParameters().getFirst("street");
        String STREET2 = uriInfo.getQueryParameters().getFirst("street2");
        String ZIPCODE = uriInfo.getQueryParameters().getFirst("zipCode");
        String STATE = uriInfo.getQueryParameters().getFirst("state");
        String CITY = uriInfo.getQueryParameters().getFirst("city");
        try {
            if ((STREET.equals("")) || (CITY.equals("")) || (STATE.equals("")) || (ZIPCODE.equals(""))) {
                if (!(STREET.equals("")) && !(ZIPCODE.equals(""))) {
                    response = smartyStreets.getSmartyStreetsData(installId, userId, udid, reportId, buName, street, street2, city, state, zipCode);
                    return Response.status(200).entity(response).build();
                } else if (!(STREET.equals("")) || !(CITY.equals("")) || !(STATE.equals(""))) {
                    response = smartyStreets.getSmartyStreetsData(installId, userId, udid, reportId, buName, street, street2, city, state, zipCode);
                    return Response.status(200).entity(response).build();
                } else {
                    response = "Each address submitted must have non-blank values for one of the following field combinations to be eligible for a positive address match \n"
                            + "\nstreet + city + state."
                            + "\nstreet + zipcode.";
                    return Response.status(200).entity(response).build();
                }
            } else {
                response = smartyStreets.getSmartyStreetsData(installId, userId, udid, reportId, buName, street, street2, city, state, zipCode);
                return Response.status(200).entity(response).build();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    /*
    @GET
    @Path("/mandrillAPI")
    public Response sendEmail(InputStream incomingData,
            @QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("claimNumber") String claimNumber,
            @QueryParam("referenceNumber") String referenceNumber,
            @QueryParam("userName") String userName,
            @QueryParam("emailId") String emailId,
            @Context UriInfo uriInfo) {
        String response = "";
        try {

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            //get current date time with Date()
            Date date = new Date();
            String dateTime = dateFormat.format(date);

            HashMap<String, String> exthm;

            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                exthm = CommonUtil.getExternalAPIConfigProperties(buName);
            } else {
                exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
            }

            String key = "";

            switch (buName) {
                case "CARRIER ENTERPRISE":
                    key = "CE_";
                    break;
                case "BAKER":
                    key = "BK_";
                    break;
                case "CARRIER ENTERPRISE MEXICO":
                    key = "CE_MX_";
                    break;
                case "GEMAIRE":
                    key = "GM_";
                    break;
                case "EAST COAST METALS":
                    key = "ECM_";
                    break;
                default:
                    break;
            }

            String emailFromMandrill = exthm.get(key + "CONFIRMATION_EMAIL");
            String keyForMandrill = exthm.get(key + "CONFIRMATION_KEY");

            String mandrillString = "{"
                    + "\"key\": \"" + keyForMandrill + "\","
                    + "\"message\": {"
                    + "\"text\":\"Hello, \\r\\n \\r\\n\\n\\tClaim " + claimNumber + " with Reference Number " + referenceNumber + " was submitted by " + userName + " on " + dateTime + ". Please log into ServiceBench for more details or to edit this claim. You can also check the claim status and edit the claim from the HVAC Contractor Assist app. \\r\\n  \\r\\nThank You.\","
                    + "\"subject\": \" Successful Claim Submission Mail - CE Assist app\","
                    + "\"from_email\": \"" + emailFromMandrill + "\","
                    + "\"to\": ["
                    + "{"
                    + "\"email\": \"" + emailId + "\","
                    + "\"type\": \"to\""
                    + "}"
                    + "]"
                    + "}"
                    + "}";

            if (emailId.length() > 0) {
                WarrantyAPIConnection mandrillAPI = new WarrantyAPIConnection();
                response = mandrillAPI.getMandrillAPI(mandrillString);
            } else {
                response = "Email ID cannot be NULL";
            }

            UsageTracking usageTracking = new UsageTracking();
            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, "Mandrill Api Successfull", "", "");
            return Response.status(200).entity(response).build();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
     */
    @GET
    @Path("/UnSubmittedClaim")
    public Response getUnSubmittedClaim(@QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId, @Context UriInfo uriInfo
    ) throws TransformerException {
        utility.printServiceUrl("GET", "WarrantyProcessing", "UnSubmittedClaim", uriInfo, "");

        WarrantyAPIConnection unSubmittedClaim = new WarrantyAPIConnection();
        String response = unSubmittedClaim.getUnSubmittedClaim(userId);
        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/deleteClaim")
    public Response deleteClaim(@QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("serialNumber") String serialNumber,
            @QueryParam("referenceNumber") String referenceNumber, @Context UriInfo uriInfo
    ) throws TransformerException {
        utility.printServiceUrl("GET", "WarrantyProcessing", "deleteClaim", uriInfo, "");

        WarrantyAPIConnection dClaim = new WarrantyAPIConnection();
        String response = dClaim.deleteClaim(serialNumber, referenceNumber);
        return Response.status(200).entity(response).build();
    }

//    http://207.127.14.48:8081/watsco-v4-services/watsco/WarrantyProcessing/retrieveClaimPortal?sbid=493878876&sbun=rtiuser&sbpw=UnRpdXNlcjEyMzQ=&sudo=&rcode=NopmzyUx7SYQ2wN&buName=CERRIER ENTERPROSE&reportId=0
    @POST
    @Path("/retrieveClaimPortal")
    public Response getRetrieveClaimPortalData(InputStream incomingData,
            //            @QueryParam("buWid") int buWid,
            @QueryParam("buName") String buName,
            @QueryParam("reportId") int reportId, @Context UriInfo uriInfo
    ) throws TransformerException, JSONException {
        JSONObject resObject = extractor.getJsonObject(incomingData);
        utility.printServiceUrl("POST", "WarrantyProcessing", "retrieveClaimPortal", uriInfo, resObject.toString());

        WarrantyAPIConnection dClaim = new WarrantyAPIConnection();

        String SB_ID = resObject.getString("sbid");
        String UId = resObject.getString("sbun");
        String password = resObject.getString("sbpw");
        String isEnable = resObject.getString("sudo");
        String retrievalCode = resObject.getString("rcode");

        String response = dClaim.getRetrieveClaimPortalData(4, buName, SB_ID, UId, password, isEnable, retrievalCode);

        return Response.status(200).entity(response)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers", "origin, x-requested-with, content-type")
                .build();
    }

    @GET
    @Path("/warrantyUpdate")
    public Response warrantyUpdate(@QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("type") String type,
            @QueryParam("value") String value,
            @QueryParam("claimWid") String claim_WID, @Context UriInfo uriInfo
    ) throws TransformerException {
        utility.printServiceUrl("GET", "WarrantyProcessing", "warrantyUpdate", uriInfo, "");

        WarrantyAPIConnection dClaim = new WarrantyAPIConnection();
        String response = dClaim.warrantyUpdate(buName, installId, userId, udid, reportId, type, value, claim_WID);
        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/fromProperties")
    public Response fromProperties(@QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("key") String key, @Context UriInfo uriInfo
    ) throws TransformerException {
        String configValue;
        try {
            utility.printServiceUrl("GET", "WarrantyProcessing", "fromProperties", uriInfo, "");
            JSONObject jObject = new JSONObject();
            HashMap<String, String> exthm;

            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                exthm = CommonUtil.getExternalAPIConfigProperties(buName);
            } else {
                exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
            }

            List<String> keysList = Arrays.asList(key.split(","));
            for (String keyList : keysList) {

                switch (buName.toUpperCase()) {
                    case "CARRIER ENTERPRISE":
                        key = "CE_" + keyList;
                        break;
                    case "BAKER":
                        key = "BK_" + keyList;
                        break;
                    case "CARRIER ENTERPRISE MEXICO":
                        key = "CE_MX_" + keyList;
                        break;
                    case "GEMAIRE":
                        key = "GM_" + keyList;
                        break;
                    case "EAST COAST METALS":
                        keyList = "ECM_" + keyList;
                        break;
                    default:
                        break;
                }
                configValue = exthm.get(key);
                jObject.put(keyList, configValue);
            }

            return Response.status(200).entity(jObject.toString()).build();
        } catch (Exception e) {
            return Response.status(200).entity(e.toString()).build();
        }
    }

    /**
     * WebService to get all manufacturers list
     *
     * @param buName
     * @param installId
     * @param userId
     * @param udid
     * @param reportId
     * @return
     * @throws TransformerException
     */
    @GET
    @Path("/manufacturersList")
    public Response manufacturersList(@QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId, @Context UriInfo uriInfo
    ) throws TransformerException {

        utility.printServiceUrl("GET", "WarrantyProcessing", "manufacturersList", uriInfo, "");
        WarrantyAPIConnection dClaim = new WarrantyAPIConnection();
        String response = dClaim.manufacturersList(buName, userId);
//        response = response.replaceAll("\\\\", "");
        return Response.status(200).entity(response).build();
    }

    /**
     * Web service to get manufacturer policy
     *
     * @param buName
     * @param installId
     * @param userId
     * @param udid
     * @param reportId
     * @param manufacturer
     * @return
     * @throws TransformerException
     */
//    watsco/WarrantyProcessing/manufacturersPolicy?buName=CARRIER%20ENTERPRISE&installId=1&userId=75503&udid=e7efebb607492094&reportId=0&manufacturer=Aeroflex
    @GET
    @Path("/manufacturersPolicy")
    public Response getManufacturersPolicy(@QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("manufacturer") String manufacturer, @Context UriInfo uriInfo
    ) throws TransformerException {
        utility.printServiceUrl("GET", "WarrantyProcessing", "manufacturersPolicy", uriInfo, "");
        WarrantyAPIConnection dClaim = new WarrantyAPIConnection();
        String response = dClaim.getManufacturersPolicy(buName, userId, manufacturer);
//        response = response.replaceAll("\\\\", "");
        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/mandrillAPI")
    public Response emailAPI(InputStream incomingData,
            @QueryParam("buName") String buName,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("claimNumber") String claimNumber,
            @QueryParam("referenceNumber") String referenceNumber,
            @QueryParam("userName") String userName,
            @QueryParam("emailId") String emailId,
            @Context UriInfo uriInfo) {
        utility.printServiceUrl("GET", "WarrantyProcessing", "mandrillAPI", uriInfo, "");

        String response = "";
        try {

            MailingAPIImpl mailAPI = new MailingAPIImpl();

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            //get current date time with Date()
            Date date = new Date();
            String dateTime = dateFormat.format(date);

            String subject = "Successful Claim Submission Mail - CE Assist app";

            String[] emailIds = emailId.split("\\s*,\\s*");

//            String emailRequest = "Hello, \n        Claim " + claimNumber + " with Reference Number " + referenceNumber + " was submitted by " + userName + " on " + dateTime + ".\n        Please log into ServiceBench for more details or to edit this claim. You can also check the claim status and edit the claim from the HVAC Contractor Assist app. \r\n  \r\nThank You.";
            String emailRequest = "<html><style type=\"text/css\">tab1 { padding-left: 2em; }</style><body>Hello, <BR/> <BR/><tab1/>Claim " + claimNumber + " with Reference Number " + referenceNumber + " was submitted by " + userName + " on " + dateTime + ". Please log into ServiceBench for more details or to edit this claim. You can also check the claim status and edit the claim from the HVAC Contractor Assist app. \r\n  <BR/><BR/>Thank You.</body></html>";

            String format = "HTML";
            String jsonString = mailAPI.sendEmail(buName, userId, emailRequest, emailIds, subject, format);

//            return jsonString;
            return Response.status(200).entity(jsonString).build();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
