package com.watsco.hvac;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.transform.TransformerException;

import com.watsco.hvac.impl.XRefDymanic;
import com.watsco.hvac.impl.XRefWsdl;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.StringUtils;
import com.watsco.hvac.utils.URLGenerationUtility;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import org.apache.log4j.Logger;

@Path("/XRef")
public class XRefController {

//    private static final Logger watscoUrlLog = Logger.getLogger(XRefController.class);
    private static final Logger watscoUrlLog = Logger.getLogger(XRefController.class);
    String serverUrl = "";

    //	protected static Logger logger = Logger.getLogger(XRefController.class);
    private String searchedData = "";

    XRefWsdl xRefWsdl = new XRefWsdl();
    UsageTracking usageTracking = new UsageTracking();

    @GET
    @Path("/CE/Id")
    //	@Produces("text/xml")
    public Response CEXrefByIdResponse(@QueryParam("manufacturer") String manufacturer,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("id") String id,
            @QueryParam("searchType") String searchType, @Context UriInfo uriInfo) throws TransformerException {

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "XRef", "/CE/Id", uriInfo, "");
        //		logger.info("=============>CEXrefByIdResponse method invoked");

        String response = xRefWsdl.getXmlCEXRefById(id, searchType, userId);

        searchedData = "id : " + id + "; Search Type: " + searchType;
        usageTracking.usageTrackingSp(installId, userId, udid, 16, "CARRIER ENTERPRISE", searchedData, id, "");

        return Response.status(200).entity(response).build();

    }

    @GET
    @Path("/CE/Params")
    //	@Produces("text/xml")
    public Response CEXrefByParamsResponse(@QueryParam("manufacturer") String manufacturer,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("cmprType") String cmprtype,
            @QueryParam("refrigerant") String refrigerant,
            @QueryParam("voltage") String voltage,
            @QueryParam("phase") int phase,
            @QueryParam("capOrTon") String CapOrTon,
            @QueryParam("capOrTonValue") String capOrTonValue, @Context UriInfo uriInfo) throws TransformerException {
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "XRef", "/CE/Params", uriInfo, "");
        //		logger.info("=============>CEXrefByParamsResponse method invoked");
        String response = xRefWsdl.getXmlCEXRefByParams(cmprtype, refrigerant, voltage, phase, CapOrTon, capOrTonValue, userId);

        searchedData = "Cmprtype : " + cmprtype + "; Refrigerant : " + refrigerant + "; Voltage : " + voltage + "; Phase :" + phase + "; CapOrTon : "
                + CapOrTon + "; CapOrTonValue : " + capOrTonValue;
        usageTracking.usageTrackingSp(installId, userId, udid, 16, "CARRIER ENTERPRISE", searchedData, "", "");

        return Response.status(200).entity(response).build();

    }

    @GET
    @Path("/BAKER/Id")
    public Response BakerXrefByIdResponse(@QueryParam("manufacturer") String manufacturer,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("id") String id,
            @QueryParam("searchType") String searchType, @Context UriInfo uriInfo) throws TransformerException {

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "XRef", "/BAKER/Id", uriInfo, "");
        //		logger.info("=============>BakerXrefByIdResponse method invoked");
        String response = xRefWsdl.getXmlBakerXRefById(id, searchType, userId);

        searchedData = "id : " + id + "; Search Type: " + searchType;
        usageTracking.usageTrackingSp(installId, userId, udid, 16, buName, searchedData, id, "");

        return Response.status(200).entity(response).build();

    }

    @GET
    @Path("/BAKER/Params")
    //	@Produces("text/xml")
    public Response BakerXrefByParamsResponse(@QueryParam("manufacturer") String manufacturer,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("cmprType") String cmprtype,
            @QueryParam("refrigerant") String refrigerant,
            @QueryParam("voltage") String voltage,
            @QueryParam("phase") int phase,
            @QueryParam("capOrTon") String CapOrTon,
            @QueryParam("capOrTonValue") String capOrTonValue, @Context UriInfo uriInfo) throws TransformerException {

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "XRef", "/BAKER/Params", uriInfo, "");
        //		logger.info("=============>BakerXrefByParamsResponse method invoked");
        String response = xRefWsdl.getXmlBakerXRefByParams(cmprtype, refrigerant, voltage, phase, CapOrTon, capOrTonValue, userId);

        searchedData = "Cmprtype : " + cmprtype + "; Refrigerant : " + refrigerant + "; Voltage : " + voltage + "; Phase :" + phase + "; CapOrTon : "
                + CapOrTon + "; CapOrTonValue : " + capOrTonValue;
        usageTracking.usageTrackingSp(installId, userId, udid, 16, buName, searchedData, "", "");

        return Response.status(200).entity(response).build();

    }

    @GET
    @Path("/dynamic")
    public Response dynamicXmlXRef(@QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("manufacturer") String manufacturer,
            @QueryParam("category") String category,
            @QueryParam("compressor") String compressor,
            @QueryParam("fields") List<String> fields, @Context UriInfo uriInfo
    ) throws TransformerException {

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "XRef", "/dynamic", uriInfo, "");

        //		logger.info("=============>dynamicXmlXRef method invoked");
        String response = "";
        HashMap<String, String> inputMap = new HashMap<String, String>();
        searchedData = "COMPRESSOR: ";
        if (compressor.contains("COMPRESSOR")) {
            searchedData += "HERMETIC;";
        } else {
            searchedData += compressor.toUpperCase() + ";";
        }

        for (String s : fields) {
            int index = s.indexOf("=");
            //			logger.info("key:"+s.substring(0, index).toUpperCase()+" value:"+s.substring(index+1,s.length()));
            inputMap.put(s.substring(0, index).toUpperCase(), s.substring(index + 1, s.length()));
            searchedData += s.substring(0, index).toUpperCase() + " : " + s.substring(index + 1, s.length()) + ", ";
        }

        XRefDymanic xrefDynamic = new XRefDymanic();
        response = xrefDynamic.getResponse(installId, userId, udid, reportId, buName, manufacturer, category, compressor, inputMap);
        usageTracking.usageTrackingSp(installId, userId, udid, 16, buName, searchedData, "", "");
        return Response.status(200).entity(response).build();

    }

}
