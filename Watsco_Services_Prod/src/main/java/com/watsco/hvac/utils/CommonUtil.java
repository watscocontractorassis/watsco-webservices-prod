package com.watsco.hvac.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

public class CommonUtil {

    protected static Logger logger = Logger.getLogger(CommonUtil.class);

    public static HashMap<String, String> getExternalAPIConfigProperties(String buName) {

        Properties prop = new Properties();
        InputStream input = null;
        HashMap<String, String> hm = new HashMap<String, String>();

        try {

            if (buName.equalsIgnoreCase("GEMAIRE")) {
                input = new FileInputStream(System.getProperty("catalina.base") + "\\conf\\GM-external-api-urls.properties");
            } else if (buName.equalsIgnoreCase("CARRIER ENTERPRISE")) {
                input = new FileInputStream(System.getProperty("catalina.base") + "\\conf\\CE-external-api-urls.properties");
            } else if (buName.equalsIgnoreCase("CARRIER ENTERPRISE MEXICO")) {
                input = new FileInputStream(System.getProperty("catalina.base") + "\\conf\\CE_MX-external-api-urls.properties");
            } else if (buName.equalsIgnoreCase("EAST COAST METALS")) {
                input = new FileInputStream(System.getProperty("catalina.base") + "\\conf\\ECM-external-api-urls.properties");
            } else if (buName.equalsIgnoreCase("BAKER")) {
                input = new FileInputStream(System.getProperty("catalina.base") + "\\conf\\BK-external-api-urls.properties");
            }

            // load a properties file
            prop.load(input);

            Set<Entry<Object, Object>> set = prop.entrySet();
            Iterator<Entry<Object, Object>> iterator = set.iterator();

            while (iterator.hasNext()) {
                Entry<Object, Object> entry = iterator.next();
                hm.put(entry.getKey().toString(), entry.getValue().toString());
            }

        } catch (IOException e) {
            logger.error("Error while Reading a mail configuration properties : ", e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return hm;
    }

    public static HashMap<String, String> getExternalAPIConfigProperties() {

        Properties prop = new Properties();
        InputStream input = null;
        HashMap<String, String> hm = new HashMap<String, String>();

        try {
            input = new FileInputStream(System.getProperty("catalina.base") + "\\conf\\external-api-urls.properties");

            // load a properties file
            prop.load(input);

            Set<Entry<Object, Object>> set = prop.entrySet();
            Iterator<Entry<Object, Object>> iterator = set.iterator();

            while (iterator.hasNext()) {
                Entry<Object, Object> entry = iterator.next();
                hm.put(entry.getKey().toString(), entry.getValue().toString());
            }

        } catch (IOException e) {
            logger.error("Error while Reading a mail configuration properties : ", e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return hm;
    }

    public static HashMap<String, String> getInternalFileProperties() {
        //		logger.info("CommonUtils getInternalFileProperties mthd invoked");
        Properties prop = new Properties();
        InputStream input = null;
        HashMap<String, String> hm = new HashMap<String, String>();

        try {

            input = new FileInputStream(System.getProperty("catalina.base") + "\\conf\\internal-File.properties");

            // load a properties file
            prop.load(input);

            Set<Entry<Object, Object>> set = prop.entrySet();
            Iterator<Entry<Object, Object>> iterator = set.iterator();

            while (iterator.hasNext()) {
                Entry<Object, Object> entry = iterator.next();
                hm.put(entry.getKey().toString(), entry.getValue().toString());
            }

        } catch (IOException e) {
            logger.error("Error while Reading internal configuration properties : ", e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return hm;
    }

    public static void updateInternalFileValues(HashMap<String, String> updateMap) throws IOException {
        //	logger.info("updateInternalFileValues() method invoked");

        FileInputStream in = new FileInputStream(System.getProperty("catalina.base") + "\\conf\\internal-File.properties");
        Properties prop = new Properties();
        prop.load(in);
        in.close();

        FileOutputStream out = new FileOutputStream(System.getProperty("catalina.base") + "\\conf\\internal-File.properties");

        for (String key : updateMap.keySet()) {
            prop.setProperty(key, updateMap.get(key));
        }

        prop.store(out, null);
        out.flush();
        out.close();

    }

}
//
