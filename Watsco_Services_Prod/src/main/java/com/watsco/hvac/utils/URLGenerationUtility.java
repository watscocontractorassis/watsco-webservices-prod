/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watsco.hvac.utils;

import com.watsco.hvac.BillBoardController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class URLGenerationUtility {

    private static final Logger logger = Logger.getLogger(URLGenerationUtility.class);

    public void printServiceUrl(String httpMethod, String controllerName, String servicePath, UriInfo uriInfo, String postRequest) {

        Object userId = 0;
        String serverUrl = "";
        StringBuffer sb = new StringBuffer();
        MultivaluedMap<String, String> params = uriInfo.getQueryParameters();
        // Populate the parameter bean properties
        for (Map.Entry<String, List<String>> param : params.entrySet()) {
            String key = param.getKey();

            if (key.equals("fields")) {
                List<String> fields = param.getValue();
                for (String field : fields) {
                    String[] splitbyAmpersand = field.split("&");
                    for (int i = 0; i < splitbyAmpersand.length; i++) {
                        String splitByEqual = splitbyAmpersand[i];
                        String[] split = splitByEqual.split("=");
                        sb.append("&fields=" + split[0] + "=" + split[1]);

                    }

                }
            } else {
                System.out.println("key" + key);
                Object value = param.getValue().iterator().next();
                if (key.equalsIgnoreCase("userId")) {
                    userId = value;
                    System.out.println("userId :"+userId);
                }
                sb.append(key + "=" + value + "&");
            }
        }
        serverUrl = getServerNameFromExternalFiles();
        String url = serverUrl + "watsco-v4-services/watsco/" + controllerName + "/" + servicePath + "?" + sb;
        url = url.substring(0, url.length() - 1);

        if (httpMethod.equalsIgnoreCase("GET")) {
            logger.info("===UserId : " + userId + "===> GET Webservice URL : " + url);
        } else if (httpMethod.equalsIgnoreCase("POST")) {
            logger.info("===UserId : " + userId + "===> POST Webservice URL : " + url);
            logger.info("===UserId : " + userId + "===> POST Request : " + postRequest);
        }

    }

    public static String getServerNameFromExternalFiles() {
        String serverUrl;
        HashMap<String, String> hm;
        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            hm = CommonUtil.getExternalAPIConfigProperties();
        } else {
            hm = LocalCommonUtil.getExternalAPIConfigProperties();
        }
        serverUrl = hm.get("SERVER_URL");
        return serverUrl;
    }

}
