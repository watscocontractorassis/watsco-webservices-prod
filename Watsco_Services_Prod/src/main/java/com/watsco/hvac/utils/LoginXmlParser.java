package com.watsco.hvac.utils;

import java.util.HashMap;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.watsco.hvac.businesslayer.LoginBusinessLayer;

public class LoginXmlParser extends DefaultHandler
{
    private HashMap<String, String> hmLoginInformation;
    private StringBuffer sb;
    public int id;
    private int userId;
    private String accountNumber;
    private String buName;
    private String regionURL;
    
    public LoginXmlParser(String buName, int userId, String accountNumber, String regionURL)
    {
    	this.buName = buName;
        this.userId = userId;
        this.accountNumber = accountNumber;
        this.regionURL = regionURL;
    }
    
    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException
    {
        super.startElement(uri, localName, qName, attributes);
        
        if(qName.equalsIgnoreCase("result"))
        {
            hmLoginInformation = new HashMap<String, String>();
        }
        
        sb = new StringBuffer();
    }
    
    @Override
    public void endElement(String uri, String localName, String qName)
    throws SAXException
    {
        super.endElement(uri, localName, qName);
        
        if(hmLoginInformation != null)
        {
            hmLoginInformation.put(qName, sb.toString());
        }
        
        if(qName.equalsIgnoreCase("result"))
        {
            LoginBusinessLayer loginBusinessLayer = new LoginBusinessLayer();
            
            //if(hmLoginInformation.get("company") != null)
            //{
            id = loginBusinessLayer.insertIntoDealers(buName,hmLoginInformation, userId, accountNumber, regionURL);
            //}
            //else
            //{
            //id = loginBusinessLayer.insertIntoContractors(hmLoginInformation, userId,accountNumber);
            //}
        }
    }
    
    @Override
    public void characters(char[] ch, int start, int length)
    throws SAXException 
    {
        super.characters(ch, start, length);
        sb.append(ch, start, length);
    }
    
}
