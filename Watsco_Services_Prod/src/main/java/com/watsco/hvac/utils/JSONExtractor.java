package com.watsco.hvac.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.watsco.hvac.dto.ClaimDTO;
import org.apache.log4j.Logger;

public class JSONExtractor {

    private static final Logger logger = Logger.getLogger(JSONExtractor.class);

    public static ClaimDTO jsonExtractorClaim(JSONObject jSONObject) {
        String response = "";
        HashMap<String, String> hmClaim = new HashMap<String, String>();
        HashMap<String, String> hmClaimPart = new HashMap<String, String>();
        ArrayList<HashMap<String, String>> al = null;
        ClaimDTO claimDTO = new ClaimDTO();
//	    synchronized (this)
//	    {
        try {
            JSONExtractor extractor = new JSONExtractor();
//            JSONObject jSONObject = extractor.getJsonObject(incomingData);
            Iterator<?> keys = jSONObject.keys();

            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (jSONObject.get(key) instanceof String) {
//	        	    	hmClaim.put(key, jSONObject.get(key).toString());
                    String value = (String) jSONObject.get(key);
                    System.out.println("key : " + key + ":" + "value : " + value);
                    value = JSONExtractor.convertTOUTF8(value);

                    value = value.replaceAll("(?=[]\\[+&|!($;){}^\"~*?:\\\\])", "\\\\");

                    if (value.equalsIgnoreCase("(null)")) {
                        String newValue = "null";
                        hmClaim.put(key, newValue);
                        if (key.contains("date")) {
                            hmClaim.put(key, "");
                        }
                    } else {
//	        	    		hmClaim.put(key, jSONObject.getString(key));
                        hmClaim.put(key, value);
                    }

//	        	    	hmClaim.put(key, jSONObject.get(key).toString());
                } else if (jSONObject.get(key) instanceof JSONArray) {

                    claimDTO.setClaimMap(hmClaim);

                    JSONArray array = jSONObject.getJSONArray(key);
                    al = new ArrayList<HashMap<String, String>>();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject jsonClaimPart = array.getJSONObject(i);
                        Iterator<?> keys1 = jsonClaimPart.keys();

                        hmClaimPart = new HashMap<String, String>();

                        while (keys1.hasNext()) {
                            key = String.valueOf(keys1.next());

                            if (jsonClaimPart.get(key) instanceof String) {
                                String value = String.valueOf(jsonClaimPart.get(key));
//                                System.out.println("key : " + key + ":" + "value : " + value);
                                value = JSONExtractor.convertTOUTF8(value);
                                value = value.replaceAll("(?=[]\\[+&|!($;){}^\"~*?:\\\\])", "\\\\");

                                if (value.equalsIgnoreCase("(null)")) {

                                    String newValue = "null";
                                    hmClaimPart.put(key, newValue);
                                    if (key.contains("date")) {
                                        hmClaimPart.put(key, "");
                                    }
                                } else {
                                    hmClaimPart.put(key, value);
                                }
                            } else if (jsonClaimPart.get(key) instanceof java.lang.Integer) {
                                String value = String.valueOf(jsonClaimPart.get(key));
//                                System.out.println("key : " + key + ":" + "value : " + value);
                                value = JSONExtractor.convertTOUTF8(value);
                                value = value.replaceAll("(?=[]\\[+&|!($;){}^\"~*?:\\\\])", "\\\\");

                                if (value.equalsIgnoreCase("(null)")) {

                                    String newValue = "null";
                                    hmClaimPart.put(key, newValue);
                                    if (key.contains("date")) {
                                        hmClaimPart.put(key, "");
                                    }
                                } else {
                                    hmClaimPart.put(key, value);
                                }
                            }
                        }

                        al.add(hmClaimPart);
                    }
                    claimDTO.setAlClaimParts(al);
//	        	    	System.out.println(claimDTO.toString());
                } else if (jSONObject.get(key) instanceof java.lang.Integer) {
                    String value = String.valueOf(jSONObject.get(key));
//                    System.out.println("key : " + key + ":" + "value : " + value);
                    value = JSONExtractor.convertTOUTF8(value);

                    value = value.replaceAll("(?=[]\\[+&|!($;){}^\"~*?:\\\\])", "\\\\");

                    if (value.equalsIgnoreCase("(null)")) {
                        String newValue = "null";
                        hmClaim.put(key, newValue);
                        if (key.contains("date")) {
                            hmClaim.put(key, "");
                        }
                    } else {
//	        	    		hmClaim.put(key, jSONObject.getString(key));
                        hmClaim.put(key, value);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//	    }
        return claimDTO;

    }

    public static ClaimDTO jsonExtractorNPortal(String incomingData) {
        String response = "";
        HashMap<String, String> hmClaim = new HashMap<String, String>();
        HashMap<String, String> hmClaimPart = new HashMap<String, String>();
        String key;
        ArrayList<HashMap<String, String>> al = null;
        ClaimDTO claimPortalDTO = new ClaimDTO();
//	    synchronized (this)
//	    {
        try {
            JSONObject jSONObject = new JSONObject(incomingData);
            Iterator<?> keys = jSONObject.keys();

            while (keys.hasNext()) {
                key = (String) keys.next();
                if (jSONObject.get(key) instanceof String) {
//	        	    	hmClaim.put(key, jSONObject.get(key).toString());
                    String value = jSONObject.getString(key);
                    if (!value.equals("")) {
                        value = value.replaceAll("(?=[]\\[+&|!($;){}^\"~*?:\\\\])", "\\\\");

                        if (value.equalsIgnoreCase("(null)")) {
                            String newValue = "null";
                            hmClaim.put(key, newValue);
                            if (key.contains("date")) {
                                hmClaim.put(key, "");
                            }
                        } else //	        	    		hmClaim.put(key, jSONObject.getString(key));
                         if ((key.contains("date")) || (key.contains("Date"))) {
                                if (!value.equals("")) {
                                    DateFormat df1 = new SimpleDateFormat("MM/dd/yy");
                                    DateFormat df3 = new SimpleDateFormat("yyyyMMdd");
                                    Date date1 = new Date();
                                    date1 = df1.parse(value);
                                    value = df3.format(date1);
                                }
                                hmClaim.put(key, value);

                            } else {
                                hmClaim.put(key, value);
                            }
                    } else {
                        hmClaim.put(key, "");
                    }

                } else if (jSONObject.get(key) instanceof JSONArray) {
                    claimPortalDTO.setClaimMap(hmClaim);

                    JSONArray array = jSONObject.getJSONArray(key);
                    al = new ArrayList<HashMap<String, String>>();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject jsonClaimPart = array.getJSONObject(i);
                        Iterator<?> keys1 = jsonClaimPart.keys();

                        hmClaimPart = new HashMap<String, String>();

                        while (keys1.hasNext()) {
                            key = (String) keys1.next();

                            if (jsonClaimPart.get(key) instanceof String) {
                                String value = jsonClaimPart.getString(key);
                                if (!value.equals("")) {
                                    value = value.replaceAll("(?=[]\\[+&|!($;){}^\"~*?:\\\\])", "\\\\");
                                    if (value.equalsIgnoreCase("(null)")) {

                                        String newValue = "null";
                                        hmClaimPart.put(key, newValue);
                                        if (key.contains("date")) {
                                            hmClaimPart.put(key, "");
                                        }
                                    } else //				        	    		hmClaim.put(key, jSONObject.getString(key));
                                     if ((key.contains("date")) || (key.contains("Date"))) {
                                            if (!value.equals("")) {
                                                DateFormat df1 = new SimpleDateFormat("MM/dd/yy");
                                                DateFormat df3 = new SimpleDateFormat("yyyyMMdd");
                                                Date date1 = new Date();
                                                date1 = df1.parse(value);
                                                value = df3.format(date1);
                                            }
                                            hmClaimPart.put(key, value);
                                        } else {
                                            hmClaimPart.put(key, value);
                                        }
                                } else {
                                    hmClaimPart.put(key, "");
                                }

                            }
                        }

                        al.add(hmClaimPart);
                    }
                    claimPortalDTO.setAlClaimParts(al);
//	        	    	System.out.println(claimDTO.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//	    }
        return claimPortalDTO;

    }

    /*public static ClaimYDTO jsonExtractorY(InputStream incomingData){
		
     String response = "";
     HashMap<String,String> hmClaim = new HashMap<String,String>();
     HashMap<String,String> hmClaimPart = new HashMap<String,String>();
     ArrayList<HashMap<String,String>> al = null;
     ClaimYDTO claimYDTO  = new ClaimYDTO();
     //	    synchronized (this)
     //	    {
     try
     {  
     JSONObject  jSONObject = getJsonObject(incomingData);
     Iterator<?> keys = jSONObject.keys();
	
     while( keys.hasNext() ) {
     String key = (String)keys.next();
     if(jSONObject.get(key) instanceof String) {
     //	        	    	hmClaim.put(key, jSONObject.get(key).toString());
     String value = jSONObject.getString(key);
     value = value.replaceAll("(?=[]\\[+&|!($@;){}^\"~*?:\\\\])", "\\\\");
     if(value.equalsIgnoreCase("(null)")){
     String newValue = "null";
     hmClaim.put(key,newValue);
     if(key.contains("date")){
     hmClaim.put(key,"");
     }
     System.out.println(key + ":" + value);
     }
     else{
     //	        	    		hmClaim.put(key, jSONObject.getString(key));
     hmClaim.put(key, value);
     System.out.println(key + ":" + value);
     }
	        	    	
     }
     else if(jSONObject.get(key) instanceof JSONArray) {
	        	    	
     claimYDTO.setClaimMap(hmClaim);
	        	    	
     JSONArray array =  jSONObject.getJSONArray(key);
     al = new ArrayList<HashMap<String,String>>();
	        	    	
     for(int i = 0;i<array.length();i++){
	        	    		
     JSONObject jsonClaimPart = array.getJSONObject(i);
     Iterator<?> keys1  = jsonClaimPart.keys();
	        	    		
     hmClaimPart = new HashMap<String, String>();
	        	    		
     while(keys1.hasNext() ) {
     key = (String)keys1.next();
			            	    
     if(jsonClaimPart.get(key) instanceof String) {
     String value = jsonClaimPart.getString(key);
     value = value.replaceAll("(?=[]\\[+&|!($@;){}^\"~*?:\\\\])", "\\\\");
     if(value.equalsIgnoreCase("(null)")){
			            	    		
     String newValue = "null";
     hmClaimPart.put(key,newValue);
     if(key.contains("date")){
     hmClaimPart.put(key,"");
     }
     }
     else{
     hmClaimPart.put(key, value);
     System.out.println(key + ":" + value);
     }
     }
     }
	        	    		
     al.add(hmClaimPart);
     }
     claimYDTO.setAlClaimParts(al);
     //	        	    	System.out.println(claimDTO.toString());
     }
     }
     }
     catch(Exception e){
     e.printStackTrace();
     }
     //	    }
     return claimYDTO;
	
     }
     */
    public JSONObject jsonMandrillExtractor(InputStream incomingData) {
        HashMap<String, String> jsonArray = new HashMap<String, String>();
        HashMap<String, String> jsonObject = new HashMap<String, String>();
        ArrayList<HashMap<String, String>> al = null;
        try {
            JSONObject jSONObject = getJsonObject(incomingData);
            Iterator<?> keys = jSONObject.keys();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static String jsonExtractorPortal(String response) {
//	    HashMap<String,String> hmClaim = new HashMap<String,String>();
//	    HashMap<String,String> hmClaimPart = new HashMap<String,String>();
//	    ArrayList<HashMap<String,String>> al = null;
        JSONObject hmClaim = new JSONObject();
        JSONObject hmClaimPart = new JSONObject();
        JSONArray al = null;
        String value;
//	    synchronized (this)
//	    {
        try {
            JSONObject jSONObject = new JSONObject(response);
            Iterator<?> keys = jSONObject.keys();

            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (jSONObject.get(key) instanceof String) {
//	        	    	hmClaim.put(key, jSONObject.get(key).toString());
                    value = jSONObject.getString(key);
                    value = value.replaceAll("(?=[]\\[+&|!($@;){}^\"~*?:\\\\])", "\\\\");
                    if (value.equalsIgnoreCase("(null)")) {
                        String newValue = "null";
                        if (key.contains("date") || key.contains("Date")) {
                            hmClaim.put(key, "");
                        } else {
                            hmClaim.put(key, newValue);

                        }
                    } else if (key.contains("date") || key.contains("Date")) {

                        value = jSONObject.getString(key);
//	        	    			value = value.replaceAll("(?=[]\\[+&|!($@;){}^\"~*?:\\\\-])", "\\\\");
                        value = value.replaceAll("?=\\+&|!$@;^\"~*?:\\\\", "\\\\");
                        if (!value.equals("")) {
                            DateFormat df1 = new SimpleDateFormat("MM/dd/yy");
                            DateFormat df3 = new SimpleDateFormat("yyyyMMdd");
                            Date date = new Date();

                            //	        	    			
                            date = df1.parse(value);
                            //	        	    			

                            value = df3.format(date);

                            value = value.replaceAll("\\W", "");
                            hmClaim.put(key, value);
                        }
                    } else {
//	        	    			hmClaim.put(key, jSONObject.getString(key));
                        hmClaim.put(key, value);
                    }

                } else if (jSONObject.get(key) instanceof JSONArray) {

//	        	    	claimNDTO.setClaimMap(hmClaim);
                    JSONArray array = jSONObject.getJSONArray(key);
                    al = new JSONArray();

                    for (int i = 0; i < array.length(); i++) {

                        JSONObject jsonClaimPart = array.getJSONObject(i);
                        Iterator<?> keys1 = jsonClaimPart.keys();

                        hmClaimPart = new JSONObject();

                        while (keys1.hasNext()) {
                            key = (String) keys1.next();

                            if (jsonClaimPart.get(key) instanceof String) {
                                value = jsonClaimPart.getString(key);
                                value = value.replaceAll("(?=[]\\[+&|!($@;){}^\"~*?:\\\\])", "\\\\");
                                if (value.equalsIgnoreCase("(null)")) {

                                    String newValue = "null";
                                    hmClaimPart.put(key, newValue);
                                    if (key.contains("date") || key.contains("Date")) {
                                        hmClaimPart.put(key, "");
                                    } else {
                                        hmClaimPart.put(key, newValue);
                                    }
                                } else {
                                    value = jsonClaimPart.getString(key);
                                    value = value.replaceAll("(?=[]\\[+&|!($@;){}^\"~*?:\\\\])", "\\\\");
                                    if (key.contains("date") || key.contains("Date")) {

                                        if (!value.equals("")) {
                                            value = value.replaceAll("\\W", "");
                                            hmClaimPart.put(key, value);
                                        } else {
                                            hmClaimPart.put(key, value);
                                        }
                                    } else {
                                        hmClaimPart.put(key, value);
                                    }
                                }
                            }
                        }

                        al.put(hmClaimPart);
                        hmClaim.put("claimParts", al);
                    }
//	        	    	claimNDTO.setAlClaimParts(al);
//	        	    	System.out.println(claimDTO.toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//	    }
        return hmClaim.toString();

    }

    public JSONObject getJsonObject(InputStream inputStreamObject) {

        try {
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStreamObject, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }

            JSONObject jsonObject = new JSONObject(responseStrBuilder.toString());

            //returns the json object
            return jsonObject;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertTOUTF8(String data) throws UnsupportedEncodingException {
        String utf8tweet = "";

        try {
            byte[] utf8Bytes = data.getBytes("UTF-8");

            data = new String(utf8Bytes, "UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        data = Normalizer.normalize(data, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");

        String s1 = Normalizer.normalize(data, Normalizer.Form.NFKD);
        String regex = Pattern.quote("[\\p{InCombiningDiacriticalMarks}\\p{IsLm}\\p{IsSk}]+");

        String s2 = new String(s1.replaceAll(regex, "").getBytes("ascii"), "ascii");


        /* System.out.println("UTF8: " + utf8tweet);
         Pattern unicodeOutliers = Pattern.compile("[^\\x00-\\x7F]",
         Pattern.UNICODE_CASE | Pattern.CANON_EQ
         | Pattern.CASE_INSENSITIVE);
         Matcher unicodeOutlierMatcher = unicodeOutliers.matcher(utf8tweet);
	
         System.out.println("Before: " + utf8tweet);
         //	     utf8tweet = unicodeOutlierMatcher.replaceAll("");
         //	     System.out.println("After: " + utf8tweet);*/
        return s2;
    }

}
