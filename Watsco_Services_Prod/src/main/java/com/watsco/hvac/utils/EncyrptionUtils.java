package com.watsco.hvac.utils;

import java.nio.charset.Charset;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.CharUtils;

/**
 * Class to encrypt and decrypt User name and password
 * @author Krishna Chaitanya Aripaka
 * @version 1.0
 * @since 2014
 */
public class EncyrptionUtils
{
    
    /**
     * Method to decrypt Password by removing a character at index 0 and index length
     * @param password
     * @return
     */
    
    
    /**
     * Method to decrypt User name
     * @param userName
     * @return
     */
    public static String decryptCredentials(String credentials)
    {
        String str = new String(Base64.decodeBase64(credentials.getBytes()),Charset.forName("UTF-8"));
        str = str.substring(0, str.length());
        return str;
    }
    
    /**
     * Method to encrypt password
     * @param password
     * @return
     */
    public static String encryptPassword(String password)
    {
        byte encoded[] = Base64.encodeBase64(password.getBytes());
        password = new String(encoded, Charset.forName("UTF-8"));
        return password;
    }
    
    public String encryptCredentialsWithKey(String credentials,String key){
		String dataStr           = "";
		String keyStr            = "";
		String encryptedPassword = "";
		String result            = "";
		for(int i=0;i<credentials.length();i++){
		/*	dataStr = credentials.substring(i, i+1);
			
			int startIndex = (i%(key.length()-1));
			keyStr = key.substring(startIndex,startIndex+1);
			
			dataStr = dataStr+keyStr;  // appends dataStr+keyStr to dataStr
			result += dataStr;*/
			

        	dataStr = credentials.substring(i, i+1);
        	char charDataStr = dataStr.charAt(0); // This gives the character 'a'
			int asciiDataStr = (int) charDataStr;
			
        	
			int startIndex = (i%(key.length()-1));
			
			keyStr = key.substring(startIndex,startIndex+1);
			char charKeyStr = keyStr.charAt(0); // This gives the character 'a'
			int asciiKeyStr = (int) charKeyStr;
			
			int asciiLetter = asciiDataStr+asciiKeyStr;  // appends dataStr+keyStr to dataStr
			
			char[] character = Character.toChars(asciiLetter); 
//			char character = (char)asciiLetter;
			
			result += character;
        
		}
		byte encoded[] = Base64.encodeBase64(result.getBytes());
        encryptedPassword = new String(encoded);
		return encryptedPassword;
		
	}
    
    public String decryptCredentialsWithKey(String credentials,String key){
    	String dataStr           = "";
		String keyStr            = "";
		String result            = "";
		int asciiLetter          = 0;
    	String str = new String(Base64.decodeBase64(credentials.getBytes()));
        str = str.substring(0, str.length());

        for(int i=0;i<str.length();i++){
        	dataStr = str.substring(i, i+1);
        	char charDataStr = dataStr.charAt(0); // This gives the character 'a'
			int asciiDataStr = (int) charDataStr;
			
			int startIndex = (i%(key.length()-1));
			
			keyStr = key.substring(startIndex,startIndex+1);
			char charKeyStr = keyStr.charAt(0); // This gives the character 'a'
			int asciiKeyStr = (int) charKeyStr;
			
			if(asciiKeyStr<asciiDataStr)
				asciiLetter = asciiDataStr-asciiKeyStr;  // appends dataStr+keyStr to dataStr
			else
				asciiLetter = asciiKeyStr-asciiDataStr; 
			
			
			asciiLetter  = onlyAlphaNumericChars(asciiLetter);
			char character = (char)asciiLetter;
			
			result += character;
        }
        return result;
	}

    
    public int onlyAlphaNumericChars(int asciiDataInt){ 

    			if((asciiDataInt >= 65 && asciiDataInt <= 90) ||
                		(asciiDataInt >= 97 && asciiDataInt <= 122)){
                	return asciiDataInt;
                }
    			else{
    				return 97;
    			}
    } 
    
    public static void main(String[] args) {
    	EncyrptionUtils ecu = new EncyrptionUtils();
    	String encryptPassword = "";
    	String pasxnsnxsn      = ""; 
    	
    /*	
    	String key = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		String credentials = "Rtiuser123";
		
		
        
        String encryptPassword = ecu.encryptCredentialsWithKey(credentials, key);
        System.out.println("Encrypt :"+encryptPassword);
        
        pasxnsnxsn = ecu.decryptCredentialsWithKey("ENNAMINADIKA", key);
        */
        
        
        encryptPassword = ecu.encryptPassword("semtester@gmail.com");
        
        encryptPassword = ecu.encryptPassword("powertool1");
        
        pasxnsnxsn = ecu.decryptCredentials(encryptPassword);
        
    }
}

