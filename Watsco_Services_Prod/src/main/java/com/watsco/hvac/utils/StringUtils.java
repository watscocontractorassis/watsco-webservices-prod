package com.watsco.hvac.utils;

import org.json.JSONArray;

import org.json.JSONException;
import org.json.JSONObject;

public class StringUtils {

    public static String replaceAll(String requestXml, String oldValue, String newValue, String specialCharcter) {

        if (!newValue.contains(specialCharcter)) {
            return requestXml;
        }

        StringBuffer sbSpecialCharacterIndexes;
        StringBuffer replacedValue = null;

        if (newValue.contains(specialCharcter)) {

            sbSpecialCharacterIndexes = new StringBuffer();
            int tempindexs = 0;
            int startIndexOfPassword;
            String indexOfDollar[];

            while ((tempindexs = newValue.indexOf(specialCharcter, ++tempindexs)) != -1) {
                sbSpecialCharacterIndexes.append(tempindexs + ",");
            }

            if (sbSpecialCharacterIndexes.length() != 0) {
                sbSpecialCharacterIndexes.substring(0, sbSpecialCharacterIndexes.length() - 1);
            }

            indexOfDollar = sbSpecialCharacterIndexes.toString().split(",");
            newValue = newValue.replaceAll("\\" + specialCharcter, "");
            requestXml = requestXml.replaceAll(oldValue, newValue);
            startIndexOfPassword = requestXml.indexOf(newValue);

            replacedValue = new StringBuffer(requestXml);

            for (int i = 0; i < indexOfDollar.length; i++) {
                replacedValue.insert(startIndexOfPassword + Integer.parseInt(indexOfDollar[i]), specialCharcter);
            }
        }

        return replacedValue.toString();
    }

    public static String convertURL(String str) {
        String specialChars = "/*!@#$%^&*()\"{}_[]|\\?/<>,.";
//    	String url = null;
        String sChar = "";
        int size = str.length();;
        for (int i = 0; i < size; i++) {
//    		sChar = str.substring(i, i+2);
            sChar = Character.toString(str.charAt(i));
            if (specialChars.contains(sChar)) {
                str = str.replace(sChar, "\\" + sChar);

            }

        }

        return str;
    }

    public boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

}
