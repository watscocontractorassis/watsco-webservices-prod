package com.watsco.hvac;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.transform.TransformerException;

import com.watsco.hvac.usagetracking.BeaconTracking;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.URLGenerationUtility;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

@Path("/SurfBITracking")
public class SurfBITracking {

    private static final Logger logger = Logger.getLogger(SurfBITracking.class);

    /**
     * URL :
     * http://207.127.14.48:8081/watsco-v4-services/watsco/SurfBITracking?manufacturer=Carrier&dataSearched=http://207.127.14.48:8080/Inventory/#p=new_user_-_list_page_-_billboard?%26utm_source=mobile-app%26utm_campaign=app-traffic%26token=utm_campaign=app-traffic&message=Billboard&installId=1&userId=75831&udid=a225bd52a3504b6f&reportId=17&buName=CARRIER%20ENTERPRISE
     *
     * @param manufacturer
     * @param dataSearched
     * @param message
     * @param installId
     * @param userId
     * @param udid
     * @param reportId
     * @param buName
     * @return
     * @throws TransformerException
     */
    @GET
    @Path("")
    public Response SurfBIUsageTracking(@QueryParam("manufacturer") String manufacturer,
            @QueryParam("dataSearched") String dataSearched,
            @QueryParam("message") String message,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName, @Context UriInfo uriInfo
    ) throws TransformerException {

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "SurfBITracking", "", uriInfo, "");
        //		logger.info("SurfBITracking SurfBIUsageTracking method invoked");
        UsageTracking usageTracking = new UsageTracking();
        String searchedData = "";

        if ((reportId == 17 && reportId == 23) || (dataSearched.isEmpty())) {
            searchedData = message;
        } else {
            searchedData = dataSearched + " " + message;
        }

        JSONObject jObject = new JSONObject();
        jObject.put("STATUS", "Success");

        //	usageTracking.setReportDataSearched(installId, userId, udid, reportId, buName, searchedData, dataSearched,"");
        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, dataSearched, "");
        return Response.status(200).entity(jObject.toString()).build();
    }

    @GET
    @Path("/Beacon")
    public void BeaconTracking(@QueryParam("buName") String buName,
            @QueryParam("udid") String udid,
            @QueryParam("userId") int userId,
            @QueryParam("major") int major,
            @QueryParam("minor") int minor,
            @QueryParam("beaconUdid") String beaconUdid,
            @QueryParam("sessionId") String sessionId,
            @QueryParam("fields") List<String> fields, @Context UriInfo uriInfo) throws TransformerException {
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "Beacon", "", uriInfo, "");

        //		logger.info("SurfBITracking BeaconTracking method invoked");
        HashMap<String, String> inputMap = new HashMap<String, String>();
        for (String s : fields) {
            int index = s.indexOf("=");
            //			logger.info("key:"+s.substring(0, index).toUpperCase()+" value:"+s.substring(index+1,s.length()));
            inputMap.put(s.substring(0, index).toUpperCase(), s.substring(index + 1, s.length()));

        }

        BeaconTracking beaconTracking = new BeaconTracking();
        beaconTracking.setBeaconData(userId, udid, major, minor, beaconUdid, sessionId, inputMap);

    }
}
