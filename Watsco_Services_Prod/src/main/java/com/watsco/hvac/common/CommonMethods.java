package com.watsco.hvac.common;

import com.google.common.base.Strings;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.NodeList;

public class CommonMethods {

    public HashMap getSoapResponseValues(String response) throws SOAPException, IOException {

        HashMap<String, String> hm = new HashMap<String, String>();

        MessageFactory factory = MessageFactory.newInstance();
        SOAPMessage message = factory.createMessage(
                new MimeHeaders(),
                new ByteArrayInputStream(response.getBytes(Charset
                                .forName("UTF-8"))));

        SOAPBody body = message.getSOAPBody();

        NodeList list = body.getElementsByTagName("detail");

        for (int i = 0; i < list.getLength(); i++) {
            NodeList innerList = list.item(i).getChildNodes();
            int count = innerList.getLength();
            String tagValue = "";
            String tagValues[] = new String[count];
            for (int j = 0; j < count; j++) {

                String tagName = innerList.item(j).getNodeName();
                tagValue = innerList.item(j).getTextContent().toString();
                tagValues[j] = tagValue;

                if (j == (count - 1)) {
                    hm.put(tagValues[0], tagValues[1]);

                }
            }
        }

        return hm;
    }

    public static String getWarrantyTagValue(final String startTag, final String endTag, String pageSource) {
        Pattern htmltag = Pattern.compile(startTag + "(.*?)" + endTag);
        Matcher tagmatch = htmltag.matcher(pageSource.toString());

        StringBuffer sb = new StringBuffer();

        int i = 0;
        while (tagmatch.find()) {
            String status = tagmatch.group()
                    .replace(startTag, "")
                    .replace(endTag, "");

            if (i == 0) {
                sb.append(status);
            }

            i++;

        }

        return sb.toString();
    }

    public static JSONObject getJsonObject(InputStream inputStreamObject) {

        try {
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStreamObject, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }

            JSONObject jsonObject = new JSONObject(responseStrBuilder.toString());

            //returns the json object
            return jsonObject;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static HashMap<Object, Object> removeNullValues(UriInfo uriInfo) {
        MultivaluedMap params = uriInfo.getQueryParameters();
        HashMap<Object, Object> keyValue = new HashMap<Object, Object>();
        Object value;

        for (Object key : params.keySet()) {
            value = params.get(key);
            if (value.toString().contains("null")) {
                value = value.toString().replaceAll(value.toString(), "");
            }
            value = value.toString().substring(1, value.toString().length() - 1);
            keyValue.put(key, value);
        }

        return keyValue;
    }

    public static HashMap<String, String> jsonToMap(String t) throws JSONException {

        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(t);
        Iterator<?> keys = jObject.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();
            String value = jObject.getString(key);
            map.put(key, value);

        }

        return map;
    }

    public static String escapeApostrophe(String str) {
        str = str.replace("'", "''");
        return str;
    }

    public static String toTitleCase(String s) {

        final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
        // to be capitalized

        StringBuilder sb = new StringBuilder();
        boolean capNext = true;

        for (char c : s.toCharArray()) {
            c = (capNext)
                    ? Character.toUpperCase(c)
                    : Character.toLowerCase(c);
            sb.append(c);
            capNext = (ACTIONABLE_DELIMITERS.indexOf((int) c) >= 0); // explicit cast not needed
        }
        return sb.toString();
    }

    public static boolean isNumeric(String str) {

        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Demonstrate checking for String that is not null, not empty using
     * standard Java classes.
     *
     * @param str
     * @return
     */
    public static boolean isNullorEmpty(String str) {

        if (str == null) {
            return true;
        } //                if(str.equals("null"))
        //			return true;
        else if (str.isEmpty()) {
            return true;
        } else if (str.contains("null")) {
            return true;
        } else {
            return false;
        }
    }

    public String stripValue(JSONObject objectValue, String key) throws JSONException {
        String newValue = null;

        if ((objectValue.get(key) != null)) {
            newValue = objectValue.get(key).toString();
            if ((key.equalsIgnoreCase("latitude")) || (key.equalsIgnoreCase("longitude")) || (key.equalsIgnoreCase("gpsEnabled") || (key.equalsIgnoreCase("pushEnabled")))) {
                if (newValue.isEmpty()) {
                    newValue = null;
                }
            }
            if (newValue.equals("0")) {
                newValue = null;
            }
            if (newValue.contains("'")) {
                newValue = newValue.replaceAll("'", "''");
            }
        } else {
            newValue = "";
        }

        return newValue;

    }

    public static String removeCharacterAfterPos(String str, int pos) {

        if (str.length() > 6) {
            int index = str.indexOf(".");
            if (str.substring(index, str.length()).length() > pos) {
                str = str.substring(0, index + pos + 1);
                return str;
            }
        }
        return str;

    }

    public static String checkLatLongValues(String str) {

        if (isNullorEmpty(str)) {
            str = null;
            return str;
        } else if (str.startsWith("0.")) {
            return str = null;
        } else if(str.equals("0")){
            str = null;
        }else {
            str = removeCharacterAfterPos(str, 6);
        }

        return str;

    }

    public static String getPromptValues(JSONObject prompts, String columnName) throws JSONException {
        return (CommonMethods.isNullorEmpty(prompts.get(columnName).toString()) ? null : (prompts.get(columnName).toString()));
    }

}
