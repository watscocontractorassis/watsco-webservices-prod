package com.watsco.hvac.common;

import java.util.HashMap;

public class AppConstants {
    /****************************************************************************************************************************/
    //CE Xref Request URL and Request XMLString Constants
    /****************************************************************************************************************************/
    public static String CE_BYID_REQUEST_XML = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<ws:getXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTID$$</id>"+
    "<searchtype xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTSEARCHTYPE$$</searchtype>"+
    "</ws:getXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    public static String CE_BYID_REQUEST_HEADER_XML = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws\">"+
	   "<soapenv:Header/>"+
	   "<soapenv:Body>"+
    "<ws:getCompressorDetail soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTID$$</id>"+
    "<searchtype xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTSEARCHTYPE$$</searchtype>"+
    "</ws:getCompressorDetail>"+
	"</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    public static String CE_BYPARAMS_REQUEST_XML ="<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<ws:getXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<cmprtype xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTCMPRTYPE$$</cmprtype>"+
    "<refrigerant xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTREFRIGERANT$$</refrigerant>"+
    "<voltage xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTVOLTAGE$$</voltage>"+
    "<phase xsi:type=\"xsd:double\" xs:type=\"type:double\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTPHASE$$</phase>"+
    "<CAPorTON xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTCAPORTON$$</CAPorTON>"+
    "<capOrTonValue xsi:type=\"xsd:double\" xs:type=\"type:double\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTCAPORTONVALUE$$</capOrTonValue>"+
    "</ws:getXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    public static String CE_HEADER_TRIM_START 		= "<compressorDetail>";
    public static String CE_HEADER_TRIM_END 		= "</compressorDetail>";
    
    public static String CE_COMPRESSORDETAIL_TAG 	= "compressorDetail";
    public static String CE_COMPRESSORDETAIL_CHANGE	= "item";
    
    public static String CE_ITEM_TAG			= "searchItem";
    public static String CE_ITEM_CHANGE			= "id";
    
    public static String CE_CAPACITY_TAG		= "actualCapacity";
    public static String CE_CAPACITY_CHANGE		= "capacity";
    
    public static String CE_CMPRTYPE_TAG		= "cmprType";
    public static String CE_CMPRTYPE_CHANGE		= "Cmptype";
    
    public static String CE_REFRIGERANT_TAG		= "Refr";
    public static String CE_REFRIGERANT_CHANGE	= "refrigerant";
    
    public static String CE_VOLTS_TAG			= "Volts";
    public static String CE_VOLTS_CHANGE		= "volts";
    
    public static String CE_HERTZ_TAG			= "Hz";
    public static String CE_HERTZ_CHANGE		= "Hertz";
    
    public static String CE_PHASE_TAG			= "Phase";
    public static String CE_PHASE_CHANGE		= "phase";
    
    
    /****************************************************************************************************************************/
    //BAKER XRef
    /****************************************************************************************************************************/
    
    public static String BAKER_BYID_REQUEST_XML = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<ws:getXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTID$$</id>"+
    "<searchtype xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTSEARCHTYPE$$</searchtype>"+
    "</ws:getXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    public static String BAKER_BYPARAMS_REQUEST_XML = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://ws\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<ws:getXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<cmprtype xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTCMPRTYPE$$</cmprtype>"+
    "<refrigerant xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTREFRIGERANT$$</refrigerant>"+
    "<voltage xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTVOLTAGE$$</voltage>"+
    "<phase xsi:type=\"xsd:double\" xs:type=\"type:double\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTPHASE$$</phase>"+
    "<CAPorTON xsi:type=\"xsd:string\" xs:type=\"type:string\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTCAPORTON$$</CAPorTON>"+
    "<capOrTonValue xsi:type=\"xsd:double\" xs:type=\"type:double\" xmlns:xs=\"http://www.w3.org/2000/XMLSchema-instance\">@@INPUTCAPORTONVALUE$$</capOrTonValue>"+
    "</ws:getXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    /****************************************************************************************************************************/
    //CE Warranty
    /****************************************************************************************************************************/
   public static String CE_ENTITLEMENT_REQUEST_XML =
    "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
    "xmlns=\"http://servicebench.com/entitlement/product/service/types\"><soap:Header>" +
    "<wsse:Security soap:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" +
    "<wsse:UsernameToken wsu:Id=\"UsernameToken-15231583\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
    "<wsse:Username>@@SB_UID$$</wsse:Username>" +
    "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">@@PASSWORD$$</wsse:Password>" +
    "</wsse:UsernameToken></wsse:Security></soap:Header>"
    + "<soap:Body><EntitlementRequest><version>1.0</version>" +
    "<sourceSystemName>Ext</sourceSystemName><sourceSystemVersion>?</sourceSystemVersion><serviceAdministrator>92</serviceAdministrator>" +
    "<modelNumber>@@MODELNUMBER$$</modelNumber><serialNumber>@@SERIALNUMBER$$</serialNumber><firstName/><lastName/>" +
    "<phone/><purchaseDate/><purchasedFrom/></EntitlementRequest></soap:Body></soap:Envelope>";
    
    
    public static String CE_SERVICEHISTORY_REQUEST_XML =
    "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
    "xmlns=\"http://servicebench.com/entitlement/product/service/types\"><soap:Header>" +
    "<wsse:Security soap:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">" +
    "<wsse:UsernameToken wsu:Id=\"UsernameToken-15231583\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
    "<wsse:Username>@@SB_UID$$</wsse:Username>" +
    "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">@@PASSWORD$$</wsse:Password>" +
    "</wsse:UsernameToken></wsse:Security></soap:Header><soap:Body><ServiceHistoryRequest><version>1.0</version>"+
    "<sourceSystemName>Ext</sourceSystemName><sourceSystemVersion>?</sourceSystemVersion>"+
    "<serviceAdministrator>92</serviceAdministrator><modelNumber>@@MODELNUMBER$$</modelNumber>" +
    "<serialNumber>@@SERIALNUMBER$$</serialNumber></ServiceHistoryRequest></soap:Body></soap:Envelope>";
    
    
    private static HashMap<Object, Object> hm = new HashMap<Object, Object>();
    
    private static HashMap<Object, Object> warrantyInfo = new HashMap<Object, Object>();
    
    public static HashMap<Object, Object> getValues() {
        if(hm == null)
            hm = new HashMap<Object, Object>();
        
        hm.put("Cove", "CoverageType");
        hm.put("Desc", "Description");
        hm.put("Effe", "EffectiveStart");
        hm.put("End ", "EndDate");
        
        return hm;
    }
    
    public static HashMap<Object, Object> getInfo() {
        if(warrantyInfo == null)
            warrantyInfo = new HashMap<Object, Object>();
        
        warrantyInfo.put("Mod", "Model");
        warrantyInfo.put("Rev", "Rev");
        warrantyInfo.put("ModelDe", "ModelDesc");
        warrantyInfo.put("Reg", "RegDate");
        warrantyInfo.put("Ins", "InstallDate");
        
        return warrantyInfo;
    }
}
