package com.watsco.hvac.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBConnection {

//    protected static Logger logger =Logger.getLogger(DBConnection.class);
    private static final Logger logger = LoggerFactory.getLogger(DBConnection.class);

    public static Connection getConnection(boolean isUsageTracking) throws Exception {
        String url;
        HashMap<String, String> hm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            hm = CommonUtil.getExternalAPIConfigProperties();
        } else {
            hm = LocalCommonUtil.getExternalAPIConfigProperties();
        }

        if (!isUsageTracking) {
            url = hm.get("DB_WATSCO_URL");
            //			url = "jdbc:jtds:sqlserver://10.196.67.17:1433/watsco_prod"; //watsco_prod
        } else {
            url = hm.get("DB_SURFBI_URL");
            //			url = "jdbc:jtds:sqlserver://10.196.67.17:1433/surfbi_prod"; //surfbi_prod
        }
        //		logger.info("URL : "+ url);
        String driverName = "net.sourceforge.jtds.jdbc.Driver";
        String userName = hm.get("DB_WATSCO_USERNAME");//"sa";
        String password = hm.get("DB_PASSWORD");//"Watsco2013";
        Class.forName(driverName);
        Connection connection = DriverManager.getConnection(url, userName,
                password);
        //		logger.info("Got DB Connection");
        return connection;
    }

    public static void closeConnection(Connection con, Statement stmt, ResultSet... rs) {

        try {

            if (rs != null) {
                for (ResultSet resultSet : rs) {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                }
            }
            if (stmt != null) {
                stmt.close();
            }
            if (con != null) {
                con.close();
            }
            //		logger.info("Close Connection");
        } catch (Exception e) {
            logger.error("Error closeConnection" + e);
        }

    }

    public static Connection getPriceDBConnection() throws Exception {

        HashMap<String, String> hm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            hm = CommonUtil.getExternalAPIConfigProperties();
        } else {
            hm = LocalCommonUtil.getExternalAPIConfigProperties();
        }

        //		String url = "jdbc:postgresql://wso-edw-instance.cedq1j6nhncg.us-east-1.redshift.amazonaws.com:5439/edwredtest";
        String url = hm.get("DB_PriceDB_URL");
        //		logger.info("URL : "+ url);
        String driverName = "org.postgresql.Driver";
        //		String userName = "cecom_client";
        //		String password = "CEhvac0605";

        String PriceuserName = hm.get("PriceDB_USERNAME");
        String Pricepassword = hm.get("PriceDB_PASSWORD");
        Class.forName(driverName);
        Connection connection = DriverManager.getConnection(url, PriceuserName,
                Pricepassword);
        //		logger.info("Got DB Connection");
        return connection;
    }

    public static void closeConnectionCS(ResultSet rs, Connection con, CallableStatement cs) {

        try {

            if (rs != null) {
                rs.close();
            }
            if (cs != null) {
                cs.close();
            }
            if (con != null) {
                con.close();
            }
            //			logger.info("Close Connection");
        } catch (Exception e) {
            logger.error("Error closeConnectionCS" + e);
        }

    }

}
