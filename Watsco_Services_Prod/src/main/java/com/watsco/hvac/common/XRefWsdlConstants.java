package com.watsco.hvac.common;

public class XRefWsdlConstants {
    
    public static String icp_motorsURL = "http://ws.icpusa.com/Railo/ICPWebServices/icp_motors.cfc?WSDL";
    
    public static String icp_motorsParams =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:icp=\"http://icpwebservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<icp:getMOTORSBYPARAM soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<motor_type xsi:type=\"xsd:string\">@@MOTOR_TYPE$$</motor_type>"+
    "<hp xsi:type=\"xsd:string\">@@HP$$</hp>"+
    "<voltage xsi:type=\"xsd:string\">@@VOLTAGE$$</voltage>"+
    "<rpm xsi:type=\"xsd:string\">@@RPM$$</rpm>"+
    "<fla xsi:type=\"xsd:string\">@@FLA$$</fla>"+
    "<frame xsi:type=\"xsd:string\">@@FRAME$$</frame>"+
    "</icp:getMOTORSBYPARAM>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    
    public static String icp_motorsId  = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:icp=\"http://icpwebservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<icp:getMOTORSXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "</icp:getMOTORSXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    
    public static String icp_filterbasesURL ="http://ws.icpusa.com/Railo/ICPWebServices/icp_filterbases.cfc?WSDL";
    public static String icp_filterbasesParams =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:icp=\"http://icpwebservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<icp:getFILTERBASESBYPARAM soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<filtersize xsi:type=\"xsd:string\">@@FILTERSIZE$$</filtersize>"+
    "<gaselec xsi:type=\"xsd:string\">@@GASELEC$$</gaselec>"+
    "<filterwidth xsi:type=\"xsd:string\">@@FILTERWIDTH$$</filterwidth>"+
    "</icp:getFILTERBASESBYPARAM>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    public static String icp_filterbasesId =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:icp=\"http://icpwebservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<icp:getFILTERBASESXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "</icp:getFILTERBASESXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    
    public static String icp_filterdriersURL ="http://ws.icpusa.com/Railo/ICPWebServices/icp_filterdriers.cfc?WSDL";
    public static String icp_filterdriersParams =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:icp=\"http://icpwebservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<icp:getFILTERDRIERSBYPARAM soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<line_type xsi:type=\"xsd:string\">@@LINE_TYPE$$</line_type>"+
    "<drier_type xsi:type=\"xsd:string\">@@DRIER_TYPE$$</drier_type>"+
    "<connection_type xsi:type=\"xsd:string\">@@CONNECTION_TYPE$$</connection_type>"+
    "<connection_size xsi:type=\"xsd:string\">@@CONNECTION_SIZE$$</connection_size>"+
    "</icp:getFILTERDRIERSBYPARAM>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    public static String icp_filterdriersId =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:icp=\"http://icpwebservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<icp:getFILTERDRIERSXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "</icp:getFILTERDRIERSXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    public static String ICPcmprxrefbyparamsURL ="http://ws.icpusa.com/Railo/ICPWebServices/ICPcmprxrefbyparams.cfc?wsdl";
    public static String ICPcmprxrefbyparams =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:icp=\"http://icpwebservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<icp:getXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<cmprtype xsi:type=\"xsd:string\">@@CMPRTYPE$$</cmprtype>"+
    "<refrigerant xsi:type=\"xsd:string\">@@REFRIGERANT$$</refrigerant>"+
    "<voltage xsi:type=\"xsd:string\">@@VOLTAGE$$</voltage>"+
    "<phase xsi:type=\"xsd:double\">@@PHASE$$</phase>"+
    "<CAPorTON xsi:type=\"xsd:string\">@@CAPORTON$$</CAPorTON>"+
    "<capOrTonValue xsi:type=\"xsd:double\">@@CAPORTONVALUE$$</capOrTonValue>"+
    "</icp:getXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    
    public static String ICPcmprxrefbyidURL ="http://ws.icpusa.com/Railo/ICPWebServices/ICPcmprxrefbyid.cfc?WSDL";
    public static String ICPcmprxrefbyidDetail ="<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:icp=\"http://icpwebservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<icp:getCompressorDetail soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "<searchtype xsi:type=\"xsd:string\">@@SEARCHTYPE$$</searchtype>"+
    "</icp:getCompressorDetail>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    public static String ICPcmprxrefbyidId = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:icp=\"http://icpwebservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<icp:getXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "<searchtype xsi:type=\"xsd:string\">@@SEARCHTYPE$$</searchtype>"+
    "</icp:getXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    
    public static String totaline_filterbasesURL = "http://ws.totaline.com/Railo/WebServices/totaline_filterbases.cfc?WSDL";
    public static String totaline_filterbasesParams =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getFILTERBASESBYPARAM soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<filtersize xsi:type=\"xsd:string\">@@FILTERSIZE$$</filtersize>"+
    "<gaselec xsi:type=\"xsd:string\">@@GASELEC$$</gaselec>"+
    "<filterwidth xsi:type=\"xsd:string\">@@FILTERWIDTH$$</filterwidth>"+
    "</web:getFILTERBASESBYPARAM>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    public static String totaline_filterbasesId =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getFILTERBASESXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "</web:getFILTERBASESXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    public static String totaline_motorsURL = "http://ws.totaline.com/Railo/WebServices/totaline_motors.cfc?WSDL";
    public static String totaline_motorsParams =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getMOTORSBYPARAM soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<motor_type xsi:type=\"xsd:string\">@@MOTOR_TYPE$$</motor_type>"+
    "<hp xsi:type=\"xsd:string\">@@HP$$</hp>"+
    "<voltage xsi:type=\"xsd:string\">@@VOLTAGE$$</voltage>"+
    "<rpm xsi:type=\"xsd:string\">@@RPM$$</rpm>"+
    "<fla xsi:type=\"xsd:string\">@@FLA$$</fla>"+
    "<frame xsi:type=\"xsd:string\">@@FRAME$$</frame>"+
    "</web:getMOTORSBYPARAM>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    public static String totaline_motorsId =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getMOTORSXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "</web:getMOTORSXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    
    public static String totaline_filterdriersURL = "http://ws.totaline.com/Railo/WebServices/totaline_filterdriers.cfc?WSDL";
    public static String totaline_filterdriersParams =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getFILTERDRIERSBYPARAM soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<line_type xsi:type=\"xsd:string\">@@LINE_TYPE$$</line_type>"+
    "<drier_type xsi:type=\"xsd:string\">@@DRIER_TYPE$$</drier_type>"+
    "<connection_type xsi:type=\"xsd:string\">@@CONNECTION_TYPE$$</connection_type>"+
    "<connection_size xsi:type=\"xsd:string\">@@CONNECTION_SIZE$$</connection_size>"+
    "</web:getFILTERDRIERSBYPARAM>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    public static String totaline_filterdriersId =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getFILTERDRIERSXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "</web:getFILTERDRIERSXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    
    public static String totaline_semihermsURL = "http://ws.totaline.com/Railo/WebServices/totaline_semiherms.cfc?WSDL";
    public static String totaline_semihermsId =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getSEMIHERMSXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "</web:getSEMIHERMSXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    
    public static String totaline_thermostatsURL = "http://ws.totaline.com/Railo/WebServices/totaline_thermostats.cfc?WSDL";
    public static String totaline_thermostatsParams =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getTHERMOSTATSBYPARAM soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<appl xsi:type=\"xsd:string\">@@APPL$$</appl>"+
    "<res_or_comm xsi:type=\"xsd:string\">@@RES_OR_COMM$$</res_or_comm>"+
    "<stages xsi:type=\"xsd:string\">@@STAGES$$</stages>"+
    "<programmable xsi:type=\"xsd:string\">@@PROGRAMMABLE$$</programmable>"+
    "<include_totaline xsi:type=\"xsd:string\">@@INCLUDE_TOTALINE$$</include_totaline>"+
    "<include_carrier xsi:type=\"xsd:string\">@@INCLUDE_CARRIER$$</include_carrier>"+
    "<include_bryant xsi:type=\"xsd:string\">@@INCLUDE_BRYANT$$</include_bryant>"+
    "</web:getTHERMOSTATSBYPARAM>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    public static String totaline_thermostatsId =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getTHERMOSTATSXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "</web:getTHERMOSTATSXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    
    public static String cmprxrefbyparamsURL = "http://ws.totaline.com/Railo/WebServices/cmprxrefbyparams.cfc?wsdl";
    public static String cmprxrefbyparams =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<cmprtype xsi:type=\"xsd:string\">@@CMPRTYPE$$</cmprtype>"+
    "<refrigerant xsi:type=\"xsd:string\">@@REFRIGERANT$$</refrigerant>"+
    "<voltage xsi:type=\"xsd:string\">@@VOLTAGE$$</voltage>"+
    "<phase xsi:type=\"xsd:double\">@@PHASE$$</phase>"+
    "<CAPorTON xsi:type=\"xsd:string\">@@CAPORTON$$</CAPorTON>"+
    "<capOrTonValue xsi:type=\"xsd:double\">@@CAPORTONVALUE$$</capOrTonValue>"+
    "</web:getXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    
    
    public static String cmprxrefbyidURL = "http://ws.totaline.com/Railo/WebServices/cmprxrefbyid.cfc?WSDL";
    public static String cmprxrefbyidDetail =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getCompressorDetail soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "<searchtype xsi:type=\"xsd:string\">@@SEARCHTYPE$$</searchtype>"+
    "</web:getCompressorDetail>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    public static String cmprxrefbyidId =
    "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.railo_war276.content.production_nas\">"+
    "<soapenv:Header/>"+
    "<soapenv:Body>"+
    "<web:getXREF soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+
    "<id xsi:type=\"xsd:string\">@@ID$$</id>"+
    "<searchtype xsi:type=\"xsd:string\">@@SEARCHTYPE$$</searchtype>"+
    "</web:getXREF>"+
    "</soapenv:Body>"+
    "</soapenv:Envelope>";
    
    public static String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    
    
    
    
}