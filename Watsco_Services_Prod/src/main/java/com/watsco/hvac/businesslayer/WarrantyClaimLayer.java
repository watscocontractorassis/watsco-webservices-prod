package com.watsco.hvac.businesslayer;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.WarrantyDocumentConvertor;
import com.watsco.hvac.dto.ClaimDTO;

public class WarrantyClaimLayer {

    private static Logger logger = Logger.getLogger(WarrantyClaimLayer.class);

    public String insertDB(ClaimDTO claimDTO, HashMap<String, String> claimsMap,
            ArrayList<HashMap<String, String>> claimParts, int CLAIM_WID, String claimNumber, String claimStatus, int userId, String save, String SB_ID, String accNo, int buWid, String processStatus) {
        Connection con;
        String sqlQuery;
        ResultSet claimSelectRS;
        Statement stmt = null;
        ResultSet claimPartsSelectRS = null;
        String response = "";
        int inQuiry = 0;
        StringBuffer claimPartsRequest = new StringBuffer();
        String createdBy = Integer.toString(userId);
        String editedBy = Integer.toString(userId);
        ResultSet rs;
        

        String retrievalCode = RandomStringUtils.randomAlphanumeric(15);

        if (claimsMap.get("accountNumber") != null) {
            accNo = claimsMap.get("accountNumber");
        } else {
            accNo = SB_ID;
        }

        if (save.equalsIgnoreCase("Y")) {
            inQuiry = 0;
        }

        try {

            con = DBConnection.getConnection(false);

//            sqlQuery = "EXEC Warranty_Claims_Insert " + CLAIM_WID + ",'" + claimNumber + "','" + claimStatus + "','" + replaceSQLValue(claimDTO, claimsMap, "referenceNumber") + "','" + accNo + "','" + replaceSQLValue(claimDTO, claimsMap, "serviceAgreementNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "authorizationNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "brand") + "','" + replaceSQLValue(claimDTO, claimsMap, "modelNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerCompanyName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerLastName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerFirstName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressLine1") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressLine2") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressCity") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressState") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressZip") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerPhone") + "','" + replaceSQLValue(claimDTO, claimsMap, "serialNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "purchaseDate") + "','" + replaceSQLValue(claimDTO, claimsMap, "warrantyType") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerComplaint") + "','" + replaceSQLValue(claimDTO, claimsMap, "defectCode") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateServiceRequested") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateServiceCompleted") + "','" + replaceSQLValue(claimDTO, claimsMap, "serviceExplanation") + "','" + replaceSQLValue(claimDTO, claimsMap, "repairCategory") + "','" + replaceSQLValue(claimDTO, claimsMap, "shippingAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "freightAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerEmail") + "','" + replaceSQLValue(claimDTO, claimsMap, "competitiveEquipment") + "','" + replaceSQLValue(claimDTO, claimsMap, "ccsJobNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "applicationType") + "','" + replaceSQLValue(claimDTO, claimsMap, "originalOwner") + "','" + replaceSQLValue(claimDTO, claimsMap, "modelLocation") + "','" + replaceSQLValue(claimDTO, claimsMap, "furnaceOrientation") + "','" + replaceSQLValue(claimDTO, claimsMap, "furnaceFuel") + "','" + replaceSQLValue(claimDTO, claimsMap, "geoLatitude") + "','" + replaceSQLValue(claimDTO, claimsMap, "geoLongitude") + "','" + retrievalCode + "','" + createdBy + "','" + editedBy + "','" + replaceSQLValue(claimDTO, claimsMap, "shared") + "','" + replaceSQLValue(claimDTO, claimsMap, "comment") + "','" + replaceSQLValue(claimDTO, claimsMap, "creditCardPayment") + "','" + replaceSQLValue(claimDTO, claimsMap, "holdB") + "'";
//            sqlQuery = "EXEC Warranty_Claims_Insert " + CLAIM_WID + ",'" + claimNumber + "','" + claimStatus + "','" + replaceSQLValue(claimDTO, claimsMap, "referenceNumber") + "','" + accNo + "','" + replaceSQLValue(claimDTO, claimsMap, "serviceAgreementNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "authorizationNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "brand") + "','" + replaceSQLValue(claimDTO, claimsMap, "modelNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerCompanyName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerLastName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerFirstName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressLine1") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressLine2") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressCity") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressState") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressZip") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerPhone") + "','" + replaceSQLValue(claimDTO, claimsMap, "serialNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "purchaseDate") + "','" + replaceSQLValue(claimDTO, claimsMap, "warrantyType") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerComplaint") + "','" + replaceSQLValue(claimDTO, claimsMap, "defectCode") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateServiceRequested") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateServiceCompleted") + "','" + replaceSQLValue(claimDTO, claimsMap, "serviceExplanation") + "','" + replaceSQLValue(claimDTO, claimsMap, "repairCategory") + "','" + replaceSQLValue(claimDTO, claimsMap, "shippingAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "freightAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerEmail") + "','" + replaceSQLValue(claimDTO, claimsMap, "competitiveEquipment") + "','" + replaceSQLValue(claimDTO, claimsMap, "ccsJobNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "applicationType") + "','" + replaceSQLValue(claimDTO, claimsMap, "originalOwner") + "','" + replaceSQLValue(claimDTO, claimsMap, "modelLocation") + "','" + replaceSQLValue(claimDTO, claimsMap, "furnaceOrientation") + "','" + replaceSQLValue(claimDTO, claimsMap, "furnaceFuel") + "','" + replaceSQLValue(claimDTO, claimsMap, "geoLatitude") + "','" + replaceSQLValue(claimDTO, claimsMap, "geoLongitude") + "','" + retrievalCode + "','" + createdBy + "','" + editedBy + "','" + replaceSQLValue(claimDTO, claimsMap, "shared") + "','" + replaceSQLValue(claimDTO, claimsMap, "comment") + "','" + replaceSQLValue(claimDTO, claimsMap, "creditCardPayment") + "','" + replaceSQLValue(claimDTO, claimsMap, "holdB") + "','" + (replaceSQLValue(claimDTO, claimsMap, "laborHours") == "" ? 0 : replaceSQLValue(claimDTO, claimsMap, "laborHours")) + "','" + replaceSQLValue(claimDTO, claimsMap, "stateTaxAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "refrigerantLbs") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "travelHours") + "','" + replaceSQLValue(claimDTO, claimsMap, "diagnosticHours") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressCountry") + "','" + buWid + "','" + replaceSQLValue(claimDTO, claimsMap, "laborAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "travelAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "diagnosticFee") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherItem1Amount") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherItem2Amount") + "'";
            sqlQuery = "EXEC Warranty_Claims_Insert " + CLAIM_WID + ",'" + claimNumber + "','" + claimStatus + "','" + replaceSQLValue(claimDTO, claimsMap, "referenceNumber") + "','" + accNo + "','" + replaceSQLValue(claimDTO, claimsMap, "serviceAgreementNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "authorizationNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "brand") + "','" + replaceSQLValue(claimDTO, claimsMap, "modelNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerCompanyName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerLastName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerFirstName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressLine1") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressLine2") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressCity") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressState") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressZip") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerPhone") + "','" + replaceSQLValue(claimDTO, claimsMap, "serialNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "purchaseDate") + "','" + replaceSQLValue(claimDTO, claimsMap, "warrantyType") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerComplaint") + "','" + replaceSQLValue(claimDTO, claimsMap, "defectCode") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateServiceRequested") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateServiceCompleted") + "','" + replaceSQLValue(claimDTO, claimsMap, "serviceExplanation") + "','" + replaceSQLValue(claimDTO, claimsMap, "repairCategory") + "','" + replaceSQLValue(claimDTO, claimsMap, "shippingAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "freightAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerEmail") + "','" + replaceSQLValue(claimDTO, claimsMap, "competitiveEquipment") + "','" + replaceSQLValue(claimDTO, claimsMap, "ccsJobNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "applicationType") + "','" + replaceSQLValue(claimDTO, claimsMap, "originalOwner") + "','" + replaceSQLValue(claimDTO, claimsMap, "modelLocation") + "','" + replaceSQLValue(claimDTO, claimsMap, "furnaceOrientation") + "','" + replaceSQLValue(claimDTO, claimsMap, "furnaceFuel") + "','" + replaceSQLValue(claimDTO, claimsMap, "geoLatitude") + "','" + replaceSQLValue(claimDTO, claimsMap, "geoLongitude") + "','" + retrievalCode + "','" + createdBy + "','" + editedBy + "','" + replaceSQLValue(claimDTO, claimsMap, "shared") + "','" + replaceSQLValue(claimDTO, claimsMap, "comment") + "','" + replaceSQLValue(claimDTO, claimsMap, "creditCardPayment") + "','" + replaceSQLValue(claimDTO, claimsMap, "holdB") + "','" + (replaceSQLValue(claimDTO, claimsMap, "laborHours") == "" ? 0 : replaceSQLValue(claimDTO, claimsMap, "laborHours")) + "','" + replaceSQLValue(claimDTO, claimsMap, "stateTaxAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "refrigerantLbs") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "travelHours") + "','" + replaceSQLValue(claimDTO, claimsMap, "diagnosticHours") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressCountry") + "','" + buWid + "','" + replaceSQLValue(claimDTO, claimsMap, "laborAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "travelAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "diagnosticFee") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherItem1Amount") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherItem2Amount") + "','" + processStatus + "'";

            logger.info("=== USER ID :" +userId+"===insertDB()====>Report Query : " + sqlQuery);
            
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            claimSelectRS = stmt.executeQuery(sqlQuery);
            while (claimSelectRS.next()) {
                CLAIM_WID = claimSelectRS.getInt("Claim_WID");
            }
            sqlQuery = "EXEC [Warranty_Claims_SELECT] '" + CLAIM_WID + "'";
            logger.info("=== USER ID :" +userId+"===insertDB()====>Report Query : " + sqlQuery);
            logger.info("=== USER ID :" + userId + "======>Query : " + sqlQuery);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sqlQuery);

            try {
                claimPartsSelectRS = warrantyClaimParts_DB(claimDTO, claimParts, CLAIM_WID, rs, claimPartsRequest, inQuiry, userId);
            } catch (Exception e) {
                e.printStackTrace();
            }

            response = WarrantyDocumentConvertor.RSStoJSONConvertor(rs, claimPartsSelectRS);

        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }

        return response;
    }

    public String updateDB(ClaimDTO claimDTO, HashMap<String, String> claimsMap, ArrayList<HashMap<String, String>> claimParts, String claimNumber, int CLAIM_WID, String claimStatus, String UId, int userId, int inQuiry,
            String errorText, String rejectReason, String SB_ID, String accNo, String processStatus) {
        CallableStatement cs;
        Connection con;
        String sqlQuery;
        ResultSet claimSelectRS;
        Statement stmt;
        ResultSet claimPartsSelectRS = null;
        String editedBy = "";
        String response = "";
        String sharedDate = "";

        StringBuffer claimPartsRequest = new StringBuffer();
//		String response = "";
        try {
            con = DBConnection.getConnection(false);
            editedBy = Integer.toString(userId);

            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss:sss");
            //get current date time with Date()
            Date date = new Date();
            String submittedDate = dateFormat.format(date);
            if (claimNumber == null) {
                claimNumber = "";
            }
            if (claimStatus == null) {
                claimStatus = "";
            }

            String comments = claimsMap.get("commentText");
//            logger.info("comments" + comments);
//            System.out.println("comments = " + comments);

//	        if(claimsMap.get("accountNumber")!=null){
//				accNo = claimsMap.get("accountNumber");
//			}
//			else 
//				accNo = SB_ID;
//            sqlQuery = "EXEC Warranty_Claims_Update " + CLAIM_WID + ",'" + claimNumber + "','" + replaceSQLValue(claimDTO, claimsMap, "referenceNumber") + "','" + claimStatus + "','" + accNo + "','" + replaceSQLValue(claimDTO, claimsMap, "serviceAgreementNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "authorizationNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "brand") + "','" + replaceSQLValue(claimDTO, claimsMap, "modelNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerCompanyName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerLastName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerFirstName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressLine1") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressLine2") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressCity") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressState") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressZip") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerPhone") + "','" + replaceSQLValue(claimDTO, claimsMap, "serialNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "purchaseDate") + "','" + replaceSQLValue(claimDTO, claimsMap, "warrantyType") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerComplaint") + "','" + replaceSQLValue(claimDTO, claimsMap, "defectCode") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateServiceRequested") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateServiceCompleted") + "','" + replaceSQLValue(claimDTO, claimsMap, "serviceExplanation") + "','" + replaceSQLValue(claimDTO, claimsMap, "repairCategory") + "','" + replaceSQLValue(claimDTO, claimsMap, "shippingAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "freightAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerEmail") + "','" + replaceSQLValue(claimDTO, claimsMap, "competitiveEquipment") + "','" + replaceSQLValue(claimDTO, claimsMap, "ccsJobNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "applicationType") + "','" + replaceSQLValue(claimDTO, claimsMap, "originalOwner") + "','" + replaceSQLValue(claimDTO, claimsMap, "modelLocation") + "','" + replaceSQLValue(claimDTO, claimsMap, "furnaceOrientation") + "','" + replaceSQLValue(claimDTO, claimsMap, "furnaceFuel") + "','" + replaceSQLValue(claimDTO, claimsMap, "geoLatitude") + "','" + replaceSQLValue(claimDTO, claimsMap, "geoLongitude") + "','" + replaceSQLValue(claimDTO, claimsMap, "shared") + "','" + replaceSQLValue(claimDTO, claimsMap, "diagnosticFee") + "','" + replaceSQLValue(claimDTO, claimsMap, "mileageAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "travelAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "localTaxAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "stateTaxAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "approvedAmount") + "','" + editedBy + "','" + errorText + "','" + rejectReason + "','" + replaceSQLValue(claimDTO, claimsMap, "msgStatus") + "','" + replaceSQLValue(claimDTO, claimsMap, "submittedDate") + "','" + replaceSQLValue(claimDTO, claimsMap, "claimDate") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateApproved") + "','" + sharedDate + "','" + inQuiry + "','" + replaceSQLValue(claimDTO, claimsMap, "comment") + "','" + replaceSQLValue(claimDTO, claimsMap, "creditCardPayment") + "','" + ((replaceSQLValue(claimDTO, claimsMap, "holdB").equals("Y") || replaceSQLValue(claimDTO, claimsMap, "holdB").equals("1")) ? 1 : 0) + "','" + replaceSQLValue(claimDTO, claimsMap, "laborHours") + "','" + replaceSQLValue(claimDTO, claimsMap, "refrigerantLbs") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "travelHours") + "','" + replaceSQLValue(claimDTO, claimsMap, "diagnosticHours") + "','" + replaceSQLValue(claimDTO, claimsMap, "laborAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherItem1Amount") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherItem2Amount") + "','" + replaceSQLValue(claimDTO, claimsMap, "laborRate") + "','" + replaceSQLValue(claimDTO, claimsMap, "refrigerantPrice") + "'";
            sqlQuery = "EXEC Warranty_Claims_Update " + CLAIM_WID + ",'" + claimNumber + "','" + replaceSQLValue(claimDTO, claimsMap, "referenceNumber") + "','" + claimStatus + "','" + accNo + "','" + replaceSQLValue(claimDTO, claimsMap, "serviceAgreementNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "authorizationNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "brand") + "','" + replaceSQLValue(claimDTO, claimsMap, "modelNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerCompanyName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerLastName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerFirstName") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressLine1") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressLine2") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressCity") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressState") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerAddressZip") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerPhone") + "','" + replaceSQLValue(claimDTO, claimsMap, "serialNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "purchaseDate") + "','" + replaceSQLValue(claimDTO, claimsMap, "warrantyType") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerComplaint") + "','" + replaceSQLValue(claimDTO, claimsMap, "defectCode") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateServiceRequested") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateServiceCompleted") + "','" + replaceSQLValue(claimDTO, claimsMap, "serviceExplanation") + "','" + replaceSQLValue(claimDTO, claimsMap, "repairCategory") + "','" + replaceSQLValue(claimDTO, claimsMap, "shippingAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "freightAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "customerEmail") + "','" + replaceSQLValue(claimDTO, claimsMap, "competitiveEquipment") + "','" + replaceSQLValue(claimDTO, claimsMap, "ccsJobNumber") + "','" + replaceSQLValue(claimDTO, claimsMap, "applicationType") + "','" + replaceSQLValue(claimDTO, claimsMap, "originalOwner") + "','" + replaceSQLValue(claimDTO, claimsMap, "modelLocation") + "','" + replaceSQLValue(claimDTO, claimsMap, "furnaceOrientation") + "','" + replaceSQLValue(claimDTO, claimsMap, "furnaceFuel") + "','" + replaceSQLValue(claimDTO, claimsMap, "geoLatitude") + "','" + replaceSQLValue(claimDTO, claimsMap, "geoLongitude") + "','" + replaceSQLValue(claimDTO, claimsMap, "shared") + "','" + replaceSQLValue(claimDTO, claimsMap, "diagnosticFee") + "','" + replaceSQLValue(claimDTO, claimsMap, "mileageAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "travelAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "localTaxAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "stateTaxAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "approvedAmount") + "','" + editedBy + "','" + errorText + "','" + rejectReason + "','" + replaceSQLValue(claimDTO, claimsMap, "msgStatus") + "','" + replaceSQLValue(claimDTO, claimsMap, "submittedDate") + "','" + replaceSQLValue(claimDTO, claimsMap, "claimDate") + "','" + replaceSQLValue(claimDTO, claimsMap, "dateApproved") + "','" + sharedDate + "','" + inQuiry + "','" + replaceSQLValue(claimDTO, claimsMap, "comment") + "','" + replaceSQLValue(claimDTO, claimsMap, "creditCardPayment") + "','" + ((replaceSQLValue(claimDTO, claimsMap, "holdB").equals("Y") || replaceSQLValue(claimDTO, claimsMap, "holdB").equals("1")) ? 1 : 0) + "','" + replaceSQLValue(claimDTO, claimsMap, "laborHours") + "','" + replaceSQLValue(claimDTO, claimsMap, "refrigerantLbs") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "travelHours") + "','" + replaceSQLValue(claimDTO, claimsMap, "diagnosticHours") + "','" + replaceSQLValue(claimDTO, claimsMap, "laborAmount") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherItem1Amount") + "','" + replaceSQLValue(claimDTO, claimsMap, "otherItem2Amount") + "','" + replaceSQLValue(claimDTO, claimsMap, "laborRate") + "','" + replaceSQLValue(claimDTO, claimsMap, "refrigerantPrice") + "','" + processStatus + "'";
            logger.info("=== USER ID :" +userId+"===updateDB()====>Report Query : " + sqlQuery);

            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            claimSelectRS = stmt.executeQuery(sqlQuery);

            try {
                claimPartsSelectRS = warrantyClaimParts_DB(claimDTO, claimParts, CLAIM_WID, claimSelectRS, claimPartsRequest, inQuiry, userId);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("=== USER ID :" + userId + "===Submit Claim ERROR=====>" + e);
            }

            response = WarrantyDocumentConvertor.RSStoJSONConvertor(claimSelectRS, claimPartsSelectRS);
            logger.info("=== USER ID :" + userId + "=== updateDB() Response =====>" + response);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("=== USER ID :" + userId + "====Submit Claim ERROR=====>" + e);
        }

        return response;

    }

    public ResultSet warrantyClaimParts_DB(ClaimDTO claimDTO, ArrayList<HashMap<String, String>> claimParts, int CLAIM_WID,
            ResultSet claimSelectRS, StringBuffer claimPartsRequest, int inQuiry, int userId) throws SQLException {
        try {
            CallableStatement cs;
            Connection con = DBConnection.getConnection(false);;
            String sqlQuery = null;
            Statement stmt;
            ResultSet claimPartsSelectRS = null;
            ResultSet rs = null;
            ArrayList<HashMap<String, String>> hm = new ArrayList<HashMap<String, String>>();
            hm = claimDTO.getAlClaimParts();
            if (!(claimParts.size() > 0)) {
                claimParts = hm;
            }

            if (claimSelectRS != null) {

                int size = claimParts.size();
                if (size > 0) {
                    for (int i = 0; i < size; i++) {

                        sqlQuery = "EXEC warranty_claim_parts_insert '" + CLAIM_WID + "','" + getClaimParts(claimDTO, claimParts, "causal_part", i) + "','" + getClaimParts(claimDTO, claimParts, "failedPartNumber", i) + "','" + getClaimParts(claimDTO, claimParts, "failedPartQuantity", i) + "','" + getClaimParts(claimDTO, claimParts, "failedPartSerial", i) + "','" + getClaimParts(claimDTO, claimParts, "failedPartInstallDate", i) + "','" + getClaimParts(claimDTO, claimParts, "partNumber", i) + "','" + getClaimParts(claimDTO, claimParts, "partQuantity", i) + "','" + getClaimParts(claimDTO, claimParts, "partSerialNumber", i) + "','" + getClaimParts(claimDTO, claimParts, "partAmount", i) + "','" + getClaimParts(claimDTO, claimParts, "partInvoiceNumber", i) + "','" + getClaimParts(claimDTO, claimParts, "partSequence", i) + "','" + getClaimParts(claimDTO, claimParts, "partDescription", i) + "','" + getClaimParts(claimDTO, claimParts, "partDisposition", i) + "','" + getClaimParts(claimDTO, claimParts, "partMarkUp", i) + "','" + inQuiry + "'";
                        logger.info("=== USER ID :" +userId+"===warrantyClaimParts_DB()====>Report Query : " + sqlQuery);
                        claimPartsRequest.append(sqlQuery).append("\n");
                    }
                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                    claimPartsSelectRS = stmt.executeQuery(claimPartsRequest.toString());
                }

            }
            return claimPartsSelectRS;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String replaceSQLValue(ClaimDTO claimDTO, HashMap<String, String> hmLoginInformation, String keyValue) {
        try {
            HashMap<String, String> claimDTOHM = new HashMap<String, String>();
            String newvalue = "";

            if (claimDTO.getClaimMap() != null) {
                claimDTOHM = claimDTO.getClaimMap();
            }

            if (hmLoginInformation == null) {
                hmLoginInformation = claimDTOHM;
            }

            if (!(hmLoginInformation.get(keyValue) == null)) {

                if (keyValue.equalsIgnoreCase("comment")) {
                    if (hmLoginInformation.containsKey("commentText")) {
                        newvalue = hmLoginInformation.get("commentText") != null ? hmLoginInformation.get("commentText") : "";
                    } else {
                        newvalue = hmLoginInformation.get(keyValue) != null ? hmLoginInformation.get(keyValue) : "";
                    }
                } else {
                    newvalue = hmLoginInformation.get(keyValue) != null ? hmLoginInformation.get(keyValue) : "";
                    System.out.println("keyValue : "+keyValue+" newvalue : "+newvalue);
                }
            } else if (claimDTOHM != null) {
                newvalue = claimDTOHM.get(keyValue) != null ? claimDTOHM.get(keyValue) : "";
            } else {
                newvalue = "";
            }

            newvalue = newvalue.replaceAll("'", "''");

            if ((keyValue.equalsIgnoreCase("originalOwner")) || (keyValue.equalsIgnoreCase("competitiveEquipment"))) {
                if ((newvalue.equalsIgnoreCase("0")) || (newvalue.equalsIgnoreCase("N")) || (newvalue.equalsIgnoreCase("false"))) {
                    newvalue = "0";
                } else if ((newvalue.equalsIgnoreCase("1")) || (newvalue.equalsIgnoreCase("Y")) || (newvalue.equalsIgnoreCase("true"))) {
                    newvalue = "1";
                }
            }

            if (newvalue == null) {
                newvalue = "";
            }

            return newvalue;
        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }

    }

    public String getClaimParts(ClaimDTO claimDTO, ArrayList<HashMap<String, String>> al, String keyValue, int i) {
        try {
            String newValue = "";
            
            if(keyValue.equals("causal_part")){
                System.out.println("Testing");
            }

            ArrayList<HashMap<String, String>> claimPartHM = new ArrayList<HashMap<String, String>>();

            if (claimDTO.getClaimMap() != null) {
                claimPartHM = claimDTO.getAlClaimParts();
            }

            if (al == null) {
                al = claimPartHM;
            }

            if (al.get(i).containsKey(keyValue)) {
                if (!(al.get(i).get(keyValue) == null)) {
                    newValue = al.get(i).get(keyValue) == null ? "" : al.get(i).get(keyValue);
                } else if (claimPartHM != null) {
                    newValue = claimPartHM.get(i).get(keyValue) == null ? "" : claimPartHM.get(i).get(keyValue);
                } else {
                    newValue = "";
                }
            } else if (claimPartHM.size() > 0) {
                if ((claimPartHM.get(i).containsKey(keyValue))) {
                    if (!(claimPartHM.get(i).get(keyValue) == null)) {
                        newValue = claimPartHM.get(i).get(keyValue) == null ? "" : claimPartHM.get(i).get(keyValue);
                    } else {
                        newValue = "";
                    }
                }
            } 
            /*else {
                newValue = "";
            }*/

            newValue = newValue.replaceAll("'", "''");
            if (keyValue.equalsIgnoreCase("causal_part")) {
                System.out.println("keyValue "+newValue);
                if ((newValue.contains("0")) || (newValue.equalsIgnoreCase("N")) || (newValue.equalsIgnoreCase("false"))) {
                    newValue = "0";
                } else if ((newValue.contains("1")) || (newValue.equalsIgnoreCase("Y")) || (newValue.equalsIgnoreCase("true"))) {
                    newValue = "1";
                }
            }

            if (newValue == null) {
                newValue = "";
            }
            
            return newValue;
        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }
    }

}
