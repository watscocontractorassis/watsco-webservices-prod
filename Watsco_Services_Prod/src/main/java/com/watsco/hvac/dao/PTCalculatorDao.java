package com.watsco.hvac.dao;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.log4j.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class PTCalculatorDao {

    private static Logger logger = Logger.getLogger(PTCalculatorDao.class);

    /**
     *
     * @param rs
     * @return
     * @throws ParserConfigurationException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws IOException
     * @throws TransformerException
     *
     */
    public static String getXMLDocument(ResultSet refrigerant_detail_rs,
            ResultSet pressure_to_temp_rs,
            ResultSet temp_to_pressure_rs)
            throws ParserConfigurationException, SQLException,
            ClassNotFoundException, IOException, TransformerException {
        //		logger.info("PTCalculatorDao getXMLDocument method invoked");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element results = doc.createElement("RESULT");

        if (refrigerant_detail_rs != null) {
            //			logger.info("refrigerant_detail_rs Not null");

            ResultSetMetaData rsmd = refrigerant_detail_rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            Element row = doc.createElement("REFRIGERANT_DETAILS");
            results.appendChild(row);

            while (refrigerant_detail_rs.next()) {
                Element child = doc.createElement("CHILD");

                //				logger.info("refrigerant_detail_rs child");
                for (int i = 1; i <= colCount; i++) {
                    Element node = doc.createElement(rsmd.getColumnName(i));

                    if (refrigerant_detail_rs.getObject(i) != null) {
                        node.appendChild(doc.createTextNode(refrigerant_detail_rs.getObject(i).toString()));
                    } else {
                        node.appendChild(doc.createTextNode(""));
                    }

                    child.appendChild(node);
                }
                row.appendChild(child);
            }
            results.appendChild(row);

            //			logger.info("refrigerant_detail_rs Executed");
        }

        if (pressure_to_temp_rs != null) {
            //			logger.info("pressure_to_temp_rs Not null");

            ResultSetMetaData rsmd = pressure_to_temp_rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            Element row = doc.createElement("PRESSURE_TO_TEMP");
            results.appendChild(row);

            while (pressure_to_temp_rs.next()) {
                Element child = doc.createElement("CHILD");

                //				logger.info("pressure_to_temp_rs child");
                for (int i = 1; i <= colCount; i++) {
                    Element node = doc.createElement(rsmd.getColumnName(i));

                    if (pressure_to_temp_rs.getObject(i) != null) {
                        node.appendChild(doc.createTextNode(pressure_to_temp_rs.getObject(i).toString()));
                    } else {
                        node.appendChild(doc.createTextNode(""));
                    }

                    child.appendChild(node);
                }
                row.appendChild(child);
            }
            results.appendChild(row);

            //			logger.info("pressure_to_temp_rs Executed");
        }

        if (temp_to_pressure_rs != null) {
            //			logger.info("temp_to_pressure_rs Not null");

            ResultSetMetaData rsmd = temp_to_pressure_rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            Element row = doc.createElement("TEMP_TO_PRESSURE");
            results.appendChild(row);

            while (temp_to_pressure_rs.next()) {
                Element child = doc.createElement("CHILD");

                //				logger.info("temp_to_pressure_rs child");
                for (int i = 1; i <= colCount; i++) {
                    Element node = doc.createElement(rsmd.getColumnName(i));

                    if (temp_to_pressure_rs.getObject(i) != null) {
                        node.appendChild(doc.createTextNode(temp_to_pressure_rs.getObject(i).toString()));
                    } else {
                        node.appendChild(doc.createTextNode(""));
                    }

                    child.appendChild(node);
                }
                row.appendChild(child);
            }
            results.appendChild(row);

            //			logger.info("temp_to_pressure_rs Executed");
        }

        doc.appendChild(results);
        //		logger.info("completed fully");
        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);

        return sw.toString();

    }

    @SuppressWarnings("unchecked")
    public static String getJSON(ResultSet refrigerant_detail_rs,
            ResultSet pressure_to_temp_rs, ResultSet temp_to_pressure_rs) throws SQLException {
        // TODO Auto-generated method stub
        //		logger.info("PTCalculatorDao getJSON method invoked");
        JSONObject rootNode = new JSONObject();
        JSONObject resultNode = new JSONObject();

        if (refrigerant_detail_rs != null) {
            //			logger.info("refrigerant_detail_rs Not null");
            ResultSetMetaData rsmd = refrigerant_detail_rs.getMetaData();
            int colCount = rsmd.getColumnCount();
            JSONObject refrigerantNode = new JSONObject();

            JSONArray child = new JSONArray();
            while (refrigerant_detail_rs.next()) {
                JSONObject node = new JSONObject();

                for (int i = 1; i <= colCount; i++) {

                    if (refrigerant_detail_rs.getObject(i) != null) {
                        node.put(rsmd.getColumnName(i), refrigerant_detail_rs.getObject(i).toString());
                    } else {
                        node.put(rsmd.getColumnName(i), "");
                    }

                }
                child.add(node);

            }
            refrigerantNode.put("CHILD", child);
            resultNode.put("REFRIGERANT_DETAILS", refrigerantNode);

            //			logger.info("refrigerant_detail_rs Executed");
        }

        if (pressure_to_temp_rs != null) {
            //			logger.info("pressure_to_temp_rs Not null");
            ResultSetMetaData rsmd = pressure_to_temp_rs.getMetaData();
            int colCount = rsmd.getColumnCount();
            JSONObject pressureToTempNode = new JSONObject();

            JSONArray child = new JSONArray();
            while (pressure_to_temp_rs.next()) {
                JSONObject node = new JSONObject();

                for (int i = 1; i <= colCount; i++) {

                    if (pressure_to_temp_rs.getObject(i) != null) {
                        node.put(rsmd.getColumnName(i), pressure_to_temp_rs.getObject(i).toString());
                    } else {
                        node.put(rsmd.getColumnName(i), "");
                    }

                }
                child.add(node);

            }
            pressureToTempNode.put("CHILD", child);
            resultNode.put("PRESSURE_TO_TEMP", pressureToTempNode);

            //			logger.info("pressure_to_temp_rs Executed");
        }

        if (temp_to_pressure_rs != null) {
            //			logger.info("temp_to_pressure_rs Not null");
            ResultSetMetaData rsmd = temp_to_pressure_rs.getMetaData();
            int colCount = rsmd.getColumnCount();
            JSONObject tempToPressureNode = new JSONObject();

            JSONArray child = new JSONArray();
            while (temp_to_pressure_rs.next()) {
                JSONObject node = new JSONObject();

                for (int i = 1; i <= colCount; i++) {

                    if (temp_to_pressure_rs.getObject(i) != null) {
                        node.put(rsmd.getColumnName(i), temp_to_pressure_rs.getObject(i).toString());
                    } else {
                        node.put(rsmd.getColumnName(i), "");
                    }

                }
                child.add(node);

            }
            tempToPressureNode.put("CHILD", child);
            resultNode.put("TEMP_TO_PRESSURE", tempToPressureNode);

            //			logger.info("temp_to_pressure_rs Executed");
        }
        rootNode.put("RESULT", resultNode);
        //		logger.info("Fully completed");
        return rootNode.toJSONString();
    }

}
