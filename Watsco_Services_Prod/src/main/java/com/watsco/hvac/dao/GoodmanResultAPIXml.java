package com.watsco.hvac.dao;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.watsco.hvac.common.AppConstants;

public class GoodmanResultAPIXml {

    private static Logger logger = Logger.getLogger(GoodmanResultAPIXml.class);

    private static final String ROOT_ELEMENT = "RESULT";
    private static final String STATUS = "status";

    public static String getWarrantyTagValue(final String startTag,
            final String endTag, String pageSource) {
        Pattern htmltag = Pattern.compile(startTag + "(.*?)"
                + endTag);
        Matcher tagmatch = htmltag.matcher(pageSource.toString());

        StringBuffer sb = new StringBuffer();

        int i = 0;
        while (tagmatch.find()) {
            String status = tagmatch.group()
                    .replace(startTag, "")
                    .replace(endTag, "");

            if (i == 0) {
                sb.append(status);
            } else {
                sb.append("~~~" + status);
            }

            i++;

        }

        return sb.toString();
    }

    public static String getGoodmanResultXML(String model, String modelDesc, String postResponse, String status)
            throws ParserConfigurationException, TransformerException {
        //		logger.info("=====>GoodmanResultAPIXml getGoodmanResultXML method invoked");
        //		logger.info("2nd API response : "+ postResponse);
        try {
            
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            Element root = doc.createElement(ROOT_ELEMENT);
            if (status.equalsIgnoreCase("success")) {
                //			logger.info("success ");
                root.setAttribute(STATUS, "Success");

                String installDate = getWarrantyTagValue("<InstallDate>", "</InstallDate>", postResponse);
                String registerDate = getWarrantyTagValue("<RegisterDate>", "</RegisterDate>", postResponse);

                try {
                    //				logger.info("Install Date : "+ installDate);
                    //				logger.info("registerDate Date : "+ registerDate);
                    if (installDate.contains("T")) {
                        installDate = installDate.substring(0, installDate.indexOf('T'));
                    } else {
                        installDate = "";
                    }

                    if (registerDate.contains("T")) {
                        registerDate = registerDate.substring(0, registerDate.indexOf('T'));
                    } else {
                        registerDate = "";
                    }
                } catch (Exception e) {
                    logger.error("Exception : " + e);
                }

                Element warrantyInformation = doc.createElement("WARRANTYINFORMATION");
                Element warrantyData = doc.createElement("WARRANTYDATA");

                warrantyInformation.setAttribute("InstallDate", installDate);
                warrantyInformation.setAttribute("RegDate", registerDate);
                warrantyInformation.setAttribute("Model", model);
                warrantyInformation.setAttribute("ModelDesc", modelDesc);

                root.appendChild(warrantyInformation);
                //			logger.info("Warranty info set");
                if (postResponse.contains("StandardDesktop")) {
                    //				logger.info("contains standard desktop");
                    String standardData = getWarrantyTagValue("<StandardDesktop>", "</StandardDesktop>", postResponse);

                    if (standardData.contains("table")) {
                        //					logger.info("StandardDesktop contains table");
                        Element standard = doc.createElement("TABLE");
                        if (standardData.contains("<th colspan=\"4\">")) {
                            //						logger.info("contains <th colspan=\"4\">");
                            constructTable(standardData, standard, doc);

                        } else if (standardData.contains("<th>")) {
                            //						logger.info("contains <th>");
                            String tableTitle = GoodmanResultAPIXml.getWarrantyTagValue("<th>", "</th>", standardData);
                            standard.setAttribute("TITLE", tableTitle);
                            Element values = doc.createElement("VALUES");
                            String tableContent = GoodmanResultAPIXml.getWarrantyTagValue("<tbody><tr><td>", "</td></tr></tbody>", standardData);
                            values.setAttribute("NORESULT", tableContent);
                            standard.appendChild(values);
                        }
                        warrantyData.appendChild(standard);
                    }

                }
                if (postResponse.contains("RegisteredDesktop")) {
                    String registeredData = getWarrantyTagValue("<RegisteredDesktop>", "</RegisteredDesktop>", postResponse);
                    if (registeredData.contains("table")) {
                        //					logger.info("RegisteredDesktop contains table");
                        Element registered = doc.createElement("TABLE");
                        if (registeredData.contains("<th colspan=\"4\">")) {
                            //						logger.info("contains colspan");
                            constructTable(registeredData, registered, doc);

                        } else if (registeredData.contains("<th>")) {

                            String tableTitle = GoodmanResultAPIXml.getWarrantyTagValue("<th>", "</th>", registeredData);
                            registered.setAttribute("TITLE", tableTitle);
                            Element values = doc.createElement("VALUES");

                            String tableContent = GoodmanResultAPIXml.getWarrantyTagValue("<tbody><tr><td>", "</td></tr></tbody>", registeredData);
                            values.setAttribute("NORESULT", tableContent);
                            registered.appendChild(values);
                        }
                        warrantyData.appendChild(registered);
                    }
                }
                if (postResponse.contains("ExtendedDesktop")) {
                    String extendedData = getWarrantyTagValue("<ExtendedDesktop>", "</ExtendedDesktop>", postResponse);

                    if (extendedData.contains("table")) {

                        Element extended = doc.createElement("TABLE");
                        if (extendedData.contains("<th colspan=\"4\">")) {
                            //						logger.info("colspan");
                            constructTable(extendedData, extended, doc);

                        } else if (extendedData.contains("<th>")) {

                            String tableTitle = GoodmanResultAPIXml.getWarrantyTagValue("<th>", "</th>", extendedData);
                            extended.setAttribute("TITLE", tableTitle);
                            Element values = doc.createElement("VALUES");
                            String tableContent = GoodmanResultAPIXml.getWarrantyTagValue("<tbody><tr><td>", "</td></tr></tbody>", extendedData);
                            values.setAttribute("NORESULT", tableContent);
                            extended.appendChild(values);
                        }
                        warrantyData.appendChild(extended);
                    }

                }
                root.appendChild(warrantyData);

            } else {
                root.setAttribute(STATUS, "Error");
                //			logger.info("====> else block---Invalid");
            }

            doc.appendChild(root);
            //		logger.info("warranty appended to doc");

            DOMSource domSource = new DOMSource(doc);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
            transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
            transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
            StringWriter sw = new StringWriter();
            StreamResult sr = new StreamResult(sw);
            transformer.transform(domSource, sr);

            //		logger.info(sw.toString());
            return sw.toString();
        } catch (Exception e) {
            logger.error("Exception : " + e);
            return null;
        }

    }

    public static void constructTable(String data, Element tableElement, Document doc) {
        //        logger.info("constructTable method invoked; data : "+data);
        Pattern htmltag = Pattern.compile("<th colspan=\"4\">" + "(.*?)" + "</th>");//to get table title
        Matcher tagmatch = htmltag.matcher(data);
        String tableTitle = "";
        while (tagmatch.find()) {
            tableTitle = tagmatch.group()
                    .replace("<th colspan=\"4\">", "")
                    .replace("</th>", "");
        }
        //		logger.info("table title: "+ tableTitle);
        tableElement.setAttribute("TITLE", tableTitle);

        String tableHeadingList = GoodmanResultAPIXml.getWarrantyTagValue("<th>", "</th>", data);
        String[] tableHeadingArr = tableHeadingList.split("~~~"); // contains table heading in an array 

        String tbodyData = GoodmanResultAPIXml.getWarrantyTagValue("<tbody>", "</tbody>", data);

        String trValuesList = getWarrantyTagValue("<tr>", "</tr>", tbodyData); // tr block in tbody
        String[] trValuesArr = trValuesList.split("~~~");

        Element[] values = new Element[trValuesArr.length];//  = doc.createElement("VALUES");

        for (int j = 0; j < trValuesArr.length; j++) {

            values[j] = doc.createElement("VALUES");
            htmltag = Pattern.compile("<td>" + "(.*?)" + "</td>");
            tagmatch = htmltag.matcher(trValuesArr[j]);

            int i = 0;
            while (tagmatch.find()) {
                String tempString = tagmatch.group()
                        .replace("<td>", "")
                        .replace("</td>", "");

                HashMap<Object, Object> a = AppConstants.getValues();
                if (a.containsKey(tableHeadingArr[i].substring(0, 4))) {
                    values[j].setAttribute(a.get(tableHeadingArr[i].substring(0, 4)).toString(), tempString);
                }

                i++;
            }
            tableElement.appendChild(values[j]);

        }

    }

}
