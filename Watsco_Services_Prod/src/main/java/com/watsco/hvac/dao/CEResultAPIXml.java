package com.watsco.hvac.dao;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.CommonMethods;
import com.watsco.hvac.usagetracking.UsageTracking;

public class CEResultAPIXml {

    private static Logger logger = Logger.getLogger(CEResultAPIXml.class);

    private static final String ROOT_ELEMENT_START = "<RESULT>";
    private static final String ROOT_ELEMENT_END = "</RESULT>";

    public static String getDataKeyFormatted(String startTag, String endTag, String pageSource) {
        Pattern htmltag = Pattern.compile(startTag + "(.*?)"
                + endTag);
        Matcher tagmatch = htmltag.matcher(pageSource.toString());

        ArrayList<String> dataKeyArray = new ArrayList<String>();
        while (tagmatch.find()) {
            String value = tagmatch.group().replace(startTag, "").replace(endTag, "");
            value = value.replaceAll("([a-z])([A-Z])", "$1 $2");
            dataKeyArray.add(value);
        }
        for (String str : dataKeyArray) {
            //			logger.info(str);
            pageSource = pageSource.replace(startTag + str.replaceAll("\\s", "") + endTag,
                    startTag + str.substring(0, 1).toUpperCase() + str.substring(1) + endTag);
        }

        return pageSource;
    }

    public static String getCEResultXML(String entitlementApiResponse, String serviceHistoryApiResponse, String serialNumber, String modelNumber,
            int installId, int userId, String udid, int reportId, String buName, String type, boolean enableMask) {
        //		logger.info("CEResultAPIXml getCEResultXML method invoked");
        String response = "";

        String status = "";
        String errorString;
        entitlementApiResponse = getDataKeyFormatted("<dataKey>", "</dataKey>", entitlementApiResponse);
        serviceHistoryApiResponse = getDataKeyFormatted("<dataKey>", "</dataKey>", serviceHistoryApiResponse);

        entitlementApiResponse = getDataKeyFormatted("<extDataKey>", "</extDataKey>", entitlementApiResponse);
        serviceHistoryApiResponse = getDataKeyFormatted("<extDataKey>", "</extDataKey>", serviceHistoryApiResponse);

        if (entitlementApiResponse.contains("<html>") || serviceHistoryApiResponse.contains("<html>")) {
            response = ROOT_ELEMENT_START + "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><errorMsgStatement>The server is temporarily unable to service your request due to maintenance downtime or capacity problems. Please try again later.</errorMsgStatement></soap:Body></soap:Envelope>" + ROOT_ELEMENT_END;
            return response;
        }
        if (entitlementApiResponse.contains("faultstring") || serviceHistoryApiResponse.contains("faultstring")) {
            errorString = CommonMethods.getWarrantyTagValue("<faultstring>", "</faultstring>", entitlementApiResponse);
            errorString = CommonMethods.getWarrantyTagValue("<faultstring>", "</faultstring>", serviceHistoryApiResponse);
            response = ROOT_ELEMENT_START + "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><errorMsgStatement>" + errorString + "</errorMsgStatement></soap:Body></soap:Envelope>" + ROOT_ELEMENT_END;
            return response;
        }

        if (!entitlementApiResponse.equalsIgnoreCase("API failed") && !serviceHistoryApiResponse.equalsIgnoreCase("API failed")) {
            if ((enableMask == true)) {

                entitlementApiResponse = maskName(entitlementApiResponse);
            }
            //Added by Chaitanya - 3 Nov 2014
            entitlementApiResponse = logicToTrimBrand(entitlementApiResponse);

            response = ROOT_ELEMENT_START + entitlementApiResponse + serviceHistoryApiResponse + ROOT_ELEMENT_END;

            if (!entitlementApiResponse.contains("<msgStatus>") && !serviceHistoryApiResponse.contains("<msgStatus>")) {
                status = "Success - EntitlementApi,ServiceHistoryApi";
            } else {
                String entitlementErrorText = CommonMethods.getWarrantyTagValue("<errorText>", "</errorText>", entitlementApiResponse);
                String serviceHistoryErrorText = CommonMethods.getWarrantyTagValue("<errorText>", "</errorText>", serviceHistoryApiResponse);

                if (!entitlementErrorText.isEmpty() && !serviceHistoryErrorText.isEmpty()) {
                    status += "Error - EntitlementApi: " + entitlementErrorText + "Error - ServiceHistoryApi: " + serviceHistoryErrorText;
                } else if (entitlementErrorText.isEmpty() && !serviceHistoryErrorText.isEmpty()) {
                    status += "Success - EntitlementApi; Error - ServiceHistoryApi: " + serviceHistoryErrorText;
                } else if (!entitlementErrorText.isEmpty() && serviceHistoryErrorText.isEmpty()) {
                    status += "Success - ServiceHistoryApi; Error - EntitlementApi: " + entitlementErrorText;
                }
            }

        } else if (!entitlementApiResponse.equalsIgnoreCase("API failed") && serviceHistoryApiResponse.equalsIgnoreCase("API failed")) {

            if ((enableMask == true)) {
                entitlementApiResponse = maskName(entitlementApiResponse);
            }
//        	if(oldCalling.equals("true")){
//        	    //	    	if(buName.equalsIgnoreCase("CARRIER ENTERPRISE"))
//	            entitlementApiResponse = maskName(entitlementApiResponse);
//        	}
            //Added by Chaitanya - 3 Nov 2014
            entitlementApiResponse = logicToTrimBrand(entitlementApiResponse);

            response = ROOT_ELEMENT_START + entitlementApiResponse + ROOT_ELEMENT_END;

            if (entitlementApiResponse.contains("<msgStatus>")) {
                String errorText = CommonMethods.getWarrantyTagValue("<errorText>", "</errorText>", entitlementApiResponse);
                status = "EntitlementApi Error : " + errorText + "; Failure - ServiceHistoryApi";
            } else {
                status = "Success - EntitlementApi; Failure - ServiceHistoryApi";
            }

        } else if (entitlementApiResponse.equalsIgnoreCase("API failed") && !serviceHistoryApiResponse.equalsIgnoreCase("API failed")) {

            response = ROOT_ELEMENT_START + serviceHistoryApiResponse + ROOT_ELEMENT_END;

            if (serviceHistoryApiResponse.contains("<msgStatus>")) {
                String errorText = CommonMethods.getWarrantyTagValue("<errorText>", "</errorText>", serviceHistoryApiResponse);
                status = "ServiceHistoryApi Error : " + errorText + "; Failure - EntitlementApi";
            } else {
                status = "Failure - EntitlementApi; Success - ServiceHistoryApi";
            }

        } else {

            response = ROOT_ELEMENT_START + "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body<errorMsgStatement>API failed</errorMsgStatement></soap:Body></soap:Envelope>" + ROOT_ELEMENT_END;

            status = "Failure - EntitlementApi,ServiceHistoryApi";

        }

        String searchedData = status + "; Serial Number : " + serialNumber;
        if (!modelNumber.isEmpty()) {
            searchedData += "; ModelNumber : " + modelNumber;
        }

        logger.info("====" + userId + "==Entitlement Request Status : " + searchedData);
        //usage tracking
        UsageTracking usageTracking = new UsageTracking();
        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");

//        		logger.info(response);
        return response;

    }

    /**
     * Methos to remove Brand from Entitlement response
     *
     * @param entitlementApiResponse
     * @return
     */
    private static String logicToTrimBrand(String entitlementApiResponse) {
        String tagStart = "<detail><dataKey>Brand</dataKey><dataDescription>";
        String tagEnd = "</dataDescription></detail>";

        StringBuffer sb = new StringBuffer(entitlementApiResponse);
        int strtIndex = 0;

        while ((strtIndex = sb.indexOf(tagStart, strtIndex)) != -1) {
            int startIndexToReplce = strtIndex;
            int endIndexToReplce = sb.indexOf(tagEnd, strtIndex) + tagEnd.length();

            sb.replace(startIndexToReplce, endIndexToReplce, "");
        }

        return entitlementApiResponse = sb.toString();
    }

    //	public static String maskName(String response){
    //		  logger.info("CEResultAPIXml maskName method invoked");
    //			StringBuffer sb = new StringBuffer(response);
    //			int strtIndex = 0;
    //			String patterStart = "<dataKey>Owner</dataKey><dataDescription>";
    //			String patterEnd = "</dataDescription>";
    //			while ((strtIndex = sb.indexOf(patterStart, strtIndex)) != -1)
    //			{
    //				int firstNameMaskStart =strtIndex+ patterStart.length() + 2;
    //				int commaIndex = sb.indexOf(",", firstNameMaskStart);
    //				int firstNameMaskEnd = commaIndex-1;
    //				int secondNameMaskStart =commaIndex+3;
    //				int secondNameMaskEnd =sb.indexOf(patterEnd, secondNameMaskStart);
    //
    //				if(firstNameMaskStart < firstNameMaskEnd)
    //					sb.replace(firstNameMaskStart, firstNameMaskEnd, replacePatern(firstNameMaskEnd-firstNameMaskStart));
    //
    //				if(secondNameMaskStart < secondNameMaskEnd)
    //					sb.replace(secondNameMaskStart, secondNameMaskEnd, replacePatern(secondNameMaskEnd-secondNameMaskStart));
    //
    //				strtIndex++;
    //
    //			}
    //
    //		return sb.toString();
    //
    //	}
    public static String maskName(String response) {
        //		  logger.info("CEResultAPIXml maskName method invoked");
        String specialChars = "(?=[]\\[+&|!($;){}^\"~*?:\\\\])";

        if (response.contains("&amp;")) {
            response = response.replaceAll("&amp; ", "");
        }

        StringBuffer sb = new StringBuffer(response);
        int strtIndex = 0;
        String patter = "<dataKey>Owner</dataKey><dataDescription>";
        String patterEnd = "</dataDescription>";
        String desc;
        String str = null;
        String specialCharPos = "";
        int posCount = 0;

        while ((strtIndex = sb.indexOf(patter, strtIndex)) != -1) {

//        	if(sb.charAt(strtIndex) ==)
            desc = sb.substring(strtIndex + patter.length(), sb.indexOf(patterEnd, strtIndex));

//            specialCharPos = Character.toString(desc.charAt(posCount));
//            if(containsChars(specialCharPos)){
//            	desc = sb.substring(strtIndex + patter.length()+posCount, sb.indexOf(patterEnd, strtIndex));
//            }
            if (desc.length() > 0) {
//            	desc = sb.substring(strtIndex + patter.length()+posCount, sb.indexOf(patterEnd, strtIndex));

//
                if (desc.contains(" ") && !desc.contains(",")) {
                    int srtPosToReplace = sb.indexOf(patter) + patter.length();
                    int stopPatternIndex = sb.indexOf(patterEnd, srtPosToReplace);  //modified  20052016
//                     int stopPatternIndex = sb.indexOf(patterEnd, srtPosToReplace+1);  
                    int spaceStrtIndex = srtPosToReplace;
                    boolean c = false;
                    while ((spaceStrtIndex = sb.indexOf(" ", spaceStrtIndex)) != -1 && spaceStrtIndex <= stopPatternIndex) {
                        srtPosToReplace = srtPosToReplace + 2;
                        if (!c) {
                            if (sb.subSequence(srtPosToReplace - 2, spaceStrtIndex).length() > 1) {

                                char charwe = sb.charAt(spaceStrtIndex - 1);
                                if (srtPosToReplace != spaceStrtIndex) {
                                    sb.replace(srtPosToReplace, spaceStrtIndex - 1, replacePatern(spaceStrtIndex - srtPosToReplace));
                                } else {
                                    sb.replace(srtPosToReplace, spaceStrtIndex, replacePatern(spaceStrtIndex - srtPosToReplace));
                                }

                                //   sb.replace(srtPosToReplace, spaceStrtIndex - 1, replacePatern(spaceStrtIndex - srtPosToReplace)); modified  20052016
                            }
                            c = true;
                        } else if (sb.subSequence(srtPosToReplace - 2, spaceStrtIndex).length() > 1) {
                            sb.replace(srtPosToReplace, spaceStrtIndex, replacePatern(spaceStrtIndex - srtPosToReplace));
                        }
                        srtPosToReplace = spaceStrtIndex + 1;
                        spaceStrtIndex++;
                    }

                    if (sb.substring(srtPosToReplace, stopPatternIndex).length() > 1) {
                        sb.replace(srtPosToReplace + 2, stopPatternIndex, replacePatern(stopPatternIndex - (srtPosToReplace + 2)));
                    }
                    break;
                    //logicToAddMaskingToWordWith2Characters
                } else if (!desc.contains(",")) {

                    /* int srtPosToReplace = sb.indexOf(patter) + patter.length();

                    int stopIndex = srtPosToReplace + (desc.length());
                    while (desc.length()!=-1) {
                        sb.replace(srtPosToReplace + 2, stopIndex, "*");
                    }
//                    break;*/
                    int firstNameMaskStart = sb.indexOf(patter) + patter.length();
                    System.out.println("" + sb.charAt(firstNameMaskStart));
                    int stopIndex = firstNameMaskStart + desc.trim().length() - 2;
                    System.out.println("stopIndex : " + stopIndex);

                    for (int i = firstNameMaskStart + 2; i <= stopIndex; i++) {
                        sb.replace(i, i + 1, "*");
                    }
                    break;
                } else {
                    int firstNameMaskStart = strtIndex + patter.length() + 2;
                    int commaIndex = sb.indexOf(",", firstNameMaskStart);
                    int countAfterComma = sb.indexOf("</dataDescription>", commaIndex) - commaIndex;

                    //******************
                    int spaceStartIndex = strtIndex;
                    int secondNameMaskStart = 0;

                    //int firstNameMaskEnd = commaIndex-1;
                    if (countAfterComma > 2) {
                        secondNameMaskStart = commaIndex + 3;
                    } else {
                        secondNameMaskStart = commaIndex + 2;
                    }
                    int secondNameMaskEnd = sb.indexOf(patterEnd, secondNameMaskStart);
                    boolean isFirstWord = false;
                    boolean isSingeWord = true;

                    while ((spaceStartIndex = sb.indexOf(" ", spaceStartIndex)) != -1 && spaceStartIndex < commaIndex) {
                        if (!isFirstWord) {
                            isFirstWord = true;
                        }

                        if (firstNameMaskStart < spaceStartIndex) {
                            if (isFirstWord) {
                                sb.replace(firstNameMaskStart, spaceStartIndex - 1, replacePatern((spaceStartIndex - 1) - firstNameMaskStart));
                            } else {
                                sb.replace(firstNameMaskStart, spaceStartIndex, replacePatern(spaceStartIndex - firstNameMaskStart));
                            }
                        }
                        spaceStartIndex++;
                        firstNameMaskStart = spaceStartIndex;
                        isSingeWord = false;
                    }

                    if (sb.substring(firstNameMaskStart, commaIndex).length() > 1) {
                        if (isSingeWord) {
                            sb.replace(firstNameMaskStart, commaIndex - 1, replacePatern(commaIndex - 1 - (firstNameMaskStart)));
                        } else if (isFirstWord) {
                            sb.replace(firstNameMaskStart + 2, commaIndex, replacePatern(commaIndex - (firstNameMaskStart + 2)));
                        } else {
                            sb.replace(firstNameMaskStart, commaIndex, replacePatern(commaIndex - (firstNameMaskStart)));
                        }
                    }

                    isFirstWord = false;

                    while ((spaceStartIndex = sb.indexOf(" ", spaceStartIndex)) != -1 && spaceStartIndex < secondNameMaskEnd) {
                        if (!isFirstWord) {
                            isFirstWord = true;
                        }
                        // 706,758,(758-706)

                        if (secondNameMaskStart < spaceStartIndex) {
                            sb.replace(secondNameMaskStart, spaceStartIndex, replacePatern(spaceStartIndex - secondNameMaskStart));
                        }
                        spaceStartIndex++;
                        secondNameMaskStart = spaceStartIndex;
                    }

                    if (sb.substring(secondNameMaskStart, secondNameMaskEnd).length() > 0) {
                        if (isFirstWord) {
                            sb.replace(secondNameMaskStart + 2, secondNameMaskEnd, replacePatern(secondNameMaskEnd - (secondNameMaskStart + 2)));
                        } else {
                            sb.replace(secondNameMaskStart, secondNameMaskEnd, replacePatern(secondNameMaskEnd - (secondNameMaskStart)));
                        }

                    }

                }

            }

            strtIndex++;
            posCount++;
        }
        if ((strtIndex = sb.indexOf(patter, strtIndex)) != -1) {
            logger.info("before : " + sb.toString());
            String newDesc = "";
            str = sb.toString().replaceAll("\\*", ".");
            String patter1 = "<dataKey>Owner</dataKey><dataDescription>";
            String patterEnd1 = "</dataDescription>";
            desc = str.substring(str.indexOf(patter1) + patter1.length(), str.indexOf(patterEnd1, str.indexOf(patter1)));
            if (desc.contains("")) {
                newDesc = desc.replaceAll("[</<>]", "");
            }
            str = str.replaceAll(desc, newDesc);
        } else {
            str = sb.toString().replaceAll("\\*", ".");
        }

        return str;

    }

    public static String replacePatern(int length) {
        //		logger.info("CEResultAPIXml replacePatern method invoked");
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= length; i++) {
            sb.append("*");
        }

        return sb.toString();

    }

    public static boolean containsChars(String str) {
        /*for (int i = 0; i < specialChars.length(); i++) {
         char c = specialChars.charAt(i);
         for (int j = 0; j < str.length(); j++) {
         if (c == str.charAt(j)) {
         return true;
         }
         }
         }*/

        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(str);
        boolean b = m.find();

        if (b) {
            return true;
        }

        return false;
    }

}
