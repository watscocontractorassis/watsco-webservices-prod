package com.watsco.hvac.dao;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.utils.StringUtils;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.simple.JSONArray;

public class WarrantyDocumentConvertor {

    protected static Logger logger = Logger.getLogger(WarrantyDocumentConvertor.class);

    public String toJSONDocument(ResultSet userInfoRS) {
        JSONObject jObject = null;
        JSONArray jArray = new JSONArray();
        try {
            ResultSetMetaData rsmd = userInfoRS.getMetaData();
            int colCount = rsmd.getColumnCount();

            if (userInfoRS != null) {
                while (userInfoRS.next()) {
                    jObject = new JSONObject();
                    for (int i = 1; i <= colCount; i++) {

                        Object value = userInfoRS.getObject(i);

                        StringUtils utility = new StringUtils();
                        if (value != null && utility.isJSONValid(value.toString())) {
                            Object json = new JSONTokener(value.toString()).nextValue();

                            if (userInfoRS.getObject(i) != null) {
                                System.out.println("value :" + value);
                                if (json instanceof JSONObject) {
//                            if (userInfoRS.getObject(i) != null) {
                                    jObject.put(rsmd.getColumnName(i), new JSONObject(value.toString()));
                                } else if (json instanceof JSONArray) {
                                    jObject.put(rsmd.getColumnName(i), new org.json.JSONArray(value.toString()));
                                }
                            } else {
                                jObject.put(rsmd.getColumnName(i), "");
                            }
                        } else if (userInfoRS.getObject(i) != null) {
                            jObject.put(rsmd.getColumnName(i), value);
                        } else {
                            jObject.put(rsmd.getColumnName(i), "");
                        }
                    }
                }
            }
            return jObject.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Exception----->" + e);
            return null;
        }
//        JSONObject jObj = new JSONObject();
//        jObj.put("Status", "No Data Found");
//        return jObj.toString();
    }

    public String toJSONConvertor(ResultSet rs) {
        JSONArray jArray = new JSONArray();
        JSONObject jObject = new JSONObject();

        try {

            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            if (rs != null) {
                while (rs.next()) {

                    for (int i = 1; i <= colCount; i++) {
                        jArray.add(rs.getObject(i).toString());
                    }

                    jObject.put("claimNumber", jArray);

                }
            }

            return jObject.toString();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;

    }

    public String JSONConvertor(ResultSet rs) {
        JSONObject jObject = null;
        JSONArray jArray = new JSONArray();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            if (rs != null) {
                while (rs.next()) {
                    jObject = new JSONObject();
                    for (int i = 1; i <= colCount; i++) {

                        if (rs.getObject(i) != null) {
                            jObject.put(rsmd.getColumnName(i), rs.getObject(i).toString());
                        } else {
                            jObject.put(rsmd.getColumnName(i), "");
                        }
                    }
                    jArray.add(jObject);
                }
            }
            return jArray.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Exception----->" + e);
        }

        return "No Data Found";
    }

    public static String RStoJSONConvertor(ResultSet rs1, ResultSet rs2) {
        JSONArray rootArray = new JSONArray();
        JSONObject rootObject = null;
        JSONArray jArray = null;
        try {

            if (rs1 != null) {
                jArray = new JSONArray();
                while (rs1.next()) {
                    ResultSetMetaData rsmd1 = rs1.getMetaData();
                    int colCount1 = rsmd1.getColumnCount();
                    rootObject = new JSONObject();
                    String warrantyClaim = rs1.getString("Claim_WID");

                    for (int i = 1; i <= colCount1; i++) {
                        try {
                            if (rs1.getObject(i) != null) {
                                rootObject.put(rsmd1.getColumnName(i), rs1.getObject(i).toString());
                            } else {
                                rootObject.put(rsmd1.getColumnName(i), "");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    if (rs2 != null) {
                        while (rs2.next()) {
                            String warrantyClaimParts = rs2.getString("Claim_WID");
                            if (warrantyClaimParts.equalsIgnoreCase(warrantyClaim)) {
                                ResultSetMetaData rsmd2 = rs2.getMetaData();
                                int colCount2 = rsmd2.getColumnCount();
                                JSONObject obj = new JSONObject();

                                for (int i = 1; i <= colCount2; i++) {
                                    try {
                                        if (rs2.getObject(i) != null) {
                                            obj.put(rsmd2.getColumnName(i), rs2.getObject(i).toString());
                                        } else {
                                            obj.put(rsmd2.getColumnName(i), "");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                                jArray.add(obj);
                            }

                        }
                        rootObject.put("claimParts", jArray);
                    }
                    rootArray.add(rootObject);
                }
            }

        } catch (Exception e) {

        }

        return rootArray.toString();

    }

    public static String RSStoVRJSONConvertor(ResultSet rs1, ResultSet rs2) {

        try {
            JSONArray rootArray = new JSONArray();
            JSONObject rootObject = null;
            JSONArray jArray = null;

            if (rs1 != null) {
                jArray = new JSONArray();
                while (rs1.next()) {
                    ResultSetMetaData rsmd = rs1.getMetaData();

                    String warrantyClaim = rs1.getString("Claim_WID");

                    int colCount = rsmd.getColumnCount();
                    rootObject = new JSONObject();
                    for (int i = 1; i <= colCount; i++) {

                        if (rs1.getObject(i) != null) {
                            rootObject.put(rsmd.getColumnName(i), rs1.getObject(i).toString());
                        } else {
                            rootObject.put(rsmd.getColumnName(i), "");
                        }
                        //Store ClaimWid Locally for each iteration
                    }
                    rootArray.add(rootObject);

                    if (rs2 != null) {

                        jArray = new JSONArray();
                        while (rs2.next()) {
                            String warrantyClaimParts = rs2.getString("Claim_WID");
                            if (warrantyClaimParts.equalsIgnoreCase(warrantyClaim)) {

                                ResultSetMetaData rsmd1 = rs2.getMetaData();
                                int colCount2 = rsmd1.getColumnCount();
                                JSONObject obj = new JSONObject();
                                for (int i = 1; i <= colCount2; i++) {

                                    if (rs2.getObject(i) != null) {
                                        obj.put(rsmd1.getColumnName(i), rs2.getObject(i).toString());
                                    } else {
                                        obj.put(rsmd1.getColumnName(i), "");
                                    }

                                }
                                jArray.add(obj);
                            }
//				     
                        }

                        rootObject.put("claimParts", jArray);

                    }
                }
                return rootArray.toString();
            } else {
                return "No Claim Found";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static String RSStoJSONConvertor(ResultSet rs1, ResultSet rs2) {
        Map<String, String> newMap = new HashMap<String, String>();
        try {

            JSONArray rootArray = new JSONArray();
            JSONObject rootObject = null;
            JSONArray jArray = null;
            String value = "";
            String key;
            if (rs1 != null) {
//					
                while (rs1.next()) {

                    jArray = new JSONArray();

                    ResultSetMetaData rsmd = rs1.getMetaData();
                    int colCount = rsmd.getColumnCount();

                    rootObject = new JSONObject();
                    for (int i = 1; i <= colCount; i++) {
                        key = rsmd.getColumnName(i);

                        Object object = rs1.getObject(i);
                        try {
                            if (object != null) {
                                value = rs1.getObject(i).toString();
                                if (key.contains("date") || key.contains("Date")) {
                                    DateFormat df1 = new SimpleDateFormat("MM/dd/yy");
                                    DateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
                                    Date date = new Date();

                                    //	        	    			
                                    date = df3.parse(value);
                                    //	        	    			

                                    value = df1.format(date);

                                    rootObject.put(key, value);
                                } else {
                                    rootObject.put(key, value);
                                }
                            } else {
                                rootObject.put(key, "");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
//			            rootArray.add(rootObject);

                    if (rs2 != null) {

                        jArray = new JSONArray();
                        while (rs2.next()) {
                            ResultSetMetaData rsmd1 = rs2.getMetaData();
                            int colCount2 = rsmd1.getColumnCount();
                            JSONObject obj = new JSONObject();

                            for (int i = 1; i <= colCount2; i++) {

                                key = rsmd1.getColumnName(i);

                                Object object = rs2.getObject(i);

                                try {
                                    if (object != null) {
                                        value = rs2.getObject(i).toString();

                                        if (key.contains("date") || key.contains("Date")) {
                                            DateFormat df1 = new SimpleDateFormat("MM/dd/yy");
                                            DateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
                                            Date date = new Date();

//	        	    			
                                            date = df3.parse(value);
//	        	    			

                                            value = df1.format(date);

                                            obj.put(key, value);
                                        } else {
                                            obj.put(key, value);
                                        }
                                    } else {
                                        obj.put(rsmd1.getColumnName(i), "");
                                    }
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                            }
                            jArray.add(obj);
                        }
                    }

                    rootObject.put("claimParts", jArray);

//				            if(!rs2.next()){
//					        	rs2.beforeFirst();
//					        }
                    rootArray.add(rootObject);
                }

            }

//					rootArray.add(rootObject);
//		        rootArray.add(rootObject);
            return rootArray.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public String portalJsonConvertor(ResultSet rs1, ResultSet rs2) {

        try {
            JSONArray rootArray = new JSONArray();
            JSONObject rootObject = null;
            JSONArray jArray = null;

            if (rs1 != null) {
                jArray = new JSONArray();
                while (rs1.next()) {
                    ResultSetMetaData rsmd = rs1.getMetaData();

                    String warrantyClaim = rs1.getString("Claim_WID");

                    int colCount = rsmd.getColumnCount();
                    rootObject = new JSONObject();
                    for (int i = 1; i <= colCount; i++) {

                        if (rs1.getObject(i) != null) {
                            rootObject.put(rsmd.getColumnName(i), rs1.getObject(i).toString());
                        } else {
                            rootObject.put(rsmd.getColumnName(i), "");
                        }
                        //Store ClaimWid Locally for each iteration
                    }
//			            rootArray.add(rootObject);

                    if (rs2 != null) {

                        jArray = new JSONArray();
                        while (rs2.next()) {
                            String warrantyClaimParts = rs2.getString("Claim_WID");
                            if (warrantyClaimParts.equalsIgnoreCase(warrantyClaim)) {

                                ResultSetMetaData rsmd1 = rs2.getMetaData();
                                int colCount2 = rsmd1.getColumnCount();
                                JSONObject obj = new JSONObject();
                                for (int i = 1; i <= colCount2; i++) {

                                    if (rs2.getObject(i) != null) {
                                        obj.put(rsmd1.getColumnName(i), rs2.getObject(i).toString());
                                    } else {
                                        obj.put(rsmd1.getColumnName(i), "");
                                    }

                                }
                                jArray.add(obj);
                            }
//					     
                        }

                        rootObject.put("claimParts", jArray);

//				            if(!rs2.next()){
//					        	rs2.beforeFirst();
//					        }
                        rootArray.add(rootObject);
                    }
                }
                return rootArray.toString();
            } else {
                rootObject = new JSONObject();
                rootObject.put("Claim Status", "NO Data Found");
                rootArray.add(rootObject);
                return rootArray.toString();

            }
        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }

    }

    public String toManufacturersJSONConvertor(ResultSet rs) {
        JSONObject jObject = new JSONObject();
        JSONArray jArray = new JSONArray();
        String value = "";
        String key = "";
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();
            jObject.put("STATUS", "Success");
            if (rs != null) {
                while (rs.next()) {
                    key = rsmd.getColumnName(1);
                    value = rs.getObject(key).toString();
                    jArray.add(value);

                }
                jObject.put("Result", jArray);
            }

            return jObject.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Exception----->" + e);
        }

        return "No Data Found";
    }

    public String productDetailConvertor(ResultSet userInfoRS) {
        JSONObject jObject = null;
        JSONArray jArray = new JSONArray();
        JSONObject mergeJSONObjects = new JSONObject();
        JSONObject mergedJSON = new JSONObject();
        try {
            ResultSetMetaData rsmd = userInfoRS.getMetaData();
            int colCount = rsmd.getColumnCount();

            if (userInfoRS != null) {
                while (userInfoRS.next()) {
                    jObject = new JSONObject();
                    for (int i = 1; i <= colCount; i++) {

                        Object value = userInfoRS.getObject(i);

                        StringUtils utility = new StringUtils();
                        if (value != null && utility.isJSONValid(value.toString())) {
                            Object json = new JSONTokener(value.toString()).nextValue();

                            JSONObject convertJSON = new JSONObject(json.toString());
                            mergeJSONObjects = mergeJSONObjects(mergedJSON, convertJSON);

                        } else if (userInfoRS.getObject(i) != null) {
                            jObject.put(rsmd.getColumnName(i), value);
                        } else {
                            jObject.put(rsmd.getColumnName(i), "");
                        }
                    }
                }
                jObject.put("Specification", mergeJSONObjects);
            }
            return jObject.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("Exception----->" + e);
            return null;
        }
//        JSONObject jObj = new JSONObject();
//        jObj.put("Status", "No Data Found");
//        return jObj.toString();

    }

    public JSONObject mergeJSONObjects(JSONObject mergedJSON, JSONObject jsonData) {

        try {
//            mergedJSON = new JSONObject(json1, JSONObject.getNames(json1));
            if (jsonData != null) {
                for (String crunchifyKey : JSONObject.getNames(jsonData)) {
                    mergedJSON.put(crunchifyKey, jsonData.get(crunchifyKey));
                }
            }

        } catch (JSONException e) {
            throw new RuntimeException("JSON Exception" + e);
        }
        return mergedJSON;
    }

}
