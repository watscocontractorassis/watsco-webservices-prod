package com.watsco.hvac.dao;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.UriInfo;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import com.watsco.hvac.cache.CacheManager;
import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;

public class BillBoardResultXml {

    private static Logger logger = Logger.getLogger(BillBoardResultXml.class);
    private static Map<String, String> cache;

    public static String getXMLDocument(ResultSet rs, String buName, String device, String udid, String appVersion, String branchNumber, int userId, String language,
            UriInfo uriInfo) throws ParserConfigurationException, TransformerException, JSONException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();
        String ROOT_ELEMENT = "RESULT";

        String VALUE = "VALUE";
        Element results = doc.createElement(ROOT_ELEMENT);
        doc.appendChild(results);
        String response = null;
        String DocValue = "";

        BillBoardResultXml billBoard = new BillBoardResultXml();
        response = billBoard.getDocValues(buName, device, udid, appVersion, branchNumber, userId, language, uriInfo);

        HashMap<String, String> docMap = new HashMap<String, String>();

        if (!response.isEmpty()) {
            JSONArray configArray = new JSONArray(response);

            for (int i = 0; i < configArray.length(); i++) {

                JSONObject configObject = configArray.getJSONObject(i);

                String configKey = configObject.getString("ConfigKey");
                String configValue = configObject.getString("ConfigValue");
                docMap.put(configKey, configValue);
                Element docLink = doc.createElement(configKey + "_LINK");
                docLink.setAttribute(VALUE, configValue);
                results.appendChild(docLink);

            }
        }

        String shopLinkValue = "";
        String productLinkValue = "";
        String showBuyValue = "";
        String productCatalogValue = "";
        String storeLocatorCookieValue = "";
        String howItWorksVideoValue = "";
        String howItWorksImageValue = "";
        String showPAQtyValue = "";
        String linkYourAccLinkValue = "";
        String registerLinkValue = "";
        String ForgotPasswordLinkValue = "";
        String privacyPolicyLinkValue = "";

        String signInValue = "";
        String gaKeyValue = "";
        String showOrderValue = "";
        String disableWarrantyProcessingValue = "";
        String warrantyDisabledMessageValue = "";
        String showHiddenMenuValue = "";
        String menuIconValue = "";
        String menuTextValue = "";
        String superscriptTextValue = "";
        String launchURLValue = "";
        String pricingApiValue = "";
        String signInMasterValue = "";
        String supercedeInfoValue = "";
        String disableInventoryValue = "";
        String minVersionValue = "";
//        String maxVersionValue = "";

        HashMap<String, String> hm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            hm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            hm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }

        if (buName.equalsIgnoreCase("GEMAIRE")) {
            shopLinkValue = hm.get("GM_SHOP_LINK");
            productLinkValue = hm.get("GM_PRODUCT_LINK");
            showBuyValue = hm.get("GM_SHOW_BUY");
            productCatalogValue = hm.get("GM_PRODUCTCATALOG_LINK");
            storeLocatorCookieValue = hm.get("GM_STORELOCATORCOOKIE_LINK");
            if (language != null) {
                if (language.startsWith("en")) {
                    howItWorksVideoValue = hm.get("GM_VIDEO_EN");
                    howItWorksImageValue = hm.get("GM_IMAGE_EN");
                } else if (language.startsWith("ru")) {
                    howItWorksVideoValue = hm.get("GM_VIDEO_RU");
                    howItWorksImageValue = hm.get("GM_IMAGE_RU");
                } else if (language.startsWith("fr")) {
                    howItWorksVideoValue = hm.get("GM_VIDEO_FR");
                    howItWorksImageValue = hm.get("GM_IMAGE_FR");
                } else {
                    howItWorksVideoValue = hm.get("GM_VIDEO_ES");
                    howItWorksImageValue = hm.get("GM_IMAGE_ES");
                }
            }

            showPAQtyValue = hm.get("GM_SHOW_PA_QTY");
            linkYourAccLinkValue = hm.get("GM_LINK_YOUR_ACC_LINK");
            registerLinkValue = hm.get("GM_REGISTER_LINK");
            ForgotPasswordLinkValue = hm.get("GM_FORGOT_PASSWORD_LINK");
            privacyPolicyLinkValue = hm.get("GM_PRIVACY_POLICY_LINK");
            signInValue = hm.get("GM_SIGNIN");
            if (device.equalsIgnoreCase("IOS")) {
                gaKeyValue = hm.get("GM_GA_IOS");
            } else {
                gaKeyValue = hm.get("GM_GA_ANDROID");
            }
            showOrderValue = hm.get("GM_SHOW_ORDER");
            disableWarrantyProcessingValue = hm.get("GM_DISABLE_WARRANTY_PROCESSING");
            warrantyDisabledMessageValue = hm.get("GM_WARRANTY_DISABLED_MESSAGE");
            showHiddenMenuValue = hm.get("GM_SHOW_HIDDEN_MENU");
            menuIconValue = hm.get("GM_MENU_ICON");
            menuTextValue = hm.get("GM_MENU_TEXT");
            superscriptTextValue = hm.get("GM_SUPER_SCRIPT_TEXT");
            launchURLValue = hm.get("GM_LAUNCH_URL");
            pricingApiValue = hm.get("GM_PRICING_API");
            signInMasterValue = hm.get("GM_SIGNIN_MASTER");
            supercedeInfoValue = hm.get("GM_SUPERCEDE_INFO");
            disableInventoryValue = hm.get("GM_DISABLE_INVENTORY");

        } else if (buName.equalsIgnoreCase("CARRIER ENTERPRISE")) {

            shopLinkValue = hm.get("CE_SHOP_LINK");
            productLinkValue = hm.get("CE_PRODUCT_LINK");
            showBuyValue = hm.get("CE_SHOW_BUY");
            if (language != null) {
                if (language.startsWith("en")) {
                    howItWorksVideoValue = hm.get("CE_VIDEO_EN");
                    howItWorksImageValue = hm.get("CE_IMAGE_EN");
                } else if (language.startsWith("ru")) {
                    howItWorksVideoValue = hm.get("CE_VIDEO_RU");
                    howItWorksImageValue = hm.get("CE_IMAGE_RU");
                } else if (language.startsWith("fr")) {
                    howItWorksVideoValue = hm.get("CE_VIDEO_FR");
                    howItWorksImageValue = hm.get("CE_IMAGE_FR");
                } else {
                    howItWorksVideoValue = hm.get("CE_VIDEO_ES");
                    howItWorksImageValue = hm.get("CE_IMAGE_ES");
                }
            }
            showPAQtyValue = hm.get("CE_SHOW_PA_QTY");
            linkYourAccLinkValue = hm.get("CE_LINK_YOUR_ACC_LINK");
            registerLinkValue = hm.get("CE_REGISTER_LINK");
            ForgotPasswordLinkValue = hm.get("CE_FORGOT_PASSWORD_LINK");
            signInValue = hm.get("CE_SIGNIN");
            if (device.equalsIgnoreCase("IOS")) {
                gaKeyValue = hm.get("CE_GA_IOS");
                minVersionValue = hm.get("CE_WP_IOS_MIN");
//                maxVersionValue = hm.get("CE_WP_IOS_MAX");
            } else {
                gaKeyValue = hm.get("CE_GA_ANDROID");
                minVersionValue = hm.get("CE_WP_ANDROID_MIN");
//                maxVersionValue = hm.get("CE_WP_ANDROID_MAX");
            }

            showOrderValue = hm.get("CE_SHOW_ORDER");
            privacyPolicyLinkValue = hm.get("CE_PRIVACY_POLICY_LINK");
            disableWarrantyProcessingValue = hm.get("CE_DISABLE_WARRANTY_PROCESSING");
            warrantyDisabledMessageValue = hm.get("CE_WARRANTY_DISABLED_MESSAGE");
            showHiddenMenuValue = hm.get("CE_SHOW_HIDDEN_MENU");
            menuIconValue = hm.get("CE_MENU_ICON");
            menuTextValue = hm.get("CE_MENU_TEXT");
            superscriptTextValue = hm.get("CE_SUPER_SCRIPT_TEXT");
            launchURLValue = hm.get("CE_LAUNCH_URL");
            pricingApiValue = hm.get("CE_PRICING_API");
            signInMasterValue = hm.get("CE_SIGNIN_MASTER");
            supercedeInfoValue = hm.get("CE_SUPERCEDE_INFO");
            disableInventoryValue = hm.get("CE_DISABLE_INVENTORY");

        } else if (buName.equalsIgnoreCase("CARRIER ENTERPRISE MEXICO")) {

            shopLinkValue = hm.get("CE_MX_SHOP_LINK");
            productLinkValue = hm.get("CE_MX_PRODUCT_LINK");
            showBuyValue = hm.get("CE_MX_SHOW_BUY");
            if (language != null) {
                if (language.startsWith("en")) {
                    howItWorksVideoValue = hm.get("CE_MX_VIDEO_EN");
                    howItWorksImageValue = hm.get("CE_MX_IMAGE_EN");
                } else if (language.startsWith("ru")) {
                    howItWorksVideoValue = hm.get("CE_MX_VIDEO_RU");
                    howItWorksImageValue = hm.get("CE_MX_IMAGE_RU");
                } else if (language.startsWith("fr")) {
                    howItWorksVideoValue = hm.get("CE_MX_VIDEO_FR");
                    howItWorksImageValue = hm.get("CE_MX_IMAGE_FR");
                } else {
                    howItWorksVideoValue = hm.get("CE_MX_VIDEO_ES");
                    howItWorksImageValue = hm.get("CE_MX_IMAGE_ES");
                }
            }

            showPAQtyValue = hm.get("CE_MX_SHOW_PA_QTY");
            linkYourAccLinkValue = hm.get("CE_MX_LINK_YOUR_ACC_LINK");
            registerLinkValue = hm.get("CE_MX__REGISTER_LINK");
            ForgotPasswordLinkValue = hm.get("CE_MX_FORGOT_PASSWORD_LINK");
            if (device.equalsIgnoreCase("IOS")) {
                gaKeyValue = hm.get("CE_MX_GA_IOS");
            } else {
                gaKeyValue = hm.get("CE_MX_GA_ANDROID");
            }

            showOrderValue = hm.get("CE_MX_SHOW_ORDER");
            privacyPolicyLinkValue = hm.get("CE_MX_PRIVACY_POLICY_LINK");
            disableWarrantyProcessingValue = hm.get("CE_MX_DISABLE_WARRANTY_PROCESSING");
            warrantyDisabledMessageValue = hm.get("CE_MX_WARRANTY_DISABLED_MESSAGE");
            showHiddenMenuValue = hm.get("CE_SHOW_HIDDEN_MENU");
            menuIconValue = hm.get("CE_MENU_ICON");
            menuTextValue = hm.get("CE_MENU_TEXT");
            superscriptTextValue = hm.get("CE_SUPER_SCRIPT_TEXT");
            launchURLValue = hm.get("CE_LAUNCH_URL");
            pricingApiValue = hm.get("CE_MX_PRICING_API");
            signInMasterValue = hm.get("CE_MX_SIGNIN_MASTER");
            supercedeInfoValue = hm.get("CE_MX_SUPERCEDE_INFO");
            disableInventoryValue = hm.get("CE_MX_DISABLE_INVENTORY");

        } else if (buName.equalsIgnoreCase("EAST COAST METALS")) {

            shopLinkValue = hm.get("ECM_SHOP_LINK");
            productLinkValue = hm.get("ECM_PRODUCT_LINK");
            showBuyValue = hm.get("ECM_SHOW_BUY");
            if (language != null) {
                if (language.startsWith("en")) {
                    howItWorksVideoValue = hm.get("ECM_VIDEO_EN");
                    howItWorksImageValue = hm.get("ECM_IMAGE_EN");
                } else if (language.startsWith("ru")) {
                    howItWorksVideoValue = hm.get("ECM_VIDEO_RU");
                    howItWorksImageValue = hm.get("ECM_IMAGE_RU");
                } else if (language.startsWith("fr")) {
                    howItWorksVideoValue = hm.get("ECM_VIDEO_FR");
                    howItWorksImageValue = hm.get("ECM_IMAGE_FR");
                } else {
                    howItWorksVideoValue = hm.get("ECM_VIDEO_ES");
                    howItWorksImageValue = hm.get("ECM_IMAGE_ES");
                }
            }
            showPAQtyValue = hm.get("ECM_SHOW_PA_QTY");
            linkYourAccLinkValue = hm.get("ECM_LINK_YOUR_ACC_LINK");
            registerLinkValue = hm.get("ECM_REGISTER_LINK");
            ForgotPasswordLinkValue = hm.get("ECM_FORGOT_PASSWORD_LINK");
            signInValue = hm.get("EC_SIGNIN");
            if (device.equalsIgnoreCase("IOS")) {
                gaKeyValue = hm.get("ECM_GA_IOS");
            } else {
                gaKeyValue = hm.get("ECM_GA_ANDROID");
            }

            showOrderValue = hm.get("ECM_SHOW_ORDER");
            privacyPolicyLinkValue = hm.get("ECM_PRIVACY_POLICY_LINK");
            disableWarrantyProcessingValue = hm.get("ECM_DISABLE_WARRANTY_PROCESSING");
            warrantyDisabledMessageValue = hm.get("ECM_WARRANTY_DISABLED_MESSAGE");
            showHiddenMenuValue = hm.get("ECM_SHOW_HIDDEN_MENU");
            menuIconValue = hm.get("ECM_MENU_ICON");
            menuTextValue = hm.get("ECM_MENU_TEXT");
            superscriptTextValue = hm.get("ECM_SUPER_SCRIPT_TEXT");
            launchURLValue = hm.get("ECM_LAUNCH_URL");
            pricingApiValue = hm.get("ECM_PRICING_API");
            signInMasterValue = hm.get("ECM_SIGNIN_MASTER");
            supercedeInfoValue = hm.get("ECM_SUPERCEDE_INFO");
            disableInventoryValue = hm.get("ECM_DISABLE_INVENTORY");

        } else if (buName.equalsIgnoreCase("BAKER")) {

            shopLinkValue = hm.get("BK_SHOP_LINK");
            productLinkValue = hm.get("BK_PRODUCT_LINK");
            showBuyValue = hm.get("BK_SHOW_BUY");
            if (language != null) {
                if (language.startsWith("en")) {
                    howItWorksVideoValue = hm.get("BK_VIDEO_EN");
                    howItWorksImageValue = hm.get("BK_IMAGE_EN");
                } else if (language.startsWith("ru")) {
                    howItWorksVideoValue = hm.get("BK_VIDEO_RU");
                    howItWorksImageValue = hm.get("BK_IMAGE_RU");
                } else if (language.startsWith("fr")) {
                    howItWorksVideoValue = hm.get("BK_VIDEO_FR");
                    howItWorksImageValue = hm.get("BK_IMAGE_FR");
                } else {
                    howItWorksVideoValue = hm.get("BK_VIDEO_ES");
                    howItWorksImageValue = hm.get("BK_MAGE_ES");
                }
            }

            showPAQtyValue = hm.get("BK_SHOW_PA_QTY");
            linkYourAccLinkValue = hm.get("BK_LINK_YOUR_ACC_LINK");
            registerLinkValue = hm.get("BK_REGISTER_LINK");
            ForgotPasswordLinkValue = hm.get("BK_FORGOT_PASSWORD_LINK");
            signInValue = hm.get("BK_SIGNIN");
            if (device.equalsIgnoreCase("IOS")) {
                gaKeyValue = hm.get("BK_GA_IOS");
                minVersionValue = hm.get("BK_WP_IOS_MIN");
//                maxVersionValue = hm.get("BK_WP_IOS_MAX");
            } else {
                gaKeyValue = hm.get("BK_GA_ANDROID");
                minVersionValue = hm.get("BK_WP_ANDROID_MIN");
//                maxVersionValue = hm.get("BK_WP_ANDROID_MAX");
            }

            showOrderValue = hm.get("BK_SHOW_ORDER");
            privacyPolicyLinkValue = hm.get("BK_PRIVACY_POLICY_LINK");
            disableWarrantyProcessingValue = hm.get("BK_DISABLE_WARRANTY_PROCESSING");
            warrantyDisabledMessageValue = hm.get("BK_WARRANTY_DISABLED_MESSAGE");
            showHiddenMenuValue = hm.get("BK_SHOW_HIDDEN_MENU");
            menuIconValue = hm.get("BK_MENU_ICON");
            menuTextValue = hm.get("BK_MENU_TEXT");
            superscriptTextValue = hm.get("BK_SUPER_SCRIPT_TEXT");
            launchURLValue = hm.get("BK_LAUNCH_URL");
            pricingApiValue = hm.get("BK_PRICING_API");
            signInMasterValue = hm.get("BK_SIGNIN_MASTER");
            supercedeInfoValue = hm.get("BK_SUPERCEDE_INFO");
            disableInventoryValue = hm.get("BK_DISABLE_INVENTORY");

        }

        if (rs != null) {

            Element imageLink = doc.createElement("IMAGE_LINK");
            Element billBoardLink = doc.createElement("BILLBOARD_LINK");
            Element targetVersion = doc.createElement("TARGET_VERSION");
            Element minVersion = doc.createElement("MIN_VERSION");
            Element downloadUrl = doc.createElement("DOWNLOAD_URL");
            Element appCount = doc.createElement("AppCount");

            try {
                String imgLink = "";
                String billBrdLink = "";

                if (rs.next()) {

                    imgLink = rs.getString(1);
                    billBrdLink = rs.getString(2);

                    targetVersion.setAttribute(VALUE, rs.getString(3));
                    minVersion.setAttribute(VALUE, rs.getString(4));
                    downloadUrl.setAttribute(VALUE, rs.getString(5));
                    appCount.setAttribute(VALUE, rs.getString("AppCount"));
                }
                while (rs.next()) {
                    imgLink = imgLink + "||" + rs.getString(1);
                    billBrdLink = billBrdLink + "||" + rs.getString(2);

                }

                imageLink.setAttribute(VALUE, imgLink);
                billBoardLink.setAttribute(VALUE, billBrdLink);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                logger.error("SQLException : " + e);
            }

            results.appendChild(targetVersion);
            results.appendChild(minVersion);
            results.appendChild(imageLink);
            results.appendChild(billBoardLink);
            results.appendChild(downloadUrl);
            results.appendChild(appCount);

        }

        Element productLink = doc.createElement("PRODUCT_LINK");
        Element shopLink = doc.createElement("SHOP_LINK");
        Element showBuy = doc.createElement("SHOW_BUY");
        Element productCatalog = doc.createElement("PRODUCT_CATALOG");
        Element storeLocatorCookie = doc.createElement("STORE_LOCATOR_COOKIE");
        Element howItWorksImage = doc.createElement("HOW_IT_WORKS_IMAGE");
        Element howItWorksVideo = doc.createElement("HOW_IT_WORKS_VIDEO");
        Element showPAQty = doc.createElement("SHOW_PA_QTY");
        Element linkYourAccLink = doc.createElement("LINK_YOUR_ACC");
        Element registerLink = doc.createElement("REGISTER");
        Element forgotPasswordLink = doc.createElement("FORGOT_PASSWORD");
        Element privacyPolicyLink = doc.createElement("PRIVACY_POLICY");
        Element showPassword = doc.createElement("SHOW_PASSWORD");
        Element signInLink = doc.createElement("SIGN_IN");
        Element gakeyLink = doc.createElement("GA");
        Element showOrderLink = doc.createElement("SHOW_ORDER");
        Element disableWarrantyProcessingLink = doc.createElement("DISABLE_WARRANTY_PROCESSING");
        Element warrantyDisabledMessageLink = doc.createElement("WARRANTY_DISABLED_MESSAGE");
        Element showHiddenMenuLink = doc.createElement("Show_Hidden_Menu");
        Element menuIconLink = doc.createElement("Menu_Icon");
        Element menuTextLink = doc.createElement("Menu_Text");
        Element superScriptTextLink = doc.createElement("Superscript_Text");
        Element launchUrlLink = doc.createElement("Launch_URL");
        Element pricingApiLink = doc.createElement("PRICING_API");
        Element signInMasterLink = doc.createElement("SIGNIN_MASTER");
        Element supercedeInfoLink = doc.createElement("SUPERCEDE_INFO");
        Element disableInventoryLink = doc.createElement("DISABLE_INVENTORY");
        Element minVersionLink = null;
//        Element maxVersionLink = null;

        if (device.equalsIgnoreCase("IOS")) {
            minVersionLink = doc.createElement("WP_IOS_MIN");
//            maxVersionLink = doc.createElement("WP_IOS_MAX");
        } else if (device.equalsIgnoreCase("ANDROID")) {
            minVersionLink = doc.createElement("WP_ANDROID_MIN");
//            maxVersionLink = doc.createElement("WP_ANDROID_MAX");
        }

        productLink.setAttribute(VALUE, productLinkValue);
        shopLink.setAttribute(VALUE, shopLinkValue);
        showBuy.setAttribute(VALUE, showBuyValue);
        productCatalog.setAttribute(VALUE, productCatalogValue);
        storeLocatorCookie.setAttribute(VALUE, storeLocatorCookieValue);
        howItWorksImage.setAttribute(VALUE, howItWorksImageValue);
        howItWorksVideo.setAttribute(VALUE, howItWorksVideoValue);
        showPAQty.setAttribute(VALUE, showPAQtyValue);
        linkYourAccLink.setAttribute(VALUE, linkYourAccLinkValue);
        registerLink.setAttribute(VALUE, registerLinkValue);
        forgotPasswordLink.setAttribute(VALUE, ForgotPasswordLinkValue);
        privacyPolicyLink.setAttribute(VALUE, privacyPolicyLinkValue);
//        docPLink.setAttribute(VALUE, DocPValue);
//        docOLink.setAttribute(VALUE, DocOValue);
//        docTLink.setAttribute(VALUE, DocTValue);
        signInLink.setAttribute(VALUE, signInValue);
        gakeyLink.setAttribute(VALUE, gaKeyValue);
        showOrderLink.setAttribute(VALUE, showOrderValue);
        disableWarrantyProcessingLink.setAttribute(VALUE, disableWarrantyProcessingValue);
        warrantyDisabledMessageLink.setAttribute(VALUE, warrantyDisabledMessageValue);
        showHiddenMenuLink.setAttribute(VALUE, showHiddenMenuValue);
        menuIconLink.setAttribute(VALUE, menuIconValue);
        menuTextLink.setAttribute(VALUE, menuTextValue);
        superScriptTextLink.setAttribute(VALUE, superscriptTextValue);
        launchUrlLink.setAttribute(VALUE, launchURLValue);
        pricingApiLink.setAttribute(VALUE, pricingApiValue);
        signInMasterLink.setAttribute(VALUE, signInMasterValue);
        supercedeInfoLink.setAttribute(VALUE, supercedeInfoValue);
        disableInventoryLink.setAttribute(VALUE, disableInventoryValue);
        minVersionLink.setAttribute(VALUE, minVersionValue);
//        maxVersionLink.setAttribute(VALUE, maxVersionValue);

        results.appendChild(productLink);
        results.appendChild(shopLink);
        results.appendChild(showBuy);
        results.appendChild(productCatalog);
        results.appendChild(storeLocatorCookie);
        results.appendChild(howItWorksImage);
        results.appendChild(howItWorksVideo);
        results.appendChild(showPAQty);
        results.appendChild(linkYourAccLink);
        results.appendChild(registerLink);
        results.appendChild(forgotPasswordLink);
        results.appendChild(privacyPolicyLink);

        results.appendChild(signInLink);
        results.appendChild(gakeyLink);
        results.appendChild(showOrderLink);
        results.appendChild(disableWarrantyProcessingLink);
        results.appendChild(warrantyDisabledMessageLink);
        results.appendChild(showHiddenMenuLink);
        results.appendChild(menuIconLink);
        results.appendChild(menuTextLink);
        results.appendChild(superScriptTextLink);
        results.appendChild(launchUrlLink);
        results.appendChild(pricingApiLink);
        results.appendChild(signInMasterLink);
        results.appendChild(supercedeInfoLink);
        results.appendChild(disableInventoryLink);
        results.appendChild(minVersionLink);
//        results.appendChild(maxVersionLink);

        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);

        // logger.info(sw.toString());
        return sw.toString();
    }

    public String getJSONDocument(ResultSet rs) {
        JSONObject jObject = null;
        JSONArray jArray = new JSONArray();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            if (rs != null) {
                while (rs.next()) {
                    jObject = new JSONObject();
                    for (int i = 1; i <= colCount; i++) {

                        if (rs.getObject(i) != null) {
                            jObject.put(rsmd.getColumnName(i), rs.getObject(i).toString());
                        } else {
                            jObject.put(rsmd.getColumnName(i), "");
                        }
                    }
                    jArray.put(jObject);
                }
            }
            return jArray.toString();
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception----->" + e);
        }

        return "No Data Found";
    }

    public String getDocValues(String buName, String device, String udid, String appVersion, String branchNumber, int userId, String language, UriInfo uriInfo) {
        Connection con = null;
        ResultSet configRS = null;
        String response = "";
        Statement stmt = null;
        String configValue;

        try {
            HashMap keyValue = new HashMap<String, String>();

            keyValue.put("device", device);
            keyValue.put("udid", udid);
            keyValue.put("appVersion", appVersion);
            keyValue.put("branchNumber", branchNumber);
            keyValue.put("userId", userId);
            keyValue.put("language", language);

            con = DBConnection.getConnection(true);

            String[] keyVakue = CacheManager.getCacheValueUsingQueryParams(uriInfo, keyValue);

            if (keyVakue != null) {
                response = keyVakue[1];
            }

            if (response == null) {
                configValue = "EXEC [DOC_Config] '" + buName + "'";
                logger.info("====" + userId + "===getDocValues()====>Report Query : " + configValue);
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                configRS = stmt.executeQuery(configValue);

                response = getJSONDocument(configRS);

                System.out.println("response : " + response);
                CacheManager.setCache(keyVakue[0], response);

            } else {

                configValue = "EXEC [DOC_Config] '" + buName + "'";

                if (CacheManager.isWriteCacheToLogEnable()) {
                    logger.info("CACHE===" + userId + "=====>Query : " + configValue);
                }
            }
            return response;

        } catch (Exception e) {
            logger.error("=======" + userId + "=====>Exeption in getBillboardDocValues() : " + e);
            return e.toString();

        }
    }

}
