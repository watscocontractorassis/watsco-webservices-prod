package com.watsco.hvac.dao;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.HttpEntity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import org.apache.log4j.Logger;

public class SoapConnection {

    private static Logger logger = Logger.getLogger(SoapConnection.class);

    private HttpURLConnection connection;
    private InputStream inStream;
    private static final int TIMEOUT_CONNECT_MILLIS = 3000 * 1000;
    private static final int TIMEOUT_READ_MILLIS = TIMEOUT_CONNECT_MILLIS - 5000;
    int respCode = 0;

    public String soapPostResponse(String xmlString, URL url, String soapUrl) throws Exception {
        String retString = "";
        if (url.toString().contains("%20")) {
            url = new URL(url.toString().replaceAll("%20", " "));
        }
        try {
            if (url.getProtocol().toLowerCase().equalsIgnoreCase("https")) {
                trustAllHosts();
                HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(TIMEOUT_CONNECT_MILLIS);
            connection.setReadTimeout(TIMEOUT_READ_MILLIS);
            connection.setRequestMethod("POST");

            if (url.toString().contains("ws.utcbis.com")) {
                String basicAuth = "d2F0c2NvLTQwNTkwNTo4MjllMDExMS04YTZiLTQxM2MtOTg3NS1hN2VjYmQ5ODE5MGI=";
//            	connection.setRequestProperty("Authorization", basicAuth);
                connection.setRequestProperty("Authorization", "Basic " + basicAuth);
//            	connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                connection.setRequestProperty("Accept", "application/json");

//            }if (url.toString().contains("quickorder")) {
            } else if (url.toString().contains("quickorder")) {
                connection.setRequestProperty("Content-Type", "text/html;charset=UTF-8");
            } else {
                connection.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
            }
            connection.setRequestProperty("Content-Length", "" + xmlString.length());
            connection.setRequestProperty("SOAPAction", soapUrl);
            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(xmlString);
            outputStream.flush();

            respCode = connection.getResponseCode();
            String message = connection.getResponseMessage();

            if (200 <= respCode && respCode <= 299) {
                inStream = connection.getInputStream();
            } else {
                inStream = connection.getErrorStream();
            }
//            inStream = connection.getInputStream();

            retString = is2Str(inStream);
            retString = retString.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
            retString = retString.replaceAll("\t", "");
            retString = retString.replaceAll("[\n\r]", "");

        } catch (Exception e) {
            //			logger.error("Exception: " +e);
            return "API failed";
        }
        return retString;
    }

    public String soapget(URL url, String access_token) throws Exception {
        String retString = "";

        if (url.toString().contains("%20")) {
            url = new URL(url.toString().replaceAll("%20", " "));
        }
        url = new URL(url.toString().trim().replaceAll(" ", "%20"));
        try {

            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }

            System.setProperty("http.keepAlive", "false");
            //		                connection.setRequestProperty("connection", "close");

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(TIMEOUT_CONNECT_MILLIS);
            connection.setReadTimeout(TIMEOUT_READ_MILLIS);

            connection.setRequestMethod("GET");
            //				connection.setRequestProperty("Authorization", "RHEEM-API-KEY apikey=\"5DA9E79472504E49BBAC\", signature=\"TD8gZ9bJCyaOien+D1AttaqAsHrUk0209TVe0Zc7SI0=\"");

            if (url.toString().contains("api.smartystreets.com")) {
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");

            }
            if (url.toString().contains("http://phdbws.carrier.com")) {
                connection.addRequestProperty("Authorization", "Bearer " + access_token);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
            }
            if (url.toString().contains("myrheem.com")) {
                connection.addRequestProperty("X-ClientID", "7d962061-44db-47d6-abde-04925d77f9ea.apps.rheemapi.com");
                connection.addRequestProperty("Authorization", "Bearer " + access_token);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
            }

//            else {
//                connection.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
//            }
            respCode = connection.getResponseCode();
            String message = connection.getResponseMessage();

            if (200 <= respCode && respCode <= 299) {
                inStream = connection.getInputStream();
            } else {
                inStream = connection.getErrorStream();
            }

//            InputStream error = connection.getErrorStream();
            //				String respMsg = connection.getResponseMessage();
            //				logger.info("Response Code = "+respCode +" Response Message = "+respMsg);
//            inStream = connection.getInputStream();
        } catch (Exception e) {
            if (respCode == 500) {
                retString = "Failure-ResponseCode500";
                logger.error("My URLPost..File Not Found " + e);
            }
            if (respCode == 400) {
                retString = "Failure-ResponseCode400";
                logger.warn("My URLPost..File Not Found " + e);
            } else {
                retString = "Failure";
                logger.error("My URLPost..File Not Found " + e);
            }
            return retString;
        }
        if (respCode == connection.HTTP_OK) {
            retString = is2Str(inStream);
        }
        retString = retString.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
        retString = retString.replaceAll("\t", "");
        retString = retString.replaceAll("[\n\r]", "");
        return retString;
    }

    public String is2Str(InputStream is) throws IOException {
        StringBuffer ret = new StringBuffer();
        if (is != null) {
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    ret.append(buffer, 0, n);
                }
            } finally {
                is.close();
            }
        }

        return ret.toString();
    }

    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private static void trustAllHosts() {
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }

                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }
            }
        };

// Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String sendWarrantyRheemGetRequest(String url, String access_token, String clientID) {
        try {
//            String url = "https://io.myrheem.com/v1/public/warranties/printcertificate?serialNumber=M210610341";

            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);

            // add request header
            request.addHeader("User-Agent", "xxxx");
            request.addHeader("Content-Type", "application/xml; charset=utf-8");
            request.addHeader("Accept", "application/xml");
            request.addHeader("X-ClientID", clientID);
            request.addHeader("Authorization", "Bearer " + access_token);
            HttpResponse response = client.execute(request);

            respCode = response.getStatusLine().getStatusCode();

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (Exception e) {
            e.getMessage();
            return e.getMessage();
        }
    }

}
