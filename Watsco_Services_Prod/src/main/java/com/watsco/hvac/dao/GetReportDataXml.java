package com.watsco.hvac.dao;

import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GetReportDataXml {

    public static String getXMLDocument(ResultSet rs)
            throws ParserConfigurationException, TransformerException {
//		logger.info("WebserviceResultXml getXMLDocument method invoked");
        String ROOT_ELEMENT = "RESULT";

        String VALUE = "VALUE";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element results = doc.createElement("RESULT");

        try {
            if (rs != null) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();
                while (rs.next()) {
                    Element child = doc.createElement("MARKETTING_CLICKS_DETAIL");

                    for (int i = 1; i <= colCount; i++) {
                        Element node = doc.createElement(rsmd.getColumnName(i).replaceAll(" ", ""));
                        if (rs.getObject(i) != null) {
                            node.appendChild(doc.createTextNode(rs.getObject(i).toString()));
                        } else {
                            node.appendChild(doc.createTextNode(""));
                        }

                        child.appendChild(node);
                    }
                    results.appendChild(child);
                }

            }
        } catch (Exception e) {
//		   logger.error("Exception : "+e.toString());
            e.printStackTrace();
        }

        doc.appendChild(results);
        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);

        return sw.toString();
    }
}
