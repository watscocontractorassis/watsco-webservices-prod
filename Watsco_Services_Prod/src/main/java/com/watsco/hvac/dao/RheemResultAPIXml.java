package com.watsco.hvac.dao;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;

public class RheemResultAPIXml {

    private static Logger logger = Logger.getLogger(RheemResultAPIXml.class);

    private static final String ROOT_ELEMENT = "RESULT";
    private static final String DATA = "DATA";
    private static final String STATUS = "status";
    private static final String HEADER = "HEADER";

    public static String getWarrantyTagValue(final String startTag,
            final String endTag, String pageSource) {
        Pattern htmltag = Pattern.compile(startTag + "(.*?)"
                + endTag);
        Matcher tagmatch = htmltag.matcher(pageSource.toString());

        StringBuffer sb = new StringBuffer();

        int i = 0;
        while (tagmatch.find()) {
            String status = tagmatch.group()
                    .replace(startTag, "")
                    .replace(endTag, "");

            if (i == 0) {
                sb.append(status);
            }
            //			else
            //				sb.append("~~~"+status);

            i++;

        }

        return sb.toString();
    }

    public static String getRheemResultXML(String pageSource, String serialNo, String postalCode,
            int installId, int userId, String udid, int reportId, String buName, String registered) throws ParserConfigurationException, TransformerException {
        //		logger.info("RheemResultAPIXml getRheemResultXML method invoked");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element root = doc.createElement(ROOT_ELEMENT);

        UsageTracking usageTracking = new UsageTracking();
        String searchedData = "SerialNo : " + serialNo + "; PostalCode : " + postalCode;
        String postalCodeAPI = "";
        //		logger.info("Rheem Webservice Response"+pageSource);

        if (pageSource.contains("ErrorList")) {
            root.setAttribute(STATUS, "failure");
            searchedData = "This unit appears not to be registered. " + searchedData;
            String errorListInfo = RheemResultAPIXml.getWarrantyTagValue("<ErrorList>", "</ErrorList>", pageSource);
            String errorInfo = RheemResultAPIXml.getWarrantyTagValue("<Error>", "</Error>", errorListInfo);
            Element errorInformation = doc.createElement("DATA");
            errorInformation.setAttribute("error", errorInfo);
            root.appendChild(errorInformation);

        } else if (!pageSource.contains("Failure")) {
            root.setAttribute(STATUS, "success");

            if (pageSource.contains("<Homeowner>")) {
                String homeOwnerInfo = RheemResultAPIXml.getWarrantyTagValue("<Homeowner>", "</Homeowner>", pageSource);

                String city = RheemResultAPIXml.getWarrantyTagValue("<City>", "</City>", homeOwnerInfo);;
                String state = RheemResultAPIXml.getWarrantyTagValue("<State>", "</State>", homeOwnerInfo);
                postalCodeAPI = RheemResultAPIXml.getWarrantyTagValue("<PostalCode>", "</PostalCode>", homeOwnerInfo);
                Element homeOwnerInformation = doc.createElement(DATA);
                homeOwnerInformation.setAttribute(HEADER, "OWNER INFORMATION");
                homeOwnerInformation.setAttribute("city", city);
                homeOwnerInformation.setAttribute("state", state);
                homeOwnerInformation.setAttribute("postalCode", postalCodeAPI);
                root.appendChild(homeOwnerInformation);
            }
            /* if (pageSource.contains("<Owner>")) {
             String contractorInfo = RheemResultAPIXml.getWarrantyTagValue("<Owner>", "</Owner>", pageSource);
             String cownerName = RheemResultAPIXml.getWarrantyTagValue("<Name>", "</Name>", contractorInfo);;
             String ownerAddress1 = RheemResultAPIXml.getWarrantyTagValue("<AddressLine1>", "</AddressLine1>", contractorInfo);
             String ownerAddress2 = RheemResultAPIXml.getWarrantyTagValue("<AddressLine2>", "</AddressLine2>", contractorInfo);
             String ownerCity = RheemResultAPIXml.getWarrantyTagValue("<City>", "</City>", contractorInfo);
             String ownerState = RheemResultAPIXml.getWarrantyTagValue("<State>", "</State>", contractorInfo);
             String ownerPostalCode = RheemResultAPIXml.getWarrantyTagValue("<PostalCode>", "</PostalCode>", contractorInfo);

             Element unitInformation = doc.createElement(DATA);
             unitInformation.setAttribute(HEADER, "OWNER INFORMATION");
             unitInformation.setAttribute("name", cownerName);
             String address = ownerAddress1 + ownerAddress2 + " ," + ownerCity + " ," + ownerState + " "
             + ownerPostalCode;
             unitInformation.setAttribute("address", address);
             root.appendChild(unitInformation);

             }*/
            if (pageSource.contains("<Units>")) {
                String unitInfo = RheemResultAPIXml.getWarrantyTagValue("<Units>", "</Units>", pageSource);
                String unitModel = RheemResultAPIXml.getWarrantyTagValue("<UnitModel>", "</UnitModel>", unitInfo);;
                String unitSerial = RheemResultAPIXml.getWarrantyTagValue("<UnitSerial>", "</UnitSerial>", unitInfo);
                String registrationDate = RheemResultAPIXml.getWarrantyTagValue("<RegistrationDate>", "</RegistrationDate>", unitInfo);
                if (registrationDate.contains(" ")) {
                    registrationDate = registrationDate.substring(0, registrationDate.indexOf(' '));
                } else {
                    registrationDate = "";
                }
                Element unitInformation = doc.createElement(DATA);
                unitInformation.setAttribute(HEADER, "UNIT INFORMATION");
                unitInformation.setAttribute("model", unitModel);
                unitInformation.setAttribute("serial_number", unitSerial);
                unitInformation.setAttribute("registrationDate", registrationDate);
                root.appendChild(unitInformation);
            }

            if (pageSource.contains("<Contractor>")) {
                String contractorInfo = RheemResultAPIXml.getWarrantyTagValue("<Contractor>", "</Contractor>", pageSource);
                String contractorName = RheemResultAPIXml.getWarrantyTagValue("<ContractorName>", "</ContractorName>", contractorInfo);;
                String contractorAddress1 = RheemResultAPIXml.getWarrantyTagValue("<AddressLine1>", "</AddressLine1>", contractorInfo);
                String contractorAddress2 = RheemResultAPIXml.getWarrantyTagValue("<AddressLine2>", "</AddressLine2>", contractorInfo);
                String contractorCity = RheemResultAPIXml.getWarrantyTagValue("<City>", "</City>", contractorInfo);
                String contractorState = RheemResultAPIXml.getWarrantyTagValue("<State>", "</State>", contractorInfo);
                String contractorPostalCode = RheemResultAPIXml.getWarrantyTagValue("<PostalCode>", "</PostalCode>", contractorInfo);
                String contractorPhone = RheemResultAPIXml.getWarrantyTagValue("<Phone>", "</Phone>", contractorInfo);

                Element unitInformation = doc.createElement(DATA);
                unitInformation.setAttribute(HEADER, "CONTRACTOR INFORMATION");
                unitInformation.setAttribute("name", contractorName);
                String address = contractorAddress1 + contractorAddress2 + " ," + contractorCity + " ," + contractorState + " "
                        + contractorPostalCode;
                unitInformation.setAttribute("address", address);
                unitInformation.setAttribute("phone", contractorPhone);
                root.appendChild(unitInformation);

            }

            String pURL = "";
            String pName = "";

            if (postalCodeAPI.equalsIgnoreCase(postalCode)) {
                String certificateUrl = RheemResultAPIXml.getWarrantyTagValue("<CertificateURL>", "</CertificateURL>", pageSource);

                pURL = certificateUrl.substring(0, certificateUrl.lastIndexOf('/') + 1);
                pName = certificateUrl.substring(certificateUrl.lastIndexOf('/') + 1, certificateUrl.indexOf(".pdf"));
            }

            //			logger.info("url : "+pURL+"name: "+pName);
            Element pdfInformation = doc.createElement(DATA);
            pdfInformation.setAttribute(HEADER, "PDF INFORMATION");
            pdfInformation.setAttribute("pdfURL", pURL);
            pdfInformation.setAttribute("pdfName", pName);
            root.appendChild(pdfInformation);

            searchedData = "A successful warranty lookup; " + searchedData;
            //Usage Tracking
//            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "Rheem");
        } else //			logger.info("Failure Blk");
        //			  root.setAttribute(STATUS, "failure");
        if (pageSource.equalsIgnoreCase("Failure")) {
            searchedData = "An error performing the lookup; " + searchedData;
            root.setAttribute(STATUS, "failure");
        } else /*
                 Pattern tenDigits = Pattern.compile("^[A-Za-z]{1}\\d{9}$");
                 Matcher tenDigitMatch = tenDigits.matcher(serialNo);
                 
                 Pattern fourteenDigits = Pattern.compile("^\\d{4}[A-Za-z]{1}\\d{9}$");
                 Matcher fourteenDigitMatch = fourteenDigits.matcher(serialNo);
         */ //if(registered user)
        if (registered != null) {
            if (registered.equalsIgnoreCase("Y")) {
                HashMap<String, String> hm;

                if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                    hm = CommonUtil.getExternalAPIConfigProperties(buName);
                } else {
                    hm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
                }

                String inputMasks = hm.get("GM_INPUT_MASK");
                boolean statusSet = false;
                String expYears = "5";
                for (String mask : inputMasks.split(",")) {
                    expYears = "5";
                    String maskAndExp = "";
                    String weekAndYearMask = "";
                    if (mask.contains("|")) {
                        maskAndExp = mask.substring(mask.indexOf("|") + 1, mask.length());
                        mask = mask.substring(0, mask.indexOf("|"));
                        if (maskAndExp.contains("|")) {
                            expYears = maskAndExp.substring(maskAndExp.indexOf("|") + 1, maskAndExp.length());
                            weekAndYearMask = maskAndExp.substring(0, maskAndExp.indexOf("|"));
                        } else {
                            weekAndYearMask = maskAndExp;
                        }
                    }
                    //								if(mask.contains("|")){
                    //									expYears=mask.substring(mask.indexOf("|")+1,mask.length());
                    //								    mask = mask.substring(0,mask.indexOf("|"));
                    //								}
                    if (mask.length() == serialNo.length()) {
                        /*char[] maskArray = mask.toCharArray();
                                 int index = 1;
                                 int charArray[] = new int[maskArray.length];
                                 int indexForCharArray = 0;
                                 for(int i=0;i<maskArray.length;i++){
                                 int ascii = (int) maskArray[i];
                                 
                                 if(ascii>57){
                                 index = i; //stores the index of a character, so index becomes > -1
                                 }
                                 else
                                 {
                                 charArray[indexForCharArray++] = ++index;
                                 }
                                 }
                                 //								logger.info("index"+index);
                                 Pattern pattern = Pattern.compile("^\\d{"+index+"}[A-Za-z]{1}\\d{"+(mask.length()-index-1)+"}$");*/

                        ArrayList<String> al = new ArrayList<String>();
                        int previousStoredIndex = 0;
                        int check = 0;
                        boolean isDigit = false;

                        for (int i = 0; i < mask.length() - 1; i++) {
                            if (!(((int) mask.charAt(i) > 57 && (int) mask.charAt(i + 1) > 57) || ((int) mask.charAt(i) <= 57 && (int) mask.charAt(i + 1) <= 57))) {
                                if ((int) mask.charAt(i) > 57) {//character
                                    al.add(((i + 1) - previousStoredIndex) + "a");
                                    previousStoredIndex = i + 1;

                                    if ((i + 1) == mask.length()) {
                                        check = 1;
                                    }

                                    isDigit = true;
                                } else {//Digit
                                    al.add(((i + 1) - previousStoredIndex) + "d");

                                    previousStoredIndex = i + 1;

                                    if ((i + 1) == mask.length()) {
                                        check = 2;
                                    }

                                    isDigit = false;
                                }
                            }
                        }

                        if (check == 1) {
                            al.add((mask.length() - previousStoredIndex) + "d");
                        } else if (check == 2) {
                            al.add((mask.length() - previousStoredIndex) + "a");
                        } else if (check == 0) {
                            if (isDigit) {
                                al.add((mask.length() - previousStoredIndex) + "d");
                            } else {
                                al.add((mask.length() - previousStoredIndex) + "a");
                            }

                        }

                        StringBuffer dynamicRegex = new StringBuffer("^");
                        for (int i = 0; i < al.size(); i++) {
                            String length = al.get(i).substring(0, al.get(i).length() - 1);
                            String type = al.get(i).substring(al.get(i).length() - 1, al.get(i).length());

                            if (type.equalsIgnoreCase("a")) {
                                dynamicRegex.append("[A-Za-z]{");
                                dynamicRegex.append(length);
                                dynamicRegex.append("}");
                            } else {
                                dynamicRegex.append("\\d{");
                                dynamicRegex.append(length);
                                dynamicRegex.append("}");
                            }
                        }
                        dynamicRegex.append("$");

                        Pattern pattern = Pattern.compile(dynamicRegex.toString());
                        Matcher match = pattern.matcher(serialNo);

                        if (match.find()) {
                            //									logger.info("pattern matched");
                            String[] responseArray = standardWarranty(serialNo, expYears, weekAndYearMask);
                            int warrantyType = Integer.parseInt(responseArray[0]);
                            String expDate = responseArray[1];

                            if (warrantyType == 1) {
                                searchedData = "Warranty is covered; " + searchedData;

                                StringBuffer sb = new StringBuffer();
                                sb.append("Warranty is covered. Equipment has not been registered but is still covered by manufacturers standard warranty based on ");
                                sb.append(serialNo);
                                sb.append(". The standard warranty expires on ");
                                sb.append(expDate);
                                sb.append(".");

                                root.setAttribute(STATUS, sb.toString());
                                statusSet = true;
                            } else if (warrantyType == 2) {
                                searchedData = "Equipment has not been registered but may still be covered by manufacturers standard warranty, check installation date; " + searchedData;
                                root.setAttribute(STATUS, "Equipment has not been registered but may still be covered by manufacturers standard warranty, check installation date");
                                statusSet = true;
                            } else if (warrantyType == 3) {
                                searchedData = "Verification Failed. We are unable to locate a warrant registration with the information provided";
                                root.setAttribute(STATUS, "Verification Failed. We are unable to locate a warrant registration with the information provided");
                                statusSet = true;

                            } else {
                                searchedData = "Out of warranty; " + searchedData;
                                root.setAttribute(STATUS, "Out of warranty");
                                statusSet = true;
                            }
                            break;
                        }
                    }
                }
                if (!statusSet) {
                    searchedData = "An unsuccessful lookup as we cannot find a match; " + searchedData;
                    root.setAttribute(STATUS, "failure");
                }
            } else {
                searchedData = "Unsuccessful Warranty lookup; " + searchedData;
                root.setAttribute(STATUS, "failure");
            }
        } else {
            searchedData = "Unsuccessful Warranty lookup; " + searchedData;
            root.setAttribute(STATUS, "failure");
        } //			  logger.info("logged data : "+ searchedData);
        // Usage tracking

        doc.appendChild(root);
        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);

        logger.info("=== USER ID :" + userId + "=====>getRheemWarrantyVerificationResponse() : " + sw.toString());
        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "Rheem");
        //		logger.info(sw.toString());
        return sw.toString();
    }

//    public static String getRheemResultXML(String pageSource, String serialNo, String postalCode,
//            int installId, int userId, String udid, int reportId, String buName, String registered) throws ParserConfigurationException, TransformerException {
//        //		logger.info("RheemResultAPIXml getRheemResultXML method invoked");
//        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//        DocumentBuilder builder = factory.newDocumentBuilder();
//        Document doc = builder.newDocument();
//
//        Element root = doc.createElement(ROOT_ELEMENT);
//
//        UsageTracking usageTracking = new UsageTracking();
//        String searchedData = "SerialNo : " + serialNo + "; PostalCode : " + postalCode;
//        String postalCodeAPI = "";
//        //		logger.info("Rheem Webservice Response"+pageSource);
//
//        if (pageSource.contains("ErrorList")) {
//            root.setAttribute(STATUS, "failure");
//            searchedData = "This unit appears not to be registered. " + searchedData;
//            String errorListInfo = RheemResultAPIXml.getWarrantyTagValue("<ErrorList>", "</ErrorList>", pageSource);
//            String errorInfo = RheemResultAPIXml.getWarrantyTagValue("<Error>", "</Error>", errorListInfo);
//            Element errorInformation = doc.createElement("DATA");
//            errorInformation.setAttribute("error", errorInfo);
//            root.appendChild(errorInformation);
//
//        } else if (!pageSource.contains("Failure")) {
//            root.setAttribute(STATUS, "success");
//
//            if (pageSource.contains("<Homeowner>")) {
//                String homeOwnerInfo = RheemResultAPIXml.getWarrantyTagValue("<Homeowner>", "</Homeowner>", pageSource);
//
//                String city = RheemResultAPIXml.getWarrantyTagValue("<City>", "</City>", homeOwnerInfo);;
//                String state = RheemResultAPIXml.getWarrantyTagValue("<State>", "</State>", homeOwnerInfo);
//                postalCodeAPI = RheemResultAPIXml.getWarrantyTagValue("<PostalCode>", "</PostalCode>", homeOwnerInfo);
//                Element homeOwnerInformation = doc.createElement(DATA);
//                homeOwnerInformation.setAttribute(HEADER, "OWNER INFORMATION");
//                homeOwnerInformation.setAttribute("city", city);
//                homeOwnerInformation.setAttribute("state", state);
//                homeOwnerInformation.setAttribute("postalCode", postalCodeAPI);
//                root.appendChild(homeOwnerInformation);
//            }
//
//            if (pageSource.contains("<Units>")) {
//                String unitInfo = RheemResultAPIXml.getWarrantyTagValue("<Units>", "</Units>", pageSource);
//                String unitModel = RheemResultAPIXml.getWarrantyTagValue("<UnitModel>", "</UnitModel>", unitInfo);;
//                String unitSerial = RheemResultAPIXml.getWarrantyTagValue("<UnitSerial>", "</UnitSerial>", unitInfo);
//                String registrationDate = RheemResultAPIXml.getWarrantyTagValue("<RegistrationDate>", "</RegistrationDate>", unitInfo);
//                if (registrationDate.contains(" ")) {
//                    registrationDate = registrationDate.substring(0, registrationDate.indexOf(' '));
//                } else {
//                    registrationDate = "";
//                }
//                Element unitInformation = doc.createElement(DATA);
//                unitInformation.setAttribute(HEADER, "UNIT INFORMATION");
//                unitInformation.setAttribute("model", unitModel);
//                unitInformation.setAttribute("serial_number", unitSerial);
//                unitInformation.setAttribute("registrationDate", registrationDate);
//                root.appendChild(unitInformation);
//            }
//
//            if (pageSource.contains("<Contractor>")) {
//                String contractorInfo = RheemResultAPIXml.getWarrantyTagValue("<Contractor>", "</Contractor>", pageSource);
//                String contractorName = RheemResultAPIXml.getWarrantyTagValue("<ContractorName>", "</ContractorName>", contractorInfo);;
//                String contractorAddress1 = RheemResultAPIXml.getWarrantyTagValue("<ContractorAddressLine1>", "</ContractorAddressLine1>", contractorInfo);
//                String contractorAddress2 = RheemResultAPIXml.getWarrantyTagValue("<ContractorAddressLine2>", "</ContractorAddressLine2>", contractorInfo);
//                String contractorCity = RheemResultAPIXml.getWarrantyTagValue("<ContractorCity>", "</ContractorCity>", contractorInfo);
//                String contractorState = RheemResultAPIXml.getWarrantyTagValue("<ContractorState>", "</ContractorState>", contractorInfo);
//                String contractorPostalCode = RheemResultAPIXml.getWarrantyTagValue("<ContractorPostalCode>", "</ContractorPostalCode>", contractorInfo);
//                String contractorPhone = RheemResultAPIXml.getWarrantyTagValue("<ContractorPhone>", "</ContractorPhone>", contractorInfo);
//
//                Element unitInformation = doc.createElement(DATA);
//                unitInformation.setAttribute(HEADER, "CONTRACTOR INFORMATION");
//                unitInformation.setAttribute("name", contractorName);
//                String address = contractorAddress1 + contractorAddress2 + " ," + contractorCity + " ," + contractorState + " "
//                        + contractorPostalCode;
//                unitInformation.setAttribute("address", address);
//                unitInformation.setAttribute("phone", contractorPhone);
//                root.appendChild(unitInformation);
//
//            }
//
//            String pURL = "";
//            String pName = "";
//
//            if (postalCodeAPI.equalsIgnoreCase(postalCode)) {
//                String certificateUrl = RheemResultAPIXml.getWarrantyTagValue("<CertificateURL>", "</CertificateURL>", pageSource);
//
//                pURL = certificateUrl.substring(0, certificateUrl.lastIndexOf('/') + 1);
//                pName = certificateUrl.substring(certificateUrl.lastIndexOf('/') + 1, certificateUrl.indexOf(".pdf"));
//            }
//
//            //			logger.info("url : "+pURL+"name: "+pName);
//            Element pdfInformation = doc.createElement(DATA);
//            pdfInformation.setAttribute(HEADER, "PDF INFORMATION");
//            pdfInformation.setAttribute("pdfURL", pURL);
//            pdfInformation.setAttribute("pdfName", pName);
//            root.appendChild(pdfInformation);
//
//            searchedData = "A successful warranty lookup; " + searchedData;
//            //Usage Tracking
////            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "Rheem");
//        } else {
//            //			logger.info("Failure Blk");
//            //			  root.setAttribute(STATUS, "failure");
//
//            if (pageSource.equalsIgnoreCase("Failure")) {
//                searchedData = "An error performing the lookup; " + searchedData;
//                root.setAttribute(STATUS, "failure");
//            } else {
//
//                /*
//                 Pattern tenDigits = Pattern.compile("^[A-Za-z]{1}\\d{9}$");
//                 Matcher tenDigitMatch = tenDigits.matcher(serialNo);
//                 
//                 Pattern fourteenDigits = Pattern.compile("^\\d{4}[A-Za-z]{1}\\d{9}$");
//                 Matcher fourteenDigitMatch = fourteenDigits.matcher(serialNo);
//                 */
//                //if(registered user)
//                if (registered != null) {
//                    if (registered.equalsIgnoreCase("Y")) {
//                        HashMap<String, String> hm;
//
//                        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
//                            hm = CommonUtil.getExternalAPIConfigProperties(buName);
//                        } else {
//                            hm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
//                        }
//
//                        String inputMasks = hm.get("GM_INPUT_MASK");
//                        boolean statusSet = false;
//                        String expYears = "5";
//                        for (String mask : inputMasks.split(",")) {
//                            expYears = "5";
//                            String maskAndExp = "";
//                            String weekAndYearMask = "";
//                            if (mask.contains("|")) {
//                                maskAndExp = mask.substring(mask.indexOf("|") + 1, mask.length());
//                                mask = mask.substring(0, mask.indexOf("|"));
//                                if (maskAndExp.contains("|")) {
//                                    expYears = maskAndExp.substring(maskAndExp.indexOf("|") + 1, maskAndExp.length());
//                                    weekAndYearMask = maskAndExp.substring(0, maskAndExp.indexOf("|"));
//                                } else {
//                                    weekAndYearMask = maskAndExp;
//                                }
//                            }
//                            //								if(mask.contains("|")){
//                            //									expYears=mask.substring(mask.indexOf("|")+1,mask.length());
//                            //								    mask = mask.substring(0,mask.indexOf("|"));
//                            //								}
//                            if (mask.length() == serialNo.length()) {
//                                /*char[] maskArray = mask.toCharArray();
//                                 int index = 1;
//                                 int charArray[] = new int[maskArray.length];
//                                 int indexForCharArray = 0;
//                                 for(int i=0;i<maskArray.length;i++){
//                                 int ascii = (int) maskArray[i];
//                                 
//                                 if(ascii>57){
//                                 index = i; //stores the index of a character, so index becomes > -1
//                                 }
//                                 else
//                                 {
//                                 charArray[indexForCharArray++] = ++index;
//                                 }
//                                 }
//                                 //								logger.info("index"+index);
//                                 Pattern pattern = Pattern.compile("^\\d{"+index+"}[A-Za-z]{1}\\d{"+(mask.length()-index-1)+"}$");*/
//
//                                ArrayList<String> al = new ArrayList<String>();
//                                int previousStoredIndex = 0;
//                                int check = 0;
//                                boolean isDigit = false;
//
//                                for (int i = 0; i < mask.length() - 1; i++) {
//                                    if (!(((int) mask.charAt(i) > 57 && (int) mask.charAt(i + 1) > 57) || ((int) mask.charAt(i) <= 57 && (int) mask.charAt(i + 1) <= 57))) {
//                                        if ((int) mask.charAt(i) > 57) {//character
//                                            al.add(((i + 1) - previousStoredIndex) + "a");
//                                            previousStoredIndex = i + 1;
//
//                                            if ((i + 1) == mask.length()) {
//                                                check = 1;
//                                            }
//
//                                            isDigit = true;
//                                        } else {//Digit
//                                            al.add(((i + 1) - previousStoredIndex) + "d");
//
//                                            previousStoredIndex = i + 1;
//
//                                            if ((i + 1) == mask.length()) {
//                                                check = 2;
//                                            }
//
//                                            isDigit = false;
//                                        }
//                                    }
//                                }
//
//                                if (check == 1) {
//                                    al.add((mask.length() - previousStoredIndex) + "d");
//                                } else if (check == 2) {
//                                    al.add((mask.length() - previousStoredIndex) + "a");
//                                } else if (check == 0) {
//                                    if (isDigit) {
//                                        al.add((mask.length() - previousStoredIndex) + "d");
//                                    } else {
//                                        al.add((mask.length() - previousStoredIndex) + "a");
//                                    }
//
//                                }
//
//                                StringBuffer dynamicRegex = new StringBuffer("^");
//                                for (int i = 0; i < al.size(); i++) {
//                                    String length = al.get(i).substring(0, al.get(i).length() - 1);
//                                    String type = al.get(i).substring(al.get(i).length() - 1, al.get(i).length());
//
//                                    if (type.equalsIgnoreCase("a")) {
//                                        dynamicRegex.append("[A-Za-z]{");
//                                        dynamicRegex.append(length);
//                                        dynamicRegex.append("}");
//                                    } else {
//                                        dynamicRegex.append("\\d{");
//                                        dynamicRegex.append(length);
//                                        dynamicRegex.append("}");
//                                    }
//                                }
//                                dynamicRegex.append("$");
//
//                                Pattern pattern = Pattern.compile(dynamicRegex.toString());
//                                Matcher match = pattern.matcher(serialNo);
//
//                                if (match.find()) {
//                                    //									logger.info("pattern matched");
//                                    String[] responseArray = standardWarranty(serialNo, expYears, weekAndYearMask);
//                                    int warrantyType = Integer.parseInt(responseArray[0]);
//                                    String expDate = responseArray[1];
//
//                                    if (warrantyType == 1) {
//                                        searchedData = "Warranty is covered; " + searchedData;
//
//                                        StringBuffer sb = new StringBuffer();
//                                        sb.append("Warranty is covered. Equipment has not been registered but is still covered by manufacturers standard warranty based on ");
//                                        sb.append(serialNo);
//                                        sb.append(". The standard warranty expires on ");
//                                        sb.append(expDate);
//                                        sb.append(".");
//
//                                        root.setAttribute(STATUS, sb.toString());
//                                        statusSet = true;
//                                    } else if (warrantyType == 2) {
//                                        searchedData = "Equipment has not been registered but may still be covered by manufacturers standard warranty, check installation date; " + searchedData;
//                                        root.setAttribute(STATUS, "Equipment has not been registered but may still be covered by manufacturers standard warranty, check installation date");
//                                        statusSet = true;
//                                    } else if (warrantyType == 3) {
//                                        searchedData = "Verification Failed. We are unable to locate a warrant registration with the information provided";
//                                        root.setAttribute(STATUS, "Verification Failed. We are unable to locate a warrant registration with the information provided");
//                                        statusSet = true;
//
//                                    } else {
//                                        searchedData = "Out of warranty; " + searchedData;
//                                        root.setAttribute(STATUS, "Out of warranty");
//                                        statusSet = true;
//                                    }
//                                    break;
//                                }
//                            }
//                        }
//                        if (!statusSet) {
//                            searchedData = "An unsuccessful lookup as we cannot find a match; " + searchedData;
//                            root.setAttribute(STATUS, "failure");
//                        }
//                    } else {
//                        searchedData = "Unsuccessful Warranty lookup; " + searchedData;
//                        root.setAttribute(STATUS, "failure");
//                    }
//                } else {
//                    searchedData = "Unsuccessful Warranty lookup; " + searchedData;
//                    root.setAttribute(STATUS, "failure");
//                }
//
//            }
//
//            //			  logger.info("logged data : "+ searchedData);
//            // Usage tracking
//        }
//
//        doc.appendChild(root);
//        DOMSource domSource = new DOMSource(doc);
//        TransformerFactory tf = TransformerFactory.newInstance();
//        Transformer transformer = tf.newTransformer();
//        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
//        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
//        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
//        StringWriter sw = new StringWriter();
//        StreamResult sr = new StreamResult(sw);
//        transformer.transform(domSource, sr);
//
//        logger.info("=== USER ID :" + userId + "=====>getRheemWarrantyVerificationResponse() : " + sw.toString());
//        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "Rheem");
//        //		logger.info(sw.toString());
//        return sw.toString();
//    }
    public static String[] standardWarranty(String serialNumber, String expYears, String weekAndYearMask) {

        //		  Pattern pat = Pattern.compile("[A-Za-z]+\\d{4}");
        //		  Matcher match = pat.matcher(serialNumber);
        String expDateStr = null;
        //		  int index = -1;
        //		  if(match.find())
        //		  {
        //			  index = match.start();

        //			  logger.info("index :"+ index);
        //			  String weekString = serialNumber.substring(index+1, index+3);
        //			  String yearString = serialNumber.substring(index+3, index+5);
        String weekString = serialNumber.substring(weekAndYearMask.indexOf("W"), weekAndYearMask.indexOf("W") + 2);
        String yearString = serialNumber.substring(weekAndYearMask.indexOf("Y"), weekAndYearMask.indexOf("Y") + 2);
        int week = Integer.parseInt(weekString);
        //			  int year = Integer.parseInt(yearString);
        if (week > 0 && week < 54) {
            int days = week * 7 - 2;

            String firstDate = yearString + "/01/" + Integer.toString(days);
            String secondDate = yearString + "/01/03";

            SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd");

            Date d1 = null;
            Date d2 = null;

            try {
                d1 = format.parse(firstDate);
                d2 = format.parse(secondDate);
            } catch (Exception e) {
                logger.error("Exception : " + e);
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d2);
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

            calendar.setTime(d1);
            calendar.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
            int mnth = calendar.get(Calendar.MONTH) + 1;
            //				  logger.info("month : "+ mnth);

            String dateStart = yearString + "/" + mnth + "/01";
            String dateEnd = "";

            Date sd1 = null;
            Date sd2 = new Date();
            Date expiryDate = null;

            try {
                sd1 = format.parse(dateStart);
                dateEnd = format.format(sd2);
                sd2 = format.parse(dateEnd);

                //						    Calendar date = Calendar.getInstance();
                //						    date.setTime(new Date());
                //					    	expiryDate = format.parse(dateStart);
                //					    	String fo = new SimpleDateFormat("mm/dd/yyyy").format(expiryDate);
                //							SimpleDateFormat f = new SimpleDateFormat("mm/dd/yyyy");
                //							Date date = f.parse(fo);
                //							date.setTime(time)
                //						    Ï(f.format(date.getTime()));
                calendar.setTime(sd1);
                calendar.add(Calendar.YEAR, Integer.parseInt(expYears));
                expiryDate = calendar.getTime();
                expDateStr = new SimpleDateFormat("MM/dd/yyyy").format(expiryDate);

            } catch (Exception e) {
                logger.error("Exception : " + e);
                return new String[]{String.valueOf(0), ""};
            }

            //					getWarrantyType(sd1); 
            return new String[]{String.valueOf(diffBWDatesInDays(sd1, sd2, Integer.parseInt(expYears))), expDateStr};

        }

        return new String[]{String.valueOf(3), ""};
        //		  }
        //		  else
        //			  return new String [] {String.valueOf(0), ""};

    }

    /*
     private static int getWarrantyType(Date manufacturedDate)
     {
     int years = 0;
     int months = 0;
     int days = 0;
     int warrantyType = 0;
     
     Calendar manufacturedDay = Calendar.getInstance();
     manufacturedDay.setTimeInMillis(manufacturedDate.getTime());
     //create calendar object for current day
     long currentTime = System.currentTimeMillis();
     Calendar now = Calendar.getInstance();
     now.setTimeInMillis(currentTime);
     //Get difference between years
     years = now.get(Calendar.YEAR) - manufacturedDay.get(Calendar.YEAR);
     int currMonth = now.get(Calendar.MONTH) + 1;
     int manufacturedMonth = manufacturedDay.get(Calendar.MONTH) + 1;
     //Get difference between months
     months = currMonth - manufacturedMonth;
     //if month difference is in negative then reduce years by one and calculate the number of months.
     if (months < 0)
     {
     years--;
     months = 12 - manufacturedMonth + currMonth;
     if (now.get(Calendar.DATE) < manufacturedDay.get(Calendar.DATE))
     months--;
     } else if (months == 0 && now.get(Calendar.DATE) < manufacturedDay.get(Calendar.DATE))
     {
     years--;
     months = 11;
     }
     //Calculate the days
     if (now.get(Calendar.DATE) > manufacturedDay.get(Calendar.DATE))
     days = now.get(Calendar.DATE) - manufacturedDay.get(Calendar.DATE);
     else if (now.get(Calendar.DATE) < manufacturedDay.get(Calendar.DATE))
     {
     int today = now.get(Calendar.DAY_OF_MONTH);
     now.add(Calendar.MONTH, -1);
     days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - manufacturedDay.get(Calendar.DAY_OF_MONTH) + today;
     } else
     {
     days = 0;
     if (months == 12)
     {
     years++;
     months = 0;
     }
     }
     //Create new Diff object 
     
     if(years<5)
     {
     warrantyType = 1;
     }else if(years==5){
     if(months==0 && days==0)
     warrantyType = 1;
     else if(months<2)
     warrantyType = 2;
     else if(months==2 && days==0)
     warrantyType = 2;
     }
     
     return warrantyType;
     }
     */
    public static int diffBWDatesInDays(Date start, Date end, int expYears) {
        //		logger.info("diffBWDatesInDays invoked");
        Calendar calendar1 = new GregorianCalendar();
        Calendar calendar2 = new GregorianCalendar();

        calendar1.setTime(start);
        calendar2.setTime(end);

        long milliseconds1 = calendar1.getTimeInMillis();
        long milliseconds2 = calendar2.getTimeInMillis();

        long diff = milliseconds2 - milliseconds1;

        long diffDays = diff / (24 * 60 * 60 * 1000);

        //	    logger.info("diffDays..."+ diffDays);
        int numberOfDays = (int) diffDays;
        int warrantyType = 0;
        if (numberOfDays >= 0 && numberOfDays <= (365 * expYears)) {
            warrantyType = 1;
        } else if (numberOfDays > (365 * expYears) && numberOfDays <= ((365 * expYears) + 60)) {
            warrantyType = 2;
        }

        //	     logger.info("warrantyType"+ warrantyType);
        return warrantyType;
    }
}
