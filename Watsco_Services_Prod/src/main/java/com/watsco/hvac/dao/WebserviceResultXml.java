package com.watsco.hvac.dao;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.watsco.hvac.utils.CommonUtil;

public class WebserviceResultXml {

    private static Logger logger = Logger.getLogger(WebserviceResultXml.class);

//	HashMap<String, String> hm = CommonUtil.getExternalAPIConfigProperties();
    public static String getXMLDocument(ResultSet rs)
            throws ParserConfigurationException, TransformerException {
//		logger.info("WebserviceResultXml getXMLDocument method invoked");
        String ROOT_ELEMENT = "RESULT";

        String VALUE = "VALUE";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element results = doc.createElement(ROOT_ELEMENT);
        doc.appendChild(results);
        Element storeIsActive = doc.createElement("Store_IsActive");
        String isActive = "0";
        if (rs != null) {
            try {

                if (rs.next()) {
                    isActive = rs.getString(1);
                }

            } catch (SQLException e) {
                // TODO Auto-generated catch block
                logger.error("Exception : " + e);
            }
            storeIsActive.setAttribute(VALUE, isActive);
            results.appendChild(storeIsActive);
        }

        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);

//		logger.info(sw.toString());
        return sw.toString();
    }

    public static String getBOMXMLDocument(ResultSet rs) throws ParserConfigurationException, SQLException,
            ClassNotFoundException, IOException, TransformerException {
//		logger.info("WebserviceResultXml getBOMXMLDocument method invoked");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element results = doc.createElement("RESULT");

        try {
            if (rs != null) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();
                while (rs.next()) {
                    Element child = doc.createElement("PART_DETAIL");

                    for (int i = 1; i <= colCount; i++) {
                        Element node = doc.createElement(rsmd.getColumnName(i).replaceAll(" ", ""));
                        if (rs.getObject(i) != null) {
                            node.appendChild(doc.createTextNode(rs.getObject(i).toString()));
                        } else {
                            node.appendChild(doc.createTextNode(""));
                        }

                        child.appendChild(node);
                    }
                    results.appendChild(child);
                }

            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            logger.error("Exception : " + e);
        }

        doc.appendChild(results);
        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);

//		logger.info(sw.toString());
        return sw.toString();
    }
    /*public static String getBOMXMLDocument(ResultSet rs) throws ParserConfigurationException, SQLException,
     ClassNotFoundException, IOException, TransformerException {
     //		logger.info("WebserviceResultXml getBOMXMLDocument method invoked");

     {  
     DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
     DocumentBuilder builder = factory.newDocumentBuilder();
     Document doc = builder.newDocument();
     String VALUE = "VALUE";
     ResultSetMetaData rsmd = rs.getMetaData();
     int colCount           = rsmd.getColumnCount();
     Element results = doc.createElement("RESULT");
     doc.appendChild(results);
     Element child = doc.createElement("CLAIM_DETAIL");
	        
     if(rs!=null){
     while(rs.next()){
     Element partNumber = doc.createElement("PARTNUMBER");
     Element partDescription = doc.createElement("PARTDESCRIPTION");
     Element supercedes = doc.createElement("SUPERCEDES");
     Element category = doc.createElement("CATEGORY");
     Element partCriticality = doc.createElement("PART_CRITICALITY");
     Element magentoID = doc.createElement("MAGENTOID");
     Element mincronNumber = doc.createElement("MINCRONNUMBER");
		   
	        		
     partNumber.setAttribute(VALUE, rs.getString(1));
     partDescription.setAttribute(VALUE, rs.getString(2));
     supercedes.setAttribute(VALUE,rs.getString(3));
     category.setAttribute(VALUE,rs.getString(8));
     partCriticality.setAttribute(VALUE,rs.getString(9));
     magentoID.setAttribute(VALUE,rs.getString(10));
     mincronNumber.setAttribute(VALUE,rs.getString(11));
	                
     child.appendChild(partNumber);
     child.appendChild(partDescription);
     child.appendChild(supercedes);
     child.appendChild(category);
     child.appendChild(partCriticality);
     child.appendChild(magentoID);
     child.appendChild(mincronNumber);
		        	
	        		
	        	
     }
	        	
	        	
	        	
	        
     }
     else
     System.out.println("Empty ResutSet");
	         	
     results.appendChild(child);
     DOMSource domSource = new DOMSource(doc);
     TransformerFactory tf = TransformerFactory.newInstance();
     Transformer transformer = tf.newTransformer();
     transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,"no"); //$NON-NLS-1$
     transformer.setOutputProperty(OutputKeys.METHOD,"xml"); //$NON-NLS-1$
     transformer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1"); //$NON-NLS-1$
     StringWriter sw = new StringWriter();
     StreamResult sr = new StreamResult(sw);
     transformer.transform(domSource, sr);
	        
     //		logger.info(sw.toString());
	        
     return sw.toString();
     }
     }*/

    public static String getMagentoXMLDocument(ResultSet rs)
            throws ParserConfigurationException, TransformerException {
//		logger.info("WebserviceResultXml getXMLDocument method invoked");
        String ROOT_ELEMENT = "RESULT";

        String VALUE = "VALUE";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element results = doc.createElement(ROOT_ELEMENT);
        doc.appendChild(results);
        Element magentoId = doc.createElement("MagentoId");
        String magentoIdVal = "";
        if (rs != null) {

            try {

                if (rs.next()) {
                    magentoIdVal = rs.getString(1);
                }

            } catch (SQLException e) {
                // TODO Auto-generated catch block
                logger.error("Exception : " + e.toString());
            }
            magentoId.setAttribute(VALUE, magentoIdVal);
            results.appendChild(magentoId);
        }

        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);

//		logger.info(sw.toString());
        return sw.toString();
    }

    public static String getPriceXmlDocument(ResultSet rs)
            throws ParserConfigurationException, TransformerException {
//		logger.info("WebserviceResultXml getXMLDocument method invoked");
        String ROOT_ELEMENT = "RESULT";

        String VALUE = "VALUE";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element results = doc.createElement(ROOT_ELEMENT);
        doc.appendChild(results);
        Element sellPrice = doc.createElement("SellPrice");
        Element netPrice = doc.createElement("NetPrice");
        float sellPriceVal = 0;
        float netPriceVal = 0;

        if (rs != null) {

            try {

                if (rs.next()) {
                    sellPriceVal = rs.getFloat(1);//9.1 9.10
                    netPriceVal = rs.getFloat(2);
                }

            } catch (SQLException e) {
                // TODO Auto-generated catch block
                logger.error("Exception : " + e);
            }
            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMinimumFractionDigits(2);
            formatter.setMaximumFractionDigits(2);

//			sellPrice.setAttribute(VALUE, Float.toString(sellPriceVal));
//			netPrice.setAttribute(VALUE, Float.toString(netPriceVal));
            sellPrice.setAttribute(VALUE, formatter.format(sellPriceVal));
            netPrice.setAttribute(VALUE, formatter.format(netPriceVal));
            results.appendChild(sellPrice);
            results.appendChild(netPrice);
        }

        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);

//		logger.info(sw.toString());
        return sw.toString();
    }

    public static String getTypeOverSearchXMLDocument(ResultSet rs) throws ParserConfigurationException, SQLException,
            ClassNotFoundException, IOException, TransformerException {
        //  logger.info("WebserviceResultXml getBOMXMLDocument method invoked");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element results = doc.createElement("RESULT");

        try {
            if (rs != null) {
                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();
                while (rs.next()) {
                    Element child = doc.createElement("PRODUCT_DETAIL");

                    for (int i = 1; i <= colCount; i++) {
                        Element node = doc.createElement(rsmd.getColumnName(i).replaceAll(" ", ""));
                        if (rs.getObject(i) != null) {
//               logger.info(node +" : "+rs.getObject(i));
                            node.appendChild(doc.createTextNode(rs.getObject(i).toString()));
                        } else {
                            node.appendChild(doc.createTextNode(""));
                        }

                        child.appendChild(node);
                    }
                    results.appendChild(child);
                }

            }
        } catch (Exception e) {
            logger.error("Exception : " + e.toString());
        }

        doc.appendChild(results);
        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);

        return sw.toString();
    }

    /*public static String getMKTChannelXMLDocument(ResultSet rs)
     throws ParserConfigurationException, TransformerException{
     //		logger.info("WebserviceResultXml getXMLDocument method invoked");

     DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
     DocumentBuilder builder = factory.newDocumentBuilder();
     Document doc = builder.newDocument();

     Element results = doc.createElement("RESULT");
		  
     try
     {
     if (rs != null) 
     {
     ResultSetMetaData rsmd = rs.getMetaData();
     int colCount = rsmd.getColumnCount();
     while (rs.next()) 
     {
     Element child = doc.createElement("CHANNEL_DETAIL");
		    
     for (int i = 1; i <= colCount; i++) 
     {
     Element node = doc.createElement(rsmd.getColumnName(i).replaceAll(" ", ""));
     if (rs.getObject(i) != null) 
     {
     node.appendChild(doc.createTextNode(rs.getObject(i).toString()));
     }
     else
     {
     node.appendChild(doc.createTextNode(""));
     }
		  
     child.appendChild(node);
     }
     results.appendChild(child);
     }
		    
     }
     }
     catch (SQLException e) {
     // TODO Auto-generated catch block
     logger.error("Exception : "+e);
     } 
			
     doc.appendChild(results);
     DOMSource domSource = new DOMSource(doc);
     TransformerFactory tf = TransformerFactory.newInstance();
     Transformer transformer = tf.newTransformer();
     transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,"no"); //$NON-NLS-1$
     transformer.setOutputProperty(OutputKeys.METHOD,"xml"); //$NON-NLS-1$
     transformer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1"); //$NON-NLS-1$
     StringWriter sw = new StringWriter();
     StreamResult sr = new StreamResult(sw);
     transformer.transform(domSource, sr);
		
     //		logger.info(sw.toString());

     return sw.toString();
     }*/
    public static String getMKTChannelJSONDocument(ResultSet rs)
            throws ParserConfigurationException, TransformerException {
        try {

            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            JSONArray jArray = new JSONArray();
            while (rs.next()) {
                JSONObject jObj = new JSONObject();

                int channelWid = rs.getInt("Channel_Wid");
                String channelName = rs.getString("Channel_Name");

                jObj.put("Channel_Wid", channelWid);
                jObj.put("Channel_Name", channelName);
//			    jArray.put(jObj);
                jArray.add(jObj);

            }

            return jArray.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public String getXMLDocumment() throws ParserConfigurationException {

        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Result");
            doc.appendChild(rootElement);

            Element status = doc.createElement("Status");
            status.appendChild(doc.createTextNode("Pricing Not Available"));
            rootElement.appendChild(status);
            /*
             Element sellPrice = doc.createElement("SellPrice");
             sellPrice.appendChild(doc.createTextNode("0.00"));
             rootElement.appendChild(sellPrice);
             */

            DOMSource domSource = new DOMSource(doc);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no"); //$NON-NLS-1$
            transformer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
            transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1"); //$NON-NLS-1$
            StringWriter sw = new StringWriter();
            StreamResult sr = new StreamResult(sw);
            transformer.transform(domSource, sr);

            return sw.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO Auto-generated method stub
        return null;
    }

}
