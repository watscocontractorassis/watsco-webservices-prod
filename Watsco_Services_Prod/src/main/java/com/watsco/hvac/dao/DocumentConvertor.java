package com.watsco.hvac.dao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import org.apache.log4j.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class DocumentConvertor {

    private static final Logger logger = Logger.getLogger(DocumentConvertor.class);

    public String SerialToDiscreteJSONConvertor(ResultSet rs) {
        JSONObject rootObject = new JSONObject();
//		rootObject.put("Status", "Success");
//		rootObject.put("ResultType", "Normal");
        JSONObject jObject = null;
        JSONArray jArray = new JSONArray();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            if (rs != null) {
                while (rs.next()) {
                    jObject = new JSONObject();
                    for (int i = 1; i <= colCount; i++) {

                        if (rs.getObject(i) != null) {
                            jObject.put(rsmd.getColumnName(i), rs.getObject(i).toString());
                        } else {
                            jObject.put(rsmd.getColumnName(i), "");
                        }
                    }
                    jArray.add(jObject);
                }
            }
//			rootObject.put("Result", jArray);
//			return rootObject.toString();

            return jArray.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "No Data Found";
    }

    public String JSONConvertor(ResultSet rs) {
        JSONObject rootObject = new JSONObject();
        rootObject.put("Status", "Success");
        JSONObject jObject = null;
        JSONArray jArray = new JSONArray();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            if (rs != null) {
                while (rs.next()) {
                    jObject = new JSONObject();
                    for (int i = 1; i <= colCount; i++) {

                        if (rs.getObject(i) != null) {
                            jObject.put(rsmd.getColumnName(i), rs.getObject(i).toString());
                        } else {
                            jObject.put(rsmd.getColumnName(i), "");
                        }
                    }
                    jArray.add(jObject);
                }
            }
            rootObject.put("Result", jArray);
            return rootObject.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return e.toString();
        }

    }

    public String toJSONConvertor(ResultSet rs) {
        JSONObject jObject = new JSONObject();
        JSONObject rootObject = null;
        JSONArray jArray = new JSONArray();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            if (rs != null) {
                while (rs.next()) {
                    
                    for (int i = 1; i <= colCount; i++) {

                        if (rs.getObject(i) != null) {
                            jObject.put(rsmd.getColumnName(i).toUpperCase(), rs.getObject(i).toString());
                        } else {
                            jObject.put(rsmd.getColumnName(i).toUpperCase(), "");
                        }
                    }
//					jArray.add(jObject);
                }
            }
            return jObject.toString();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return e.toString();
        }

    }

    public String dataConvertor(ResultSet rs) {
        JSONObject jObject = null;
        JSONObject rootObject = null;
        JSONArray jArray = new JSONArray();
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int colCount = rsmd.getColumnCount();

            if (rs != null) {
                while (rs.next()) {
                    jObject = new JSONObject();
                    for (int i = 1; i <= colCount; i++) {

                        if (rs.getObject(i) != null) {
                            jObject.put(rsmd.getColumnName(i).toUpperCase(), rs.getObject(i).toString());
                        } else {
                            jObject.put(rsmd.getColumnName(i).toUpperCase(), "");
                        }
                    }
                    jArray.add(jObject);
                }
            }
            return jArray.toString();
        } catch (Exception e) {
            e.getMessage();
            return e.getMessage();
        }

    }

}
