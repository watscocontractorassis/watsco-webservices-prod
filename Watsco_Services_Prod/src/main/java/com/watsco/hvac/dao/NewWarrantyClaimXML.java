package com.watsco.hvac.dao;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.HashSet;


import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class NewWarrantyClaimXML {

	private static Logger logger = Logger.getLogger(NewWarrantyClaimXML.class);
	
	/*public static String getWarrantyClaimInfoDocument(ResultSet rs)
	{
		
		try {
			  
			ResultSetMetaData rsmd = rs.getMetaData();
		    int colCount = rsmd.getColumnCount();
		    HashSet<String> hashValue = new HashSet<String>();

		    JSONArray jArray = new JSONArray();
			JSONObject jObj = new JSONObject();
			JSONObject entryObj = new JSONObject();
			JSONArray entryArray = new JSONArray();
			
			boolean rowState = false;
			int rowCount=0;
			while(rs.next()){
				rowCount++;
			}
			String[] entryField = new String[rowCount];
			while (rs.next())
			{
					for (int i = 1; i <= colCount; i++)
					{
					  
					   if (rs.getObject(i) != null){
						   
						  if((entryArray.size()>0) && !(hashValue.contains(rs.getObject(1).toString()))){
							  jObj.put(entryField,entryArray);
							  jArray.add(jObj);
							  jObj= new JSONObject();
							  entryObj =new JSONObject();
								
						   }
						   
						   if(!(hashValue.contains(rs.getObject(1).toString())) && (i == 1)){
							   hashValue.add(rs.getObject(1).toString());
							   entryObj = new JSONObject();
							   entryArray = new JSONArray();
							   entryField[0] = rs.getObject(1).toString();						   
						   }
						   else{
							   if(i!=1){
								   entryObj.put(rsmd.getColumnName(i), rs.getObject(i).toString());
							   }
							  
						   }
					   }
					   else
						   jObj.put(rsmd.getColumnName(i), "");
					   if(i==colCount){
						   entryArray.add(entryObj);
						   entryObj = new JSONObject();
					   }
					   
					
				}
				
			}
			jObj.put(entryField,entryArray);
//			jArray.add(jObj);
			return jObj.toString();
			
		   }
		  catch(Exception e) {
			  e.printStackTrace();
		  }
		return null;
	}*/
	
	public static String getWarrantyClaimInfoDocument(ResultSet rs)
	{
		
		try {
			  
			ResultSetMetaData rsmd = rs.getMetaData();
		    int colCount = rsmd.getColumnCount();
		    HashSet<String> hashValue = new HashSet<String>();
		    String entryField="";
			JSONArray jArray = new JSONArray();
			JSONObject entryObj = new JSONObject();
			JSONArray entryArray = new JSONArray();
			
			HashMap<String,Object> hm= new HashMap<String,Object>();
			
			boolean rowState = false;
			while (rs.next())
			{
					for (int i = 1; i <= colCount; i++)
					{
					  
					   if (rs.getObject(i) != null){
						   
						  if((entryArray.size()>0) && !(hashValue.contains(rs.getObject(1).toString()))){
							  hm.put(entryField, entryArray);
//							  jObj.put(entryField,entryArray);
//							  jArray.add(hm);
							  entryObj =new JSONObject();
								
						   }
						   
						   if(!(hashValue.contains(rs.getObject(1).toString())) && (i == 1)){
							   
							   hashValue.add(rs.getObject(1).toString());
							   entryObj = new JSONObject();
							   entryArray = new JSONArray();
							   
							   entryField = rs.getObject(1).toString();						   
						   }
						   else{
							   if(i!=1){
								   entryObj.put(rsmd.getColumnName(i), rs.getObject(i).toString());
							   }
							  
						   }
					   }
					   else{
						   entryObj.put(rsmd.getColumnName(i), "");
					   }
					   if(i==colCount){
						   entryArray.add(entryObj);
						   entryObj = new JSONObject();
					   }
					   
					
				}
				
			}
//			jObj.put(entryField,entryArray);
			hm.put(entryField, entryArray);
			jArray.add(hm);
//			entryArray.add(jArray);
			return jArray.toString();
			
		   }
		  catch(Exception e) {
			  e.printStackTrace();
		  }
		return null;
	}
	public static String getWarrantyClaimDocument(ResultSet rs) {
		
		try {
			  
			ResultSetMetaData rsmd = rs.getMetaData();
		    int colCount = rsmd.getColumnCount();
		    
		    
			JSONArray jArray = new JSONArray();
			while (rs.next())
			{
				JSONObject jObj = new JSONObject();
				for (int i = 1; i <= colCount; i++)
				{
				   
				   if (rs.getObject(i) != null){
					   jObj.put(rsmd.getColumnName(i), rs.getObject(i).toString());
				   }
				   else
					   jObj.put(rsmd.getColumnName(i), "");
				   
				}
				jArray.add(jObj);
			}

			return jArray.toString();
			
		   }
		  catch(Exception e) {
		   e.printStackTrace();
		  }
		// TODO Auto-generated method stub
		return null;
	}

	public static String getWarrantyClaimXmlDocument(ResultSet rs) {
		
		try {
			  
			ResultSetMetaData rsmd = rs.getMetaData();
		    int colCount = rsmd.getColumnCount();
		    
		    
			JSONArray jArray = new JSONArray();
			while (rs.next())
			{
				JSONObject jObj = new JSONObject();
				for (int i = 1; i <= colCount; i++)
				{
				   
				   if (rs.getObject(i) != null){
					   jObj.put(rsmd.getColumnName(i), rs.getObject(i).toString());
				   }
				   else
					   jObj.put(rsmd.getColumnName(i), "");
				   
				}
				jArray.add(jObj);
			}

			return jArray.toString();
			
		   }
		  catch(Exception e) {
		   e.printStackTrace();
		  }
		// TODO Auto-generated method stub
		return null;
	}
	
}

