package com.watsco.hvac.dao;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class HttpConnection {

    private HttpURLConnection connection;
    private InputStream inStream;
    private static final int TIMEOUT_CONNECT_MILLIS = 3000 * 1000;
    private static final int TIMEOUT_READ_MILLIS = TIMEOUT_CONNECT_MILLIS - 5000;

    String rcode;

    public String postRequest(String response, URL url) throws Exception {
        String retString = "";
        int respCode = 0;

        try {
            if (url.getProtocol().toLowerCase().equals("https")) {
                trustAllHosts();
                HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                connection = https;
            } else {
                connection = (HttpURLConnection) url.openConnection();
            }

            System.setProperty("http.keepAlive", "false");
            //		                connection.setRequestProperty("connection", "close");

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(TIMEOUT_CONNECT_MILLIS);
            connection.setReadTimeout(TIMEOUT_READ_MILLIS);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Length", "" + response.length());
            connection.setRequestProperty("Content-Type", "text/xml");
//        	connection.setRequestProperty("Accept", "application/json");
            DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(response);
            outputStream.flush();
            inStream = connection.getInputStream();

            retString = is2Str(inStream);

            respCode = connection.getResponseCode();

            // inStream = connection.getInputStream();
        } catch (Exception e) {
            if (respCode == 500) {
                retString = "Failure-ResponseCode500";
            } else {
                retString = "Failure";
            }
            return retString;
        }
        if (respCode == connection.HTTP_OK) {
            return retString;
        }

        return retString;
    }

    public String is2Str(InputStream is) throws IOException {

        try {
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }

            //returns the json object
            return responseStrBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    private static void trustAllHosts() {
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }

                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }
            }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
