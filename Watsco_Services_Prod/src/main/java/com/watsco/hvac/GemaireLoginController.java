package com.watsco.hvac;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;

import com.watsco.hvac.impl.GemaireLoginImpl;
import com.watsco.hvac.utils.EncyrptionUtils;
import com.watsco.hvac.utils.StringUtils;
import com.watsco.hvac.utils.URLGenerationUtility;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Path("/GemaireLogin")
public class GemaireLoginController {

    private static Logger logger = Logger.getLogger(GemaireLoginController.class);

    @GET
    @Path("/customerAuthentication")
    public Response customerAuthenticate(@QueryParam("email") String email,
            @QueryParam("password") String password,
            @QueryParam("buName") String buName,
            @QueryParam("userId") int userId,
            @QueryParam("installId") int installId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @Context UriInfo uriInfo) throws TransformerException {

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "GemaireLogin", "customerAuthentication", uriInfo, "");

        //			logger.info("Pass sent: "+password);
        email = EncyrptionUtils.decryptCredentials(email);
        password = EncyrptionUtils.decryptCredentials(password);

        //			logger.info("Pass decyrpts: "+password);
        GemaireLoginImpl gemaireLoginImpl = new GemaireLoginImpl();
        String response = gemaireLoginImpl.getCustomerAuthentication(email, password, buName,
                userId, installId, udid, reportId);

        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/customerAuthenticationByAccNo")
    public Response customerAuthenticateByAccNo(@QueryParam("email") String email,
            @QueryParam("accountNo") String accountNo,
            @QueryParam("buName") String buName,
            @QueryParam("userId") int userId,
            @QueryParam("installId") int installId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("regionURL") String regionURL,
            @Context UriInfo uriInfo) throws TransformerException {
        //		logger.info("GemaireLoginController customerAuthenticationByAccNo method invoked");

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "GemaireLogin", "customerAuthentication", uriInfo, "");

        email = EncyrptionUtils.decryptCredentials(email);
        accountNo = EncyrptionUtils.decryptCredentials(accountNo);
        GemaireLoginImpl gemaireLoginImpl = new GemaireLoginImpl();
        String response = gemaireLoginImpl.getCustomerAuthenticationByAccNo(email, accountNo, buName, userId, installId,
                udid, reportId, regionURL);

        return Response.status(200).entity(response).build();
    }

}
