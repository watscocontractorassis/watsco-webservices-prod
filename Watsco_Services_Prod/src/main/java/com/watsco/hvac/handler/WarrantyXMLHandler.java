package com.watsco.hvac.handler;

import java.util.ArrayList;
import java.util.HashMap;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.watsco.hvac.dto.ClaimDTO;
import com.watsco.hvac.dto.WarrantyResponseDTO;
import com.watsco.hvac.impl.WarrantyClaimImpl;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WarrantyXMLHandler extends DefaultHandler {

    String claimNumber = "";
    String response = "";
    String claimStatus = "";
    int CLAIM_WID = 0;
    int userId = 0;
    String UId = "";
    Boolean mapping = false;
    Boolean error = false;
    String errorString;
    String errorText = "";
    String SB_ID = "";
    String buName = "";
    ClaimDTO claimDTO;
    private HashMap<String, String> claimsMap;
    private HashMap<String, String> partsMap;
    ArrayList<HashMap<String, String>> claimParts = new ArrayList<HashMap<String, String>>();
    private StringBuffer sb;
    private StringBuffer errorBuffer = new StringBuffer();
    WarrantyResponseDTO responseDTO = new WarrantyResponseDTO();

    public WarrantyXMLHandler() {

    }

    public WarrantyXMLHandler(ClaimDTO claimDTO, String UId, int userId, int CLAIM_WID, String SB_ID, String buName) {
        this.claimDTO = claimDTO;
        this.CLAIM_WID = CLAIM_WID;
        this.userId = userId;
        this.UId = UId;
        this.SB_ID = SB_ID;
        this.buName = buName;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        if (qName.equalsIgnoreCase("claimSubmitResponse")) {
            mapping = true;
            claimsMap = new HashMap<String, String>();
        } else if (qName.equalsIgnoreCase("claimPart")) {
            partsMap = new HashMap<String, String>();
        } else if (qName.equalsIgnoreCase("claimInquiryResponse")) {
            claimsMap = new HashMap<String, String>();
        }
        if (qName.equalsIgnoreCase("errorText") || qName.equalsIgnoreCase("validationText")) {
            error = true;
        }
        sb = new StringBuffer();

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);

        if (qName.equalsIgnoreCase("errorText") || qName.equalsIgnoreCase("validationText")) {
            if (errorString != null) {
                errorBuffer.append(errorString);
                errorBuffer.append("|");

            }
        } else if (partsMap != null) {
            partsMap.put(qName, sb.toString());
            if (qName.equalsIgnoreCase("claimPart")) {
                claimParts.add(partsMap);

                partsMap = null;

            }
        } else if (claimsMap != null) {
            claimsMap.put(qName, sb.toString());
        }

        if (qName.equalsIgnoreCase("claimSubmitResponse")) {
            if (mapping) {
                if (errorBuffer.length() > 0) {
                    errorText = errorBuffer.substring(0, errorBuffer.length() - 1);
                }
                WarrantyClaimImpl submitClaimImpl = new WarrantyClaimImpl();
                try {
                    response = submitClaimImpl.getSOAPSubmitClaim(claimDTO, claimsMap, claimParts, UId, userId, CLAIM_WID, errorText, SB_ID);
                } catch (SQLException ex) {
                    Logger.getLogger(WarrantyXMLHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
                WarrantyResponseDTO.setSubmitClaimResponse(response);
            }
        }
        if (qName.equalsIgnoreCase("claimInquiryResponse")) {
            WarrantyClaimImpl inQuiryLayer = new WarrantyClaimImpl();
            response = inQuiryLayer.inQuiryClaim(claimDTO, claimsMap, claimParts, CLAIM_WID, userId, errorText, SB_ID, buName);

            WarrantyResponseDTO.setInQuiryResponse(response);
        }

    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        super.characters(ch, start, length);
        if (error) {
            errorString = new String(ch, start, length);
        } else {
            sb.append(ch, start, length);
        }
    }

}
