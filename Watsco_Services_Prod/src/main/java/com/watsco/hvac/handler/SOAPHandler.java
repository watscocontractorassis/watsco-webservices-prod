package com.watsco.hvac.handler;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.HashMap;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;

public class SOAPHandler {

    public void getSOAPHandler(String dataSent, String claimUrl, String buName, String SB_ID, String UId, String password) {

        HashMap<String, String> exthm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }

        if ((SB_ID == null) && (UId == null) && (password == null)) {
            SB_ID = exthm.get("SerivceBench-ID");
            UId = exthm.get("ServiceBench-User");
            password = exthm.get("ServiceBench-Password");
        }

        String testURL = exthm.get("CE_WARRANTY_PROD");
        String SB_UID = SB_ID + ":" + UId;

        String version = exthm.get("CE_version");
        String sourceSystemName = exthm.get("CE_sourceSystemName");
        String sourceSystemVersion = exthm.get("CE_sourceSystemVersion");
        String serviceAdministratorId = exthm.get("CE_serviceAdministratorId");

        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(dataSent), claimUrl);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static SOAPMessage createSOAPRequest(String xml) throws Exception {

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage message = messageFactory.createMessage(new MimeHeaders(), new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8"))));
        return message;

    }
    /* public  SOAPHeader addCustomHeader(){
     String header = "<soapenv:Header>"+
     "<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"+
     "<wsse:UsernameToken wsu:Id=\"UsernameToken-15231583\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"+
     "<wsse:Username>@@SB_UID$$</wsse:Username>"+
     "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">@@password$$</wsse:Password>"+
     "</wsse:UsernameToken>"+
     "</wsse:Security>"+
     "<soapenv:Header/>";
		 
		 
     return header;
		 
     }*/

}
