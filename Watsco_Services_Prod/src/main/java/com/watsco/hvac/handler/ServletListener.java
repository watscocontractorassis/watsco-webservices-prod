package com.watsco.hvac.handler;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;


public class ServletListener extends Thread implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(ServletListener.class);

    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
        ServletContext sc;
        try {
            sc = contextEvent.getServletContext();

            System.out.println("Connection registered");
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public final void contextDestroyed(ServletContextEvent sce) {
//	    	Connection con;
//			try {
//				con = DBConnection.getConnection(false);
//				con.close();
//				logger.info("Context Deregistered");
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.getMessage();
//			}

        // ... First close any background tasks which may be using the DB ...
        // ... Then close any DB connection pools ...
        // Now deregister JDBC drivers in this context's ClassLoader:
        // Get the webapp's ClassLoader
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        // Loop through all drivers
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            if (driver.getClass().getClassLoader() == cl) {
                // This driver was registered by the webapp's ClassLoader, so deregister it:
                try {
                    logger.info("Deregistering " + driver + " driver");
                    DriverManager.deregisterDriver(driver);
                } catch (SQLException ex) {
                    logger.error("Error Deregistering " + driver + " driver, Exception :" + ex);
                }
            } else {
                // driver was not registered by the webapp's ClassLoader and may be in use elsewhere
                logger.trace("Not deregistering JDBC driver {} as it does not belong to this webapp's ClassLoader" + driver);
            }
        }

    }

}
