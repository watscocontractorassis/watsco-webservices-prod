package com.watsco.hvac;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.transform.TransformerException;

import com.watsco.hvac.impl.GoodmanWarrantyAPI;
import com.watsco.hvac.utils.URLGenerationUtility;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import org.apache.log4j.Logger;

@Path("/warrantyecm")
public class GoodmanWarrantyController {

    //	protected static Logger logger = Logger.getLogger(GoodmanWarrantyController.class);
    private static final Logger logger = Logger.getLogger(GoodmanWarrantyController.class);

    @GET
    @Path("")
    public Response GoodmanWarrantyVerificationResponse(@QueryParam("manufacturer") String manufacturer,
            @QueryParam("serialNo") String serialNo,
            @QueryParam("lastName") String lastName,
            @QueryParam("zipCode") String zipCode,
            @QueryParam("installType") String installType,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @Context UriInfo uriInfo) throws TransformerException {

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "warrantyecm", "", uriInfo, "");
        //		logger.info("==========>GoodmanWarrantyVerificationResponse method invoked");
        GoodmanWarrantyAPI goodmanWarrantyAPI = new GoodmanWarrantyAPI();
        String response = goodmanWarrantyAPI.getGoodmanWarrantyVerificationResponse(manufacturer, serialNo, lastName, zipCode, installType, installId, userId, udid, reportId, buName);

        return Response.status(200).entity(response).build();
    }
}
