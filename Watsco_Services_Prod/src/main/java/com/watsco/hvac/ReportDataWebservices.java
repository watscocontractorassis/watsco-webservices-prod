package com.watsco.hvac;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

import org.apache.log4j.Logger;

import com.watsco.hvac.impl.ReportDataImpl;
///reportDataService/reportData
@Path("/reportDataService")
public class ReportDataWebservices {

    private static Logger logger = Logger.getLogger(ReportDataWebservices.class);

    @GET
    @Path("/reportData")
    public void getReportData(@QueryParam("reportId") int reportId, @QueryParam("BU_CODE") String BU_CODE,
            @QueryParam("udid") String udid, @QueryParam("userId") int userId, @QueryParam("pid") String pid,
            @QueryParam("cid") String cid, @QueryParam("device") String device,
            @Context HttpServletRequest request) {
        String response = "";
        String ipAddress = request.getRemoteAddr();

        ReportDataImpl reportDataimpl = new ReportDataImpl();
        reportDataimpl.getReportDataService(ipAddress, reportId, BU_CODE, udid, userId, pid, cid, device);

//	    	return Response.status(200).entity(response).build();
    }

}
//reportDataService/reportData