package com.watsco.hvac;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.watsco.hvac.DBConnection.WarrantyAPIConnection;
import com.watsco.hvac.common.CommonMethods;
import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.WarrantyDocumentConvertor;
import com.watsco.hvac.impl.WarrantyAPI;
import com.watsco.hvac.impl.WatscoCommonImpl;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;
import com.watsco.hvac.utils.URLGenerationUtility;
import java.math.BigInteger;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.transform.TransformerException;
import org.json.JSONObject;

@Path("/CommonServices")
public class WatscoCommonServices {

    private static Logger logger = Logger.getLogger(WatscoCommonServices.class);
//    private static final Logger watscoLogger = Logger.getLogger("WatscoLog");
    String serverUrl = "";
//	http://localhost:8080/watsco-v4-services_DEV/watsco/CommonServices/login?installId=1&userId=112401&udid=CEC64003-AD7F-462B-922A-DB3803963A3A&reportId=40&buName=CARRIER%20ENTERPRISE&manufacturer=CARRIER&SB_ID=493878876&UId=rtiuser&password=UnRpdXNlcjEyMzQ=&name=Mi&email=m@g.com&companyName=M&provider=ServiceBench&isDev=N&isSave=N"; 

    URLGenerationUtility utility = new URLGenerationUtility();

    @GET
    @Path("/login")
    public Response loginService(
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("manufacturer") String manufacturer,
            @QueryParam("provider") String provider,
            @QueryParam("SB_ID") String SB_ID,
            @QueryParam("UId") String UId,
            @QueryParam("password") String password,
            @QueryParam("name") String name,
            @QueryParam("email") String email,
            @QueryParam("companyName") String companyName,
            @QueryParam("isDev") String isDev,
            @QueryParam("isSave") String isSave,
            @Context UriInfo uriInfo) throws SQLException, Exception {

        utility.printServiceUrl("GET", "CommonServices", "login", uriInfo, "");

        WarrantyAPIConnection warranty = new WarrantyAPIConnection();

        String unicodeText = "[^\\x09\\x0A\\x0D\\x20-\\xD7FF\\xE000-\\xFFFD\\x10000-x10FFFF]";

        HashMap<Object, Object> keyValue = CommonMethods.removeNullValues(uriInfo);

        if (isSave != null && isSave.equalsIgnoreCase("Y")) {
            try {

                warranty.insertUserCredentials(provider, installId, userId, udid, reportId, buName, SB_ID, UId, password, name, email, companyName, keyValue);
                JSONObject jObject = new JSONObject();
                jObject.put("Status", "Success");

                return Response.status(200).entity(jObject.toString()).build();
            } catch (Exception e) {
                return Response.status(200).entity(e.getMessage().toString()).build();
            }
        }

        String response = "";
        WarrantyAPI warrantyAPI = new WarrantyAPI();
        String ceUrl = "";
        String modelNumber = "";
        String serialNo = "1611C62438";

        warranty.insertUserCredentials(provider, installId, userId, udid, reportId, buName, SB_ID, UId, password, name, email, companyName, keyValue);

        if (SB_ID == null || UId == null || password == null) {
            response = "SB_ID or UID or PASSWORD cannot be empty";
            return Response.status(200).entity(response).build();
        }

        HashMap<String, String> exthm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }

        String key = "";

        switch (buName) {
            case "CARRIER ENTERPRISE":
                key = "CE_";
                break;
            case "BAKER":
                key = "BK_";
                break;
            case "CARRIER ENTERPRISE MEXICO":
                key = "CE_MX_";
                break;
            case "GEMAIRE":
                key = "GM_";
                break;
            case "EAST COAST METALS":
                key = "ECM_";
                break;
            default:
                break;
        }

        if (isDev != null) {
            isDev = isDev.trim();
            if (isDev.equalsIgnoreCase("Y")) {
                ceUrl = exthm.get(key + "Entitlement_SerivceBench_API_DEV");
                if (ceUrl == null || ceUrl.isEmpty()) {
                    logger.error("=== USER ID :" + userId + "=====>ServiceBench API not Found");
                    return Response.status(200).entity(response).build();
                }
//             	   ceUrl = "https://test.servicebench.com/servicebenchv5/services/ProductEntitlementService";
            } else {
                ceUrl = exthm.get(key + "Entitlement_SerivceBench_API_PROD");
//     			   ceUrl = "https://www.servicebench.com/servicebenchv5/services/ProductEntitlementService";
                if (ceUrl == null || ceUrl.isEmpty()) {
                    logger.error("=== USER ID :" + userId + "=====>ServiceBench API not Found");
                    return Response.status(200).entity(response).build();
                }
            }
        }
        response = warrantyAPI.getCEWarrantyLoginResponse(manufacturer, serialNo, provider, modelNumber, installId, userId, udid, reportId, buName, SB_ID, UId, password, name, email, companyName, ceUrl, isDev);

        response = response.replaceAll(unicodeText, "");
        if (!response.contains("failed")) {
            warranty.insertUserCredentials(provider, installId, userId, udid, reportId, buName, SB_ID, UId, password, name, email, companyName, keyValue);
        } else {
            logoutImpl(userId, buName, provider);
        }
        return Response.status(200).entity(response).build();
    }

    @GET
    @Path("/logout")
    public Response logoutService(
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("provider") String provider, @Context UriInfo uriInfo
    ) {

        utility.printServiceUrl("GET", "CommonServices", "logout", uriInfo, "");
        try {

            JSONObject jObject = logoutImpl(userId, buName, provider);

            logger.info("=== USER ID :" + userId + "=====>Logout Response : " + jObject.toString());
            return Response.status(200).entity(jObject.toString()).build();

        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(200).entity(e.toString()).build();
        }

    }

    private JSONObject logoutImpl(int userId, String buName, String provider) {
        try {
            Connection con = null;
            ResultSet logoutRS = null;
            Statement stmt = null;
            con = DBConnection.getConnection(false);
            int USER_ID = 0;

            String sqlQuery = "EXEC Warranty_ServiceBench_Logout '" + provider + "','" + buName + "','" + userId + "'";
            sqlQuery = sqlQuery.replaceAll("'null'", null);
            logger.info("=== USER ID :" + userId + "=====>Report Query : " + sqlQuery);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            logoutRS = stmt.executeQuery(sqlQuery);

            while (logoutRS.next()) {
                USER_ID = logoutRS.getInt("User_ID");
            }

            JSONObject jObject = new JSONObject();

            if (USER_ID != 0) {
                jObject.put("Status", "Success");
            } else {
                jObject.put("Status", "Failure");
            }
            return jObject;
        } catch (SQLException ex) {
            logger.info("=======" + userId + "==>logoutImpl Exception : " + ex.toString());
        } catch (Exception ex) {
            logger.info("=======" + userId + "==>logoutImpl Exception : " + ex.toString());
        }
        return null;
    }

    @GET
    @Path("/UsersInfo")
    public Response UsersInfo(
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buName") String buName,
            @QueryParam("provider") String provider, @Context UriInfo uriInfo
    ) {

        utility.printServiceUrl("GET", "CommonServices", "UsersInfo", uriInfo, "");
        Connection con = null;
        ResultSet userInfoRS = null;
        String response = null;
        Statement stmt = null;
        try {

            con = DBConnection.getConnection(false);

            String sqlQuery = "EXEC Warranty_ServiceBench_User_Info '" + provider + "','" + buName + "','" + userId + "'";
            sqlQuery = sqlQuery.replaceAll("'null'", null);
            logger.info("=== USER ID :" + userId + "=====>Query : " + sqlQuery);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            userInfoRS = stmt.executeQuery(sqlQuery);

            WarrantyDocumentConvertor getJson = new WarrantyDocumentConvertor();
            response = getJson.toJSONDocument(userInfoRS);
            logger.info("=======" + userId + "==>Warranty_ServiceBench_User_Info Response : " + response);

            return Response.status(200).entity(response).build();

        } catch (Exception e) {
            return Response.status(200).entity(e.toString()).build();
        } finally {
            DBConnection.closeConnection(con, stmt, userInfoRS);
        }

    }

//    http://localhost:8080/Watsco-v7-Services/watsco/CommonServices/productDetails?buWid=4&installId=1&userId=1&udid=&reportId=1&regionId=3&productId=1391689335607&language=en
    @GET
    @Path("/productDetails")
    @Produces(MediaType.APPLICATION_JSON)
    public Response productDetails(
            @QueryParam("buWid") int buWid,
            @QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("regionId") int regionId,
            @QueryParam("productId") BigInteger productId,
            @QueryParam("language") String language, @Context UriInfo uriInfo
    ) throws TransformerException {

        utility.printServiceUrl("GET", "productDetails", "UsersInfo", uriInfo, "");
        String response = "";
        String searchedData = "ProductDetails : " + productId;
        WatscoCommonImpl document = new WatscoCommonImpl();

        JSONObject productDetails = document.getProductDetails(buWid, userId, regionId, productId, language);

        UsageTracking usageTracking = new UsageTracking();
        usageTracking.usageTrackingSp(installId, userId, udid, reportId, String.valueOf(buWid), searchedData, "", "");

        return Response.status(200).entity(productDetails.toString()).build();
    }

}
