package com.watsco.hvac.cache;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    
    public static Date getCurrentDateAndTime() {
        Calendar cal = Calendar.getInstance();
        return getDateInDateFormat(getDateInStringFormat(cal.getTime()));
    }
    
    public static String setCurrentDateAndTimeWithDelay(int delay) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, delay);
        
        return getDateInStringFormat(cal.getTime());
    }
    
    public static String getDateInStringFormat(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy HH mm ss");
        return sdf.format(date);
    }
    
    public static Date getDateInDateFormat(String dateInString) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy HH mm ss");
        try {
            return sdf.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        return null;
    }
}
