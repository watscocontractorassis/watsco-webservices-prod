package com.watsco.hvac.cache;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;
import org.apache.commons.lang3.ArrayUtils;

public class CacheManager {

    private static StringBuffer sb;

    private static ConcurrentLRUCache<String, String> cacheMap;// = new ConcurrentLRUCache<String,String>(50);
    private static HashMap<String, String> cacheSettings;// = new HashMap<String,String>(2);

    private static final String CACHE_DATE_TIME = "CACHE_DATE_TIME";
    private static final String CACHE_ENABLE = "CACHE_ENABLE";
    private static final String WRITE_CACHE_TO_LOG = "WRITE_CACHE_TO_LOG";

    public static String[] getCacheValueUsingQueryParams(UriInfo uriInfo, HashMap<String,String> excludeParams) {

        if (cacheMap == null) {
            cacheMap = new ConcurrentLRUCache<String, String>(50);
        }

        if (cacheSettings == null) {
            cacheSettings = new HashMap<String, String>(5);
        }

        if (cacheSettings.get(CACHE_ENABLE) == null || cacheSettings.get(WRITE_CACHE_TO_LOG) == null) {
            HashMap<String, String> hm;

            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                hm = CommonUtil.getExternalAPIConfigProperties();
            } else {
                hm = LocalCommonUtil.getExternalAPIConfigProperties();
            }

            cacheSettings.put(CACHE_ENABLE, hm.get("WATSCO_CACHE_ENABLE"));
            cacheSettings.put(WRITE_CACHE_TO_LOG, hm.get("WATSCO_WRITE_CACHE_TO_LOG"));
        }
        
        String k = generateKey(uriInfo, excludeParams);

        if (cacheSettings.get(CACHE_ENABLE).equalsIgnoreCase("N")) {
            return new String[]{k, null};
        }

        if (isToGetDataFromCache()) {
            return new String[]{k, cacheMap.get(k)};
        } else {
            return new String[]{k, null};
        }
    }

    private static String generateKey(UriInfo uriInfo, HashMap<String,String> excludeParams) {
        int counter = 0;
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        Set<String> keySet = queryParams.keySet();
        sb = new StringBuffer();

        
       /* for (String string : keySet) {

            if (excludeParams != null && excludeParams.length > counter) {
               if (!string.equalsIgnoreCase(excludeParams[counter])) {
                    sb.append(string);
                    sb.append("=");
                    sb.append(queryParams.getFirst(string));
                    sb.append("&");
                } else {
                    counter++;
                }
            } else {
                sb.append(string);
                sb.append("=");
                sb.append(queryParams.getFirst(string));
                sb.append("&");
            }

        }*/
     
        
        for(String key : keySet){
            
//            String excludeKey = excludeParams.get(key);
//            excludeParams.containsKey(key);
            System.out.println(key);
            String value = queryParams.getFirst(key);
             if (excludeParams != null && excludeParams.size() > counter) {
                 if(!excludeParams.containsKey(key)){
                    sb.append(key);
                    sb.append("=");
                    sb.append(value);
                    sb.append("&");
                 }else{
                     counter++;
                 }
             }else{
                sb.append(key);
                sb.append("=");
                sb.append(value);
                sb.append("&");
             }
            
            
        }
        return sb.toString();
    }

    private static boolean isToGetDataFromCache() {
        String timeFromCache = cacheSettings.get(CACHE_DATE_TIME);

        if (timeFromCache == null) {
            cacheSettings.put(CACHE_DATE_TIME, DateUtils.setCurrentDateAndTimeWithDelay(getDataFromFile()));
            return false;
        } else {

            Date currentTime = DateUtils.getCurrentDateAndTime();
            Date cacheStartTime = DateUtils.getDateInDateFormat(cacheSettings.get(CACHE_DATE_TIME));

            try {
                if (currentTime.after(cacheStartTime)) {
                    cacheSettings.put(CACHE_DATE_TIME, DateUtils.setCurrentDateAndTimeWithDelay(getDataFromFile()));
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return true;

    }

    private static int getDataFromFile() {
        HashMap<String, String> hm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            hm = CommonUtil.getExternalAPIConfigProperties();
        } else {
            hm = LocalCommonUtil.getExternalAPIConfigProperties();
        }

        return Integer.parseInt(hm.get("WATSCO_CACHE_MIN") != null ? hm.get("WATSCO_CACHE_MIN") : "15");
    }

    public static void setCache(String key, String value) {
        cacheMap.put(key, value);
        System.out.println(key + " : " + value);
    }

    /*public static void clearCache() {
     HashMap<String, String> hm = CommonUtil.getExternalAPIConfigProperties();
     
     if(hm.get(CACHE_ENABLE) != null)
     cacheSettings.put(CACHE_ENABLE, hm.get("WATSCO_CACHE_ENABLE"));
     
     if(cacheMap == null)
     return;
     
     cacheMap.clear();
     cacheMap = null;
     }*/
    public static String clearCache() {
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdftr = new SimpleDateFormat("HH:mm:ss");
        String dateStr = sdf.format(today);
        String timeStr = sdftr.format(today);

        HashMap<String, String> hm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            hm = CommonUtil.getExternalAPIConfigProperties();
        } else {
            hm = LocalCommonUtil.getExternalAPIConfigProperties();
        }

        if (cacheSettings == null) {
            cacheSettings = new HashMap<String, String>();
        }

        cacheSettings.put(CACHE_ENABLE, hm.get("WATSCO_CACHE_ENABLE"));
        cacheSettings.put(WRITE_CACHE_TO_LOG, hm.get("WATSCO_WRITE_CACHE_TO_LOG"));

        if (cacheMap != null) {
            cacheMap.clear();
            cacheMap = null;
        }
        return "Success : " + dateStr + " - " + timeStr;
    }

    public static boolean isWriteCacheToLogEnable() {

        if (cacheSettings == null) {
            cacheSettings = new HashMap<String, String>();
        }

        if (cacheSettings.get(WRITE_CACHE_TO_LOG) == null) {
            HashMap<String, String> hm;
            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                hm = CommonUtil.getExternalAPIConfigProperties();
            } else {
                hm = LocalCommonUtil.getExternalAPIConfigProperties();
            }

            cacheSettings.put(WRITE_CACHE_TO_LOG, hm.get("WATSCO_WRITE_CACHE_TO_LOG"));
        }

        if (cacheSettings.get(WRITE_CACHE_TO_LOG).equalsIgnoreCase("Y")) {
            return true;
        }

        return false;
    }
}
