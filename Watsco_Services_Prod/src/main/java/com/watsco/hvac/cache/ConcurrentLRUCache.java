package com.watsco.hvac.cache;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ConcurrentLRUCache<Key, Value> {
    private final int maxSize;
    private ConcurrentHashMap<Key, Value> map;
    private ConcurrentLinkedQueue<Key> queue;
    
    public ConcurrentLRUCache(final int maxSize) {
        this.maxSize = maxSize;
        map = new ConcurrentHashMap<Key, Value>(maxSize);
        queue = new ConcurrentLinkedQueue<Key>();
    }
    
    public void put(final Key key, final Value value) {
        if (map.containsKey(key)) {
            queue.remove(key); // remove the key from the FIFO queue
        }
        
        while (queue.size() >= maxSize) {
            Key oldestKey = queue.poll();
            if (null != oldestKey) {
                map.remove(oldestKey);
            }
        }
        queue.add(key);
        map.put(key, value);
    }
    
    public Value get(Key key) {
        return map.get(key);
    }
    
    public void clear() {
        
        if(map != null)
            map.clear();
        
        if(queue != null)
            queue.clear();
    }
}