package com.watsco.hvac.DBConnection;

import com.google.common.base.Strings;
import com.watsco.hvac.common.CommonMethods;
import static com.watsco.hvac.common.CommonMethods.checkLatLongValues;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.DocumentConvertor;

public class UserDetailsAPIConnection {

    private static Logger logger = Logger.getLogger(UserDetailsAPIConnection.class);

    public String getUserDetailsImpl(int userId, String udid, String buName, JSONObject prompts) throws JSONException {
        Connection con = null;
        SQLWarning warning;
        CallableStatement cs;
        JSONObject jObject = new JSONObject();
        ResultSet userRS;
        Statement stmt;
        String response;
        String userDetails = "";
        JSONArray jArray;
        JSONObject responseObject;
        String latitude = null;
        String longitude = null;
        int regionId = 0;
        try {
            con = DBConnection.getConnection(true);
            userDetails = "{call [UserDetails] ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?}";

            latitude = checkLatLongValues(prompts.get("latitude").toString());
            longitude = checkLatLongValues(prompts.get("longitude").toString());

//            if(latitude == null || longitude == null){
//                latitude = latitude==null ? "0" : latitude; 
//                longitude = latitude==null ? "0" : longitude; 
//            }
//            String QueryToPrint = "EXEC [UserDetails] " + (userId == 0 ? null : "'"+userId+"'") + ",'" + (prompts.get("type") == null ? "" : prompts.get("type")) + "','" + ((Strings.isNullOrEmpty(prompts.get("userName").toString())) ? null : (prompts.get("userName").toString())) + "','" + buName + "','" + (Strings.isNullOrEmpty(prompts.get("zipCode").toString()) ? null : (prompts.get("zipCode").toString())) + "','" + (Strings.isNullOrEmpty(prompts.get("branchNumber").toString()) ? null : (prompts.get("branchNumber").toString())) + "','" + (prompts.get("regionId") == null ? null : prompts.get("regionId")) + "','" + (Strings.isNullOrEmpty(prompts.get("appVersion").toString()) ? null : (prompts.get("appVersion").toString())) + "','" + (Strings.isNullOrEmpty(prompts.get("deviceType").toString()) ? null : (prompts.get("deviceType").toString())) + "','" + (Strings.isNullOrEmpty(prompts.get("preferredBranch").toString()) ? null : (prompts.get("preferredBranch").toString())) + "','" + (Strings.isNullOrEmpty(prompts.get("firstName").toString()) ? null : (prompts.get("firstName").toString())) + "','" + (Strings.isNullOrEmpty(prompts.get("lastName").toString()) ? null : (prompts.get("lastName").toString())) + "','" + (Strings.isNullOrEmpty(prompts.get("companyName").toString()) ? null : (prompts.get("companyName").toString())) + "','" + (Strings.isNullOrEmpty(prompts.get("companyAddress").toString()) ? null : (prompts.get("companyAddress").toString())) + "','" + (Strings.isNullOrEmpty(prompts.get("emailId").toString()) ? null : (prompts.get("emailId").toString())) + "','" + udid + "','" + (prompts.get("appLanguage") == null ? "" : prompts.get("appLanguage")) + "','" + (Strings.isNullOrEmpty(prompts.get("appMarket").toString()) ? null : prompts.get("appMarket").toString()) + "','" + (Strings.isNullOrEmpty(prompts.get("marketingChannel").toString()) ? null : (prompts.get("marketingChannel").toString())) + "','" + ((Strings.isNullOrEmpty(prompts.get("watscoUsers").toString())) ? null : prompts.get("watscoUsers").toString()) + "','" + latitude + "','" + longitude + "','" + (prompts.get("gpsEnabled").toString().isEmpty() ? 0 : prompts.get("gpsEnabled")) + "','" + (prompts.get("pushEnabled").toString().isEmpty() ? 0 : prompts.get("pushEnabled")) + "','" + ((Strings.isNullOrEmpty(prompts.get("deviceOS").toString())) ? null : prompts.get("deviceOS")) + "','" + (Strings.isNullOrEmpty(prompts.get("ipAddress").toString()) ? null : prompts.get("ipAddress")) + "'";
            String QueryToPrint = "EXEC [UserDetails] " + (userId == 0 ? null : "'" + userId + "'") + "," + (CommonMethods.getPromptValues(prompts, "type") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "type") + "'") + "," + (CommonMethods.getPromptValues(prompts, "userName") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "userName") + "'") + "," + (buName == null ? null : "'" + buName + "'") + "," + (CommonMethods.getPromptValues(prompts, "zipCode") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "zipCode") + "'") + "," + (CommonMethods.getPromptValues(prompts, "branchNumber") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "branchNumber") + "'") + "," + (CommonMethods.getPromptValues(prompts, "regionId") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "regionId") + "'") + "," + (CommonMethods.getPromptValues(prompts, "appVersion") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "appVersion") + "'") + "," + (CommonMethods.getPromptValues(prompts, "deviceType") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "deviceType") + "'") + "," + (CommonMethods.getPromptValues(prompts, "preferredBranch") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "preferredBranch") + "'") + "," + (CommonMethods.getPromptValues(prompts, "firstName") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "firstName") + "'") + "," + (CommonMethods.getPromptValues(prompts, "lastName") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "lastName") + "'") + "," + (CommonMethods.getPromptValues(prompts, "companyName") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "companyName") + "'") + "," + (CommonMethods.getPromptValues(prompts, "companyAddress") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "companyAddress") + "'") + "," + (CommonMethods.getPromptValues(prompts, "emailId") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "emailId") + "'") + "," + (udid == null ? null : "'" + udid + "'") + "," + (CommonMethods.getPromptValues(prompts, "appLanguage") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "appLanguage") + "'") + "," + (CommonMethods.getPromptValues(prompts, "appMarket") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "appMarket") + "'") + "," + (CommonMethods.getPromptValues(prompts, "marketingChannel") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "marketingChannel") + "'") + "," + (CommonMethods.getPromptValues(prompts, "watscoUsers") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "watscoUsers") + "'") + "," + (latitude == null ? null : latitude) + "," + (longitude == null ? null : longitude) + "," + (CommonMethods.getPromptValues(prompts, "gpsEnabled") == null ? 0 : "'" + CommonMethods.getPromptValues(prompts, "gpsEnabled") + "'") + "," + (CommonMethods.getPromptValues(prompts, "pushEnabled") == null ? 0 : "'" + CommonMethods.getPromptValues(prompts, "pushEnabled") + "'") + "," + (CommonMethods.getPromptValues(prompts, "deviceOS") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "deviceOS") + "'") + "," + (CommonMethods.getPromptValues(prompts, "ipAddress") == null ? null : "'" + CommonMethods.getPromptValues(prompts, "ipAddress") + "'") + "";
            logger.info("====" + userId + "=====> getUserDetailsImpl() Report Query : " + QueryToPrint);
            cs = con.prepareCall(userDetails);

//            cs.setInt("USER_ID", userId);
//            cs.setString("Type", (prompts.get("type") == null ? "" : prompts.get("type")).toString());  // type 
//            cs.setString("USERNAME", (Strings.isNullOrEmpty(prompts.get("userName").toString())) ? null : (prompts.get("userName").toString())); // username
//            cs.setString("BU_NAME", buName);
//            cs.setString("ZIP_Code", (Strings.isNullOrEmpty(prompts.get("zipCode").toString()) ? null : (prompts.get("zipCode").toString()))); // zipcode
//            cs.setString("Branch_Number", (Strings.isNullOrEmpty(prompts.get("branchNumber").toString()) ? null : (prompts.get("branchNumber").toString()))); //branch Number
//            cs.setInt("Region_Id", checkNullorEmpty(prompts, "regionId"));  // region Id
//            cs.setString("APP_VERSION", (Strings.isNullOrEmpty(prompts.get("appVersion").toString()) ? null : (prompts.get("appVersion").toString())));  // app version
//            cs.setString("DEVICE_TYPE", (Strings.isNullOrEmpty(prompts.get("deviceType").toString()) ? null : (prompts.get("deviceType").toString())));  //device type
//            cs.setString("PREFERRED_BRANCH", (Strings.isNullOrEmpty(prompts.get("preferredBranch").toString()) ? null : (prompts.get("preferredBranch").toString())));  // preferredBranch
//            cs.setString("FIRST_NAME", (Strings.isNullOrEmpty(prompts.get("firstName").toString()) ? null : (prompts.get("firstName").toString())));  // firstname
//            cs.setString("LAST_NAME", (Strings.isNullOrEmpty(prompts.get("lastName").toString()) ? null : (prompts.get("lastName").toString())));  // lastname
//            cs.setString("COMPANY_NAME", (Strings.isNullOrEmpty(prompts.get("companyName").toString()) ? null : (prompts.get("companyName").toString())));  // company name
//            cs.setString("COMPANY_ADDRESS", (Strings.isNullOrEmpty(prompts.get("companyAddress").toString()) ? null : (prompts.get("companyAddress").toString())));  // company address
//            cs.setString("EMAIL_ID", (Strings.isNullOrEmpty(prompts.get("emailId").toString()) ? null : (prompts.get("emailId").toString())));  // company email Id
//            cs.setString("APP_ID", udid);  // app Id
//            cs.setString("APP_LANGUAGE", (Strings.isNullOrEmpty(prompts.get("appLanguage").toString()) ? null : prompts.get("appLanguage").toString()));  // app language
//            cs.setString("APP_MARKET", (Strings.isNullOrEmpty(prompts.get("appMarket").toString()) ? null : prompts.get("appMarket").toString()));  // app market
//            cs.setString("MARKETING_CHANNEL", (Strings.isNullOrEmpty(prompts.get("marketingChannel").toString()) ? null : (prompts.get("marketingChannel").toString())));  // marketting channel
//            cs.setString("Watsco_Users", ((Strings.isNullOrEmpty(prompts.get("watscoUsers").toString())) ? null : prompts.get("watscoUsers").toString()));  // watsco users
//            cs.setBigDecimal("latitude", latitude == null ? null : new BigDecimal(latitude));
//            cs.setBigDecimal("longitude", longitude == null ? null : new BigDecimal(longitude));
//            cs.setInt("gpsenabled", (Integer.parseInt(prompts.get("gpsEnabled") == null ? null : prompts.get("gpsEnabled").toString())));  // gps enabled
//            cs.setInt("pushenabled", (Integer.parseInt(prompts.get("pushEnabled") == null ? null : prompts.get("pushEnabled").toString())));  // push enabled
//            cs.setString("deviceOS", ((Strings.isNullOrEmpty(prompts.get("deviceOS").toString())) ? null : prompts.get("deviceOS")).toString());  // device OS
//            cs.setString("IPAddress", (Strings.isNullOrEmpty(prompts.get("ipAddress").toString()) ? null : prompts.get("ipAddress")).toString());  // IPAddress
            cs.setInt("USER_ID", userId);
            cs.setString("Type", (CommonMethods.getPromptValues(prompts, "type")));  // type 
            cs.setString("USERNAME", (CommonMethods.getPromptValues(prompts, "userName"))); // username
            cs.setString("BU_NAME", buName);
            cs.setString("ZIP_Code", (CommonMethods.getPromptValues(prompts, "zipCode"))); // zipcode
            cs.setString("Branch_Number", (CommonMethods.getPromptValues(prompts, "branchNumber"))); //branch Number
            cs.setInt("Region_Id", checkNullorEmpty(prompts, "regionId"));  // region Id
            cs.setString("APP_VERSION", (CommonMethods.getPromptValues(prompts, "appVersion")));  // app version
            cs.setString("DEVICE_TYPE", (CommonMethods.getPromptValues(prompts, "deviceType")));  //device type
            cs.setString("PREFERRED_BRANCH", (CommonMethods.getPromptValues(prompts, "preferredBranch")));  // preferredBranch
            cs.setString("FIRST_NAME", (CommonMethods.getPromptValues(prompts, "firstName")));  // firstname
            cs.setString("LAST_NAME", (CommonMethods.getPromptValues(prompts, "lastName"))); // lastname
            cs.setString("COMPANY_NAME", (CommonMethods.getPromptValues(prompts, "companyName")));  // company name
            cs.setString("COMPANY_ADDRESS", (CommonMethods.getPromptValues(prompts, "companyAddress")));  // company address
            cs.setString("EMAIL_ID", (CommonMethods.getPromptValues(prompts, "emailId")));  // company email Id
            cs.setString("APP_ID", udid);  // app Id
            cs.setString("APP_LANGUAGE", (CommonMethods.getPromptValues(prompts, "appLanguage"))); // app language
            cs.setString("APP_MARKET", (CommonMethods.getPromptValues(prompts, "appMarket")));  // app market
            cs.setString("MARKETING_CHANNEL", (CommonMethods.getPromptValues(prompts, "marketingChannel")));  // marketting channel
            cs.setString("Watsco_Users", (CommonMethods.getPromptValues(prompts, "watscoUsers"))); // watsco users
//            cs.setBigDecimal("latitude", ((CommonMethods.isNullorEmpty(prompts.getString("latitude"))) ? null :  new BigDecimal(latitude)));

            cs.setBigDecimal("latitude", latitude == null ? null : new BigDecimal(latitude));
            cs.setBigDecimal("longitude", longitude == null ? null : new BigDecimal(longitude));
            cs.setInt("gpsenabled", ((CommonMethods.isNullorEmpty(prompts.getString("gpsEnabled"))) ? 0 : Integer.parseInt(prompts.getString("gpsEnabled"))));  // gps enabled
            cs.setInt("pushenabled", ((CommonMethods.isNullorEmpty(prompts.getString("pushEnabled"))) ? 0 : Integer.parseInt(prompts.getString("pushEnabled"))));   // push enabled
            cs.setString("deviceOS", (CommonMethods.getPromptValues(prompts, "deviceOS")));  // device OS
            cs.setString("IPAddress", (CommonMethods.getPromptValues(prompts, "ipAddress")));  // IPAddress
            boolean status = cs.execute();
            if (status) {
                userRS = cs.getResultSet();

                DocumentConvertor dc = new DocumentConvertor();
                response = dc.toJSONConvertor(userRS);
                jObject.put("STATUS", "Success");
                jObject.put("RESULT", new JSONObject(response));
                logger.info("=======" + userId + "===== getUserDetailsImpl() Response : " + jObject.toString());
                return jObject.toString();
            } else {
                warning = cs.getWarnings();
                if (warning != null) {
                    jObject.put("STATUS", warning.toString());
                    jObject.put("RESULT", new JSONObject());
                    logger.info("=======" + userId + "===== getUserDetailsImpl() Response : " + jObject.toString());
                    return jObject.toString();
                }
                return null;
            }
        } catch (NumberFormatException e) {
            logger.error("======" + userId + "===>Exeption in UserDetailsImpl() : " + e);
            jObject.put("STATUS", e.getMessage().toString());
            return jObject.toString();
        } catch (Exception e) {
            logger.error("======" + userId + "===>Exeption in UserDetailsImpl() : ", e);
            jObject.put("STATUS", e.getMessage().toString());
            return jObject.toString();
        }
    }

    public int checkNullorEmpty(JSONObject prompts, String columnName) throws JSONException, NumberFormatException {

        int paramValue = 0;
        if (!Strings.isNullOrEmpty(prompts.get(columnName).toString())) {
            if (!CommonMethods.isNumeric(prompts.get(columnName).toString())) {
                paramValue = 0;
            } else {
                paramValue = Integer.parseInt(prompts.get(columnName).toString());
            }
        }
        return paramValue;
    }

    public String getUserBranchUpdateImpl(int installId, int userId, String udid, int reportId, HashMap<Object, Object> keyValue) throws JSONException {
        // TODO Auto-generated method stub
        Connection con = null;
        SQLWarning warning;
        JSONObject jObject = new JSONObject();
        ResultSet userUpdateRS;
        Statement stmt;
        try {
            con = DBConnection.getConnection(true);

            String branchUpdate = "EXEC [UserBranchUpdate] '" + userId + "','" + udid + "','" + keyValue.get("buWid") + "','" + keyValue.get("branchNo") + "','" + (keyValue.get("zipCode").toString().isEmpty() ? null : keyValue.get("zipCode")) + "'";
            logger.info("====" + userId + "=====> getUserBranchUpdateImpl() Report Query : " + branchUpdate);
            stmt = con.createStatement();
            stmt.executeUpdate(branchUpdate);

            warning = stmt.getWarnings();

            if (warning != null) {
                jObject.put("STATUS", warning.toString());
                logger.info("====" + userId + "=====> getUserBranchUpdateImpl() Response : " + jObject.toString());
                return jObject.toString();
            } else {
                jObject.put("STATUS", "Success");
                logger.info("====" + userId + "=====> getUserBranchUpdateImpl() Response : " + jObject.toString());
                return jObject.toString();
            }

        } catch (Exception e) {
            e.getMessage();
            jObject.put("STATUS", e.getMessage());
            return jObject.toString();
        }

    }

}
