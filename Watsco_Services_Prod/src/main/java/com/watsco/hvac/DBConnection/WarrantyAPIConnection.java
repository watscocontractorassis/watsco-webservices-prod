package com.watsco.hvac.DBConnection;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
//import org.json.JSONObject;

import com.watsco.hvac.cache.CacheManager;
import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.NewWarrantyClaimXML;
import com.watsco.hvac.dao.SoapConnection;
import com.watsco.hvac.dao.WarrantyDocumentConvertor;
import com.watsco.hvac.dto.ClaimDTO;
import com.watsco.hvac.impl.SubmitClaimImpl;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.JSONExtractor;
import com.watsco.hvac.utils.LocalCommonUtil;
import org.json.JSONArray;
import org.json.JSONObject;

public class WarrantyAPIConnection {

    private static Logger logger = Logger.getLogger(WarrantyAPIConnection.class);

    public void insertUserCredentials(String provider, int installId,
            int userId, String udid, int reportId, String buName, String SB_ID,
            String UId, String password, String name, String email, String companyName, HashMap<Object, Object> keyvalue) {
        // TODO Auto-generated method stub

        Connection con;
        String sqlQuery;
        Statement stmt = null;

        try {

            con = DBConnection.getConnection(false);

            sqlQuery = "EXEC Warranty_User_Details '" + keyvalue.get("provider") + "','" + keyvalue.get("userId") + "','" + keyvalue.get("buName") + "','" + keyvalue.get("UId") + "','" + keyvalue.get("password") + "','" + keyvalue.get("email") + "','" + keyvalue.get("SB_ID") + "','" + keyvalue.get("name") + "','" + keyvalue.get("companyName") + "'";

            logger.info("===USER ID :" + userId + "===insertUserCredentials()====>Report Query : " + sqlQuery);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            stmt.executeUpdate(sqlQuery);

        } catch (Exception e) {
            logger.error("=== USER ID :" + userId + "======>Warranty API : Claim Settings : " + e.getMessage().toString());
        }

    }

    public String getNewWarrantyClaimAPIInfo(String buName, int installId, int userId, String udid, int reportId, String type, String partNumber, UriInfo uriInfo) {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String response = "";
        String sqlQuery = null;
        try {

            con = DBConnection.getConnection(false);

            String[] keyValue = CacheManager.getCacheValueUsingQueryParams(uriInfo, null);

            if (keyValue != null) {
                response = keyValue[1];
            }

            if (response == null) {

                if (type.equalsIgnoreCase("Defect")) {
                    sqlQuery = "EXEC Warranty_Component_Defect '" + buName + "', '" + partNumber + "'";
                } else {
                    sqlQuery = "EXEC Warranty_Dropdown '" + buName + "'";
                }

                logger.info("===USER ID :" + userId + "===getNewWarrantyClaimAPIInfo()====>Report Query : " + sqlQuery);
                pstmt = con.prepareStatement(sqlQuery);
                rs = pstmt.executeQuery();

                response = NewWarrantyClaimXML.getWarrantyClaimInfoDocument(rs);
                logger.info("=== USER ID :" + userId + "====>getNewWarrantyClaimAPIInfo() : " + response);

                CacheManager.setCache(keyValue[0], response);
//	            CacheManager.getCacheValueUsingQueryParams(uriInfo);

            } else {
                if (type.equalsIgnoreCase("Defect")) {
                    sqlQuery = "EXEC Warranty_Component_Defect '" + buName + "', '" + partNumber + "'";
                } else {
                    sqlQuery = "EXEC Warranty_Dropdown '" + buName + "'";
                }

                if (CacheManager.isWriteCacheToLogEnable()) {
                    logger.info("===USER ID :" + userId + "===getNewWarrantyClaimAPIInfo()====>Report Query : " + sqlQuery);
//                    logger.info("CACHE====" + userId + "====>Report Query : " + sqlQuery);
                }
            }

        } catch (Exception e) {
            logger.error("=== USER ID :" + userId + "======>Exception in getNewWarrantyClaimDropDownsAPIInfo() : " + e);
        }
        return response;
    }

    public String getViewClaimAuthentication(String referenceNumber, String buName, int installId,
            int userId, String udid, int reportId) {
        Connection con = null;
        ResultSet view_claim_rs = null;
        ResultSet view_claimpart_rs = null;
        String response = null;
        Statement stmt = null;
        try {
            con = DBConnection.getConnection(false);

            String viewClaim = "EXEC Warranty_ViewClaim '" + referenceNumber + "','" + userId + "'";
            viewClaim = viewClaim.replaceAll("'null'", null);
            logger.info("===USER ID :" + userId + "===getViewClaimAuthentication()====>Report Query : " + viewClaim);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            view_claim_rs = stmt.executeQuery(viewClaim);

            WarrantyDocumentConvertor dom = new WarrantyDocumentConvertor();

            if (!referenceNumber.equalsIgnoreCase("")) {
                String viewClaimParts = "EXEC Warranty_ViewClaimParts '" + referenceNumber + "','" + userId + "'";
                
                viewClaimParts = viewClaimParts.replaceAll("'null'", null);
                logger.info("===USER ID :" + userId + "===getViewClaimAuthentication()====>Report Query : " + viewClaimParts);
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                view_claimpart_rs = stmt.executeQuery(viewClaimParts);
                response = dom.RSStoVRJSONConvertor(view_claim_rs, view_claimpart_rs);
            } else {

                response = dom.JSONConvertor(view_claim_rs);
            }

            logger.info("=== USER ID :" + userId + "==>getViewClaimAuthentication() Response : " + response);

        } catch (Exception e) {
            logger.error("=========>Exeption in getViewClaimAuthentication() : " + e);

        } finally {
        }
        return response;

    }

    public String getRetreiveClaimAuthentication(String retrievalCode, String buName, int installId, int userId, String udid, int reportId) {

        Connection con = null;
        ResultSet retreive_claim_rs = null;
        ResultSet retreive_claimpart_rs = null;
        String response = null;
        Statement stmt = null;
        try {
            con = DBConnection.getConnection(false);

            String retreiveClaim = "EXEC Warranty_RetreiveClaim '" + retrievalCode + "'";
            logger.info("===USER ID :" + userId + "===getRetreiveClaimAuthentication()====>Report Query : " + retreiveClaim);
            
//            cs = con.prepareCall(retreiveClaim);
//            retreive_claim_rs = cs.executeQuery();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            retreive_claim_rs = stmt.executeQuery(retreiveClaim);

            String retreiveClaimParts = "EXEC Warranty_RetreiveClaimParts '" + retrievalCode + "'";
            logger.info("===USER ID :" + userId + "===getRetreiveClaimAuthentication()====>Report Query : " + retreiveClaimParts);
//            cs = con.prepareCall(retreiveClaimParts);
//            retreive_claimpart_rs = cs.executeQuery();
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            retreive_claimpart_rs = stmt.executeQuery(retreiveClaimParts);

            WarrantyDocumentConvertor dom = new WarrantyDocumentConvertor();
            response = dom.RSStoVRJSONConvertor(retreive_claim_rs, retreive_claimpart_rs);
            logger.info("=== USER ID :" + userId + "====>getRetreiveClaimAuthentication() Response : " + response);

        } catch (Exception e) {
            logger.error("=== USER ID :" + userId + "======>Exeption in getRetreivalClaimAuthentication() : " + e);

        } finally {
        }
        return response;
    }

    public String getSmartyStreetsData(int installId, int userId, String udid,
            int reportId, String buName, String street, String street2,
            String city, String state, String zipCode) {
        // TODO Auto-generated method stub
        StringBuilder sb = new StringBuilder();
        String inputLine;

        try {

//			String AddressURL = "https://api.smartystreets.com/street-address?auth-id=5a259f9f-e0e1-40c3-9e52-287f6d09a432&auth-token=bW3wsXK6A32AQRVoWgF0&addressee=undefined&street="+address+"&city="+city+"&state="+state+"&zipcode="+zipCode+"&candidates="+candidates+"";
            String AddressURL = "https://api.smartystreets.com/street-address?auth-id=5a259f9f-e0e1-40c3-9e52-287f6d09a432&auth-token=bW3wsXK6A32AQRVoWgF0&street=" + street + "&street2=" + street2 + "&city=" + city + "&state=" + state + "&zipcode=" + zipCode;
            AddressURL = AddressURL.replace(" ", "%20");
            logger.info("=== USER ID :" + userId + "==>Smarty Streets URL : " + AddressURL);
            URL url = new URL(AddressURL);
            SoapConnection soapConnection = new SoapConnection();
            inputLine = soapConnection.soapget(url, "");
            return inputLine;

        } catch (Exception e) {
            logger.error(" SmartyStreets====>" + e.toString());
        }
        UsageTracking usageTracking = new UsageTracking();
        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, "Smarty Streets Successfull", "", "");

        return null;
    }

    public String getMandrillAPI(String jsonstring) {

        StringBuilder sb = new StringBuilder();
        InputStream inStream;
        String retString = "";
        try {
            String url = "https://mandrillapp.com/api/1.0/messages/send.json";
            url = url.replace(" ", "%20");
            URL mandrillURL = new URL(url);
            logger.info("=========>Mandrill API  URL : " + url);

            HttpsURLConnection httpsCon = (HttpsURLConnection) mandrillURL.openConnection();
            httpsCon.setRequestProperty("User-Agent", "Mozilla/5.0");
            httpsCon.setDoInput(true);
            httpsCon.setDoOutput(true);
            httpsCon.setUseCaches(false);
            httpsCon.setRequestMethod("POST");
            httpsCon.setRequestProperty("Content-Type", "application/xml,application/json");
            httpsCon.setRequestProperty("Accept", "application/json");

            try {
                DataOutputStream outputStream = new DataOutputStream(httpsCon.getOutputStream());
                outputStream.writeBytes(jsonstring);
                outputStream.flush();
            } catch (Exception e) {
                e.printStackTrace();

            }
            if (httpsCon.getResponseCode() >= 400) {
                inStream = httpsCon.getErrorStream();
            } else {
                inStream = httpsCon.getInputStream();
            }
            retString = is2Str(inStream);

            return retString;
        } catch (Exception e) {
            e.printStackTrace();

        }
        // TODO Auto-generated method stub
        return null;
    }

    public String is2Str(InputStream is) throws IOException {
        StringBuffer ret = new StringBuffer();
        if (is != null) {
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    ret.append(buffer, 0, n);
                }
            } finally {
                is.close();
            }
        }

        return ret.toString();
    }

    public String getUnSubmittedClaim(int userId) {
        // TODO Auto-generated method stub
        Connection con = null;
        ResultSet unSubmitClaimRS = null;
        String response = null;
        Statement stmt = null;

        try {
            con = DBConnection.getConnection(false);

            String unSubmitClaim = "EXEC Warranty_Unsubmitted_Claim_Count '" + userId + "'";
            logger.info("=== USER ID :" + userId + "===getUnSubmittedClaim()====>Report Query : " + unSubmitClaim);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            unSubmitClaimRS = stmt.executeQuery(unSubmitClaim);

            WarrantyDocumentConvertor dom = new WarrantyDocumentConvertor();
            response = dom.toJSONConvertor(unSubmitClaimRS);
            logger.info("=== USER ID :" + userId + "==>getUnSubmittedClaim() Response : " + response);

        } catch (Exception e) {
            logger.error("=== USER ID :" + userId + "====>Exeption in getUnSubmittedClaim() : " + e);

        }
        return response;

    }

    public String deleteClaim(String serialNumber, String referenceNumber) {
        // TODO Auto-generated method stub
        Connection con = null;
        String response = null;
        Statement stmt = null;
        try {
            con = DBConnection.getConnection(false);

            String deleteClaim = "EXEC Warranty_Claims_Delete '" + serialNumber + "','" + referenceNumber + "'";
            logger.info("=====deleteClaim()====>Report Query : " + deleteClaim);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            stmt.executeUpdate(deleteClaim);

        } catch (Exception e) {
            logger.error("=========>Exeption in deleteClaim() : " + e);

        }
        return response;

    }

    public String getRetrieveClaimPortalData(int buWid, String buName, String SB_ID, String UId, String password, String isEnable, String retrievalCode) {

        Connection con = null;
        ResultSet retreive_claim_portal = null;
        ResultSet retreive_claim_parts_portal = null;
        String response = null;
        Statement stmt = null;
        String isDev = "";
        String submitResponse = "";
        String returnType = "";
        int installId = 0;
        int userId = -443;
        String udid = "";
        int reportId = 0;
        String save = "N";
        String nullColumn = "";
        StringBuffer sb = new StringBuffer();
        String s;
        ClaimDTO claimPortalDTO = null;
        UsageTracking usageTracking = new UsageTracking();
        try {

            HashMap<String, String> exthm;

            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                exthm = CommonUtil.getExternalAPIConfigProperties(buName);
            } else {
                exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
            }

            isDev = exthm.get("CE_PORTAL_ISDEV");

            con = DBConnection.getConnection(false);

            if (retrievalCode.length() > 0) {

                String retreiveClaimPortal = "EXEC [Warranty_Portal_RetreiveClaim] '" + retrievalCode + "','" + isEnable + "','" + buName + "'";
                
                retreiveClaimPortal = retreiveClaimPortal.replaceAll("'null'", null);
                logger.info("=== USER ID :" + userId + "===getRetrieveClaimPortalData()====>Report Query : " + retreiveClaimPortal);
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                retreive_claim_portal = stmt.executeQuery(retreiveClaimPortal);

                String retreiveClaimPartsPortal = "EXEC [Warranty_Portal_RetreiveClaimParts] '" + retrievalCode + "','" + isEnable + "','" + buName + "'";
                retreiveClaimPartsPortal = retreiveClaimPartsPortal.replaceAll("'null'", null);
                logger.info("=== USER ID :" + userId + "===getRetrieveClaimPortalData()====>Report Query : " + retreiveClaimPartsPortal);
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                retreive_claim_parts_portal = stmt.executeQuery(retreiveClaimPartsPortal);

                WarrantyDocumentConvertor dom = new WarrantyDocumentConvertor();
                response = dom.portalJsonConvertor(retreive_claim_portal, retreive_claim_parts_portal);
                logger.info("=== USER ID :" + userId + "=====>getRetrieveClaimPortalData() Response : " + response);

                usageTracking.usageTrackingSp(installId, userId, udid, 40, buName, "WebPortalRetrieve", "", "");

                if (response.equals("[]")) {
                    JSONObject jObj = new JSONObject();
                    jObj.put("Claim Status", "NO Data Found");
                    return jObj.toString();
                }

                if (response.length() > 0) {
                    response = response.substring(1, response.length() - 1);
                    if (response.contains("nullColumnsFromClaim")) {

                        JSONObject jObject = new JSONObject(response);
                        nullColumn = jObject.getString("nullColumnsFromClaim");

                    } else {
                        sb.append("");
                    }

                    claimPortalDTO = JSONExtractor.jsonExtractorNPortal(response);
                    SubmitClaimImpl submitClaimImpl = new SubmitClaimImpl();

                    submitResponse = submitClaimImpl.getSubmitClaimAuthentication(claimPortalDTO, installId, userId, udid, reportId, buName, returnType, SB_ID, UId, password, save, isDev, buWid, "");

                    if (!submitResponse.contains("API failed")) {
                        JSONObject jObject = new JSONObject(submitResponse);
                        StringBuffer sbClaim = new StringBuffer();
                        sbClaim.append("{\n");
                        s = jObject.getString("Claim_WID");
                        sbClaim.append("\"Claim WID\" : \"" + s + "\",\n");
                        s = jObject.getString("claimStatus");

                        if (s.equalsIgnoreCase("")) {
                            s = "Claim Transfer to ServiceBench Failed.";
                        }

                        sbClaim.append("\"Claim Status\" : \"" + s + "\",\n");
                        s = jObject.getString("claimNumber");
                        sbClaim.append("\"Claim Number\" : \"" + s + "\",\n");
                        s = jObject.getString("serialNumber");
                        sbClaim.append("\"Serial Number\" : \"" + s + "\",\n");
                        s = jObject.getString("referenceNumber");
                        sbClaim.append("\"Reference Number\" : \"" + s + "\",\n");
                        s = jObject.getString("errorText");
                        sbClaim.append("\"Error Text\" : \"" + s + "\",\n");
                        s = jObject.getString("rejectReason");
                        sbClaim.append("\"Reject Reason\" : \"" + s + "\",\n");
                        sbClaim.append("\"Default Values\" :\"" + nullColumn + "\"\n");
                        sbClaim.append("}");

                        usageTracking.usageTrackingSp(installId, userId, udid, 40, buName, "WebPortalSubmit", "", "");
                        return sbClaim.toString();
                    } else {
                        usageTracking.usageTrackingSp(installId, userId, udid, 40, buName, "WebPortalRetrieve Error", "", "");
                        return "{ \"Claim Status\": \"Invalid Credentials Please Check\" }";
                    }
                }
            }

            usageTracking.usageTrackingSp(installId, userId, udid, 40, buName, "WebPortalRetrieve Error", "", "");
            return "{\"Claim Status\": \"Please Enter Valid Retrieval Claim Code\"}";
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("=== USER ID :" + userId + "====>Exeption in getRetrieveClaimPortalData() : " + e);
            return e.toString();
        }

    }

    public String warrantyUpdate(String buName, int installId, int userId, String udid, int reportId, String type,
            String value, String claim_WID) {
        String response = "";
        String warrantyUpdate = "";
        ResultSet warrantyUpdateRS;
        Connection con;
        Statement stmt;
        try {
            con = DBConnection.getConnection(false);

            warrantyUpdate = "EXEC [Warranty_Claims_Field_Update] '" + type + "','" + value + "','" + claim_WID + "'";
            
            warrantyUpdate = warrantyUpdate.replaceAll("'null'", null);
            logger.info("=== USER ID :" + userId + "===warrantyUpdate()====>Report Query : " + warrantyUpdate);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            warrantyUpdateRS = stmt.executeQuery(warrantyUpdate);

            WarrantyDocumentConvertor dom = new WarrantyDocumentConvertor();
            response = dom.JSONConvertor(warrantyUpdateRS);
            logger.info("=== USER ID :" + userId + "==>Warranty Field Update Response : " + response);
            return response;

        } catch (Exception e) {
            logger.error("=== USER ID :" + userId + "====>Exeption in warrantyUpdate() : " + e);
            return e.toString();
        }

    }

    public String manufacturersList(String buName, int userId) {

        String response = "";
        String manufacturersList = "";
        ResultSet manufacturersListRS;
        Connection con;
        Statement stmt;
        try {
            con = DBConnection.getConnection(false);

            manufacturersList = "EXEC [WarrantyPolicy_List] ";
            logger.info("====" + userId + "===manufacturersList()====>Report Query : " + manufacturersList);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            manufacturersListRS = stmt.executeQuery(manufacturersList);

            WarrantyDocumentConvertor dom = new WarrantyDocumentConvertor();
            response = dom.toManufacturersJSONConvertor(manufacturersListRS);
            logger.info("=== USER ID :" + userId + "==>Manufacturers List Response : " + response);
            return response;

        } catch (Exception e) {
            logger.error("=== USER ID :" + userId + "====>Exeption in Manufacturers List : " + e);
            return e.toString();
        }
    }

    public String getManufacturersPolicy(String buName, int userId, String manufacturer) {
        String response = "";
        String manufacturersPolicy = "";
        ResultSet manufacturersPolicyRS;
        Connection con;
        Statement stmt;
        JSONObject jObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            con = DBConnection.getConnection(false);

            manufacturersPolicy = "EXEC [WarrantyPolicy_Details] '" + manufacturer + "','" + buName + "'";
            logger.info("=== USER ID :" + userId + "===getManufacturersPolicy()====>Report Query : " + manufacturersPolicy);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            manufacturersPolicyRS = stmt.executeQuery(manufacturersPolicy);

            WarrantyDocumentConvertor dom = new WarrantyDocumentConvertor();
            response = dom.toJSONDocument(manufacturersPolicyRS);

            System.out.println("response" + response);

            if (response != null) {
                jObject.put("Result", new JSONObject(response));
                jObject.put("STATUS", "Success");
            } else {
                jsonArray = new JSONArray();
                jObject.put("Result", new JSONObject());
                jObject.put("STATUS", "Failure");
            }

            logger.info("=== USER ID :" + userId + "==>Manufacturers Policy Response : " + jObject.toString());
            return jObject.toString();

        } catch (Exception e) {
            logger.error("=== USER ID :" + userId + "====>Exeption in Manufacturers Policy Method : " + e);
            return e.toString();
        }
    }

}
