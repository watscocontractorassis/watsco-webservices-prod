package com.watsco.hvac;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;

import com.watsco.hvac.impl.PTCalculatorImpl;
import com.watsco.hvac.utils.URLGenerationUtility;
import java.io.IOException;
import java.io.StringWriter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

@Path("/PTCalculator")
public class PTCalculator {

    private static Logger logger = Logger.getLogger(PTCalculator.class);

    @POST
    @Path("/Refrigerator")
    @Consumes(MediaType.TEXT_XML)
    public Response PTCalculatorResponse(InputStream incomingData,
            @QueryParam("returnType") String returnType,
            @QueryParam("userId") int userId, @Context UriInfo uriInfo) throws TransformerException, ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document doc = parser.parse(incomingData);
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("POST", "PTCalculator", "Refrigerator", uriInfo, writer.toString());

        //		logger.info("=============>PTCalculator PTCalculatorResponse method invoked");
        StringBuilder tempBuilder = new StringBuilder();
        String response = "";

        synchronized (this) {
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
                String line = null;

                while ((line = in.readLine()) != null) {
                    tempBuilder.append(line);
                }

                logger.info("====" + userId + "===PTCalculatorResponse()====>PTCalculator Data Received: " + tempBuilder.toString());
                PTCalculatorImpl ptCalculatorImpl = new PTCalculatorImpl();
                response = ptCalculatorImpl.getRefrigerantInfo(tempBuilder.toString(), returnType, userId);
            } catch (Exception e) {
                logger.info("====" + userId + "===PTCalculatorResponse()====>Error Parsing: - ");
            }
        }

        return Response.status(200).entity(response).build();

    }

}
