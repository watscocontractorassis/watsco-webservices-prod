package com.watsco.hvac.dto;

import java.util.ArrayList;
import java.util.HashMap;

public class ClaimDTO {
	
	private  HashMap<String,String> claimMap;
	private  ArrayList<HashMap<String,String>> alClaimParts;
	
	public  ArrayList<HashMap<String,String>> getAlClaimParts() {
		return alClaimParts;
	}
	
	public  void setAlClaimParts(ArrayList<HashMap<String,String>> alClaimParts) {
		this.alClaimParts = alClaimParts;
	}
	
	public  HashMap<String,String> getClaimMap() {
		return claimMap;
	}
	
	public  void setClaimMap(HashMap<String,String> claimMap) {
		this.claimMap = claimMap;
	}
	
	public void makeEmpty(){
		alClaimParts = null;
		claimMap = null;
	}

}
