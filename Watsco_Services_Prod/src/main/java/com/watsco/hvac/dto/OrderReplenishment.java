package com.watsco.hvac.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

//@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "productId",
    "productQty",
    "productName",
    "productSku",
    "erpItem"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderReplenishment {

    @JsonProperty("productId")
    private String productId;
    
    @JsonProperty("productQty")
    private String productQty;
    
    @JsonProperty("productName")
    private String productName;
    
    @JsonProperty("productSku")
    private String productSku;
    
    @JsonProperty("erpItem")
    private String erpItem;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return The productId
     */
    @JsonProperty("productId")
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId The productId
     */
    @JsonProperty("productId")
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return The productQty
     */
    @JsonProperty("productQty")
    public String getProductQty() {
        return productQty;
    }

    /**
     *
     * @param productQty The productQty
     */
    @JsonProperty("productQty")
    public void setProductQty(String productQty) {
        this.productQty = productQty;
    }

    /**
     *
     * @return The productName
     */
    @JsonProperty("productName")
    public String getProductName() {
        return productName;
    }

    /**
     *
     * @param productName The productName
     */
    @JsonProperty("productName")
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     *
     * @return The productSku
     */
    @JsonProperty("productSku")
    public String getProductSku() {
        return productSku;
    }

    /**
     *
     * @param productSku The productSku
     */
    @JsonProperty("productSku")
    public void setProductSku(String productSku) {
        this.productSku = productSku;
    }

    @JsonProperty("erpItem")
    public String getErpItem() {
        return erpItem;
    }

    @JsonProperty("erpItem")
    public void setErpItem(String erpItem) {
        this.erpItem = erpItem;
    }
    
  

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
