package com.watsco.hvac.dto;

public class WarrantyResponseDTO {
	   private static String  response ="";
	   public WarrantyResponseDTO(){
		   
	   }
	   public static void setInQuiryResponse(String response){
		   WarrantyResponseDTO.response = response;
	   }
	   
	   public static String getInQuiryResponse(){
		   return response;
	   }
	
	   public static void setSubmitClaimResponse(String response){
		   WarrantyResponseDTO.response = response;
	   }
	   
	   public static String getSubmitClaimResponse(){
		   return response;
	   }
	   
	   public static void makeSubmitClaimNULL()
	   {
		   response = null;
	   }
	   
	   public static void makeInQuiryClaimNULL()
	   {
		   response = null;
	   }
}


