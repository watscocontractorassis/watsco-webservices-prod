package com.watsco.hvac.dto;

public class ClaimPartsDTO {


    private String claimParts;
    String failedPartNumber;
    String failedPartQuantity;
    String failedPartSerial;
    String failedPartInstallDate;
    String partNumber;
    String partQuantity; 
    String partSerialNumber;
    String partAmount; 
    String partInvoiceNumber;
    String partSequence;

    String casual_part;
    public String getClaimParts() {
		return claimParts;
	}
	public void setClaimParts(String claimParts) {
		this.claimParts = claimParts;
	}
	public String getCasual_part() {
		return casual_part;
	}
	public void setCasual_part(String casual_part) {
		this.casual_part = casual_part;
	}
	public String getFailedPartNumber() {
		return failedPartNumber;
	}
	public void setFailedPartNumber(String failedPartNumber) {
		this.failedPartNumber = failedPartNumber;
	}
	public String getFailedPartQuantity() {
		return failedPartQuantity;
	}
	public void setFailedPartQuantity(String failedPartQuantity) {
		this.failedPartQuantity = failedPartQuantity;
	}
	public String getFailedPartSerial() {
		return failedPartSerial;
	}
	public void setFailedPartSerial(String failedPartSerial) {
		this.failedPartSerial = failedPartSerial;
	}
	public String getFailedPartInstallDate() {
		return failedPartInstallDate;
	}
	public void setFailedPartInstallDate(String failedPartInstallDate) {
		this.failedPartInstallDate = failedPartInstallDate;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getPartQuantity() {
		return partQuantity;
	}
	public void setPartQuantity(String partQuantity) {
		this.partQuantity = partQuantity;
	}
	public String getPartSerialNumber() {
		return partSerialNumber;
	}
	public void setPartSerialNumber(String partSerialNumber) {
		this.partSerialNumber = partSerialNumber;
	}
	public String getPartAmount() {
		return partAmount;
	}
	public void setPartAmount(String partAmount) {
		this.partAmount = partAmount;
	}
	public String getPartInvoiceNumber() {
		return partInvoiceNumber;
	}
	public void setPartInvoiceNumber(String partInvoiceNumber) {
		this.partInvoiceNumber = partInvoiceNumber;
	}
	public String getPartSequence() {
		return partSequence;
	}
	public void setPartSequence(String partSequence) {
		this.partSequence = partSequence;
	}
	
}
