package com.watsco.hvac.dto;

public class ReportDataDTO {
	
	String ipAddress;
	String BU_CODE;
	String pid;
	String cid;
	String device;
	
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getBU_CODE() {
		return BU_CODE;
	}
	public void setBU_CODE(String bU_CODE) {
		BU_CODE = bU_CODE;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}

}
