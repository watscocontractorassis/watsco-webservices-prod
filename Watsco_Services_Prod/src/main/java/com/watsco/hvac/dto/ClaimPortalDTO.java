package com.watsco.hvac.dto;

import java.util.ArrayList;
import java.util.HashMap;

public class ClaimPortalDTO {
	
	private static HashMap<String,String> claimMap;
	private static ArrayList<HashMap<String,String>> alClaimParts;
	
	public static ArrayList<HashMap<String,String>> getAlClaimParts() {
		return alClaimParts;
	}
	
	public static void setAlClaimParts(ArrayList<HashMap<String,String>> alClaimParts) {
		ClaimPortalDTO.alClaimParts = alClaimParts;
	}
	
	public static HashMap<String,String> getClaimMap() {
		return claimMap;
	}
	
	public static void setClaimMap(HashMap<String,String> claimMap) {
		ClaimPortalDTO.claimMap = claimMap;
	}

}
