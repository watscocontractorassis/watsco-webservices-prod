package com.watsco.hvac.dto;

public class ChannelDTO {
	
	private int channelWid;
	private String channelName;
	
	public ChannelDTO(){
		
	}
	
	public int getChannelWid() {
		return channelWid;
	}
	public void setChannelWid(int channelWid) {
		this.channelWid = channelWid;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	
}
