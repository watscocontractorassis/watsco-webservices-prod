package com.watsco.hvac.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import com.watsco.hvac.common.AppConstants;
import com.watsco.hvac.dao.CEResultAPIXml;
import com.watsco.hvac.dao.RheemResultAPIXml;
import com.watsco.hvac.dao.SoapConnection;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.EncyrptionUtils;
import com.watsco.hvac.utils.LocalCommonUtil;
import java.io.IOException;
import static org.apache.commons.lang3.StringEscapeUtils.escapeJava;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class WarrantyAPI {

//    protected static Logger logger = Logger.getLogger(WarrantyAPI.class);
    private static final Logger logger = Logger.getLogger(WarrantyAPI.class);

    public String getRheemWarrantyVerificationResponse(String manufacturer, String serialNo, String postalCode, int installId,
            int userId, String udid, int reportId, String buName, String registered) throws TransformerException {
        /*   String response = null;
         //		String rheemUrl = "http://api.myrheem.com:8105/v1/models/warranty/{serialNumber}?serialNumber="+serialNo;
         String rheemUrl = "https://io.myrheem.com/v1/public/warranties/printcertificate?serialNumber=" + serialNo;
         SoapConnection soapConnection = new SoapConnection();

         try {
         logger.info("====" + userId + "=====>RequestURL : " + rheemUrl);
         String apiResponse = soapConnection.sendWarrantyRheemGetRequest(rheemUrl);
         //            String apiResponse = soapConnection.soapget(new URL(rheemUrl), "");

         response = RheemResultAPIXml.getRheemResultXML(apiResponse, serialNo,
         postalCode, installId, userId, udid, reportId, buName, registered);

         //            logger.info("=== USER ID :" + userId + "=====>getRheemWarrantyVerificationResponse() : " + response);
         } catch (Exception e) {
         // TODO Auto-generated catch block
         logger.error("=== USER ID :" + userId + "=====>Exception : " + e);
         }

         return response;*/
        String apiResponse = null;
        String response = null;
        try {
            SoapConnection soapConnection = new SoapConnection();
            String key = "";

            switch (buName) {
                case "CARRIER ENTERPRISE":
                    key = "CE_";
                    break;
                case "BAKER":
                    key = "BK_";
                    break;
                case "CARRIER ENTERPRISE MEXICO":
                    key = "CE_MX_";
                    break;
                case "GEMAIRE":
                    key = "GM_";
                    break;
                case "EAST COAST METALS":
                    key = "ECM_";
                    break;
                default:
                    break;
            }
            String accessToken = "";
            HashMap<String, String> exthm;
            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                exthm = CommonUtil.getExternalAPIConfigProperties(buName);
            } else {
                exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
            }
            HashMap<String, String> hm;

            String accessTokenAPI;
            String clientID = exthm.get(key + "RHEEM_CLIENT_ID");
            String accessTokenURL = exthm.get(key + "RHEEM_ACCESSTOKEN_URL");
            String rheemUrl = exthm.get(key + "RHEEM_URL");

            if (clientID == null || accessTokenURL == null || rheemUrl == null) {
                logger.error("====User ID: " + userId + "===>API Not Found");
                return response;
            }

            serialNo = serialNo.replaceAll("%20", "");
            postalCode = postalCode.replaceAll("%20", "");
            accessTokenAPI = accessTokenURL + "v1/oauth2/authorize?response_type=token&client_id=" + clientID.trim();
            String certificateAPI = rheemUrl + "/v1/warranty/certificate?SerialNumber=" + serialNo.trim() + "&IncludePrintedCertificate=true";

            logger.info("UserID :" + userId + "====Warranty Token Request : " + accessTokenAPI);

            logger.info("UserID :" + userId + "====Warranty Certificate API Request : " + certificateAPI);

            accessToken = getAccessToken(accessTokenAPI);

            if (accessToken != null && accessToken.length() > 0) {
                apiResponse = soapConnection.sendWarrantyRheemGetRequest(certificateAPI, accessToken, clientID);
//                response = soapConnection.soapget(new URL(certificateAPI), accessToken);
            }

            response = RheemResultAPIXml.getRheemResultXML(apiResponse, serialNo.trim(), postalCode.trim(), installId, userId, udid, reportId, buName, registered);

        } catch (MalformedURLException e) {
            logger.error("====UserID :" + userId + "===MalformedURLException : " + e.getMessage());
        } catch (IOException e) {
            logger.error("====UserID :" + userId + "===IOException : " + e.getMessage());
        } catch (Exception e) {
            logger.error("====UserID :" + userId + "===Exception : " + e.getMessage());
        }

        return response;

    }

    /**
     * This method will get the access token from API and also update in in
     * config file so that if user is trying to get within x duration it will
     * not hit API again and take it from config file
     *
     * @param url
     * @return access Token
     * @throws MalformedURLException
     * @throws IOException
     * @throws Exception
     */
    public String getAccessToken(String url) throws MalformedURLException, IOException, Exception {
        String access_token = "";

        SoapConnection soapConnection = new SoapConnection();

        String response = soapConnection.soapget(new URL(url), "");
        JSONObject jObject = new JSONObject(response);
        access_token = jObject.getString("access_token");
        //		     logger.info("sessionId: "+sessionId);
//        HashMap<String, String> updateMap = new HashMap<String, String>();
//        updateMap.put("rheem_accessToken", access_token);
//        
//        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
//            CommonUtil.updateInternalFileValues(updateMap);
//        } else {
//            LocalCommonUtil.updateInternalFileValues(updateMap);
//        }
//        
        return access_token;
    }

    public String getNordyneWarrantyVerificationResponse(String manufacturer, String serialNo, String state, int installId,
            int userId, String udid, int reportId, String buName) throws TransformerException {
        //	logger.info("WarrantyAPI getNordyneWarrantyVerificationResponse method invoked");

        //	SurfBIWebservicesImpl surfBIWebservicesImpl = new SurfBIWebservicesImpl();
        //	surfBIWebservicesImpl.updateRemainingAttempts(buName, userId);
        String response = null;
        String nordyneUrl = "https://secure.nordyne.com/WarrantyInquiry/WarrantyInquiry.svc/xml/" + serialNo + "/" + state;
        SoapConnection soapConnection = new SoapConnection();

        try {
            logger.info("====" + userId + "=====>RequestURL : " + nordyneUrl.replaceAll(" ", "%20"));
            response = soapConnection.soapget(new URL(nordyneUrl.replaceAll(" ", "%20")), "");

            logger.info("====" + userId + "=====> getNordyneWarrantyVerificationResponse() Response : " + response);
            String searchedData = "SerialNo : " + serialNo + "; State : " + state;

            String invalidMsg = "<a:Errors>The Serial Number you have entered is invalid.</a:Errors>";
            if (response.contains(invalidMsg)) {
                searchedData = "Invalid Serial Number. " + searchedData;
            } else {
                searchedData = "Warranty Success. " + searchedData;
            }
            UsageTracking usageTracking = new UsageTracking();
            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "Nordyne");

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            logger.error("=== USER ID :" + userId + "=====>Exception : " + e);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("=== USER ID :" + userId + "=====>Exception : " + e);
        }

        return response;

    }

    public String getCEWarrantyLoginResponse(String manufacturer, String serialNumber, String provider, String modelNumber,
            int installId, int userId, String udid, int reportId, String buName, String SB_ID, String UId,
            String password, String name, String email, String companyName, String ceUrl, String isDev) {

        String unicodeText = "[^\\x09\\x0A\\x0D\\x20-\\xD7FF\\xE000-\\xFFFD\\x10000-x10FFFF]";
        String response = "";
        String decryptPassword = "";
        boolean oldCalling = false;
        String type = "User";
//	     HashMap<String, String> exthm = CommonUtil.getExternalAPIConfigProperties(buName);

        HashMap<String, String> exthm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }

        String key = "";

        switch (buName) {
            case "CARRIER ENTERPRISE":
                key = "CE_";
                break;
            case "BAKER":
                key = "BK_";
                break;
            case "CARRIER ENTERPRISE MEXICO":
                key = "CE_MX_";
                break;
            case "GEMAIRE":
                key = "GM_";
                break;
            case "EAST COAST METALS":
                key = "ECM_";
                break;
            default:
                break;
        }

        if (ceUrl.isEmpty() || ceUrl == null) {
            ceUrl = "https://www.servicebench.com/servicebenchv5/services/ProductEntitlementService";
        }

        if (SB_ID.equals("") || UId.equals("") || password.equals("")) {
            if (isDev.equalsIgnoreCase("Y")) {
                ceUrl = "https://test.servicebench.com/servicebenchv5/services/ProductEntitlementService";
//		    	
                SB_ID = exthm.get(key + "SerivceBench_ID_DEV");
                UId = exthm.get(key + "ServiceBench_User_DEV");
                password = exthm.get(key + "ServiceBench_Password_DEV");
                oldCalling = true;
//				 password = EncyrptionUtils.encryptPassword(password);
            } else {
                ceUrl = "https://www.servicebench.com/servicebenchv5/services/ProductEntitlementService";
                SB_ID = exthm.get(key + "SerivceBench_ID_PROD");
                UId = exthm.get(key + "ServiceBench_User_PROD");
                password = exthm.get(key + "ServiceBench_Password_PROD");
                oldCalling = true;
//			    password = EncyrptionUtils.encryptPassword(password);
            }
        }

        String SB_UID = SB_ID + ":" + UId;

        HashMap<String, String> keyValue = new HashMap<String, String>();

        String entitlementRequestXml = AppConstants.CE_ENTITLEMENT_REQUEST_XML;
        String serviceHistoryRequestXml = AppConstants.CE_SERVICEHISTORY_REQUEST_XML;

        entitlementRequestXml = entitlementRequestXml.replaceAll("\\@\\@PASSWORD\\$\\$", password != "" ? password : "Watsco14!");
        entitlementRequestXml = entitlementRequestXml.replaceAll("\\@\\@SB_UID\\$\\$", SB_UID != "" ? SB_UID : "493878970:srupp123");

        entitlementRequestXml = entitlementRequestXml.replaceAll("\\@\\@MODELNUMBER\\$\\$", modelNumber);
        entitlementRequestXml = entitlementRequestXml.replaceAll("\\@\\@SERIALNUMBER\\$\\$", serialNumber);

        logger.info("=== USER ID :" + userId + "===getCEWarrantyLoginResponse()==>RequestURL : " + ceUrl);
        logger.info("=== USER ID :" + userId + "===getCEWarrantyLoginResponse()==>EntitlementRequestXml : " + entitlementRequestXml);

        serviceHistoryRequestXml = serviceHistoryRequestXml.replaceAll("\\@\\@PASSWORD\\$\\$", password != "" ? password : "Watsco14!");
        serviceHistoryRequestXml = serviceHistoryRequestXml.replaceAll("\\@\\@SB_UID\\$\\$", SB_UID != "" ? SB_UID : "493878970:srupp123");

        serviceHistoryRequestXml = serviceHistoryRequestXml.replaceAll("\\@\\@MODELNUMBER\\$\\$", modelNumber);
        serviceHistoryRequestXml = serviceHistoryRequestXml.replaceAll("\\@\\@SERIALNUMBER\\$\\$", serialNumber);

        logger.info("=== USER ID :" + userId + "===getCEWarrantyLoginResponse()====>RequestURL : " + ceUrl);
        logger.info("=== USER ID :" + userId + "===getCEWarrantyLoginResponse()====>ServiceHistoryRequestXml : " + serviceHistoryRequestXml);

        decryptPassword = EncyrptionUtils.decryptCredentials(password);

        decryptPassword = escapeJava(decryptPassword);

        entitlementRequestXml = entitlementRequestXml.replace(password, decryptPassword);
        entitlementRequestXml = entitlementRequestXml.replace(unicodeText, "");
        serviceHistoryRequestXml = serviceHistoryRequestXml.replace(password, decryptPassword);
        serviceHistoryRequestXml = serviceHistoryRequestXml.replace(unicodeText, "");

        try {
            SoapConnection soapConnection = new SoapConnection();

            String entitlementApiResponse = soapConnection.soapPostResponse(entitlementRequestXml, new URL(ceUrl), "");

            String serviceHistoryApiResponse = soapConnection.soapPostResponse(serviceHistoryRequestXml, new URL(ceUrl), "");

            response = CEResultAPIXml.
                    getCEResultXML(entitlementApiResponse, serviceHistoryApiResponse, serialNumber, modelNumber, installId, userId, udid, reportId, buName, type, oldCalling);

//            response = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><soap:Fault><faultcode xmlns:ns1=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">ns1:FailedAuthentication</faultcode><faultstring>Authentication failed.</faultstring></soap:Fault></soap:Body></soap:Envelope>\"";
            if ((!response.contains("API failed")) || (!response.contains("Authentication failed."))) {
                logger.info("=== USER ID :" + userId + "=====>ENTITLEMENT FINAL RESPONSE : " + response);
            } else {
                logger.error("=== USER ID :" + userId + "=====>ENTITLEMENT FINAL RESPONSE : " + response);
            }
            return response;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("=== USER ID :" + userId + "=====>Exception : " + e);
        }
        return response;

    }

    public String getCEWarrantyVerificationResponse(int installId, int userId, String udid, int reportId, String buName,
            String manufacturer, String serialNumber, String modelNumber, String type, String ceUrl, String isDev) {
        try {
            String unicodeText = "[^\\x09\\x0A\\x0D\\x20-\\xD7FF\\xE000-\\xFFFD\\x10000-x10FFFF]";
            String response = "";
            String decryptPassword = "";
            String SB_ID = "";
            String UId = "";
            String password = "";
            String entitlementApiResponse = "";
            String serviceHistoryApiResponse = "";

//    	        HashMap<String, String> exthm = CommonUtil.getExternalAPIConfigProperties(buName);
            HashMap<String, String> exthm;

            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                exthm = CommonUtil.getExternalAPIConfigProperties(buName);
            } else {
                exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
            }

            String key = "";

            switch (buName) {
                case "CARRIER ENTERPRISE":
                    key = "CE_";
                    break;
                case "BAKER":
                    key = "BK_";
                    break;
                case "CARRIER ENTERPRISE MEXICO":
                    key = "CE_MX_";
                    break;
                case "GEMAIRE":
                    key = "GM_";
                    break;
                case "EAST COAST METALS":
                    key = "ECM_";
                    break;
                default:
                    break;
            }

            if (ceUrl.equals("")) {
                ceUrl = exthm.get(key + "Entitlement_SerivceBench_API_PROD");
                if (ceUrl == null || ceUrl.isEmpty()) {
                    logger.error("=== USER ID :" + userId + "=====>ServiceBench API not Found");
                    return response;
                }

            }
//    		    	 ceUrl = "https://www.servicebench.com/servicebenchv5/services/ProductEntitlementService";

            WatscoCommonImpl credentialsDetails = new WatscoCommonImpl();
            String jsonResponse = credentialsDetails.getCredentials("ServiceBench", type, buName, userId, isDev);

            JSONObject jObject = new JSONObject(jsonResponse);
            SB_ID = jObject.getString("SB_ID");
            UId = jObject.getString("UId");
            password = jObject.getString("password");
//    		   	 String OldCalling   = jObject.getString("oldCalling");
            Boolean maskEnable = jObject.getBoolean("maskEnable");

            String SB_UID = SB_ID + ":" + UId;

            HashMap<String, String> keyValue = new HashMap<String, String>();

            String entitlementRequestXml = AppConstants.CE_ENTITLEMENT_REQUEST_XML;
            String serviceHistoryRequestXml = AppConstants.CE_SERVICEHISTORY_REQUEST_XML;

            entitlementRequestXml = entitlementRequestXml.replaceAll("\\@\\@PASSWORD\\$\\$", password != "" ? password : "Watsco14!");
            entitlementRequestXml = entitlementRequestXml.replaceAll("\\@\\@SB_UID\\$\\$", SB_UID != "" ? SB_UID : "493878970:srupp123");

            entitlementRequestXml = entitlementRequestXml.replaceAll("\\@\\@MODELNUMBER\\$\\$", modelNumber);
            entitlementRequestXml = entitlementRequestXml.replaceAll("\\@\\@SERIALNUMBER\\$\\$", serialNumber);

            logger.info("=== USER ID :" + userId + "===getCEWarrantyVerificationResponse()====RequestURL : " + ceUrl);
            logger.info("=== USER ID :" + userId + "===getCEWarrantyVerificationResponse()====Request() : " + entitlementRequestXml);

            serviceHistoryRequestXml = serviceHistoryRequestXml.replaceAll("\\@\\@PASSWORD\\$\\$", password != "" ? password : "Watsco14!");
            serviceHistoryRequestXml = serviceHistoryRequestXml.replaceAll("\\@\\@SB_UID\\$\\$", SB_UID != "" ? SB_UID : "493878970:srupp123");

            serviceHistoryRequestXml = serviceHistoryRequestXml.replaceAll("\\@\\@MODELNUMBER\\$\\$", modelNumber);
            serviceHistoryRequestXml = serviceHistoryRequestXml.replaceAll("\\@\\@SERIALNUMBER\\$\\$", serialNumber);

            logger.info("=== USER ID :" + userId + "===getCEWarrantyVerificationResponse()====>RequestURL : " + ceUrl);
            logger.info("=== USER ID :" + userId + "===getCEWarrantyVerificationResponse()====>serviceHistoryRequestXml : " + serviceHistoryRequestXml);

            decryptPassword = EncyrptionUtils.decryptCredentials(password);

            decryptPassword = escapeJava(decryptPassword);

            entitlementRequestXml = entitlementRequestXml.replace(password, decryptPassword);

            serviceHistoryRequestXml = serviceHistoryRequestXml.replace(password, decryptPassword);

            try {
                SoapConnection soapConnection = new SoapConnection();

                entitlementApiResponse = soapConnection.soapPostResponse(entitlementRequestXml, new URL(ceUrl), "");
                serviceHistoryApiResponse = soapConnection.soapPostResponse(serviceHistoryRequestXml, new URL(ceUrl), "");
                entitlementApiResponse = entitlementApiResponse.replaceAll(unicodeText, "");
                serviceHistoryApiResponse = serviceHistoryApiResponse.replaceAll(unicodeText, "");
                logger.info("EntitlementApiResponse" + entitlementApiResponse);
                logger.info("serviceHistoryApiResponse" + serviceHistoryApiResponse);

                if (containsIgnoreCase(entitlementApiResponse, "warrantyStop")) {
                    entitlementApiResponse = entitlementApiResponse.replaceAll("<extDataKey>warrantyStop</extDataKey><extDataDescription></extDataDescription>", "<extDataKey>warrantyStop</extDataKey><extDataDescription>N/A</extDataDescription>");
                }

                response = CEResultAPIXml.
                        getCEResultXML(entitlementApiResponse, serviceHistoryApiResponse, serialNumber, modelNumber, installId, userId, udid, reportId, buName, type, maskEnable);

                if (!entitlementApiResponse.contains("failed")) {
                    logger.info("=== USER ID :" + userId + "=====>ENTITLEMENT FINAL RESPONSE : " + response);
                } else {
                    logger.error("=== USER ID :" + userId + "=====>ENTITLEMENT FINAL RESPONSE : " + response);
                }

                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return null;
    }

}
