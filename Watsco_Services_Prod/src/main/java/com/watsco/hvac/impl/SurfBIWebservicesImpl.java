package com.watsco.hvac.impl;

import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.SoapConnection;
import com.watsco.hvac.dao.WebserviceResultXml;
import com.watsco.hvac.dto.ChannelDTO;

public class SurfBIWebservicesImpl {

//    protected static Logger logger = Logger.getLogger(SurfBIWebservicesImpl.class);
    private static final Logger logger = Logger.getLogger(SurfBIWebservicesImpl.class);

    public String getCheckStoreAvailability(String buName, int branchWid, int userId) {
        Connection con = null;
        //		PreparedStatement  pstmt 	= null;
        ResultSet rs = null;
        String response = null;
        CallableStatement cs = null;
        try {

            con = DBConnection.getConnection(false);

            /*String sqlQuery = "select Is_Active from branch_d where BU_WID = " +
             "(select BU_WID from MANUFACTURER_D where BU_NAME = ?) " +
             "and BRANCH_WID = ?";
             
             logger.info("===="+userId+"=====>Query : select Is_Active from branch_d where BU_WID = " +
             "(select BU_WID from MANUFACTURER_D where BU_NAME = "+buName+") " +
             "and BRANCH_WID = "+branchWid);
             
             pstmt 	= con.prepareStatement(sqlQuery);
             pstmt.setString(1, buName);
             pstmt.setInt(2, branchWid);
             rs = pstmt.executeQuery();*/
            String sqlQuery = "exec branch_is_acive :BRANCH_WID, ':BU_NAME'";
            sqlQuery = sqlQuery.replaceAll(":BRANCH_WID", String.valueOf(branchWid));
            sqlQuery = sqlQuery.replaceAll(":BU_NAME", buName);
            logger.info("====" + userId + "===getCheckStoreAvailability()==>Report Query : " + sqlQuery);
            cs = con.prepareCall(sqlQuery);
            rs = cs.executeQuery();

            response = WebserviceResultXml.getXMLDocument(rs);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("====" + userId + "=====>Exeption in getCheckStoreAvailability() : " + e);

        } finally {
            DBConnection.closeConnectionCS(rs, con, cs);
        }
        return response;
    }

    public String getBOMUsingModelNumber(String buName, String modelNumber, String subBU) {
        Connection con = null;
        //		PreparedStatement  pstmt 	= null;
        ResultSet rs = null;
        String response = null;
        CallableStatement cs = null;

        try {
            con = DBConnection.getConnection(false);
            //			logger.info("====================>Got Metadata Connection");
            /*
             String sqlQuery = "SELECT model_wid FROM model_d WHERE ( model_number = ? " +
             "OR model_description = ? OR model_name = ? OR model_name_1 = ? " +
             "OR model_number_1 = ? ) AND manufacturer_wid IN (SELECT manufacturer_wid FROM " +
             "manufacturer_d WHERE BU_NAME = ?)";
             
             logger.info("SELECT model_wid FROM model_d WHERE ( model_number ='"+modelNumber +
             "' OR model_description ='"+modelNumber+"' OR model_name = '"+modelNumber+"' OR model_name_1 ='"+modelNumber +
             "' OR model_number_1 = '"+modelNumber+"' ) AND manufacturer_wid IN (SELECT manufacturer_wid FROM " +
             "manufacturer_d WHERE BU_NAME ='"+buName+"')");
             
             pstmt 	= con.prepareStatement(sqlQuery);
             
             pstmt.setString(1, modelNumber);
             pstmt.setString(2, modelNumber);
             pstmt.setString(3, modelNumber);
             pstmt.setString(4, modelNumber);
             pstmt.setString(5, modelNumber);
             pstmt.setString(6, buName);
             //			logger.info("====================> pstmt set :" );
             
             rs = pstmt.executeQuery();
             
             
             if (rs != null)
             {
             int modelWid = 0;
             try {
             if(rs.next())
             modelWid = rs.getInt(1);
             } catch (SQLException e) {
             logger.info("SQLException"+e);
             }
             if(modelWid!=0){
             //old
             sqlQuery = "SELECT TOP 175 part.part_number AS \"Part Number\", part.part_description " +
             "AS \"Part Description\", sup.replacement_part_number AS \"Supercedes\" FROM " +
             "part_d part INNER JOIN model_to_part_d ON part.part_wid = model_to_part_d.part_wid " +
             "AND MODEL_TO_PART_D.MODEL_WID IN (?) LEFT OUTER JOIN dbo.supercedes_part_d sup ON " +
             "part.part_wid = sup.part_wid AND part.manufacturer_wid = sup.manufacturer_wid AND " +
             "part.manufacturer_wid = (SELECT manufacturer_wid FROM manufacturer_d WHERE bu_name = ?)";
             //
             sqlQuery ="SELECT TOP 175  \"Part Number\",\"Part Description\",\"Supercedes\" ,\"MagentoId\",\"MincronNumber\" from "+
             "(SELECT TOP 175 part.part_number AS \"Part Number\",part.part_description AS \"Part Description\", "+
             "sup.replacement_part_number AS \"Supercedes\" FROM   part_d part INNER JOIN model_to_part_d "+
             "ON part.part_wid = model_to_part_d.part_wid AND MODEL_TO_PART_D.MODEL_WID IN (?) "+
             "LEFT OUTER JOIN dbo.supercedes_part_d sup ON part.part_wid = sup.part_wid AND "+
             "part.manufacturer_wid = sup.manufacturer_wid AND part.manufacturer_wid = (SELECT manufacturer_wid "+
             "FROM   manufacturer_d WHERE  bu_name = ?) )tb1 left outer join "+
             "(select  PRODUCT_NUM,INTERNAL_PRODUCT_NUM, m.magento_id as \"MagentoId\", m.mincron_number as \"MincronNumber\" from PRODUCT_D p join "+
             "(select mincron_number,magento_id from (select mincron_number,magento_id,RANK() over (partition by mincron_number "+
             "order by magento_id) as r from mincron_magento_id )a WHERE r=1) m "+
             "on  p.INTERNAL_PRODUCT_NUM = m.mincron_number )tb2 on tb2.PRODUCT_NUM = tb1.\"Part Number\" order by tb1.\"Part Number\"";
             
             logger.info("SELECT TOP 175  \"Part Number\",\"Part Description\",\"Supercedes\" ,\"MagentoId\",\"MincronNumber\" from "+
             "(SELECT TOP 175 part.part_number AS \"Part Number\",part.part_description AS \"Part Description\", "+
             "sup.replacement_part_number AS \"Supercedes\" FROM   part_d part INNER JOIN model_to_part_d "+
             "ON part.part_wid = model_to_part_d.part_wid AND MODEL_TO_PART_D.MODEL_WID IN ("+modelWid+") "+
             "LEFT OUTER JOIN dbo.supercedes_part_d sup ON part.part_wid = sup.part_wid AND "+
             "part.manufacturer_wid = sup.manufacturer_wid AND part.manufacturer_wid = (SELECT manufacturer_wid "+
             "FROM   manufacturer_d WHERE  bu_name = '"+buName+"') )tb1 left outer join "+
             "(select  PRODUCT_NUM,INTERNAL_PRODUCT_NUM, m.magento_id as \"MagentoId\", m.mincron_number as \"MincronNumber\" from PRODUCT_D p join "+
             "(select mincron_number,magento_id from (select mincron_number,magento_id,RANK() over (partition by mincron_number "+
             "order by magento_id) as r from mincron_magento_id )a WHERE r=1) m "+
             "on  p.INTERNAL_PRODUCT_NUM = m.mincron_number )tb2 on tb2.PRODUCT_NUM = tb1.\"Part Number\" order by tb1.\"Part Number\"");
             
             pstmt 	= con.prepareStatement(sqlQuery);
             pstmt.setInt(1, modelWid);
             pstmt.setString(2, buName);
             rs = pstmt.executeQuery();*/
            String sqlQuery = "";
            if (buName.equalsIgnoreCase("GEMAIRE")) {
                sqlQuery = "exec model_part_search_website ':MODEL_NUMBER', ':BU_NAME'";
            } else if (buName.equalsIgnoreCase("CARRIER ENTERPRISE")) {
                sqlQuery = "EXEC model_details_search_CE_BOM_website ':MODEL_NUMBER', ':BU_NAME',:SUB_BU ";
            }
//                sqlQuery = "exec model_part_search_website ':MODEL_NUMBER', ':BU_NAME'";

            sqlQuery = sqlQuery.replaceAll(":MODEL_NUMBER", modelNumber);
            sqlQuery = sqlQuery.replaceAll(":BU_NAME", buName);
            sqlQuery = sqlQuery.replaceAll(":SUB_BU", subBU.isEmpty() ? "9" : subBU);

            logger.info("====67807===getBOMUsingModelNumber()==>Report Query : " + sqlQuery);
            cs = con.prepareCall(sqlQuery);
            rs = cs.executeQuery();

            response = WebserviceResultXml.getBOMXMLDocument(rs);
            //				}else
            //					response="<RESULT/>";

            //			}else
            //				response="<RESULT/>";
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("Exception in getBOMUsingModelNumber(): " + e);

        } finally {
            DBConnection.closeConnectionCS(rs, con, cs);
        }
        return response;
    }

    /*public void updateRemainingAttempts(String buName, int userId) {
     //		logger.info("SurfBIWebservicesImpl updateRemainingAttempts method invoked");
     Connection con 				= null;
     PreparedStatement  pstmt 	= null;
     ResultSet rs 				= null;
     
     try {
     con=DBConnection.getConnection(true);
     //			logger.info("====================>Got Metadata Connection");
     
     String sqlQuery = "UPDATE SURFBI_USERS SET REMAINING_ATTEMPTS=(REMAINING_ATTEMPTS-1) " +
     "WHERE USER_ID=? AND BU_NAME = ?";
     
     pstmt 	= con.prepareStatement(sqlQuery);
     
     
     pstmt.setInt(1, userId);
     pstmt.setString(2, buName);
     
     //				logger.info("====================> pstmt set :" );
     
     pstmt.executeUpdate();
     //				logger.info("====================> pstmt.executeUpdate : "+ value);
     
     
     } catch (Exception e) {
     // TODO Auto-generated catch block
     logger.error("=======> Error Retrieving updateRemainingAttempts " + e);
     
     } finally {
     DBConnection.closeConnection(con, pstmt, rs);
     }
     
     }*/

    public String getMagentoId(String buName, String mincronNumber, int userId) {
        Connection con = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        String response = null;
        try {
            con = DBConnection.getConnection(false);
            String sqlQuery = "exec Mincron_Magento_Select ':MINCRON_NUMBER',':BU_NAME'";
            sqlQuery = sqlQuery.replaceAll(":MINCRON_NUMBER", mincronNumber);
            sqlQuery = sqlQuery.replaceAll(":BU_NAME", buName);
            logger.info("====" + userId + "===getMagentoId()==>Report Query : " + sqlQuery);
            cs = con.prepareCall(sqlQuery);
            rs = cs.executeQuery();
            response = WebserviceResultXml.getMagentoXMLDocument(rs);
        } catch (Exception e) {
            logger.error("Exception in getMagentoId(): " + e);

        } finally {
            DBConnection.closeConnectionCS(rs, con, cs);
        }
        return response;
    }

    public String getPrice(String buName, int customerNumber, String productNumber, int userId, String mincronNumber, int branchId) {
        String response = "";

        String requestUrl = "http://stage.ws.gemaire.com/GemERPWs/GemERPSrv01.asmx";

        String requestXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<tem:GetIItemPrice>"
                + "<tem:GetItemPriceReq>"
                + "<![CDATA[<ItemsPrice>"
                + "<ItemPrice>"
                + "<Branch>" + branchId + "</Branch>"
                + "<CustAccnt>" + customerNumber + "</CustAccnt>"
                + "<ItemNum>" + mincronNumber + "</ItemNum>"
                + "<JobNum></JobNum>"
                + "</ItemPrice>"
                + "</ItemsPrice>]]>"
                + "</tem:GetItemPriceReq>"
                + "</tem:GetIItemPrice>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        SoapConnection soapConnection = new SoapConnection();
        try {
            logger.info("====" + userId + "=====>requestUrl : " + requestUrl);
            logger.info("====" + userId + "=====>Request : " + requestXML);
            response = soapConnection.soapPostResponse(requestXML, new URL(requestUrl), "");
            if (response.equalsIgnoreCase("API failed")) {
                logger.error("====" + userId + "===getPrice()==>Response : " + response);
            } else {
                logger.info("====" + userId + "===getPrice()==>Response : " + response);
            }

        } catch (Exception e) {
            logger.error("====" + userId + "===getPrice()==>Exception: " + e);
        }

        /* Connection con 				= null;
         PreparedStatement  pstmt 	= null;
         ResultSet rs 				= null;
         String response             = null;
         try {
         con=DBConnection.getPriceDBConnection();
         //			logger.info("====================>Got Metadata Connection");
         
         String sqlQuery = "select sell_price,net_price from ecommerce_pricing " +
         "where customer_num=? and erpitem_num=?;";
         
         logger.info("===="+userId+"=====>Query : select sell_price,net_price from ecommerce_pricing " +
         "where customer_num="+customerNumber+" and erpitem_num='"+mincronNumber+"';");
         
         pstmt 	= con.prepareStatement(sqlQuery);
         
         pstmt.setInt(1, customerNumber);
         pstmt.setString(2, mincronNumber);
         
         rs = pstmt.executeQuery();
         
         
         response = WebserviceResultXml.getPriceXmlDocument(rs);
         } catch (Exception e) {
         // TODO Auto-generated catch block
         logger.error("Exception : " + e);
         
         } finally {
         DBConnection.closeConnection(con, pstmt, rs);
         }*/
        return response;
    }

    public void updateRegistration(int id, int userId) {
        //		logger.info("SurfBIWebservicesImpl updateRemainingAttempts method invoked");

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            con = DBConnection.getConnection(false);
            String sqlQuery = "";
            //			if(isDealer.equalsIgnoreCase("Y"))
            //			     sqlQuery = "UPDATE dealers SET ActiveInd = 0 WHERE DealerId=?";
            //			else
            sqlQuery = "UPDATE Contractors SET ActiveInd = 0 WHERE ContractorId=?";
            logger.info("====" + userId + "==updateRegistration()===>Report Query : UPDATE Contractors SET ActiveInd = 0 WHERE ContractorId=" + id);
            pstmt = con.prepareStatement(sqlQuery);

            pstmt.setInt(1, id);

            pstmt.executeUpdate();

        } catch (Exception e) {

            logger.error("Exception in updateRegistration(): " + e);

        } finally {
            DBConnection.closeConnection(con, pstmt, rs);
        }

    }

    /*public String typeOverSearch(int userId, String search,String buName,String searchType) {
     Connection con     = null;
     ResultSet rs     = null;
     String response             = "";
     CallableStatement cs     = null;
     String sqlQuery="";
     try {
     con=DBConnection.getConnection(false);
		   
     if(searchType.equalsIgnoreCase("Product"))
     sqlQuery = "Exec PA_Typeahead ':SEARCH', ':BU_NAME'";
     else if(searchType.equalsIgnoreCase("EquipBOM"))
     sqlQuery = "Exec Model_Typeahead ':SEARCH', ':BU_NAME'";
     else 
     sqlQuery = "EXEC Document_Typeahead ':SEARCH', ':BU_NAME'";
		   
     sqlQuery = sqlQuery.replaceAll(":SEARCH", search);
     sqlQuery = sqlQuery.replaceAll(":BU_NAME", buName);
     //sqlQuery = sqlQuery.replaceAll(":SEARCH_TYPE", searchType);
		   
     logger.info("===="+userId+"=====>Query : "+sqlQuery);
     cs = con.prepareCall(sqlQuery);
     rs = cs.executeQuery();
     response = WebserviceResultXml.getTypeOverSearchXMLDocument(rs);
		   
     } catch (Exception e) {
     // TODO Auto-generated catch block
     logger.error("===="+userId+"=====>Exeption : " + e);
     
     } finally {
     DBConnection.closeConnectionCS(rs, con, cs);
     }
     return response;
     
     
     }*/

    public String getMarketingChannel(String buName) {
        Connection con = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        ChannelDTO channelDTO;
        String response = null;

        try {
            con = DBConnection.getConnection(false);
            String sqlQuery = "";
            channelDTO = new ChannelDTO();
            sqlQuery = "Exec Marketing_Channels_Select ':BU_NAME'";
            sqlQuery = sqlQuery.replaceAll(":BU_NAME", buName);

            logger.info("====getMarketingChannel()=====>Report Query : " + buName);

            cs = con.prepareCall(sqlQuery);
            rs = cs.executeQuery();

//             
//             response = WebserviceResultXml.getMKTChannelXMLDocument(rs);
            response = WebserviceResultXml.getMKTChannelJSONDocument(rs);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("===getMarketingChannel()=====>Exception : " + e);

        } finally {
            DBConnection.closeConnectionCS(rs, con, cs);
        }
        return response;

    }

}
