package com.watsco.hvac.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.PTCalculatorDao;
import com.watsco.hvac.dao.RheemResultAPIXml;

public class PTCalculatorImpl {

//    protected static Logger logger = Logger.getLogger(WarrantyAPI.class);
    private static final Logger logger = Logger.getLogger(PTCalculatorImpl.class);

    public String getRefrigerantInfo(String dataSent, String returnType, int userId) {
        //		logger.info("PTCalculatorDao getRefrigerantInfo method invoked");

        String dataSentValue = RheemResultAPIXml.getWarrantyTagValue("<REFRIGERANT>", "</REFRIGERANT>", dataSent);
        //		String[] dataSentValueArray = dataSentValue.split(",");
        Connection con = null;
        //		PreparedStatement pstmt = null;
        CallableStatement cs = null;
        ResultSet refrigerant_detail_rs = null;
        ResultSet pressure_to_temp_rs = null;
        ResultSet temp_to_pressure_rs = null;
        String response = "";
        try {

            con = DBConnection.getConnection(false);
            /*
             StringBuilder builder = new StringBuilder();
             
             for( int i = 0 ; i < dataSentValueArray.length; i++ ) {
             builder.append("?,");
             }
             String dynamicIN = builder.deleteCharAt( builder.length() -1 ).toString();
             String refrigerant_detail_query = "SELECT * FROM REFRIGERANT_DETAILS";
             logger.info("Query : "+refrigerant_detail_query);
             //"where REFRIGERANT NOT IN ("+dynamicIN+")";
             
             //			logger.info("refrigerant_detail_query : "+refrigerant_detail_query);
             
             //			String fileLocation = "";
             
             pstmt = con.prepareStatement(refrigerant_detail_query);
             
             //			int index = 1;
             //			for( Object o : dataSentValueArray ) {
             //			   pstmt.setObject(  index++, o );
             //			}
             refrigerant_detail_rs = pstmt.executeQuery();
             //			logger.info("====================> refrigerant_detail executed");*/

            String refrigerant_detail_query = "exec Refrigerants_Select";
            logger.info("====" + userId + "=====>Report Query : " + refrigerant_detail_query);
            cs = con.prepareCall(refrigerant_detail_query);
            refrigerant_detail_rs = cs.executeQuery();

            if (refrigerant_detail_rs != null) {
                /*String pressure_to_temp_query = "SELECT * FROM PRESSURE_TO_TEMP WHERE REFRIGERANT IN (SELECT " +
                 "DISTINCT REFRIGERANT FROM REFRIGERANT_DETAILS WHERE REFRIGERANT NOT IN ("
                 + dynamicIN+"))";
                 String temp_to_pressure_query = "SELECT * FROM TEMP_TO_PRESSURE WHERE REFRIGERANT IN (SELECT " +
                 "DISTINCT REFRIGERANT FROM REFRIGERANT_DETAILS WHERE REFRIGERANT NOT IN ("
                 + dynamicIN+"))";
                 logger.info("=====>Query : "+pressure_to_temp_query);
                 logger.info("=====>Query : "+temp_to_pressure_query);
                 
                 pstmt = con.prepareStatement(pressure_to_temp_query);
                 
                 int index = 1;
                 for( Object o : dataSentValueArray ) {
                 pstmt.setObject(  index++, o );
                 }
                 pressure_to_temp_rs = pstmt.executeQuery();
                 //				logger.info("====================> pressure_to_temp executed");
                 
                 pstmt = con.prepareStatement(temp_to_pressure_query);
                 
                 index = 1;
                 for( Object o : dataSentValueArray ) {
                 pstmt.setObject(  index++, o );
                 }
                 temp_to_pressure_rs = pstmt.executeQuery();
                 //				logger.info("====================> temp_to_pressure executed");*/

                String pressure_to_temp_query = "exec Refrigerants_Select_Pressure_To_Temp '" + dataSentValue + "'";
                logger.info("====" + userId + "=====>Report Query : " + pressure_to_temp_query);
                cs = con.prepareCall(pressure_to_temp_query);
                pressure_to_temp_rs = cs.executeQuery();

                String temp_to_pressure_query = "exec Refrigerants_Select_Temp_To_Pressure '" + dataSentValue + "'";
                logger.info("====" + userId + "=====>Report Query : " + temp_to_pressure_query);
                cs = con.prepareCall(temp_to_pressure_query);
                temp_to_pressure_rs = cs.executeQuery();
            }

            if (returnType.equalsIgnoreCase("XML")) {
                response = PTCalculatorDao.getXMLDocument(refrigerant_detail_rs, pressure_to_temp_rs, temp_to_pressure_rs);
                //				fileLocation += ".xml";
            } else {
                response = PTCalculatorDao.getJSON(refrigerant_detail_rs, pressure_to_temp_rs, temp_to_pressure_rs);
                //				fileLocation += ".json";
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("=======> Exception : " + e.getMessage());
        } finally {
            //			
            DBConnection.closeConnectionCS(refrigerant_detail_rs, con, cs);
            try {
                if (pressure_to_temp_rs != null) {
                    pressure_to_temp_rs.close();
                }
                if (temp_to_pressure_rs != null) {
                    temp_to_pressure_rs.close();
                }
            } catch (Exception e) {
                logger.error("Error connecting to DB");
            }
        }

        return response;

    }

}
