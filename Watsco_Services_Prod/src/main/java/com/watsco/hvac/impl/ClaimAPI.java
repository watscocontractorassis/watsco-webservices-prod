package com.watsco.hvac.impl;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.watsco.hvac.dao.SoapConnection;

public class ClaimAPI {

    private static Logger logger = Logger.getLogger(ClaimAPI.class);

    public String getClaimResponse(String manufacturer,
            int installId, int userId, String udid, int reportId, String buName, HashMap<String, String> fieldsMap) {
//	logger.info("ClaimAPI getClaimResponse method invoked");

        String claimUrl = "https://test.servicebench.com/servicebenchv5/services/RTIClaimService";

        String claimRequestXml
                = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://servicebench.com/claim/service/types\">"
                + "<soapenv:Header> <wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken wsu:Id=\"UsernameToken-15231583\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
                + "<wsse:Username>775296564:watsco</wsse:Username>"
                + "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">RTItest1</wsse:Password>"
                + "</wsse:UsernameToken>"
                + "</wsse:Security>"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<typ:claimSubmitRequest>"
                + "<typ:version>1.0</typ:version>"
                + "<typ:sourceSystemName>Watsco Mobile</typ:sourceSystemName>"
                + "<typ:sourceSystemVersion>1.0</typ:sourceSystemVersion>"
                + "<typ:claim>"
                + "<typ:importFormatId>19</typ:importFormatId>"
                + "<typ:serviceAdministratorId>1</typ:serviceAdministratorId>"
                + "<typ:referenceNumber>@@REFERENCENUMBER$$</typ:referenceNumber>"
                + "<typ:accountNumber>HVAC123</typ:accountNumber>"
                + "<typ:brand>CAR</typ:brand>"
                + "<typ:modelNumber>HV101</typ:modelNumber>"
                + "<typ:customerLastName>@@LASTNAME$$</typ:customerLastName>"
                + "<typ:customerFirstName>@@FIRSTNAME$$</typ:customerFirstName>"
                + "<typ:customerAddressLine1>@@ADDRESSLINE$$</typ:customerAddressLine1>"
                + "<typ:customerAddressCity>@@ADDRESSCITY$$</typ:customerAddressCity>"
                + "<typ:customerAddressState>@@ADDRESSSTATE$$</typ:customerAddressState>"
                + "<typ:customerAddressZip>@@ADDRESSZIP$$</typ:customerAddressZip>"
                + "<typ:customerAddressCountry>US</typ:customerAddressCountry>"
                + "<typ:customerPhone>4435399700</typ:customerPhone>"
                + "<typ:serialNumber>@@SERIALNUMBER$$</typ:serialNumber>"
                + "<typ:purchaseDate>@@PURCHASEDATE$$</typ:purchaseDate>"
                + "<typ:purchasedFrom>Watsco</typ:purchasedFrom>"
                + "<typ:purchasedFromAddress>5201 Gulf Dr, Port Richey, 34652</typ:purchasedFromAddress>"
                + "<typ:purchasedFromState>FL</typ:purchasedFromState>"
                + "<typ:warrantyType>01</typ:warrantyType>"
                + "<typ:customerComplaint>@@CUSTOMERCOMPLAINT$$</typ:customerComplaint>"
                + "<typ:defectCode>301</typ:defectCode>"
                + "<typ:dateServiceRequested>@@DATESERVICEREQUESTED$$</typ:dateServiceRequested>"
                + "<typ:dateServiceCompleted>@@DATESERVICECOMPLETED$$</typ:dateServiceCompleted>"
                + "<typ:serviceExplanation>@@SERVICEEXPLANATION$$</typ:serviceExplanation>"
                + "<typ:servicePerformed>11</typ:servicePerformed>"
                + "<typ:manufacturerRefCode>30</typ:manufacturerRefCode>"
                + "<typ:repairCategory>20</typ:repairCategory>"
                + "<typ:timeOnJob>@@TIMEONJOB$$</typ:timeOnJob>"
                + "<typ:laborAmount>@@LABORAMOUNT$$</typ:laborAmount>"
                + "<typ:diagnosticFee>80.00</typ:diagnosticFee>"
                + "<typ:technicianName>Dave Whiteside</typ:technicianName>"
                + "<typ:claimParts>"
                + "<typ:claimPart>"
                + "<typ:partNumber>@@PARTNUMBER$$</typ:partNumber>"
                + "<typ:partQuantity>@@PARTQUANTITY$$</typ:partQuantity>"
                + "<typ:partPrice>733.00</typ:partPrice>"
                + "<typ:partInvoiceNumber>123456</typ:partInvoiceNumber>"
                + "<typ:partReplacementPartNumber>HV100</typ:partReplacementPartNumber>"
                + "</typ:claimPart>"
                + "</typ:claimParts>"
                + "</typ:claim>"
                + "</typ:claimSubmitRequest>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss");
        Date now = new Date();

        claimRequestXml = claimRequestXml.replaceAll("\\@\\@REFERENCENUMBER\\$\\$", "Demo" + sdfTime.format(now));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@LASTNAME\\$\\$", fieldsMap.get("1") == null ? "" : fieldsMap.get("1"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@FIRSTNAME\\$\\$", fieldsMap.get("2") == null ? "" : fieldsMap.get("2"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@ADDRESSLINE\\$\\$", fieldsMap.get("3") == null ? "" : fieldsMap.get("3"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@ADDRESSCITY\\$\\$", fieldsMap.get("4") == null ? "" : fieldsMap.get("4"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@ADDRESSSTATE\\$\\$", fieldsMap.get("5") == null ? "" : fieldsMap.get("5"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@ADDRESSZIP\\$\\$", fieldsMap.get("6") == null ? "" : fieldsMap.get("6"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@SERIALNUMBER\\$\\$", fieldsMap.get("7") == null ? "" : fieldsMap.get("7"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@PURCHASEDATE\\$\\$", fieldsMap.get("8") == null ? "" : fieldsMap.get("8"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@CUSTOMERCOMPLAINT\\$\\$", fieldsMap.get("9") == null ? "" : fieldsMap.get("9"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@DATESERVICEREQUESTED\\$\\$", fieldsMap.get("10") == null ? "" : fieldsMap.get("10"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@DATESERVICECOMPLETED\\$\\$", fieldsMap.get("11") == null ? "" : fieldsMap.get("11"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@SERVICEEXPLANATION\\$\\$", fieldsMap.get("12") == null ? "" : fieldsMap.get("12"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@TIMEONJOB\\$\\$", fieldsMap.get("13") == null ? "" : fieldsMap.get("13"));
        String timeOnJob = fieldsMap.get("13") == null ? "" : fieldsMap.get("13");
        String laborAmount = "";
        Pattern p = Pattern.compile("^\\d+$");
        Matcher m = p.matcher(timeOnJob);

        if (m.matches()) {
            timeOnJob = timeOnJob.replaceAll("^0+", "");
        }

        if (!timeOnJob.isEmpty() && timeOnJob != null) {
            int la = Integer.parseInt(timeOnJob);
            int hours = la / 100;
            float minutes = (float) ((la % 100) / 60.0);
            laborAmount = String.valueOf((hours + minutes) * 120);
        } else {
            laborAmount = "0";

        }
//	logger.info("laborAmount : "+laborAmount);
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@LABORAMOUNT\\$\\$", laborAmount);
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@PARTNUMBER\\$\\$", fieldsMap.get("14") == null ? "" : fieldsMap.get("14"));
        claimRequestXml = claimRequestXml.replaceAll("\\@\\@PARTQUANTITY\\$\\$", fieldsMap.get("15") == null ? "" : fieldsMap.get("15"));
        logger.info("===getClaimResponse()====> RequestUrl : " + claimUrl);
        logger.info("===getClaimResponse()====> claimRequestXml : " + claimRequestXml);

        String response = "";

        try {
            SoapConnection soapConnection = new SoapConnection();
            response = soapConnection.soapPostResponse(claimRequestXml, new URL(claimUrl), "");

//		    logger.info("response : "+ response);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("Exception in getClaimResponse(): " + e);
        }

        return response;

    }

}
