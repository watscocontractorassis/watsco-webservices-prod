package com.watsco.hvac.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.WarrantyDocumentConvertor;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;
import java.math.BigInteger;

public class WatscoCommonImpl {

    private static Logger logger = Logger.getLogger(WatscoCommonImpl.class);

    public String getCredentials(String provider, String type, String buName, int userId, String isDev) {
        try {
            String SB_ID = "";
            String password = "";
            String UId = "";
            boolean getFromConfigFiles = false;
//		    	HashMap<String, String> exthm = CommonUtil.getExternalAPIConfigProperties(buName);

            HashMap<String, String> exthm;

            String key = "";

            switch (buName) {
                case "CARRIER ENTERPRISE":
                    key = "CE_";
                    break;
                case "BAKER":
                    key = "BK_";
                    break;
                case "CARRIER ENTERPRISE MEXICO":
                    key = "CE_MX_";
                    break;
                case "GEMAIRE":
                    key = "GM_";
                    break;
                case "EAST COAST METALS":
                    key = "ECM_";
                    break;
                default:
                    break;
            }
            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                exthm = CommonUtil.getExternalAPIConfigProperties(buName);
            } else {
                exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
            }

            boolean maskEnable = false;
            if (type != null && type.equalsIgnoreCase("User")) {
                try {
                    Statement stmt;
                    Connection con = DBConnection.getConnection(false);
                    String sqlQuery = "EXEC Warranty_ServiceBench_Login_Check '" + provider + "','" + buName + "','" + userId + "'";

                    sqlQuery = sqlQuery.replaceAll("'null'", null);
                    logger.info("====" + userId + "===getCredentials()====>Report Query : " + sqlQuery);
                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                    ResultSet warrantyRS = stmt.executeQuery(sqlQuery);

                    while (warrantyRS.next()) {
                        SB_ID = warrantyRS.getString("login_id");
                        UId = warrantyRS.getString("login_un");
                        password = warrantyRS.getString("login_pw");
                        break;
                    }

                    if (SB_ID.length() == 0 || UId.length() == 0 || password.length() == 0) {
                        getFromConfigFiles = true;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                getFromConfigFiles = true;
            }

            if (getFromConfigFiles) {
                if (isDev != null && isDev.equalsIgnoreCase("Y")) {
                    SB_ID = exthm.get(key + "SerivceBench_ID_DEV");
                    UId = exthm.get(key + "ServiceBench_User_DEV");
                    password = exthm.get(key + "ServiceBench_Password_DEV");
                    maskEnable = true;
                } else {
                    SB_ID = exthm.get(key + "SerivceBench_ID_PROD");
                    UId = exthm.get(key + "ServiceBench_User_PROD");
                    password = exthm.get(key + "ServiceBench_Password_PROD");
                    maskEnable = true;
                }

                try {
                    Statement stmt;
                    Connection con = DBConnection.getConnection(false);
                    String sqlQuery = "EXEC Warranty_ServiceBench_Login_Check '" + provider + "','" + buName + "','" + userId + "'";
                    sqlQuery = sqlQuery.replaceAll("'null'", null);
                    logger.info("====" + userId + "===getCredentials()====>Report Query : " + sqlQuery);
                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                    ResultSet warrantyRS = stmt.executeQuery(sqlQuery);
                    String pw = "";
                    while (warrantyRS.next()) {
                        pw = warrantyRS.getString("login_pw");//;
                        break;
                    }

                    if (pw.length() > 0) {
                        maskEnable = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            JSONObject jObject = new JSONObject();
            jObject.put("SB_ID", SB_ID);
            jObject.put("UId", UId);
            jObject.put("password", password);
            jObject.put("maskEnable", maskEnable);
            return jObject.toString();
        } catch (Exception e) {
            e.toString();
            return e.toString();
        }

    }

    public JSONObject getProductDetails(int buWid, int userId, int regionId, BigInteger productId, String language) {
        Connection con = null;
        ResultSet userInfoRS = null;
        String response = null;
        Statement stmt = null;
        JSONObject jObject = new JSONObject();
        try {

            con = DBConnection.getConnection(false);

            String sqlQuery = "EXEC ProductDetails '" + buWid + "','" + regionId + "','" + productId + "','" + language + "'";
            sqlQuery = sqlQuery.replaceAll("'null'", null);
            logger.info("=== USER ID :" + userId + "=====>Query : " + sqlQuery);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            userInfoRS = stmt.executeQuery(sqlQuery);

            WarrantyDocumentConvertor getJson = new WarrantyDocumentConvertor();
            response = getJson.productDetailConvertor(userInfoRS);

            if (response != null) {
                jObject.put("STATUS", "Success");
                jObject.put("RESULT", new JSONObject(response));
            } else {
                jObject.put("STATUS", "Failure");
                jObject.put("RESULT", new JSONObject());
            }
            System.out.println("response :" + response);
            logger.info("=======" + userId + "==>ProductDetails Response : " + response);
            return jObject;
        } catch (Exception e) {
            jObject.put("STATUS", "Failure");
            jObject.put("RESULT", new JSONObject(e.toString()));
            return jObject;
        } finally {
            DBConnection.closeConnection(con, stmt, userInfoRS);
        }

    }

}
