package com.watsco.hvac.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.XRefWsdlConstants;
import com.watsco.hvac.dao.SoapConnection;

public class XRefDymanic {

    private static Logger logger = Logger.getLogger(XRefDymanic.class);

    SoapConnection soapPost = new SoapConnection();

    private enum CompressorTypes {

        MOTORS, FILTERBASES, FILTERDRIERS, COMPRESSORS1, COMPRESSORS2, DRIERS, SEMIHERMS, THERMOSTATS;

    }

    public String getResponse(int installId, int userId, String udid, int reportId, String buName, String manufacturer,
            String category, String compressor, HashMap<String, String> inputMap) {
        //		logger.info("XRefDymanic getResponse method invoked");
        String url = "";
        String requestXml = "";
        String requestXml2 = "";
        String response = "";

        CompressorTypes type = CompressorTypes.valueOf(compressor.toUpperCase());

        switch (type) {
            case MOTORS:
                //			logger.info("MOTORS");

                if (category.equalsIgnoreCase("FAST")) {
                    url = XRefWsdlConstants.icp_motorsURL;
                    if (inputMap.containsKey("ID")) {
                        requestXml = XRefWsdlConstants.icp_motorsId;
                    } else {
                        requestXml = XRefWsdlConstants.icp_motorsParams;
                    }
                } else if (category.equalsIgnoreCase("TOTALINE")) {
                    url = XRefWsdlConstants.totaline_motorsURL;
                    if (inputMap.containsKey("ID")) {
                        requestXml = XRefWsdlConstants.totaline_motorsId;
                    } else {
                        requestXml = XRefWsdlConstants.totaline_motorsParams;
                    }
                }

                break;

            case FILTERBASES:
                //			logger.info("FILTERBASES");

                if (category.equalsIgnoreCase("FAST")) {
                    url = XRefWsdlConstants.icp_filterbasesURL;
                    if (inputMap.containsKey("ID")) {
                        requestXml = XRefWsdlConstants.icp_filterbasesId;
                    } else {
                        requestXml = XRefWsdlConstants.icp_filterbasesParams;
                    }
                } else if (category.equalsIgnoreCase("TOTALINE")) {
                    url = XRefWsdlConstants.totaline_filterbasesURL;
                    if (inputMap.containsKey("ID")) {
                        requestXml = XRefWsdlConstants.totaline_filterbasesId;
                    } else {
                        requestXml = XRefWsdlConstants.totaline_filterbasesParams;
                    }
                }

                break;

            /*case FILTERDRIERS :
             //			logger.info("FILTERDRIERS");
                 
             url = XRefWsdlConstants.icp_filterdriersURL;
             if(inputMap.containsKey("ID"))
             requestXml = XRefWsdlConstants.icp_filterdriersId;
             else
             requestXml = XRefWsdlConstants.icp_filterdriersParams;
                 
             break;
             */
            case COMPRESSORS1:
                //			logger.info("COMPRESSORS1");

                if (category.equalsIgnoreCase("FAST")) {
                    url = XRefWsdlConstants.ICPcmprxrefbyparamsURL;
                    requestXml = XRefWsdlConstants.ICPcmprxrefbyparams;

                } else if (category.equalsIgnoreCase("TOTALINE")) {
                    url = XRefWsdlConstants.cmprxrefbyparamsURL;
                    requestXml = XRefWsdlConstants.cmprxrefbyparams;
                }

                break;

            case COMPRESSORS2:
                //			logger.info("COMPRESSORS2");

                if (category.equalsIgnoreCase("FAST")) {
                    url = XRefWsdlConstants.ICPcmprxrefbyidURL;
                    requestXml = XRefWsdlConstants.ICPcmprxrefbyidDetail;
                    requestXml2 = XRefWsdlConstants.ICPcmprxrefbyidId;

                } else if (category.equalsIgnoreCase("TOTALINE")) {
                    url = XRefWsdlConstants.cmprxrefbyidURL;
                    requestXml = XRefWsdlConstants.cmprxrefbyidDetail;
                    requestXml2 = XRefWsdlConstants.cmprxrefbyidId;
                }

                break;
            case DRIERS:
                //			logger.info("DRIERS");
                if (category.equalsIgnoreCase("FAST")) {
                    url = XRefWsdlConstants.icp_filterdriersURL;
                    if (inputMap.containsKey("ID")) {
                        requestXml = XRefWsdlConstants.icp_filterdriersId;
                    } else {
                        requestXml = XRefWsdlConstants.icp_filterdriersParams;
                    }
                } else if (category.equalsIgnoreCase("TOTALINE")) {
                    url = XRefWsdlConstants.totaline_filterdriersURL;
                    if (inputMap.containsKey("ID")) {
                        requestXml = XRefWsdlConstants.totaline_filterdriersId;
                    } else {
                        requestXml = XRefWsdlConstants.totaline_filterdriersParams;
                    }
                }

                break;
            case SEMIHERMS:
                //			logger.info("SEMIHERMS");

                if (category.equalsIgnoreCase("TOTALINE")) {
                    url = XRefWsdlConstants.totaline_semihermsURL;
                    if (inputMap.containsKey("ID")) {
                        requestXml = XRefWsdlConstants.totaline_semihermsId;
                    }
                }

                break;
            case THERMOSTATS:
                //			logger.info("THERMOSTATS");

                if (category.equalsIgnoreCase("TOTALINE")) {
                    url = XRefWsdlConstants.totaline_thermostatsURL;
                    if (inputMap.containsKey("ID")) {
                        requestXml = XRefWsdlConstants.totaline_thermostatsId;
                    } else {
                        requestXml = XRefWsdlConstants.totaline_thermostatsParams;
                    }
                }

                break;
            default:
                logger.info("default");
                break;
        }

        for (String key : inputMap.keySet()) {
            requestXml = requestXml.replaceAll("@@" + key.toUpperCase() + "\\$\\$", inputMap.get(key));
            if (compressor.equalsIgnoreCase("COMPRESSORS2")) {
                requestXml2 = requestXml2.replaceAll("@@" + key.toUpperCase() + "\\$\\$", inputMap.get(key));
            }
        }

        try {
            if (requestXml != null && !requestXml.isEmpty() && url != null && !url.isEmpty()) {
                //				logger.info("url"+ url);
                requestXml = requestXml.replaceAll("@@[A-Z_]+\\$\\$", "");
                //				logger.info("requestXml Before"+ requestXml);
                requestXml = requestXml.replaceAll("�", "&#183;");
                logger.info("=== USER ID :" + userId + "===getResponse()====>RequestURL : " + url);
                logger.info("=== USER ID :" + userId + "===getResponse()====>RequestXML : " + requestXml);
                response = soapPost.soapPostResponse(requestXml, new URL(url), "");

                if (compressor.equalsIgnoreCase("COMPRESSORS2")) {
                    requestXml2 = requestXml2.replaceAll("@@[A-Z_]+\\$\\$", "");
                    requestXml = requestXml.replaceAll("�", "&#183;");
                    logger.info("=== USER ID :" + userId + "===getResponse()====>requestXml2" + requestXml2);
                    logger.info("=== USER ID :" + userId + "===getResponse()====>RequestURL : " + url);
                    logger.info("=== USER ID :" + userId + "===getResponse()====>RequestXML : " + requestXml2);
                    String tempResp = soapPost.soapPostResponse(requestXml2, new URL(url), "");
                    tempResp = tempResp.replace(XRefWsdlConstants.xmlHeader, "");
                    response += tempResp;

                }
                if (response.equalsIgnoreCase("API failed")) {
                    response = "<RESULT>" + response;
                } else {
                    response = response.replace(XRefWsdlConstants.xmlHeader,
                            XRefWsdlConstants.xmlHeader + "<RESULT>");
                }
                response += "</RESULT>";
                //				logger.info("response :"+ response);
            }
        } catch (MalformedURLException e) {
            logger.error("=== USER ID :" + userId + "===getResponse()====>Exception : " + e);
        } catch (Exception e) {
            logger.error("=== USER ID :" + userId + "===getResponse()====>Exception : " + e);
        }

        return response;
    }

}
