package com.watsco.hvac.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.BillBoardResultXml;

public class GetBillBoardReport {

//    protected static Logger logger = Logger.getLogger(GetBillBoardReport.class);
    private static final Logger watscoLog = Logger.getLogger(GetBillBoardReport.class);

    public String getBillBoardImageLink(String buName, String device, int userId, String udid,
            String language, String appVersion, String branchNumber, UriInfo uriInfo) {

        Connection con = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        String response = null;

        try {
            con = DBConnection.getConnection(true);

            String sqlQuery = "";
            if (udid != null) {
                sqlQuery = "exec billboard_select ':DEVICE',':BU_NAME',:USERID,':UDID',':Language',':App_Version',':Branch_Number'";
                sqlQuery = sqlQuery.replaceAll(":DEVICE", device);
                sqlQuery = sqlQuery.replaceAll(":BU_NAME", buName);
                sqlQuery = sqlQuery.replaceAll(":USERID", "'"+String.valueOf(userId)+"'");
                sqlQuery = sqlQuery.replaceAll(":UDID", udid);
                sqlQuery = sqlQuery.replaceAll(":Language", language != null ? language : "NULL");
                sqlQuery = sqlQuery.replaceAll("':App_Version'", appVersion != null ? "'" + appVersion + "'" : "NULL");
                if (appVersion != null && appVersion.contains("v")) {
                    sqlQuery = sqlQuery.replaceAll("v", "");
                }
                sqlQuery = sqlQuery.replaceAll("':Branch_Number'", branchNumber.trim().length() != 0 ? "'" + branchNumber + "'" : "NULL");
            } else {
                sqlQuery = "exec billboard_select ':DEVICE', ':BU_NAME'";
                sqlQuery = sqlQuery.replaceAll(":DEVICE", device);
                sqlQuery = sqlQuery.replaceAll(":BU_NAME", buName);
            }

            watscoLog.info("=== USER ID :" + userId + "===getBillBoardImageLink()====>Report Query : " + sqlQuery);
            cs = con.prepareCall(sqlQuery,ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            cs.setQueryTimeout(60);
            rs = cs.executeQuery();

            response = BillBoardResultXml.getXMLDocument(rs, buName, device, udid, appVersion, branchNumber, userId, language, uriInfo);

        } catch (Exception e) {
            watscoLog.error("=== USER ID :" + userId + "===getBillBoardImageLink()====>Exception : " + e);

        } finally {
            DBConnection.closeConnectionCS(rs, con, cs);
        }
        return response;
    }

    public String getFeaturedBillBoardImageLink(String buName, String device, int userId, String udid, String language,
            String appVersion, String branchNumber, String feature) {

        Connection con = null;
        CallableStatement cs = null;
        ResultSet rs = null;
        String response = null;
        try {
            con = DBConnection.getConnection(true);

            String sqlQuery = "";
            if (udid != null) {

                if (appVersion != null && appVersion.contains("v")) {
                    appVersion = appVersion.replaceAll("v", "");
                }

                sqlQuery = "exec billboard_select '" + device + "','" + buName + "','" + String.valueOf(userId) + "','" + udid + "','" + (language != null ? language : "NULL") + "'," + (appVersion != null ? "'" + appVersion + "'" : "NULL") + "," + (branchNumber.trim().length() != 0 ? "'" + branchNumber + "'" : "NULL") + ",'" + feature + "'";

            }
            watscoLog.info("=== USER ID :" + userId + "===getFeaturedBillBoardImageLink()====>Report Query : " + sqlQuery);

            cs = con.prepareCall(sqlQuery);
            rs = cs.executeQuery();

            BillBoardResultXml utility = new BillBoardResultXml();
            response = utility.getJSONDocument(rs);
            watscoLog.info("=== USER ID :" + userId + "=====>Featured BillBoard Response : " + response);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            watscoLog.error("=== USER ID :" + userId + "====>Exception getFeaturedBillBoardImageLink(): " + e);

        } finally {
            DBConnection.closeConnectionCS(rs, con, cs);
        }
        return response;
    }

}
