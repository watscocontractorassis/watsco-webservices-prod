package com.watsco.hvac.impl;

import java.net.URL;
import java.util.HashMap;

import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;

import com.watsco.hvac.dao.GoodmanResultAPIXml;
import com.watsco.hvac.dao.SoapConnection;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;

public class GoodmanWarrantyAPI {

    private static Logger logger = Logger.getLogger(GoodmanWarrantyAPI.class);

    public String getGoodmanWarrantyVerificationResponse(String manufacturer, String serialNo, String lastName, String zipCode, String installType,
            int installId, int userId, String udid, int reportId, String buName)
            throws TransformerException {
        //		logger.info("GoodmanWarrantyAPI======> getGoodmanWarrantyVerificationResponse() method called");
        
        String encodeInstallType = "";
        if (installType.equalsIgnoreCase("Residential")) {
            encodeInstallType = "RES";
        } else if (installType.equalsIgnoreCase("Commercial")) {
            encodeInstallType = "COM";
        } else if (installType.equalsIgnoreCase("Multi-family")) {
            encodeInstallType = "MFY";
        }

        String response = null;
        String model = null;
        String modelDesc = null;

        UsageTracking usageTracking = new UsageTracking();
        String searchedData = "SerialNo : " + serialNo + "; PostalCode : " + zipCode + "; LastName : " + lastName + "; InstallType : " + installType;

//        HashMap<String, String> exthm = CommonUtil.getExternalAPIConfigProperties();
        HashMap<String, String> exthm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties();
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties();
        }

        String strURL = exthm.get("GW_API");

        String lookupModelRequestXML = "<LookupModelBySerNumRequest xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://goodmanmfg.com/GMC/WebService/BP/Warranty/LookupService\">"
                + "<TokenID xmlns=\"http://goodmanmfg.com/GMC/WebService/BP/Warranty/Entities\">FB8BA468-A5B0-4E42-BA54-402B519D94AE</TokenID>"
                + "<SerNum>" + serialNo + "</SerNum>"
                + "<ItemType>M</ItemType>"
                + "</LookupModelBySerNumRequest>";

        SoapConnection soapConnection = new SoapConnection();
        try {
            logger.info("====" + userId + "===getGoodmanWarrantyVerificationResponse()====>requestUrl : " + strURL);
            logger.info("====" + userId + "===getGoodmanWarrantyVerificationResponse()====>requestRequest : " + lookupModelRequestXML);
            String postResponse = soapConnection.soapPostResponse(lookupModelRequestXML, new URL(strURL), "");

            //			    logger.error(" 1 API postResponse : "+ postResponse);
            String severityCode = GoodmanResultAPIXml.getWarrantyTagValue("<SeverityCode>", "</SeverityCode>", postResponse);

            //				logger.error("1 API severityCode : "+ severityCode);
            if (severityCode.equalsIgnoreCase("0")) {

                //					logger.info("lookupModel(1st API) success");
                String itemModelList = GoodmanResultAPIXml.getWarrantyTagValue("<ItemModel>", "</ItemModel>", postResponse);
                String[] itemModelArr = itemModelList.split("~~~"); // contains model list in an array

                String modelDescList = GoodmanResultAPIXml.getWarrantyTagValue("<Description>", "</Description>", postResponse);
                String[] modelDescArr = modelDescList.split("~~~"); // contains model desc in an array

                if (itemModelArr != null && itemModelArr[0] != null) {
                    model = itemModelArr[0];
                }

                if (modelDescArr != null && modelDescArr[0] != null) {
                    modelDesc = modelDescArr[0];
                }

                String entitlementRequestXML = "<RetrieveEntitlementTableRequest xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://goodmanmfg.com/GMC/WebService/BP/Warranty/Entities/EntitlementService\">"
                        + "<TokenID xmlns=\"http://goodmanmfg.com/GMC/WebService/BP/Warranty/Entities\">FB8BA468-A5B0-4E42-BA54-402B519D94AE</TokenID>"
                        + "<SerNum>" + serialNo + "</SerNum>"
                        + "<Model>" + model + "</Model>"
                        + "<ZipCode i:nil=\"true\" />"
                        + "<InstallType>" + encodeInstallType + "</InstallType>"
                        + "<LastName>" + lastName + "</LastName>"
                        + "<Internal>false</Internal>"
                        + "</RetrieveEntitlementTableRequest>";

                logger.info("====" + userId + "===getGoodmanWarrantyVerificationResponse()====>requestUrl : " + strURL);
                logger.info("====" + userId + "===getGoodmanWarrantyVerificationResponse()====>requestRequest : " + entitlementRequestXML);
                postResponse = soapConnection.soapPostResponse(entitlementRequestXML, new URL(strURL), "");

                postResponse.replaceAll("&gt;", ">").replaceAll("&lt;", "<");
                severityCode = GoodmanResultAPIXml.getWarrantyTagValue("<SeverityCode>", "</SeverityCode>", postResponse);
                if (severityCode.equalsIgnoreCase("0")) {
                    //						logger.info("entitlementRequest(2nd API) success");

                    response = GoodmanResultAPIXml.getGoodmanResultXML(model, modelDesc, postResponse, "success");
                    searchedData = "Status: Success; " + searchedData;
                    usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");

                } else {
                    //						logger.info("entitlementRequest(2nd API) failure");
                    response = GoodmanResultAPIXml.getGoodmanResultXML(model, modelDesc, postResponse, "failure");
                    searchedData = "Status: Failure; " + searchedData;
                    usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");
                }

            } else {
                //					logger.info("entitlementRequest(1st API) failure");
                response = GoodmanResultAPIXml.getGoodmanResultXML(model, modelDesc, postResponse, "failure");
                searchedData = "Status: Failure; " + searchedData;
                usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");
                //					logger.info("error");
            }
        } catch (Exception e) {
            logger.error("====" + userId + "===getGoodmanWarrantyVerificationResponse()====>Exception : " + e);
            searchedData = "Status: Error; " + searchedData;
            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");
        }

        return response;

    }

}
