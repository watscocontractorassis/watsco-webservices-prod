package com.watsco.hvac.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.watsco.hvac.businesslayer.WarrantyClaimLayer;
import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dto.ClaimDTO;
import com.watsco.hvac.usagetracking.UsageTracking;
import java.sql.SQLException;
import java.util.logging.Level;

public class WarrantyClaimImpl {

//    protected static Logger logger = Logger.getLogger(WarrantyClaimImpl.class);
    private static final Logger logger = Logger.getLogger(WarrantyClaimImpl.class);

    WarrantyClaimLayer warrantyLayer = new WarrantyClaimLayer();

    public String getSOAPSubmitClaim(ClaimDTO claimDTO, HashMap<String, String> claimsMap, ArrayList<HashMap<String, String>> claimParts, String UId, int userId, int CLAIM_WID, String errorText, String SB_ID) throws SQLException {
        String status = "";
        String response = "";
        int inQuiry = 1;
        String processStatus = "";
        String claimNumber = "";
        String claimStatus = "";
        String rejectReason = "";
        String accNo = "";
        if (claimsMap != null) {
            claimNumber = claimsMap.get("claimNumber");
            claimStatus = claimsMap.get("claimStatus");

            if (claimsMap.containsKey("rejectReason")) {
                rejectReason = claimsMap.get("rejectReason");
                if (rejectReason.equalsIgnoreCase("Claim submission time exceeded")) {
                    rejectReason = "Claim time submission exceeded - the time between submitting the claim and the date service was performed is too long.";
                } else {
                    rejectReason = claimsMap.get("rejectReason");
                }
            }

            if (errorText.contains("'")) {
                errorText = errorText.replace("'", "''");
            }

            if (claimNumber != null) {
                if (claimNumber.length() <= 0) {
                    HashMap<String, String> hm = new HashMap<String, String>();
                    hm = claimDTO.getClaimMap();
                    claimNumber = hm.get("claimNumber");
                }
            }

            if (claimsMap.get("accountNumber") != null) {
                accNo = claimsMap.get("accountNumber");
                if (accNo.isEmpty()) {
                    accNo = SB_ID;
                }
            } else if (claimDTO.getClaimMap() != null) {
                if (claimDTO.getClaimMap().get("accountNumber") != null) {
                    accNo = claimDTO.getClaimMap().get("accountNumber");
                }

                if (accNo.isEmpty()) {
                    accNo = SB_ID;
                }
            } else {
                accNo = SB_ID;
            }
        }

        status = selectWarranty(CLAIM_WID, userId, status);

        if (status.equalsIgnoreCase("Save")) {
            processStatus = "Submitted";
        } else if (status.equalsIgnoreCase("Submitted")) {
            processStatus = "Save Submitted";
        }

        response = warrantyLayer.updateDB(claimDTO, claimsMap, claimParts, claimNumber, CLAIM_WID, claimStatus, UId, userId, inQuiry, errorText, rejectReason, SB_ID, accNo, processStatus);
        return response;
    }

    public String getTransferClaim(ClaimDTO claimDTO,
            String save, String SB_ID, int installId, String udid, int userId,
            String UId, int reportId, String buName, String returnType, int buWid, String processStatus) throws JSONException {

        String response = null;
        String searchedData = "";
        String shared = "";
        String CLAIM_Wid = "";
        String errorText = "";
        String rejectReason = "";
        String sqlQuery;
        Statement stmt;
        Connection con = null;
        ResultSet rs;
        String status = null;
        try {

            int CLAIM_WID = 0;
            int inQuiry = 0;

            HashMap<String, String> claimsMap = claimDTO.getClaimMap();
//            System.out.println("claimsMap :"+claimsMap);
//            logger.info(claimsMap);

            ArrayList<HashMap<String, String>> claimParts = claimDTO.getAlClaimParts();
            for (HashMap<String, String> claimPart : claimParts) {
//                System.out.println("claimParts :"+claimParts);
//                logger.info(claimParts);
            }

            String claimNumber = claimsMap.get("claimNumber");

//            if(claimsMap != null){
            if (claimsMap.containsKey("Claim_WID")) {
                CLAIM_Wid = claimsMap.get("Claim_WID");
                if (CLAIM_Wid.length() > 0) {
                    CLAIM_WID = Integer.parseInt(CLAIM_Wid);
                }
            } //            }
            else {
                CLAIM_WID = 0;
            }

            String accNo = "";
            if (claimsMap.get("accountNumber") != null) {
                accNo = claimsMap.get("accountNumber");
            } else {
                accNo = SB_ID;
            }

            String claimStatus = claimsMap.get("claimStatus");
            shared = claimsMap.get("shared");;
            if (claimStatus == null) {
                claimStatus = "";
            }
            if (claimNumber == null) {
                claimNumber = "";
            }

            if (CLAIM_WID == 0) {
                response = warrantyLayer.insertDB(claimDTO, claimsMap, claimParts, CLAIM_WID, claimNumber, claimStatus, userId, save, SB_ID, accNo, buWid, processStatus);
            } else {

                status = selectWarranty(CLAIM_WID, userId, status);

                if (status != null && status.equalsIgnoreCase("Save")) {
                    processStatus = "Save";
                } else if (status != null && status.equalsIgnoreCase("Submitted")) {
                    processStatus = "Save Submitted";
                }

                System.out.println("processStatus" + processStatus);
                response = warrantyLayer.updateDB(claimDTO, claimsMap, claimParts, claimNumber, CLAIM_WID, claimStatus, UId, userId, inQuiry, errorText, rejectReason, SB_ID, accNo, processStatus);

            }

            if (response.length() > 0) {
                response = response.substring(1, response.length() - 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("=== USER ID :" + userId + "===getTransferClaim()=====>" + e);
        }

        response.trim();
        JSONObject jObject = new JSONObject(response);
        CLAIM_Wid = jObject.getString("Claim_WID");

        if (shared != null) {
            if (shared.equalsIgnoreCase("Y")) {
                logger.info("=== USER ID :" + userId + "===getTransferClaim() Response =====>" + response);
                searchedData = "Claim Saved for Claim Transfer. CLAIM_WID - " + CLAIM_Wid;
                UsageTracking usageTracking = new UsageTracking();
                usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");
            } else {
                logger.info("=== USER ID :" + userId + "===getTransferClaim() Response =====>" + response);
                searchedData = "Claim Saved. CLAIM_WID - " + CLAIM_Wid;
                UsageTracking usageTracking = new UsageTracking();
                usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");

            }
        } else {
            logger.info("=== USER ID :" + userId + "===getTransferClaim() Response =====>" + response);
            searchedData = "Claim Saved, CLAIM_WID - " + CLAIM_Wid;
            UsageTracking usageTracking = new UsageTracking();
            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");

        }

        return response;
    }

    public String inQuiryClaim(ClaimDTO claimDTO, HashMap<String, String> claimsMap,
            ArrayList<HashMap<String, String>> claimParts, int Claim_WID, int userId, String errorText, String SB_ID,
            String buName) {
        ResultSet rs = null;
        Statement stmt;
        Connection con = null;
        String sqlQuery;
        String tempClaimNumber = null;
        String response = "";
        String claimNumber = "";
        String claimStatus = "";
        String UId = "";
        String RejectReason = "";
        String accNo = "";
        int accNoLength = 0;
        try {
            claimNumber = claimsMap.get("claimNumber");
            claimStatus = claimsMap.get("claimStatus");
            accNoLength = claimsMap.get("accountNumber").length();

            con = DBConnection.getConnection(false);
            sqlQuery = "EXEC [Warranty_Claims_SELECT] '" + Claim_WID + "'";
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sqlQuery);
            logger.info("=== USER ID :" + userId + "========>" + sqlQuery);
            while (rs.next()) {
                tempClaimNumber = rs.getString("claimNumber");
                accNo = rs.getString("accountNumber");
            }

            if (accNoLength != 5) {
                if (claimsMap.get("accountNumber") != null) {
                    accNo = claimsMap.get("accountNumber");
                } else {
                    accNo = SB_ID;
                }
            }

            if ((claimsMap.get("rejectReason") != null) && (claimsMap.get("rejectReason").length() > 0)) {
                RejectReason = claimsMap.get("rejectReason");
            } else {
                RejectReason = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
//        if(claimNumber!=null){
        if (claimNumber.equalsIgnoreCase(tempClaimNumber)) {
            int inQuiry = 1;
            response = warrantyLayer.updateDB(claimDTO, claimsMap, claimParts, claimNumber, Claim_WID, claimStatus, UId, userId, inQuiry, errorText, RejectReason, SB_ID, accNo, "");

            return response;
        } else {
            logger.info("=== USER ID :" + userId + "===inQuiryClaim() Response =====> Cannot Update Database, Please enter valid ClaimNumber");
            return "  Cannot Update Database, Please enter valid ClaimNumber  ";
        }

//	    }
    }

    public String selectWarranty(int CLAIM_WID, int userId, String status) {
        try {
            Connection con = DBConnection.getConnection(false);
            Statement stmt;
            ResultSet rs;
            String sqlQuery = "EXEC [Warranty_Claims_SELECT] '" + CLAIM_WID + "'";
            logger.info("=== USER ID :" + userId + "===selectWarranty()====>Report Query : " + sqlQuery);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sqlQuery);
            while (rs.next()) {
                status = rs.getString("processStatus");
            }
            return status;
        } catch (SQLException ex) {
            logger.error("=== USER ID :" + userId + "===selectWarranty()=====>" + ex);
        } catch (Exception ex) {
            logger.error("=== USER ID :" + userId + "===selectWarranty()=====>" + ex);
        }
        return null;
    }

}
