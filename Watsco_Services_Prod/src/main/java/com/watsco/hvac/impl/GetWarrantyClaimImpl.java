package com.watsco.hvac.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.NewWarrantyClaimXML;

public class GetWarrantyClaimImpl {
	private static Logger logger = Logger.getLogger(GetWarrantyClaimImpl.class); 
	
	public String getNewWarrantyClaimAPIInfo(String buName,int userId, String type,String partNumber) 	{
		Connection con 				= null;
        PreparedStatement  pstmt 	= null;
        ResultSet rs 				= null;
        String response             = null;
        String sqlQuery;
        try {
            con=DBConnection.getConnection(false);
           
            if(type.equalsIgnoreCase("Defect"))
            {
            	sqlQuery = "EXEC Warranty_Component_Defect '"+buName+"', '"+partNumber+"'";
            }
            else
            	sqlQuery = "EXEC Warranty_Dropdown '"+buName+"'";
            
            logger.info("=== USER ID :"+userId+"===>Query : "+sqlQuery);
            pstmt 	= con.prepareStatement(sqlQuery);
            rs = pstmt.executeQuery();
            response = NewWarrantyClaimXML.getWarrantyClaimInfoDocument(rs);        
            
        } catch (Exception e) {
            logger.error("=== USER ID :"+userId+"======>Exeption in getNewWarrantyClaimAPIInfo() : " + e);
            
        } finally {
//        	DBConnection.closeConnection(con, pstmt, rs1,rs2);        
        	}
        return response;
	}

	/*public String getViewClaimAuthentication(String referenceNumber,
			String buName, int installId, int userId, String udid,
			int reportId) {
	    String response             = null;
	    String sqlQuery = "";
		Connection con 				= null;
        PreparedStatement  pstmt 	= null;
        ResultSet rs 				= null;
        
        try {
            con=DBConnection.getConnection(false);
      
            sqlQuery =  "select * from dbo.Warranty_Claims where referenceNumber = "+"'"+referenceNumber+"'";
            logger.info("=========>Query : "+sqlQuery);
            pstmt 	= con.prepareStatement(sqlQuery);

            rs = pstmt.executeQuery();
            response = NewWarrantyClaimXML.getViewWarrantyClaimXmlDocument(rs);
	        
	        return response;
	    }
        catch(Exception e){
        	e.printStackTrace();
        }
		return response;
	}*/
}
