/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watsco.hvac.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sendgrid.SendGridException;
import com.watsco.hvac.common.SendGrid;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class MailingAPIImpl {
    
    private static final Logger logger = Logger.getLogger(MailingAPIImpl.class);

    public String sendEmail(String buName, int userId, String emailRequest, String[] emailId, String subject, String format) throws JsonProcessingException, SendGridException {

        String response;
        HashMap<String, String> exthm;
        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }
        
        String key = "";
        switch (buName.toUpperCase()) {
            case "CARRIER ENTERPRISE":
                key = "CE_";
                break;
            case "BAKER":
                key = "BK_";
                break;
            case "CARRIER ENTERPRISE MEXICO":
                key = "CE_MX_";
                break;
            case "GEMAIRE":
                key = "GM_";
                break;
            case "EAST COAST METALS":
                key = "ECM_";
                break;
            default:
                break;
        }
        String email = exthm.get(key + "MAILINGAPI_EMAIL");
        String password = exthm.get(key + "MAILINGAPI_PASSWORD");
        String emailFrom = exthm.get(key + "MAILINGAPI_EMAILFROM");
        if (emailId.length > 0) {
            SendGrid sendgrid = new SendGrid(email, password);
            SendGrid.Email emailSD = new SendGrid.Email();
//            String emailTo[] = {"mlalwani@teksystems.com"};
//                email.addSmtpApiTo(emailId);

            emailSD.addTo(emailId);
            emailSD.setFrom(emailFrom);
            emailSD.setSubject(subject);

            switch (format.toUpperCase()) {
                case "TEXT":
                    emailSD.setText(emailRequest);
                    break;
                case "HTML":
                    emailSD.setHtml(emailRequest);
                    break;
                default:
                    break;

            }

//        email.setHtml("<h1>Hello , </h1><html>I am inside HTMl</html>");
            SendGrid.Response responseSD = sendgrid.send(emailSD);
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writeValueAsString(responseSD);
            return jsonInString;
        } else {
            response = "Email ID cannot be NULL";
        }
        return null;
    }

}
