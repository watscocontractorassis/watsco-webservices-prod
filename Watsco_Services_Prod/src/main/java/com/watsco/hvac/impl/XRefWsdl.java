package com.watsco.hvac.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.AppConstants;
import com.watsco.hvac.dao.SoapConnection;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;

public class XRefWsdl {

    private static Logger logger = Logger.getLogger(XRefWsdl.class);

    String response;
    SoapConnection soapPost = new SoapConnection();

    public String getXmlCEXRefById(String id, String searchType, int userId) {

        //		logger.info("getXmlCEXRefById method invoked");
//        HashMap<String, String> hm = CommonUtil.getExternalAPIConfigProperties();
        HashMap<String, String> hm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            hm = CommonUtil.getExternalAPIConfigProperties();
        } else {
            hm = LocalCommonUtil.getExternalAPIConfigProperties();
        }

        String strURL = hm.get("CE_ID_URL");
        String strRequestXML = AppConstants.CE_BYID_REQUEST_XML;
        String strRequestHeaderXML = AppConstants.CE_BYID_REQUEST_HEADER_XML;

        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTID\\$\\$", id);
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTSEARCHTYPE\\$\\$", searchType);

        strRequestHeaderXML = strRequestHeaderXML.replaceAll("\\@\\@INPUTID\\$\\$", id);
        strRequestHeaderXML = strRequestHeaderXML.replaceAll("\\@\\@INPUTSEARCHTYPE\\$\\$", searchType);

        try {
            logger.info("=== USER ID :" + userId + "===getXmlCEXRefById()====>RequestURL : " + strURL);
            logger.info("=== USER ID :" + userId + "===getXmlCEXRefById()====>RequestXml : " + strRequestXML);
            response = soapPost.soapPostResponse(strRequestXML, new URL(strURL), "");
            //			logger.info("response : "+response);

            logger.info("=== USER ID :" + userId + "===getXmlCEXRefById()====>RequestURL : " + strURL);
            logger.info("=== USER ID :" + userId + "===getXmlCEXRefById()====>RequestXml : " + strRequestHeaderXML);
            String responseHeader = soapPost.soapPostResponse(strRequestHeaderXML, new URL(strURL), "");

            responseHeader = changeCompressorTags(responseHeader);

            response = response.replaceAll("\\]\\]\\>", "");
            response = response.replaceAll("\\<\\!\\[CDATA\\[", "");
            response = response.replaceAll("\\<xrefdata\\>", "\\<xrefdata\\>" + responseHeader);

        } catch (MalformedURLException e) {
            logger.error("Exception : " + e);
        } catch (Exception e) {
            logger.error("Exception : " + e);
        }

        return response;
    }

    public String getXmlCEXRefByParams(String cmprtype, String refrigerant, String voltage, int phase, String capOrTon, String capOrTonValue, int userId) {
        //		logger.info("getXmlCEXRefByParams method invoked");

        HashMap<String, String> hm = CommonUtil.getExternalAPIConfigProperties();

        String responseHeader = "\\<item\\>"
                + "\\<capacity\\>" + capOrTonValue + "\\</capacity\\>"
                + "\\<Cmptype\\>" + cmprtype + "\\</Cmptype\\>"
                + "\\<refrigerant\\>" + refrigerant + "\\</refrigerant\\>"
                + "\\<volts\\>" + voltage + "\\</volts\\>"
                + "\\<Hertz\\>60\\</Hertz\\>"
                + "\\<phase\\>" + phase + "\\</phase\\>"
                + "\\</item\\>";

        String response = "";

        String strURL = hm.get("CE_PARAM_URL");
        String strRequestXML = AppConstants.CE_BYPARAMS_REQUEST_XML;

        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTCMPRTYPE\\$\\$", cmprtype);
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTREFRIGERANT\\$\\$", refrigerant);
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTVOLTAGE\\$\\$", voltage);
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTPHASE\\$\\$", Integer.toString(phase));
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTCAPORTON\\$\\$", capOrTon);
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTCAPORTONVALUE\\$\\$", capOrTonValue);

        logger.info("=== USER ID :" + userId + "===getXmlCEXRefByParams()====>RequestXml : " + strRequestXML);
        try {
            response = soapPost.soapPostResponse(strRequestXML, new URL(strURL), "");

            response = response.replaceAll("\\]\\]\\>", "");
            response = response.replaceAll("\\<\\!\\[CDATA\\[", "");
            response = response.replaceAll("\\<xrefdata\\>", "\\<xrefdata\\>" + responseHeader);
        } catch (MalformedURLException e) {
            logger.info("=== USER ID :" +userId+"===="+e);
        } catch (Exception e) {
            logger.info("=== USER ID :" +userId+"===="+e);
        }
        return response;
    }

    public String getXmlBakerXRefById(String id, String searchType, int userId) {
        //		logger.info("getXmlBakerXRefById method invoked");

        HashMap<String, String> hm = CommonUtil.getExternalAPIConfigProperties();

        String strURL = hm.get("BAKER_ID_URL");
        String strRequestXML = AppConstants.BAKER_BYID_REQUEST_XML;
        String strRequestHeaderXML = AppConstants.CE_BYID_REQUEST_HEADER_XML;

        String response = "";

        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTID\\$\\$", id);
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTSEARCHTYPE\\$\\$", searchType);

        strRequestHeaderXML = strRequestHeaderXML.replaceAll("\\@\\@INPUTID\\$\\$", id);
        strRequestHeaderXML = strRequestHeaderXML.replaceAll("\\@\\@INPUTSEARCHTYPE\\$\\$", searchType);

        logger.info("=== USER ID :" + userId + "===getXmlBakerXRefById()====>RequestXml : " + strRequestXML);
        try {
            response = soapPost.soapPostResponse(strRequestXML, new URL(strURL), "");

            String responseHeader = soapPost.soapPostResponse(strRequestHeaderXML, new URL(strURL), "");
            logger.info("=== USER ID :" + userId + "===getXmlBakerXRefById()====>responseHeader Resp: " + responseHeader);

            responseHeader = changeCompressorTags(responseHeader);
            logger.info("=== USER ID :" + userId + "===getXmlBakerXRefById()====>responseHeader Final : " + responseHeader);

            response = response.replaceAll("\\]\\]\\>", "");
            response = response.replaceAll("\\<\\!\\[CDATA\\[", "");
            response = response.replaceAll("\\<xrefdata\\>", "\\<xrefdata\\>" + responseHeader);
        } catch (MalformedURLException e) {
            logger.info("===="+userId+"===="+e);
        } catch (Exception e) {
            logger.info("===="+userId+"===="+e);
        }
        return response;
    }

    public String getXmlBakerXRefByParams(String cmprtype, String refrigerant, String voltage, int phase, String capOrTon, String capOrTonValue, int userId) {
        //		logger.info("getXmlBakerXRefByParams method invoked");

        HashMap<String, String> hm = CommonUtil.getExternalAPIConfigProperties();

        String responseHeader = "\\<item\\>"
                + "\\<capacity\\>" + capOrTonValue + "\\</capacity\\>"
                + "\\<Cmptype\\>" + cmprtype + "\\</Cmptype\\>"
                + "\\<refrigerant\\>" + refrigerant + "\\</refrigerant\\>"
                + "\\<volts\\>" + voltage + "\\</volts\\>"
                + "\\<Hertz\\>60\\</Hertz\\>"
                + "\\<phase\\>" + phase + "\\</phase\\>"
                + "\\</item\\>";

        String response = "";

        String strURL = hm.get("BAKER_PARAM_URL");
        String strRequestXML = AppConstants.BAKER_BYPARAMS_REQUEST_XML;

        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTCMPRTYPE\\$\\$", cmprtype);
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTREFRIGERANT\\$\\$", refrigerant);
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTVOLTAGE\\$\\$", voltage);
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTPHASE\\$\\$", Integer.toString(phase));
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTCAPORTON\\$\\$", capOrTon);
        strRequestXML = strRequestXML.replaceAll("\\@\\@INPUTCAPORTONVALUE\\$\\$", capOrTonValue);

        logger.info("====" + userId + "===getXmlBakerXRefByParams()====>RequestXml : " + strRequestXML);
        try {
            response = soapPost.soapPostResponse(strRequestXML, new URL(strURL), "");

            response = response.replaceAll("\\]\\]\\>", "");
            response = response.replaceAll("\\<\\!\\[CDATA\\[", "");
            response = response.replaceAll("\\<xrefdata\\>", "\\<xrefdata\\>" + responseHeader);
        } catch (MalformedURLException e) {
            logger.info("=== USER ID :" +userId+"===="+e);
        } catch (Exception e) {
            logger.info("=== USER ID :" +userId+"===="+e);
        }
        return response;
    }

    private String changeCompressorTags(String responseHeader) {

        responseHeader = trimXrefHeader(responseHeader);

        responseHeader = responseHeader.replaceAll(AppConstants.CE_COMPRESSORDETAIL_TAG, AppConstants.CE_COMPRESSORDETAIL_CHANGE);
        responseHeader = responseHeader.replaceAll(AppConstants.CE_ITEM_TAG, AppConstants.CE_ITEM_CHANGE);
        responseHeader = responseHeader.replaceAll(AppConstants.CE_CAPACITY_TAG, AppConstants.CE_CAPACITY_CHANGE);
        responseHeader = responseHeader.replaceAll(AppConstants.CE_CMPRTYPE_TAG, AppConstants.CE_CMPRTYPE_CHANGE);
        responseHeader = responseHeader.replaceAll(AppConstants.CE_REFRIGERANT_TAG, AppConstants.CE_REFRIGERANT_CHANGE);
        responseHeader = responseHeader.replaceAll(AppConstants.CE_VOLTS_TAG, AppConstants.CE_VOLTS_CHANGE);
        responseHeader = responseHeader.replaceAll(AppConstants.CE_HERTZ_TAG, AppConstants.CE_HERTZ_CHANGE);
        responseHeader = responseHeader.replaceAll(AppConstants.CE_PHASE_TAG, AppConstants.CE_PHASE_CHANGE);

        return responseHeader;
    }

    private String trimXrefHeader(String responseHeader) {

        int startIndex = responseHeader.indexOf(AppConstants.CE_HEADER_TRIM_START);
        int EndIndex = responseHeader.indexOf(AppConstants.CE_HEADER_TRIM_END);

        responseHeader = responseHeader.substring(startIndex, EndIndex + AppConstants.CE_HEADER_TRIM_END.length());

        return responseHeader;
    }

}
