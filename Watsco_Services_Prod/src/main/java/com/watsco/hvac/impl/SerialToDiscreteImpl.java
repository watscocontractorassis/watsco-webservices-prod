package com.watsco.hvac.impl;

import com.watsco.hvac.common.CommonMethods;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.dao.DocumentConvertor;
import com.watsco.hvac.dao.SoapConnection;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;

public class SerialToDiscreteImpl {

//    private static Logger logger = Logger.getLogger(SerialToDiscreteImpl.class);
    private static final Logger logger = Logger.getLogger(SerialToDiscreteImpl.class);

    int index = 0;
    String modelSent = "";
    StringBuffer modelRequested = new StringBuffer();
    String newResultType = "";
    String oldModelNum = "";
    String discreetSubNo = "";
    String newDiscreetSubNo = "";

    public String OAuthAccess(String buName, int userId, int installId, String udid, int reportId, String serialNo,
            String modelNo, String latitude, String longitude) {
        String access_token = "";
        String response = "";

        ArrayList<String> discreetNo = new ArrayList<String>();
        JSONObject jObject = new JSONObject();
        JSONArray jArray = new JSONArray();

        if (((serialNo.contains(" ")) || (serialNo.contains("%20"))) || (serialNo.length() <= 6)) {
            response = internalModelSearch(serialNo, userId, reportId, installId, udid, discreetNo, buName, latitude, longitude, jObject);
            return response;
        }

        try {
            SoapConnection soapConnection = new SoapConnection();
            ///  expired case pending
            access_token = getSessionId(buName, soapConnection);

            if (access_token != null && access_token.length() > 0) {
                response = getRestModelSeriallookup(buName, userId, access_token, soapConnection, serialNo, modelNo);

                JSONObject jObjectResponse = new JSONObject(response);
                String msg = jObjectResponse.get("msg").toString();
//                response = "{\"$id\":\"1\",\"dataQualityIndicator\":0,\"derivedMfgDate\":null,\"mfgDate\":null,\"mfgWeek\":0,\"mfgYear\":0,\"model\":null,\"msg\":\"500: Internal Server Error: Please contact vendor web support team to notify them of the problem. Please include the Model and Serial being requested.\",\"serial\":null,\"serialVerified\":false,\"shippedDate\":null,\"suggestedResults\":null}";
                if (msg.contains("500")) {
                    logger.error("=== USER ID :" + userId + "=====>getRestModelSeriallookup() : " + response);
                    response = internalModelSearch(serialNo, userId, reportId, installId, udid, discreetNo, buName, latitude, longitude, jObject);
                    return response;
                }
                logger.info("=== USER ID :" + userId + "=====>getRestModelSeriallookup() : " + response);

                if (response != null && !response.equalsIgnoreCase("failure")) {
                    JSONObject jObj = new JSONObject(response);
                    JSONArray searchData = jObj.getJSONArray("suggestedResults");
                    if (searchData.length() == 0) {
                        if (modelNo.length() > 0 && modelNo.length() >= 12) {
                            modelNo = modelNo.substring(0, 12);
                            modelSent = modelNo;
                            newResultType = "Serial";
                            response = getRestModelSeriallookup(buName, userId, access_token, soapConnection, serialNo, modelNo);
                        }
                    }
                } else {
                    jObject.put("Status", "Failure");
                    jObject.put("ResultType", newResultType);
                    jObject.put("Result", new JSONArray());
                    jObject.put("ModelRequested", "");
                    jObject.put("ModelSent", "");
                    logger.info("====" + userId + "=====>" + jObject.toString());
                    return jObject.toString();
                }
            } else {
                jObject.put("Status", "Failure");
                jObject.put("ResultType", newResultType);
                jObject.put("Result", new JSONArray());
                jObject.put("ModelRequested", modelRequested);
                jObject.put("ModelSent", "");
// 	            	 jObject.put("Model Result", "[]");
                logger.info("=== USER ID :" + userId + "=====>getRestModelSeriallookup() : " + jObject.toString());
                return jObject.toString();
            }

            if ((response != null) && (response.contains("suggestedResults"))) {
                response = getDataFromAPI(buName, userId, reportId, installId, udid, serialNo, modelNo, access_token, response, discreetNo,
                        jObject, jArray, soapConnection, latitude, longitude, modelSent, modelRequested, newResultType);
//	            	if(response.contains("Multiple")){
//	            		return response;
//	            	}
                if (response.contains("discreetNo")) {
                    discreetNo.add(response);
                }
                if (response.contains("Status")) {
                    modelRequested.setLength(0);
                    return response;
                }
            }

            if (discreetNo != null && discreetNo.size() > 0) {
                response = serialSearch(userId, reportId, installId, udid, serialNo, discreetNo, buName, latitude, longitude, modelSent, modelRequested, newResultType);
                if (response == null && response.length() <= 0) {
                    if (modelNo.contains("-")) {
                        index = modelNo.indexOf("-");
                        oldModelNum = modelNo.substring(0, index);
                    }

                    jObject.put("Status", "Failure");
                    jObject.put("ResultType", newResultType);
                    jObject.put("Result", new JSONArray(response));
                    jObject.put("ModelRequested", modelRequested);
                    jObject.put("ModelSent", modelNo);
                    logger.info("====" + userId + "=====>" + jObject.toString());
                    return jObject.toString();
                } else {
                    if (modelNo.contains("-")) {
                        index = modelNo.indexOf("-");
                        oldModelNum = modelNo.substring(0, index);
                    }

                    jObject.put("Status", "Success");
                    jObject.put("ResultType", newResultType);
                    jObject.put("Result", new JSONArray(response));
                    jObject.put("ModelRequested", modelRequested);
                    jObject.put("ModelSent", modelNo);
                    logger.info("====" + userId + "=====>" + jObject.toString());
                    return jObject.toString();
                }
            }

            if (response.contains("Invalid user credentials")) {
                jObject.put("Status", "Invalid user credentials");
                jObject.put("Result", jArray);

                logger.info("====" + userId + "=====>" + jObject.toString());

                return jObject.toString();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public String internalModelSearch(String serialNo, int userId, int reportId, int installId, String udid, ArrayList<String> discreetNo,
            String buName, String latitude, String longitude, JSONObject jObject) throws JSONException {
        String response;

        modelSent = serialNo;
        modelRequested.append(serialNo);
        newResultType = "Model";
        response = modelSearch(userId, reportId, installId, udid, serialNo, discreetNo, buName, latitude, longitude, modelSent, modelRequested, newResultType);
        logger.info("=== USER ID :" + userId + "=====>getRestModelSeriallookup() : " + response);
        if (response.contains("Status")) {
            return response;
        } else if (response.contains("[]")) {
            jObject.put("Status", "Success");
            jObject.put("ResultType", newResultType);
            jObject.put("Result", new JSONArray());
            jObject.put("ModelRequested", modelRequested);
            jObject.put("ModelSent", modelSent);
            return jObject.toString();
        } else {
            jObject.put("Status", "Success");
            jObject.put("ResultType", newResultType);
            jObject.put("Result", new JSONArray(response));
            jObject.put("ModelRequested", modelRequested);
            jObject.put("ModelSent", modelSent);
            return jObject.toString();
        }
    }

    public String getDataFromAPI(String buName, int userId, int reportId, int installId, String udid,
            String serialNo, String modelNo, String access_token, String response,
            ArrayList<String> discreetNo, JSONObject jObject, JSONArray jArray,
            SoapConnection soapConnection, String latitude, String longitude, String modelSent,
            StringBuffer modelrequested, String newResultType) throws JSONException {

        JSONObject jSONObject = new JSONObject(response);
        Iterator<?> keys = jSONObject.keys();
        JSONArray array = jSONObject.getJSONArray("suggestedResults");

        while (keys.hasNext()) {

            if (array instanceof JSONArray) {

                for (int i = 0; i < array.length(); i++) {

                    JSONObject suggestedResults = array.getJSONObject(i);

                    if (!(suggestedResults.isNull("discreetNo"))) {
                        discreetNo.add(suggestedResults.getString("discreetNo"));
                    } else if (!(suggestedResults.isNull("model"))) {

                        discreetNo.add(suggestedResults.getString("model").trim());
                    } else {
                        jObject.put("Status", "Failure");
                        jObject.put("ResultType", newResultType);
                        jObject.put("Result", jArray);
                        jObject.put("ModelRequested", modelRequested);
                        jObject.put("ModelSent", "");
                        logger.info("=== USER ID :" + userId + "=====>getRestModelSeriallookup() : " + jObject.toString());
                        return jObject.toString();

                    }
                    modelRequested.append(discreetNo.get(i) + ",");
//		    		

                }
                if (modelRequested.length() > 0) {
//		    		modelRequested.substring(1, modelRequested.length()-1);
                    modelRequested.setLength(modelRequested.length() - 1);
                }
                if (array.length() > 1) {
                    jObject.put("Status", "Success");
                    jObject.put("ResultType", "Multiple");
                    jObject.put("Result", discreetNo);
                    jObject.put("ModelRequested", "");
                    jObject.put("ModelSent", "");
                    logger.info("=== USER ID :" + userId + "=====>getRestModelSeriallookup() : " + jObject.toString());
                    return jObject.toString();
                } else {
                    response = serialSearch(userId, reportId, installId, udid, serialNo, discreetNo, buName, latitude, longitude, modelSent, modelRequested, newResultType);
                    if (response.contains("Status")) {
                        return response;
                    }
                    if (!response.contains("[]")) {
                        jObject.put("Status", "Success");
                        jObject.put("ResultType", newResultType);
                        jObject.put("Result", new JSONArray(response));
                        jObject.put("ModelRequested", modelRequested);
                        jObject.put("ModelSent", modelSent);
                        //		             jObject.toString().replaceAll("\\\\", "");
                        logger.info("=== USER ID :" + userId + "=====>getRestModelSeriallookup() : " + jObject.toString());
                        return jObject.toString();
                    } else {
                        jObject.put("Status", "Failure");
                        jObject.put("ResultType", newResultType);
                        jObject.put("Result", new JSONArray());
                        jObject.put("ModelRequested", modelRequested);
                        jObject.put("ModelSent", serialNo);
//			             jObject.toString().replaceAll("\\\\", "");

                        logger.info("=== USER ID :" + userId + "=====>getRestModelSeriallookup() : " + jObject.toString());
                        return jObject.toString();
                    }
                }
            }
            break;   // break the while loop
        }

        if (discreetNo.isEmpty()) {
            if (modelNo.length() > 12) {
                oldModelNum = modelNo.substring(0, 12);
            }

//			 || (discreetNo.size()<=0)
//			 if((modelNo.length()<=0) ){
//				 jObject.put("Status", "Failure");
//				 jObject.put("ResultType", "Normal");
//				 jObject.put("Serial Result", array);
////				 jObject.put("Model Result", "[]");
//				 logger.info("=== USER ID :"+userId+"=====>getRestModelSeriallookup() : "+jObject.toString());
//				 return jObject.toString();
//			 }
            response = getRestModelSeriallookup(buName, userId, access_token, soapConnection, serialNo, oldModelNum);

            response = getDataFromAPI(buName, userId, reportId, installId, udid, serialNo, modelNo, access_token, response, discreetNo,
                    jObject, jArray, soapConnection, latitude, longitude, modelSent, modelRequested, newResultType);
        }

        if (discreetNo.size() == 0) {
            jObject.put("Status", "Failure");
            jObject.put("ResultType", newResultType);
            jObject.put("Result", new JSONArray(response));
            jObject.put("ModelRequested", modelRequested);
            jObject.put("ModelSent", "");
            logger.info("=== USER ID :" + userId + "=====>getRestModelSeriallookup() : " + jObject.toString());
            return jObject.toString();
        }

        if (response.length() >= 1) {
            return response;
        }

        return response;
    }

    private String getRestModelSeriallookup(String buName, int userId, String access_token,
            SoapConnection soapConnection, String serialNo, String modelNo) {

        try {
            String lookUpModelSerialUrl = "";
            String response = "";

            if (modelNo != null & modelNo.length() > 0) {
                lookUpModelSerialUrl = "http://phdbws.carrier.com/api/RestModelSeriallookup/" + modelNo + "/" + serialNo + "";
            } else {
                lookUpModelSerialUrl = "http://phdbws.carrier.com/api/RestSeriallookup/" + serialNo;
            }

            logger.info("====" + userId + "=====>RequestUrl : " + lookUpModelSerialUrl);

            response = soapConnection.soapget(new URL(lookUpModelSerialUrl), access_token);
            return response;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getSessionId(String buName, SoapConnection soapConnection) {

        String key = "";
        String access_token = "";

        switch (buName) {
            case "CARRIER ENTERPRISE":
                key = "CE_";
                break;
            case "BAKER":
                key = "BK_";
                break;
            case "CARRIER ENTERPRISE MEXICO":
                key = "CE_MX_";
                break;
            case "GEMAIRE":
                key = "GM_";
                break;
            case "EAST COAST METALS":
                key = "ECM_";
                break;
            default:
                break;
        }

//    	HashMap<String, String> exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        HashMap<String, String> exthm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }

        String url = exthm.get(key + "PHDB_OAUTH_ACCESS_API");
        String userName = exthm.get(key + "PHDB_OAUTH_ACCESS_USERNAME");
        String password = exthm.get(key + "PHDB_OAUTH_ACCESS_PASSWORD");

        String urlParameters = "grant_type=password&username=" + userName + "&password=" + password;

        try {
            String response = soapConnection.soapPostResponse(urlParameters, new URL(url), "");
            if (response != null || response.contains("API failed")) {
                JSONObject jObject = new JSONObject(response);
                access_token = jObject.getString("access_token");
            }
            //		     logger.info("sessionId: "+sessionId);
            HashMap<String, String> updateMap = new HashMap<String, String>();
            updateMap.put("access_token", access_token);
//            CommonUtil.updateInternalFileValues(updateMap);

            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                CommonUtil.updateInternalFileValues(updateMap);
            } else {
                LocalCommonUtil.updateInternalFileValues(updateMap);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("Exception : " + e);
        }
        return access_token;
    }

    public static String serialSearch(int userId, int reportId, int installId, String udid, String serialNo, ArrayList<String> discreetNo, String buName, String latitude, String longitude, String modelSent, StringBuffer modelRequested, String newResultType) {

        String serialSearch = "";
        String jsonResponse = "";
        String discreetSubNo = "";
        Connection con = null;
        ResultSet modelSearchRS = null;
        String response = "";
        Statement stmt = null;
        String newDiscreetSubNo = "";
//	        JSONArray jArrayModel = new JSONArray();
        int index = 0;
        JSONObject jObject = new JSONObject();
        try {
            con = DBConnection.getConnection(false);
            if (discreetNo.size() > 0) {
                discreetSubNo = discreetNo.get(0);
                modelSent = discreetSubNo;
                newResultType = "Serial";
            } else if (serialNo.length() > 0) {
                discreetSubNo = serialNo;
//	            	modelSent = serialNo;
//	            	modelRequested.append(serialNo);
                modelSent = "";
                modelRequested.append(serialNo);
                newResultType = "Model";
            } else {
                jObject.put("Status", "Failure");
                jObject.put("ResultType", newResultType);
                jObject.put("Result", new JSONArray(response));
                jObject.put("ModelRequested", discreetSubNo);
                jObject.put("ModelSent", serialNo);
                return jObject.toString();
            }

//	            discreetSubNo = discreetSubNo+"*";
            serialSearch = "exec model_search '" + discreetSubNo + "', '\"" + discreetSubNo + "*" + "\"', '" + buName + "','" + (CommonMethods.isNullorEmpty(latitude) ? null : CommonMethods.removeCharacterAfterPos(latitude, 6)) + "','" + (CommonMethods.isNullorEmpty(longitude) ? null : CommonMethods.removeCharacterAfterPos(longitude, 6)) + "'";
            serialSearch = serialSearch.replaceAll("'null'", "null");
            logger.info("=== USER ID :" + userId + "=====>Report Query : " + serialSearch);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            modelSearchRS = stmt.executeQuery(serialSearch);

            DocumentConvertor modelSearchConvertor = new DocumentConvertor();
            response = modelSearchConvertor.SerialToDiscreteJSONConvertor(modelSearchRS);

            if (response.contains("[]")) {

                if (discreetSubNo.contains("-")) {
                    index = discreetSubNo.indexOf("-");
                    newDiscreetSubNo = discreetSubNo.substring(0, index);
                    discreetNo.clear();
                    discreetNo.add(newDiscreetSubNo);
                    modelSent = newDiscreetSubNo;
                    newResultType = "Serial";
                    response = serialSearch(userId, reportId, installId, udid, serialNo, discreetNo, buName, latitude, longitude, modelSent, modelRequested, newResultType);

                }
                if (response.contains("[]")) {
                    discreetNo.clear();
                    discreetNo.add(serialNo);
                    modelSent = "";
                    newResultType = "Model";
//		            	response =  modelSearch(  userId,   reportId,  installId,  udid,  serialNo, discreetNo,  buName, latitude, longitude, modelSent, modelRequested,newResultType);
                    if (response.contains("Status")) {
                        return response;
                    }
                    if (!response.contains("[]")) {

                        jObject.put("Status", "Success");
                        jObject.put("ResultType", newResultType);
                        jObject.put("Result", new JSONArray(response));
                        jObject.put("ModelRequested", modelRequested);
                        jObject.put("ModelSent", modelSent);
                        return jObject.toString();
                    } else {
                        jObject.put("Status", "Success");
                        jObject.put("ResultType", "Serial");
                        jObject.put("Result", new JSONArray(response));
                        jObject.put("ModelRequested", modelRequested);
                        jObject.put("ModelSent", modelSent);
                        return jObject.toString();
                    }

                }
            } else if (!response.contains("[]")) {
                jObject.put("Status", "Success");
                jObject.put("ResultType", newResultType);
                jObject.put("Result", new JSONArray(response));
                jObject.put("ModelRequested", modelRequested);
                jObject.put("ModelSent", modelSent);
//		             jObject.toString().replaceAll("\\\\", "");
                logger.info("=== USER ID :" + userId + "=====>getRestModelSeriallookup() : " + jObject.toString());
                return jObject.toString();
            }

        } catch (Exception e) {
            logger.error("=========>Exeption in getViewClaimAuthentication() : " + e);

        }
        return response;

    }

    public static String modelSearch(int userId, int reportId, int installId, String udid, String serialNo, ArrayList<String> discreetNo, String buName, String latitude, String longitude, String modelSent, StringBuffer modelRequested, String newResultType) {

        String response;
        String serialSerach;
        ResultSet modelSearchRS;
        Statement stmt;
        Connection con;
        JSONObject jObject = new JSONObject();
        int index = 0;

        try {
            con = DBConnection.getConnection(false);
            serialSerach = "exec model_search '" + serialNo + "', '\"" + serialNo + "*" + "\"', '" + buName + "','" + (CommonMethods.isNullorEmpty(latitude) ? null : CommonMethods.removeCharacterAfterPos(latitude, 6)) + "','" + (CommonMethods.isNullorEmpty(longitude) ? null : CommonMethods.removeCharacterAfterPos(longitude, 6)) + "'";
            
            serialSerach = serialSerach.replaceAll("'null'", "null");

            logger.info("=== USER ID :" + userId + "=====>Report Query : " + serialSerach);
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            modelSearchRS = stmt.executeQuery(serialSerach);

            DocumentConvertor modelSearchConvertor = new DocumentConvertor();
            response = modelSearchConvertor.SerialToDiscreteJSONConvertor(modelSearchRS);

            if (response.contains("[]")) {

                if (serialNo.contains("-")) {
                    index = serialNo.indexOf("-");
                    serialNo = serialNo.substring(0, index);
//		            	discreetNo.clear();
//		            	discreetNo.add(newDiscreetSubNo);
                    response = modelSearch(userId, reportId, installId, udid, serialNo, discreetNo, buName, latitude, longitude, modelSent, modelRequested, newResultType);

                    if (response.contains("Status")) {
                        return response;
                    }

                }
                if (response.contains("[]")) {
                    jObject.put("Status", "Success");
                    jObject.put("ResultType", newResultType);
                    jObject.put("Result", new JSONArray());
                    jObject.put("ModelRequested", modelRequested);
                    jObject.put("ModelSent", modelSent);
                    return jObject.toString();

                }
            }

            return response;

        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }
    }
}
