package com.watsco.hvac.impl;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.watsco.hvac.dao.SoapConnection;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.LocalCommonUtil;
import com.watsco.hvac.utils.LoginXmlParser;

public class GemaireLoginImpl {

//    protected static Logger logger = Logger.getLogger(GemaireLoginImpl.class);
    private static final Logger logger = Logger.getLogger(GemaireLoginImpl.class);

    public String getCustomerAuthentication(String email, String password, String buName, int userId, int installId, String udid, int reportId) {
        //		logger.info("GemaireLoginImpl getCustomerAuthenticate method invoked");
        //		String url = "http://gemaire.zeondemo.com/index.php/api/v2_soap/index/";
        String key1 = "";

        switch (buName) {
            case "CARRIER ENTERPRISE":
                key1 = "CE_";
                break;
            case "BAKER":
                key1 = "BK_";
                break;
            case "CARRIER ENTERPRISE MEXICO":
                key1 = "CE_MX_";
                break;
            case "GEMAIRE":
                key1 = "GM_";
                break;
            case "EAST COAST METALS":
                key1 = "ECM_";
                break;
            default:
                break;
        }
        HashMap<String, String> exthm;
        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }
        String url = exthm.get(key1 + "API");

        HashMap<String, String> hm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            hm = CommonUtil.getInternalFileProperties();
        } else {
            hm = LocalCommonUtil.getInternalFileProperties();
        }

//        HashMap<String, String> hm = CommonUtil.getInternalFileProperties();
        String sessionId = hm.get("sessionId");
        String expiredMessage = "<faultcode>5</faultcode>";
        String response = "";

        try {
            SoapConnection soapConnection = new SoapConnection();
            logger.info("====getCustomerAuthentication()=====>RequestUrl : " + url);

            response = getResponseForGemaireLogin(email, password, buName, userId, url, sessionId, soapConnection);

            if (response.contains(expiredMessage)) {
                logger.info("===getCustomerAuthentication()======>sessionId expired");
                sessionId = getSessionId(buName);

                logger.info("===getCustomerAuthentication()======>New Session Id : " + sessionId);

                response = getResponseForGemaireLogin(email, password, buName, userId, url, sessionId, soapConnection);
            }
            String searchedData = "";
            if (response.contains("The user is inactive")) {
                response = response.replaceAll("The user is inactive", "Account is inactive, please contact Gemaire");
                searchedData = "Account is inactive, please contact Gemaire";
                logger.info("====" + userId + "=====>Error - Account is inactive, please contact Gemaire");
            } else if (response.contains("<email xsi:type=\"xsd:string\">")) {
                LoginXmlParser loginXmlParser = new LoginXmlParser(buName, userId, String.valueOf(0), "");
                SAXParserFactory spf = SAXParserFactory.newInstance();
                SAXParser saxParser = spf.newSAXParser();
                XMLReader xmlReader = saxParser.getXMLReader();

                xmlReader.setContentHandler(loginXmlParser);
                xmlReader.parse(new InputSource(new ByteArrayInputStream(response.getBytes())));
                searchedData = "Login Successfull";
                String endResp = "</result></ns1:mobilecustomerauthCustomerResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>";
                response = response.replace(endResp, "<id>" + loginXmlParser.id + "</id>" + endResp);

            } else if (response.contains(expiredMessage)) {
                response = response.replaceAll("Session expired. Try to relogin.", "Authentication Error. Please contact Watsco Administrator");
                searchedData = "Error - Invalid Authentication Credentials";
                logger.info("===getCustomerAuthentication()======>Error - Invalid Authentication Credentials");
            } else {
                response = response.replaceAll("Invalid login or password.", "There was a problem validating your username or password. Please check your login credentials and try to sign in again");
                searchedData = "Invalid Login Credentials";
                logger.info("===getCustomerAuthentication()======>Invalid Login Credentials");
            }
            UsageTracking usageTracking = new UsageTracking();
            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, email, "");
        } catch (Exception e) {

            logger.error("UserId =" + userId + ", Email Id = " + email + ", Password = " + password + ", buName = " + buName + ", InstallId = " + installId + ", udid = " + udid + ", reportId = " + reportId + "\n Exception ===> " + e);
        }
        return response;
    }

    private String getResponseForGemaireLogin(String email, String password, String buName,
            int userId, String url, String sessionId,
            SoapConnection soapConnection) throws Exception,
            MalformedURLException {

        String requestXml;
        String response;
        String sendFinalRequest;

        String customerAuthRequestXmlPrint = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
                + "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
                + "xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
                + "xmlns:urn=\"urn:Magento\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<urn:mobilecustomerauthCustomer soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                + "<sessionId xsi:type=\"xsd:string\">" + sessionId + "</sessionId>"
                + "<email xsi:type=\"xsd:string\">" + email + "</email>"
                + "<password xsi:type=\"xsd:string\">XXXXXX</password>"
                + //"<![CDATA[<password xsi:type=\"xsd:string\">@@SECPARAMETER$$</password>]]>"+
                "</urn:mobilecustomerauthCustomer>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        requestXml = getRequestXml(sessionId, email, password, false);

        sendFinalRequest = requestXml.replaceAll("&lt;", "<");
        sendFinalRequest = sendFinalRequest.replaceAll("&gt;", ">");
        String key1 = "";
        switch (buName) {
            case "CARRIER ENTERPRISE":
                key1 = "CE_";
                break;
            case "BAKER":
                key1 = "BK_";
                break;
            case "CARRIER ENTERPRISE MEXICO":
                key1 = "CE_MX_";
                break;
            case "GEMAIRE":
                key1 = "GM_";
                break;
            case "EAST COAST METALS":
                key1 = "ECM_";
                break;
            default:
                break;
        }

        HashMap<String, String> exthm;
        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            if (CommonUtil.getExternalAPIConfigProperties(buName).get(key1 + "LOGIN_SHOW_PASSWORD").equalsIgnoreCase("Y")) {
                logger.info("====" + userId + "===getResponseForGemaireLogin()====>RequestXml: " + sendFinalRequest);
            } else {
                logger.info("====" + userId + "===getResponseForGemaireLogin()====>RequestXml : " + customerAuthRequestXmlPrint);
            }
        } else if (LocalCommonUtil.getExternalAPIConfigProperties(buName).get(key1 + "LOGIN_SHOW_PASSWORD").equalsIgnoreCase("Y")) {
            logger.info("====" + userId + "===getResponseForGemaireLogin()====>RequestXml: " + sendFinalRequest);
        } else {
            logger.info("====" + userId + "===getResponseForGemaireLogin()====>RequestXml : " + customerAuthRequestXmlPrint);
        }

        /* if (CommonUtil.getExternalAPIConfigProperties(buName).get(key1 + "LOGIN_SHOW_PASSWORD").equalsIgnoreCase("Y")) {
            logger.info("====" + userId + "=====>RequestXml: " + sendFinalRequest);
        } else {
            logger.info("====" + userId + "=====>RequestXml : " + customerAuthRequestXmlPrint);
        }*/
        response = soapConnection.soapPostResponse(sendFinalRequest, new URL(url), "");
        return response;
    }

    public String getCustomerAuthenticationByAccNo(String email, String accNo, String buName, int userId, int installId, String udid, int reportId, String regionURL) {
        //		logger.info("GemaireLoginImpl getCustomerAuthenticationByAccNo method invoked");
        String key1 = "";
        switch (buName) {
            case "CARRIER ENTERPRISE":
                key1 = "CE_";
                break;
            case "BAKER":
                key1 = "BK_";
                break;
            case "CARRIER ENTERPRISE MEXICO":
                key1 = "CE_MX_";
                break;
            case "GEMAIRE":
                key1 = "GM_";
                break;
            case "EAST COAST METALS":
                key1 = "ECM_";
                break;
            default:
                break;
        }

        HashMap<String, String> exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        String url = exthm.get(key1 + "API");
        //		String url = "http://gemaire.zeondemo.com/index.php/api/v2_soap/index/";
        HashMap<String, String> hm = CommonUtil.getInternalFileProperties();

        String sessionId = hm.get("sessionId");
        String requestXml = getRequestXml(sessionId, email, accNo, true);
        String expiredMessage = "<faultcode>5</faultcode>";
        String response = "";
        try {
            SoapConnection soapConnection = new SoapConnection();
            logger.info("====" + userId + "===getCustomerAuthenticationByAccNo()====>RequestUrl : " + url);
            logger.info("====" + userId + "===getCustomerAuthenticationByAccNo()====>RequestXml : " + requestXml);
            //		   	 logger.info("===="+userId+"=====>RequestXml : "+requestXml.replaceAll(accNo, "xxxxxxx"));
            response = soapConnection.soapPostResponse(requestXml, new URL(url), "");

            if (response.contains(expiredMessage)) {
                logger.info("====" + userId + "===getCustomerAuthenticationByAccNo()====>sessionId expired");
                sessionId = getSessionId(buName);

                logger.info("====" + userId + "=====>Session Id : " + sessionId);
                requestXml = getRequestXml(sessionId, email, accNo, true);
                logger.info("====" + userId + "===getCustomerAuthenticationByAccNo()====>RequestUrl : " + url);
                logger.info("====" + userId + "===getCustomerAuthenticationByAccNo()====>RequestXml : " + requestXml);
                //			   	 logger.info("===="+userId+"=====>RequestXml : "+requestXml.replaceAll(accNo, "xxxxxxx"));
                response = soapConnection.soapPostResponse(requestXml, new URL(url), "");
            }
            String searchedData = "";
            if (response.contains("The user is inactive")) {
                response = response.replaceAll("The user is inactive", "Account is inactive, please contact Gemaire");
                searchedData = "Account is inactive, please contact Gemaire";
                logger.info("====" + userId + "===getCustomerAuthenticationByAccNo()====>Error - Account is inactive, please contact Gemaire");
            } else if (response.contains("<email xsi:type=\"xsd:string\">")) {
                LoginXmlParser loginXmlParser = new LoginXmlParser(buName, userId, accNo, regionURL);
                SAXParserFactory spf = SAXParserFactory.newInstance();
                SAXParser saxParser = spf.newSAXParser();
                XMLReader xmlReader = saxParser.getXMLReader();

                xmlReader.setContentHandler(loginXmlParser);
                xmlReader.parse(new InputSource(new ByteArrayInputStream(response.getBytes())));
                searchedData = "Authentication Successfull";
            } else if (response.contains(expiredMessage)) {
                response = response.replaceAll("Session expired. Try to relogin.", "Authentication Error. Please contact Watsco Administrator");
                searchedData = "Error - Invalid Authentication Credentials";
                logger.info("====" + userId + "=====>Error - Invalid Authentication Credentials");
            } else {
                response = response.replaceAll("Email address and customer-account number mismatch", "There is a problem validating your credentials, please try again or try later");
                searchedData = "Email address and customer-account number mismatch";
                logger.info("====" + userId + "===getCustomerAuthenticationByAccNo()==>Email address and customer-account number mismatch");
            }

            UsageTracking usageTracking = new UsageTracking();
            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("UserId =" + userId + ", Email Id = " + email + ", accNo = " + accNo + ", buName = " + buName + ", udid = " + udid + ", InstallId = " + installId + ", reportId = " + reportId + "\n Exception ===> " + e);
        }
        //	logger.info("response: "+ response);
        return response;
    }

    public String getSessionId(String buName) {

        String key1 = "";

        switch (buName) {
            case "CARRIER ENTERPRISE":
                key1 = "CE_";
                break;
            case "BAKER":
                key1 = "BK_";
                break;
            case "CARRIER ENTERPRISE MEXICO":
                key1 = "CE_MX_";
                break;
            case "GEMAIRE":
                key1 = "GM_";
                break;
            case "EAST COAST METALS":
                key1 = "ECM_";
                break;
            default:
                break;
        }

        HashMap<String, String> exthm;
        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }

//        HashMap<String, String> exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        String url = exthm.get(key1 + "API");
        String userName = exthm.get(key1 + "USERNAME");
        String password = exthm.get(key1 + "PASSWORD");
        //		String url = "http://gemaire.zeondemo.com/index.php/api/v2_soap/index/";
        String loginRequestXml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:Magento\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<urn:login soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                + "<username xsi:type=\"xsd:string\">" + userName + "</username>"
                + "<apiKey xsi:type=\"xsd:string\">" + password + "</apiKey>"
                + "</urn:login>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        String sessionId = "";

        try {
            SoapConnection soapConnection = new SoapConnection();
            String response = soapConnection.soapPostResponse(loginRequestXml, new URL(url), "");
            sessionId = getTagValue("<loginReturn xsi:type=\"xsd:string\">", "</loginReturn>", response);
            //		     logger.info("sessionId: "+sessionId);
            HashMap<String, String> updateMap = new HashMap<String, String>();

            updateMap.put("sessionId", sessionId);

            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                CommonUtil.updateInternalFileValues(updateMap);
            } else {
                LocalCommonUtil.updateInternalFileValues(updateMap);
            }

//            CommonUtil.updateInternalFileValues(updateMap);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("Exception : " + e);
        }
        return sessionId;
    }

    public static String getTagValue(final String startTag,
            final String endTag, String pageSource) {
        //		logger.info("getTagValue method invoked");
        Pattern htmltag = Pattern.compile(startTag + "(.*?)"
                + endTag);
        Matcher tagmatch = htmltag.matcher(pageSource.toString());

        StringBuffer sb = new StringBuffer();

        int i = 0;
        while (tagmatch.find()) {
            String status = tagmatch.group()
                    .replace(startTag, "")
                    .replace(endTag, "");

            if (i == 0) {
                sb.append(status);
            }

            i++;

        }

        return sb.toString();
    }

    public static String getRequestXml(String sessionId, String email, String parameter, boolean isAccNo) {
        //		logger.info("GemaireLoginImpl getRequestXml");

        parameter = parameter.replaceAll("<", "&lt;");
        parameter = parameter.replaceAll(">", "&gt;");

        String customerAuthRequestXml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:Magento\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<urn:mobilecustomerauthCustomer soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                + "<sessionId xsi:type=\"xsd:string\">" + sessionId + "</sessionId>"
                + "<email xsi:type=\"xsd:string\">" + email + "</email>"
                + "<password xsi:type=\"xsd:string\"><![CDATA[" + parameter + "]]></password>"
                + //"<![CDATA[<password xsi:type=\"xsd:string\">@@SECPARAMETER$$</password>]]>"+
                "</urn:mobilecustomerauthCustomer>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        String customerAuthByAccNoRequestXml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:Magento\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<urn:mobilecustomerauthCustomerByAcctno soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                + "<sessionId xsi:type=\"xsd:string\">" + sessionId + "</sessionId>"
                + "<email xsi:type=\"xsd:string\">" + email + "</email>"
                + "<accountNumber xsi:type=\"xsd:string\">" + parameter + "</accountNumber>"
                + "</urn:mobilecustomerauthCustomerByAcctno>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";
        String requestXml = "";
        if (isAccNo) {
            requestXml = customerAuthByAccNoRequestXml;
        } else {
            requestXml = customerAuthRequestXml;
        }

        //
        //		requestXml=requestXml.replaceAll("@@SESSIONID\\$\\$", sessionId);
        //		requestXml=requestXml.replaceAll("@@EMAIL\\$\\$", email);
        //		requestXml = StringUtils.replaceAll(requestXml, "@@SECPARAMETER\\$\\$", parameter, "$");
        ////
        //		if(requestXml.contains("@@SECPARAMETER$$"))
        //			requestXml = StringUtils.replaceAll(requestXml, "@@SECPARAMETER\\$\\$", parameter, "{");
        //		else
        //			requestXml = StringUtils.replaceAll(requestXml, parameter, parameter, "{");
        ////		
        //		if(requestXml.contains("@@SECPARAMETER$$"))
        //			requestXml = StringUtils.replaceAll(requestXml, "@@SECPARAMETER\\$\\$", parameter, "}");
        //		else
        //			requestXml = StringUtils.replaceAll(requestXml, parameter, parameter, "}");
        ////
        //		if(requestXml.contains("@@SECPARAMETER$$"))
        //			requestXml = StringUtils.replaceAll(requestXml, "@@SECPARAMETER\\$\\$", parameter, "]");
        //		else
        //			requestXml = StringUtils.replaceAll(requestXml, parameter, parameter, "]");
        //
        //		requestXml=requestXml.replaceAll("@@SECPARAMETER\\$\\$", parameter);
        //		requestXml=requestXml.replaceAll("@@<![CDATA[pass>word]]>\\$\\$", parameter);
        //		logger.info("requestXml"+requestXml);
        return requestXml;

    }

    /*public void signOutActiveInd(String status,String buName, int userId) {
     //		logger.info("signOutActiveInd method invoked");
     Connection con 				= null;
     PreparedStatement  pstmt 	= null;
     ResultSet rs 				= null;
     
     try {
     con=DBConnection.getConnection(false);
     //			logger.info("====================>Got Metadata Connection");
     
     String sqlQuery = "UPDATE SURFBI_USERS SET REGISTERED= ? where USER_ID= ? and BU_NAME=?";
     
     logger.info("UPDATE SURFBI_USERS SET REGISTERED='"+status+"' where USER_ID="+userId+" and BU_NAME='"+buName+"'");
     
     pstmt 	= con.prepareStatement(sqlQuery);
     
     pstmt.setString(1, status);
     pstmt.setInt(2, userId);
     pstmt.setString(3, buName);
     
     pstmt.executeUpdate();
     
     logger.info("UPDATE SURFBI_USERS SET REGISTERED='"+status+"' where USER_ID="+userId+" and BU_NAME='"+buName+"'");
     
     } catch (Exception e) {
     // TODO Auto-generated catch block
     logger.error("=======> Error updating loguserRegistration: " + e);
     
     } finally {
     DBConnection.closeConnection(con, pstmt, rs);
     }
     
     }*/
}
