package com.watsco.hvac.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sendgrid.SendGridException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.xml.sax.InputSource;

import com.watsco.hvac.common.CommonMethods;
import com.watsco.hvac.dao.SoapConnection;
import com.watsco.hvac.dto.ClaimDTO;
import com.watsco.hvac.dto.WarrantyResponseDTO;
import com.watsco.hvac.handler.WarrantyXMLHandler;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.EncyrptionUtils;
import com.watsco.hvac.utils.LocalCommonUtil;

public class GetClaimInquiryImpl {

//    protected static Logger logger = Logger.getLogger(GetClaimInquiryImpl.class);
    
    private static final Logger logger = Logger.getLogger(GetClaimInquiryImpl.class);

    public String GetClaimInquiryReport(String claimNumber, int Claim_WID, String buName, int installId, int userId, String udid, int reportId, String type, String isDev) {
        String errorText = "";
        String searchedData = "";
        String InQuiryRequest = "";
        String mandrilResponse = "";
        String SB_ID = "";
        String UId = "";
        String password = "";
        String InQuiryResponse = "";
        SoapConnection soapConnection = new SoapConnection();
//		HashMap<String, String> exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        HashMap<String, String> exthm;

        if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
            exthm = CommonUtil.getExternalAPIConfigProperties(buName);
        } else {
            exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
        }

        String key = "";

        switch (buName) {
            case "CARRIER ENTERPRISE":
                key = "CE_";
                break;
            case "BAKER":
                key = "BK_";
                break;
            case "CARRIER ENTERPRISE MEXICO":
                key = "CE_MX_";
                break;
            case "GEMAIRE":
                key = "GM_";
                break;
            case "EAST COAST METALS":
                key = "ECM_";
                break;
            default:
                break;
        }
        String claimUrl;
        if (!(isDev.equalsIgnoreCase("Y"))) {
            claimUrl = exthm.get(key + "WarrantyClaim_SerivceBench_API_PROD");
//			   claimUrl = "https://www.servicebench.com/servicebenchv5/services/RTIClaimService";
            if (claimUrl == null || claimUrl.isEmpty()) {
                logger.error("=== USER ID :" + userId + "===GetClaimInquiryReport()====>WarrantyServiceBench API not Found");
                return InQuiryResponse;
            }
        } else {
            claimUrl = exthm.get(key + "WarrantyClaim_SerivceBench_API_DEV");
//			   claimUrl = "https://test.servicebench.com/servicebenchv5/services/RTIClaimService";
            if (claimUrl == null || claimUrl.isEmpty()) {
                logger.error("=== USER ID :" + userId + "===GetClaimInquiryReport()====>WarrantyServiceBench API not Found");
                return InQuiryResponse;
            }
        }

        try {
            WatscoCommonImpl credentialsDetails = new WatscoCommonImpl();
            String jsonResponse = credentialsDetails.getCredentials("ServiceBench", type, buName, userId, isDev);
            JSONObject jObject = new JSONObject(jsonResponse);
            SB_ID = jObject.getString("SB_ID");
            UId = jObject.getString("UId");
            password = jObject.getString("password");

            String SB_UID = SB_ID + ":" + UId;

            String version = exthm.get(key + "version");
            String sourceSystemName = exthm.get(key + "sourceSystemName");
            String sourceSystemVersion = exthm.get(key + "sourceSystemVersion");
            String serviceAdministratorId = exthm.get(key + "serviceAdministratorId");

            InQuiryRequest
                    = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://servicebench.com/claim/service/types\">"
                    + "<soapenv:Header>"
                    + "<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                    + "<wsse:UsernameToken wsu:Id=\"UsernameToken-15231583\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
                    + "<wsse:Username>" + SB_UID + "</wsse:Username>"
                    + "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + password + "</wsse:Password>"
                    + "</wsse:UsernameToken>"
                    + "</wsse:Security>"
                    + "</soapenv:Header>"
                    + "<soapenv:Body>"
                    + "<typ:claimInquiryRequest>"
                    + "<typ:version>" + version + "</typ:version>"
                    + "<typ:sourceSystemName>" + sourceSystemName + "</typ:sourceSystemName>"
                    + "<typ:sourceSystemVersion>" + sourceSystemVersion + "</typ:sourceSystemVersion>"
                    + "<typ:claim>"
                    + "<typ:claimNumber>" + claimNumber + "</typ:claimNumber>"
                    + "<typ:serviceAdministratorId>" + serviceAdministratorId + "</typ:serviceAdministratorId>"
                    + "<typ:referenceNumber></typ:referenceNumber>"
                    + "</typ:claim>"
                    + "</typ:claimInquiryRequest>"
                    + "</soapenv:Body>"
                    + "</soapenv:Envelope>";

            logger.info("=== USER ID :" + userId + "===GetClaimInquiryReport()====>requestUrl : " + claimUrl);
            logger.info("=== USER ID :" + userId + "===GetClaimInquiryReport()====>Soap Request : " + InQuiryRequest);
            String decryptPassword = EncyrptionUtils.decryptCredentials(password);
            InQuiryRequest = InQuiryRequest.replaceAll(password, decryptPassword);

            InQuiryResponse = soapConnection.soapPostResponse(InQuiryRequest, new URL(claimUrl), "");

            String claimStatus = CommonMethods.getWarrantyTagValue("<claimStatus>", "</claimStatus>", InQuiryResponse);
            logger.info("=== USER ID :" + userId + "===GetClaimInquiryReport()====>Claim Status : " + claimStatus);
            if (!InQuiryResponse.equalsIgnoreCase("API failed")) {
                logger.info("=== USER ID :" + userId + "===GetClaimInquiryReport()====>Soap Response : " + InQuiryResponse);
                if (!claimNumber.equalsIgnoreCase("")) {
                    SAXParserFactory factory = SAXParserFactory.newInstance();
                    SAXParser saxParser = factory.newSAXParser();
                    ClaimDTO claimDTO = new ClaimDTO();
                    WarrantyXMLHandler userhandler = new WarrantyXMLHandler(claimDTO, UId, userId, Claim_WID, SB_ID, buName);
                    saxParser.parse(new InputSource(new StringReader(InQuiryResponse)), userhandler);
                    InQuiryResponse = WarrantyResponseDTO.getInQuiryResponse();

                    if (InQuiryResponse.length() > 0) {
                        InQuiryResponse = InQuiryResponse.substring(1, InQuiryResponse.length() - 1);
                    }
                    searchedData = " Warranty Processing Claim InQuiry. Claim Number - " + claimNumber;

                    UsageTracking usageTracking = new UsageTracking();
                    usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");

                    return InQuiryResponse;
                }
            }
            logger.error("=== USER ID :" + userId + "===GetClaimInquiryReport()====>Soap Response : " + InQuiryResponse);
            if (InQuiryResponse.contains("errorText")) {
                errorText = CommonMethods.getWarrantyTagValue("<errorText>", "</errorText>", InQuiryResponse);
                mandrilResponse = sendMail(buName, userId, InQuiryRequest, InQuiryResponse, claimNumber, Claim_WID, errorText);

                System.out.println("mandrilResponse : " + mandrilResponse);
                searchedData = "Warranty Processing InQuiry Claim API : " + errorText;
            } else {
                searchedData = "Warranty Processing InQuiry Claim API Failure ";
            }

            InQuiryResponse = "<RESULT>" + InQuiryResponse + "</RESULT>";

            logger.info("=== USER ID :" + userId + "====CLAIM INQUIRY INFO=====>" + searchedData);

            UsageTracking usageTracking = new UsageTracking();
            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");

            return InQuiryResponse;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            logger.error("=== USER ID :" + userId + "===CLAIM INQUIRY ERROR=====>" + e);

            mandrilResponse = sendMail(buName, userId, InQuiryRequest, InQuiryResponse, claimNumber, Claim_WID, e.toString());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("=== USER ID :" + userId + "===CLAIM INQUIRY ERROR=====>" + e);

            mandrilResponse = sendMail(buName, userId, InQuiryRequest, InQuiryResponse, claimNumber, Claim_WID, e.toString());
        }
        return null;
    }

    /* public String sendMail(String buName, String claimRequest, String claimResponse, String claimNumber, int Claim_WID, String error) {

     String response = "";
     try {

     //			HashMap<String, String> exthm = CommonUtil.getExternalAPIConfigProperties(buName);
     HashMap<String, String> exthm;

     if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
     exthm = CommonUtil.getExternalAPIConfigProperties(buName);
     } else {
     exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
     }

     String key = "";

     switch (buName) {
     case "CARRIER ENTERPRISE":
     key = "CE_";
     break;
     case "BAKER":
     key = "BK_";
     break;
     case "CARRIER ENTERPRISE MEXICO":
     key = "CE_MX_";
     break;
     case "GEMAIRE":
     key = "GM_";
     break;
     case "EAST COAST METALS":
     key = "ECM_";
     break;
     default:
     break;
     }

     String emailFromMandrill = exthm.get(key + "CONFIRMATION_EMAIL");
     String keyForMandrill = exthm.get(key + "CONFIRMATION_KEY");

     claimRequest = claimRequest.replaceAll("\"", "\\\\\\\\\"");
     claimResponse = claimResponse.replaceAll("\"", "\\\\\\\\\"");

     String messageBody = "Hello,\\\\n\\\\nClaim InQuiry Error Details:\\\\nClaim_WID : " + Claim_WID + "\\\\n\\\\nClaim Number : " + claimNumber + "\\\\n\\\\nClaim Request: \\\\n\\\\t" + claimRequest + "\\\\n\\\\nClaim Response: \\\\n\\\\t" + claimResponse + "\\\\n\\\\nError: \\\\n\\\\t" + error + "\\\\n\\\\nThanks.";
     String subject = "Claim InQuiry Error Details - CE Assist App";

     String mandrillString = "{"
     + "\"key\": \"" + keyForMandrill + "\","
     + "\"message\": {"
     + "\"text\": \"" + messageBody + "\","
     + "\"subject\": \"" + subject + "\","
     + "\"from_email\": \"" + emailFromMandrill + "\","
     + "\"to\": ["
     + "{"
     + "\"email\": \"mlalwani@teksystems.com\","
     + "\"name\": \"Mamta Lalwani\","
     + "\"type\": \"to\""
     + "},"
     + "{"
     + "\"email\": \"pjayapal@teksystems.com\","
     + "\"name\": \"PrithiviRaj Jayapal\","
     + "\"type\": \"to\""
     + "},"
     + "{"
     + "\"email\": \"mlamba@teksystems.com\","
     + "\"name\": \"Lamba, Manik\","
     + "\"type\": \"to\""
     + "},"
     + "{"
     + "\"email\": \"mkaur@teksystems.com\","
     + "\"name\": \"Kaur, Monalisa\","
     + "\"type\": \"to\""
     + "},"
     + "{"
     + "\"email\": \"uyalanat@teksystems.com\","
     + "\"name\": \"Yalanati, Uma\","
     + "\"type\": \"to\""
     + "}"
     + "]"
     + "}"
     + "}";

     mandrillString = mandrillString.replaceAll("\\$\\$subject\\@\\@", "Claim InQuiry Error Details - CE Assist App");


     WarrantyAPIConnection mandrillAPI = new WarrantyAPIConnection();
     response = mandrillAPI.getMandrillAPI(mandrillString);
     logger.info("=========>Mandril Email Response : " + response);

     return response;

     } catch (Exception e) {
     e.printStackTrace();
     return e.toString();
     }

     }*/
    public String sendMail(String buName, int userId, String claimRequest, String claimResponse, String claimNumber, int Claim_WID, String error) {

        try {
            
            String emailRequest = "Hello,\n\nClaim InQuiry Error Details:\nClaim_WID : " + Claim_WID + "\n\nClaim Number : " + claimNumber + "\n\nClaim Request: \n\t" + claimRequest + "\n\nClaim Response: \n\t" + claimResponse + "\n\nError: \n\t" + error + "\n\nThanks.";
            String subject = "Claim InQuiry Error Details - CE Assist App";

            String[] emailIds = {"mlalwani@teksystems.com", "pjayapal@teksystems.com", "mkaur@teksystems.com", "uyalant@teksystems.com"};

            MailingAPIImpl mailAPI = new MailingAPIImpl();

            String format = "TEXT";

//        String[] emailIds = emailId.split("\\s*,\\s*");
            String jsonString = mailAPI.sendEmail(buName, userId, emailRequest, emailIds, subject, format);

            return jsonString;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (SendGridException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
