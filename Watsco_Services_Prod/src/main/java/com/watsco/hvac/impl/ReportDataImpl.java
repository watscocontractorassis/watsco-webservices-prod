package com.watsco.hvac.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.watsco.hvac.common.DBConnection;
import com.watsco.hvac.usagetracking.UsageTracking;

public class ReportDataImpl {

//    protected static Logger logger = Logger.getLogger(ReportDataImpl.class);
    private static final Logger logger = Logger.getLogger(ReportDataImpl.class);

    public void getReportDataService(String ipAddress, int reportId,
            String BU_CODE, String udid, int userId, String pid, String cid,
            String device) {
        // TODO Auto-generated method stub

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sqlQuery;
        String insertQuery;
        String response = null;
        try {

            con = DBConnection.getConnection(true);

//            insertQuery = "INSERT INTO Marketing_Clicks (IP_Address,BU_Code,PID,CID,Device) VALUES (?,?,?,?,?)";
            insertQuery = "EXEC Marketing_Clicks_Insert '" + ipAddress + "','" + BU_CODE + "','" + pid + "','" + cid + "','" + device + "'";

            pstmt = con.prepareStatement(insertQuery);
            /*pstmt.setString(1, ipAddress);
             pstmt.setString(2, BU_CODE);
             pstmt.setString(3, pid);
             pstmt.setString(4, cid);
             pstmt.setString(5, device);*/
            String buName = "";
            logger.info("=== USER ID :" + userId + "Report Query : " + insertQuery);

            pstmt.executeUpdate();

            if (BU_CODE.equalsIgnoreCase("GM")) {
                buName = "GEMAIRE";
            }
            if (BU_CODE.equalsIgnoreCase("CE")) {
                buName = "CARRIER ENTERPRISE";
            }
            if (BU_CODE.equalsIgnoreCase("BK")) {
                buName = "BAKER";
            }
            if (BU_CODE.equalsIgnoreCase("CE_MX")) {
                buName = "ARRIER ENTERPRISE MEXICO";
            }
            if (BU_CODE.equalsIgnoreCase("ECM") || BU_CODE.equalsIgnoreCase("ECMD")) {
                buName = "EAST COAST METALS";
            }
            String searchedData = "ReportData Services";

            int installId = 1;
            UsageTracking usageTracking = new UsageTracking();
            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                pstmt.close();
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
