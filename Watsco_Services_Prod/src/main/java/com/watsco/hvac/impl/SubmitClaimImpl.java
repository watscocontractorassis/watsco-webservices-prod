package com.watsco.hvac.impl;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.InputSource;

import com.watsco.hvac.businesslayer.WarrantyClaimLayer;
import com.watsco.hvac.common.CommonMethods;
import com.watsco.hvac.dao.SoapConnection;
import com.watsco.hvac.dto.ClaimDTO;
import com.watsco.hvac.dto.WarrantyResponseDTO;
import com.watsco.hvac.handler.WarrantyXMLHandler;
import com.watsco.hvac.usagetracking.UsageTracking;
import com.watsco.hvac.utils.CommonUtil;
import com.watsco.hvac.utils.ConnectConfig;
import com.watsco.hvac.utils.EncyrptionUtils;
import com.watsco.hvac.utils.LocalCommonUtil;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import static org.apache.commons.lang3.StringEscapeUtils.escapeJava;

public class SubmitClaimImpl {

//    protected Logger logger = Logger.getLogger(SubmitClaimImpl.class);
    private static final Logger logger = Logger.getLogger(SubmitClaimImpl.class);
    WarrantyClaimImpl claimImpl = new WarrantyClaimImpl();

    public String getSubmitClaimAuthentication(ClaimDTO claimNDTO, int installId, int userId, String udid, int reportId,
            String buName, String returnType, String SB_ID, String UId,
            String password, String save, String isDev, int buWid, String processStatus) throws ParserConfigurationException, IOException {

        try {
            // TODO Auto-generated method stub
            String decryptPassword = "";
            String claimUrl;
            String s = null;
            String CLAIM_WidResponse = "";
            String claimNumber = "";
            String claimStatus = "";
            String CWID = "";
            int CLAIM_WID = 0;
            String submitClaimRequestXML = "";
            String submitClaimResponse = "";
            int inQuiry = 0;
            String errorText = "";
            String rejectReason = "";
            String searchedData = "";
            String shared = "";
            String msgStatus = "";
            Connection con = null;
            String status = null;

            Statement stmt;
            ResultSet rs;
            HashMap<String, String> claimMap = new HashMap<String, String>();
            ArrayList<HashMap<String, String>> alClaimParts = new ArrayList<HashMap<String, String>>();

            SoapConnection soapConnection = new SoapConnection();
            WarrantyClaimLayer warrantyLayer = new WarrantyClaimLayer();

            HashMap<String, String> exthm;

            if (ConnectConfig.setConnectConfig.equalsIgnoreCase("Y")) {
                exthm = CommonUtil.getExternalAPIConfigProperties(buName);
            } else {
                exthm = LocalCommonUtil.getExternalAPIConfigProperties(buName);
            }

            String key = "";

            switch (buName) {
                case "CARRIER ENTERPRISE":
                    key = "CE_";
                    break;
                case "BAKER":
                    key = "BK_";
                    break;
                case "CARRIER ENTERPRISE MEXICO":
                    key = "CE_MX_";
                    break;
                case "GEMAIRE":
                    key = "GM_";
                    break;
                case "EAST COAST METALS":
                    key = "ECM_";
                    break;
                default:
                    break;
            }

            if ((SB_ID == null) && (UId == null) && (password == null)) {
                SB_ID = exthm.get(key + "SerivceBench-ID");
                UId = exthm.get(key + "ServiceBench-User");
                password = exthm.get(key + "ServiceBench-Password");
            }

            if (!(isDev.equalsIgnoreCase("Y"))) {
                claimUrl = exthm.get(key + "WarrantyClaim_SerivceBench_API_PROD");
                if (claimUrl == null || claimUrl.isEmpty()) {
                    logger.error("=== USER ID :" + userId + "===getSubmitClaimAuthentication()====>WarrantyServiceBench API not Found");
                    return submitClaimResponse;
                }
//			   claimUrl = "https://www.servicebench.com/servicebenchv5/services/RTIClaimService";
            } else {
                claimUrl = exthm.get(key + "WarrantyClaim_SerivceBench_API_DEV");
//			   claimUrl = "https://test.servicebench.com/servicebenchv5/services/RTIClaimService";

                if (claimUrl == null || claimUrl.isEmpty()) {
                    logger.error("=== USER ID :" + userId + "===getSubmitClaimAuthentication()====>WarrantyServiceBench API not Found");
                    return submitClaimResponse;
                }
            }

            String SB_UID = SB_ID + ":" + UId;

            String version = exthm.get(key + "version");
            String sourceSystemName = exthm.get(key + "CE_sourceSystemName");
            String sourceSystemVersion = exthm.get(key + "sourceSystemVersion");
            String serviceAdministratorId = exthm.get(key + "serviceAdministratorId");

            String accNo;

            HashMap<String, String[]> hm = new HashMap<String, String[]>();
            if (!(claimNDTO == null)) {
                claimMap = claimNDTO.getClaimMap();
                alClaimParts = claimNDTO.getAlClaimParts();
            }

            if (claimMap.containsKey("Claim_WID")) {
                CWID = claimMap.get("Claim_WID");
                if ((CWID.length() > 0) && !(CWID.equals(" "))) {
                    CLAIM_WID = Integer.parseInt(CWID);
                }
            }

            if (claimMap.get("accountNumber") != null) {
                accNo = claimMap.get("accountNumber");
                boolean isAccountNumber = accNo.isEmpty();
                if (isAccountNumber) {
                    accNo = SB_ID;
                }
            } else {
                accNo = SB_ID;
            }

            if (CLAIM_WID == 0) {
                CLAIM_WidResponse = warrantyLayer.insertDB(claimNDTO, claimMap, alClaimParts, CLAIM_WID, claimNumber, claimStatus, userId, save, SB_ID, accNo, buWid, processStatus);
            } else {

                status = claimImpl.selectWarranty(CLAIM_WID, userId, status);

                if (status != null && status.equalsIgnoreCase("Save")) {
                    processStatus = "Submitted";
                } else if (status != null && status.equalsIgnoreCase("Submitted")) {
                    processStatus = "Save Submitted";
                }

                CLAIM_WidResponse = warrantyLayer.updateDB(claimNDTO, claimMap, alClaimParts, claimNumber, CLAIM_WID, claimStatus, UId, userId, inQuiry, errorText, rejectReason, SB_ID, accNo, processStatus);
            }

            JSONObject jObject;
            JSONArray jArray = new JSONArray(CLAIM_WidResponse);
            for (int i = 0; i < jArray.length(); i++) {
                jObject = jArray.getJSONObject(i);
                s = jObject.getString("Claim_WID");
            }

            CLAIM_WID = Integer.parseInt(s);

            if (claimMap.get("originalOwner") == null) {
                claimMap.put("originalOwner", "");
            }

            shared = claimMap.get("shared");
            try {

                submitClaimRequestXML
                        = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://servicebench.com/claim/service/types\">"
                        + "<soapenv:Header>"
                        + "<wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                        + "<wsse:UsernameToken wsu:Id=\"UsernameToken-15231583\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
                        + "<wsse:Username>" + SB_UID + "</wsse:Username>"
                        + "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + password + "</wsse:Password>"
                        + "</wsse:UsernameToken>"
                        + "</wsse:Security>"
                        + "</soapenv:Header>"
                        + "<soapenv:Body>"
                        + "<typ:claimSubmitRequest>"
                        + "<typ:version>" + version + "</typ:version>"
                        + "<typ:sourceSystemName><![CDATA[" + sourceSystemName + "]]></typ:sourceSystemName>"
                        + "<typ:sourceSystemVersion><![CDATA[" + sourceSystemVersion + "]]></typ:sourceSystemVersion>"
                        + "<typ:claim>"
                        + "<typ:importFormatId>123</typ:importFormatId>"
                        //                        + "<typ:claimNumber>" + (claimMap.get("claimNumber") == null ? "" : claimMap.get("claimNumber")) + "</typ:claimNumber>"
                        + "<typ:serviceAdministratorId><![CDATA[" + serviceAdministratorId + "]]></typ:serviceAdministratorId>"
                        + "<typ:referenceNumber><![CDATA[" + (claimMap.get("referenceNumber") == null ? "" : claimMap.get("referenceNumber")) + "]]></typ:referenceNumber>"
                        + "<typ:claimStatus></typ:claimStatus>"
                        + "<typ:accountNumber>" + accNo + "</typ:accountNumber>"
                        + "<typ:storeNumber></typ:storeNumber>"
                        + "<typ:serviceAgreementNumber><![CDATA[" + (claimMap.get("serviceAgreementNumber") == null ? "" : claimMap.get("serviceAgreementNumber")) + "]]></typ:serviceAgreementNumber>"
                        + "<typ:authorizationNumber><![CDATA[" + (claimMap.get("authorizationNumber") == null ? "" : claimMap.get("authorizationNumber")) + "]]></typ:authorizationNumber>"
                        + "<typ:brand><![CDATA[" + (claimMap.get("brand") == null ? "CAR" : claimMap.get("brand")) + "]]></typ:brand>"
                        + "<typ:modelNumber><![CDATA[" + (claimMap.get("modelNumber") == null ? "" : claimMap.get("modelNumber")) + "]]></typ:modelNumber>"
                        + "<typ:customerCompanyName><![CDATA[" + (claimMap.get("customerCompanyName") == null ? "" : claimMap.get("customerCompanyName")) + "]]></typ:customerCompanyName>"
                        + "<typ:customerLastName><![CDATA[" + (claimMap.get("customerLastName") == null ? "" : claimMap.get("customerLastName")) + "]]></typ:customerLastName>"
                        + "<typ:customerFirstName><![CDATA[" + (claimMap.get("customerFirstName") == null ? "" : claimMap.get("customerFirstName")) + "]]></typ:customerFirstName>"
                        + "<typ:customerAddressLine1><![CDATA[" + (claimMap.get("customerAddressLine1") == null ? "" : claimMap.get("customerAddressLine1")) + "]]></typ:customerAddressLine1>"
                        + "<typ:customerAddressLine2><![CDATA[" + (claimMap.get("customerAddressLine2") == null ? "" : claimMap.get("customerAddressLine2")) + "]]></typ:customerAddressLine2>"
                        + "<typ:customerAddressCity><![CDATA[" + (claimMap.get("customerAddressCity") == null ? "" : claimMap.get("customerAddressCity")) + "]]></typ:customerAddressCity>"
                        + "<typ:customerAddressState><![CDATA[" + (claimMap.get("customerAddressState") == null ? "" : claimMap.get("customerAddressState")) + "]]></typ:customerAddressState>"
                        + "<typ:customerAddressZip><![CDATA[" + (claimMap.get("customerAddressZip") == null ? "" : claimMap.get("customerAddressZip")) + "]]></typ:customerAddressZip>"
                        + "<typ:customerAddressCountry></typ:customerAddressCountry>"
                        + "<typ:customerPhone><![CDATA[" + (claimMap.get("customerPhone") == null ? "" : claimMap.get("customerPhone")) + "]]></typ:customerPhone>"
                        + "<typ:customerPhoneAlternate></typ:customerPhoneAlternate>"
                        + "<typ:serialNumber><![CDATA[" + (claimMap.get("serialNumber") == null ? "" : claimMap.get("serialNumber")) + "]]></typ:serialNumber>"
                        + "<typ:purchaseDate><![CDATA[" + (claimMap.get("purchaseDate") == null ? "" : claimMap.get("purchaseDate")) + "]]></typ:purchaseDate>"
                        + "<typ:purchasedFrom></typ:purchasedFrom>"
                        + "<typ:purchasedFromAddress></typ:purchasedFromAddress>"
                        + "<typ:purchasedFromState></typ:purchasedFromState>"
                        + "<typ:warrantyType><![CDATA[" + (claimMap.get("warrantyType") == null ? "" : claimMap.get("warrantyType")) + "]]></typ:warrantyType>"
                        + "<typ:customerComplaint><![CDATA[" + (claimMap.get("customerComplaint") == null ? "" : claimMap.get("customerComplaint")) + "]]></typ:customerComplaint>"
                        + "<typ:defectCode><![CDATA[" + (claimMap.get("defectCode") == null ? "" : claimMap.get("defectCode")) + "]]></typ:defectCode>"
                        + "<typ:dateServiceRequested><![CDATA[" + (claimMap.get("dateServiceRequested") == null ? "" : claimMap.get("dateServiceRequested")) + "]]></typ:dateServiceRequested>"
                        + "<typ:dateServiceCompleted><![CDATA[" + (claimMap.get("dateServiceCompleted") == null ? "" : claimMap.get("dateServiceCompleted")) + "]]></typ:dateServiceCompleted>"
                        + "<typ:serviceExplanation></typ:serviceExplanation>"
                        + "<typ:servicePerformed></typ:servicePerformed>"
                        + "<typ:manufacturerRefCode></typ:manufacturerRefCode>"
                        + "<typ:repairCategory><![CDATA[" + (claimMap.get("repairCategory") == null ? "" : claimMap.get("repairCategory")) + "]]></typ:repairCategory>"
                        + "<typ:timeStarted></typ:timeStarted>"
                        + "<typ:timeCompleted></typ:timeCompleted>"
                        + "<typ:timeOnJob></typ:timeOnJob>"
                        + "<typ:checkNumber></typ:checkNumber>"
                        + "<typ:rejectReason></typ:rejectReason>"
                        + "<typ:laborAmount><![CDATA[" + (claimMap.get("laborAmount") == null ? "" : claimMap.get("laborAmount")) + "]]></typ:laborAmount>"
                        + "<typ:diagnosticFee><![CDATA[" + (claimMap.get("diagnosticFee") == null ? "" : claimMap.get("diagnosticFee")) + "]]></typ:diagnosticFee>"
                        + "<typ:shippingAmount><![CDATA[" + (claimMap.get("shippingAmount") == null ? "" : claimMap.get("shippingAmount")) + "]]></typ:shippingAmount>"
                        + "<typ:freightAmount><![CDATA[" + (claimMap.get("freightAmount") == null ? "" : claimMap.get("freightAmount")) + "]]></typ:freightAmount>"
                        + "<typ:mileageAmount></typ:mileageAmount>"
                        + "<typ:travelAmount><![CDATA[" + (claimMap.get("travelAmount") == null ? "" : claimMap.get("travelAmount")) + "]]></typ:travelAmount>"
                        + "<typ:localTaxAmount></typ:localTaxAmount>"
                        + "<typ:stateTaxAmount><![CDATA[" + (claimMap.get("stateTaxAmount") == null ? "" : claimMap.get("stateTaxAmount")) + "]]></typ:stateTaxAmount>"
                        + "<typ:otherAmount><![CDATA[" + (claimMap.get("otherAmount") == null ? "" : claimMap.get("otherAmount")) + "]]></typ:otherAmount>"//serviceMaterialsAmount
                        + "<typ:otherItem1Amount><![CDATA[" + (claimMap.get("otherItem1Amount") == null ? "" : claimMap.get("otherItem1Amount")) + "]]></typ:otherItem1Amount>" // refrigerantAmount
                        + "<typ:otherItem1Code></typ:otherItem1Code>"
                        + "<typ:otherItem2Amount><![CDATA[" + (claimMap.get("otherItem2Amount") == null ? "" : claimMap.get("otherItem2Amount")) + "]]></typ:otherItem2Amount>" // adminAllowanceAmount
                        + "<typ:otherItem2Code></typ:otherItem2Code>"
                        + "<typ:travelHours><![CDATA[" + (claimMap.get("travelHours") == null ? "" : claimMap.get("travelHours")) + "]]></typ:travelHours>"
                        + "<typ:dispatchNumber></typ:dispatchNumber>"
                        + "<typ:customerEmail><![CDATA[" + (claimMap.get("customerEmail") == null ? "" : claimMap.get("customerEmail")) + "]]></typ:customerEmail>"
                        + "<typ:technicianName></typ:technicianName>"
                        + "<typ:technicianID></typ:technicianID>"
                        + "<typ:dateServiceStarted></typ:dateServiceStarted>"
                        + "<typ:claimParts>" + getClaimParts(alClaimParts) + "</typ:claimParts>"
                        + "<typ:comment><![CDATA[" + (claimMap.get("comment") == null ? null : claimMap.get("comment")) + "]]></typ:comment>"
                        + "<typ:laborHours><![CDATA[" + (claimMap.get("laborHours") == null ? null : claimMap.get("laborHours")) + "]]></typ:laborHours>"
                        + "<typ:zipCode></typ:zipCode>"
                        + "<typ:productType></typ:productType>"
                        + "<typ:productWarranty></typ:productWarranty>"
                        + "<typ:extendedWarranty></typ:extendedWarranty>"
                        + "<typ:partWarranty></typ:partWarranty>"
                        + "<typ:specialLaborAllowance></typ:specialLaborAllowance>"
                        + "<typ:rmaOrTagNumber></typ:rmaOrTagNumber>"
                        + "<typ:dateInstalled></typ:dateInstalled>"
                        + "<typ:dateService></typ:dateService>"
                        + "<typ:otherModelNumber></typ:otherModelNumber>"
                        + "<typ:otherSerialNumber></typ:otherSerialNumber>"
                        + "<typ:contractorNumber></typ:contractorNumber>"
                        + "<typ:serviceContractor></typ:serviceContractor>"
                        + "<typ:serviceAddress></typ:serviceAddress>"
                        + "<typ:serviceCity></typ:serviceCity>"
                        + "<typ:serviceState></typ:serviceState>"
                        + "<typ:serviceZipCode></typ:serviceZipCode>"
                        + "<typ:componentCode><![CDATA[" + (claimMap.get("componentCode") == null ? "" : claimMap.get("componentCode")) + "]]></typ:componentCode>"
                        + "<typ:causeCode></typ:causeCode>"
                        + "<typ:projectCode></typ:projectCode>"
                        + "<typ:compressorFailedSerial></typ:compressorFailedSerial>"
                        + "<typ:compressorNewSerial></typ:compressorNewSerial>"
                        + "<typ:reasonForFailure></typ:reasonForFailure>"
                        + "<typ:extServiceContract></typ:extServiceContract>"
                        + "<typ:expDate></typ:expDate>"
                        + "<typ:partsSource></typ:partsSource>"
                        + "<typ:totalCharges></typ:totalCharges>"
                        + "<typ:invoiceNumber></typ:invoiceNumber>"
                        + "<typ:crdChkAmount></typ:crdChkAmount>"
                        + "<typ:creditMemo></typ:creditMemo>"
                        + "<typ:replacementAmount></typ:replacementAmount>"
                        + "<typ:prfDebitNumber></typ:prfDebitNumber>"
                        + "<typ:mileageCartage></typ:mileageCartage>"
                        + "<typ:distRepresentative></typ:distRepresentative>"
                        + "<typ:formDate></typ:formDate>"
                        + "<typ:success></typ:success>"
                        + "<typ:competitiveEquipment><![CDATA[" + (claimMap.get("competitiveEquipment") == null ? "" : claimMap.get("competitiveEquipment")) + "]]></typ:competitiveEquipment>"
                        + "<typ:additionalRefNo></typ:additionalRefNo>"
                        + "<typ:ccsJobNumber><![CDATA[" + (claimMap.get("ccsJobNumber") == null ? "" : claimMap.get("ccsJobNumber")) + "]]></typ:ccsJobNumber>"
                        + "<typ:applicationType><![CDATA[" + (claimMap.get("applicationType") == null ? "" : claimMap.get("applicationType")) + "]]></typ:applicationType>"
                        + "<typ:originalOwner><![CDATA[" + (claimMap.get("originalOwner").equals("") ? "1" : claimMap.get("originalOwner")) + "]]></typ:originalOwner>"
                        + "<typ:modelLocation><![CDATA[" + (claimMap.get("modelLocation") == null ? "" : claimMap.get("modelLocation")) + "]]></typ:modelLocation>"
                        + "<typ:furnaceOrientation><![CDATA[" + (claimMap.get("furnaceOrientation") == null ? "" : claimMap.get("furnaceOrientation")) + "]]></typ:furnaceOrientation>"
                        + "<typ:furnaceFuel><![CDATA[" + (claimMap.get("furnaceFuel") == null ? "" : claimMap.get("furnaceFuel")) + "]]></typ:furnaceFuel>"
                        + "<typ:diagnosticHours><![CDATA[" + (claimMap.get("diagnosticHours") == null ? "" : claimMap.get("diagnosticHours")) + "]]></typ:diagnosticHours>"
                        + "<typ:refrigerantLbs><![CDATA[" + (claimMap.get("refrigerantLbs") == null ? "" : claimMap.get("refrigerantLbs")) + "]]></typ:refrigerantLbs>"
                        + "<typ:creditCardPayment><![CDATA[" + (claimMap.get("creditCardPayment") == null ? "N" : claimMap.get("creditCardPayment")) + "]]></typ:creditCardPayment>"
                        + "<typ:holdB><![CDATA[" + (claimMap.get("holdB") == null ? 0 : claimMap.get("holdB")) + "]]></typ:holdB>"
                        + "<typ:geoLatitude><![CDATA[" + (claimMap.get("geoLatitude") == null ? "" : claimMap.get("geoLatitude")) + "]]></typ:geoLatitude>"
                        + "<typ:geoLongitude><![CDATA[" + (claimMap.get("geoLongitude") == null ? "" : claimMap.get("geoLongitude")) + "]]></typ:geoLongitude>"
                        + "<typ:dateApproved></typ:dateApproved>"
                        + "<typ:dateRejected></typ:dateRejected>"
                        + "<typ:datePaid></typ:datePaid>"
                        + "<typ:claimDate></typ:claimDate>"
                        + "<typ:dateSubmitted></typ:dateSubmitted>"
                        + "</typ:claim>"
                        + "</typ:claimSubmitRequest>"
                        + "</soapenv:Body>"
                        + "</soapenv:Envelope>";

            } catch (Exception e) {
                e.printStackTrace();
                logger.error("SOAP Error " + e);
            }

            logger.info("=== USER ID :" + userId + "===getSubmitClaimAuthentication()====>requestUrl : " + claimUrl);
            logger.info("=== USER ID :" + userId + "===getSubmitClaimAuthentication()====>Request : " + submitClaimRequestXML);
            decryptPassword = EncyrptionUtils.decryptCredentials(password);

            decryptPassword = escapeJava(decryptPassword);

            submitClaimRequestXML = submitClaimRequestXML.replaceAll(password, decryptPassword);
            submitClaimResponse = soapConnection.soapPostResponse(submitClaimRequestXML, new URL(claimUrl), "");

            if (submitClaimResponse.contains("<html>")) {

                String serviceResponse = CommonMethods.getWarrantyTagValue("<html><body>", "</body></html>", submitClaimResponse);
                JSONObject errorObject = new JSONObject();
                errorObject.put("Claim_WID", CLAIM_WID);
                errorObject.put("errorMsgStatement", serviceResponse);
                logger.error("=== USER ID :" + userId + "===getSubmitClaimAuthentication()====>Response : " + errorObject.toString());
                UsageTracking usageTracking = new UsageTracking();
                usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, "Claim Submission Error - Server is temporarily unable", "", "");
                return errorObject.toString();
            }

            claimNumber = CommonMethods.getWarrantyTagValue("<claimNumber>", "</claimNumber>", submitClaimResponse);
            claimStatus = CommonMethods.getWarrantyTagValue("<claimStatus>", "</claimStatus>", submitClaimResponse);
            logger.info("=== USER ID :" + userId + "===getSubmitClaimAuthentication()====>Claim Status : " + claimStatus);
            msgStatus = CommonMethods.getWarrantyTagValue("<msgStatus>", "</msgStatus>", submitClaimResponse);

            if (submitClaimResponse.length() > 0 && !submitClaimResponse.contains("<soap:Fault>")) {
//	        if(!submitClaimResponse.contains("<statusDetails>")){
                logger.info("=== USER ID :" + userId + "===getSubmitClaimAuthentication()====>SOAP Response : " + submitClaimResponse);
                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser saxParser = factory.newSAXParser();
                WarrantyXMLHandler userhandler = new WarrantyXMLHandler(claimNDTO, UId, userId, CLAIM_WID, SB_ID, buName);
                saxParser.parse(new InputSource(new StringReader(submitClaimResponse)), userhandler);
//		        
                CLAIM_WidResponse = WarrantyResponseDTO.getSubmitClaimResponse();

                logger.info("=== USER ID :" + userId + "===getSubmitClaimAuthentication()====>JSON Response: " + CLAIM_WidResponse);

                if (CLAIM_WidResponse != null && CLAIM_WidResponse.length() > 0) {
                    CLAIM_WidResponse = CLAIM_WidResponse.substring(1, CLAIM_WidResponse.length() - 1);
                }

                if (claimStatus.equals("")) {
                    claimStatus = msgStatus;
                }
                if (shared != null) {
                    if (shared.equalsIgnoreCase("Y")) {
                        searchedData = "Claim Saved for Claim Transfer. CLAIM_WID - " + CLAIM_WID;
                        UsageTracking usageTracking = new UsageTracking();
                        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");
                    } else {
                        searchedData = "Successfully Submitted Claim. Status : " + claimStatus;
                        UsageTracking usageTracking = new UsageTracking();
                        usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");
                    }
                } else {
                    searchedData = "Successfully Submitted Claim.  Status : " + claimStatus;
                    UsageTracking usageTracking = new UsageTracking();
                    usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, searchedData, "", "");
                }

                return CLAIM_WidResponse;
//	        }
            } else {
                JSONObject errorObject = new JSONObject();
                errorObject.put("Claim_WID", CLAIM_WID);
                String warrantyTagValue = CommonMethods.getWarrantyTagValue("<faultstring>", "</faultstring>", submitClaimResponse);
                errorObject.put("errorMsgStatement", warrantyTagValue);
                logger.error("=== USER ID :" + userId + "===getSubmitClaimAuthentication()====>Response : " + errorObject.toString());
                UsageTracking usageTracking = new UsageTracking();
                usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, "Claim Submission Error - " + warrantyTagValue, "", "");
                return errorObject.toString();
            }

//            submitClaimResponse = "<RESULT>" + submitClaimResponse + "</RESULT>";
//            logger.error("=== USER ID :" + userId + "===getSubmitClaimAuthentication()====>Response : " + submitClaimResponse);
//            UsageTracking usageTracking = new UsageTracking();
//            usageTracking.usageTrackingSp(installId, userId, udid, reportId, buName, "Claim Submission Error", "", "");
//            return submitClaimResponse;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error("=== USER ID :" + userId + "===Submit Claim ERROR=====>" + e);
            return e.toString();
        }

    }

    private String getClaimParts(ArrayList<HashMap<String, String>> al) {

        StringBuffer claimPartsRequest = new StringBuffer();

        for (int i = 0; i < al.size(); i++) {

            claimPartsRequest.append(
                    "<typ:claimPart>"
                    + "<typ:partNumber><![CDATA[" + (al.get(i).get("partNumber") == null ? "" : al.get(i).get("partNumber")) + "]]></typ:partNumber>"
                    + "<typ:partQuantity><![CDATA[" + (al.get(i).get("partQuantity") == null ? "" : al.get(i).get("partQuantity")) + "]]></typ:partQuantity>"
                    + "<typ:partPrice><![CDATA[" + (al.get(i).get("partPrice") == null ? "" : al.get(i).get("partPrice")) + "]]></typ:partPrice>"
                    + "<typ:partReferenceNumber><![CDATA[" + (al.get(i).get("partReferenceNumber") == null ? "" : al.get(i).get("partReferenceNumber")) + "]]></typ:partReferenceNumber>"
                    + "<typ:partInvoiceNumber><![CDATA[" + (al.get(i).get("partInvoiceNumber") == null ? "" : al.get(i).get("partInvoiceNumber")) + "]]></typ:partInvoiceNumber>"
                    + "<typ:partDistributorNumber><![CDATA[" + (al.get(i).get("partDistributorNumber") == null ? "" : al.get(i).get("partDistributorNumber")) + "]]></typ:partDistributorNumber>"
                    + "<typ:partDescription><![CDATA[" + (al.get(i).get("partDescription") == null ? "" : al.get(i).get("partDescription")) + "]]></typ:partDescription>"
                    + "<typ:partReplacementPartNumber><![CDATA[" + (al.get(i).get("partReplacementPartNumber") == null ? "" : al.get(i).get("partReplacementPartNumber")) + "]]></typ:partReplacementPartNumber>"
                    + "<typ:failedPartNumber><![CDATA[" + (al.get(i).get("failedPartNumber") == null ? "" : al.get(i).get("failedPartNumber")) + "]]></typ:failedPartNumber>"
                    + "<typ:failedPartQuantity><![CDATA[" + (al.get(i).get("failedPartQuantity") == null ? "" : al.get(i).get("failedPartQuantity")) + "]]></typ:failedPartQuantity>"
                    + "<typ:failedPartSerial><![CDATA[" + (al.get(i).get("failedPartSerial") == null ? "" : al.get(i).get("failedPartSerial")) + "]]></typ:failedPartSerial>"
                    + "<typ:failedPartInstallDate><![CDATA[" + (al.get(i).get("failedPartInstallDate") == null ? "" : al.get(i).get("failedPartInstallDate")) + "]]></typ:failedPartInstallDate>"
                    + "<typ:partDisposition><![CDATA[" + (al.get(i).get("partDisposition") == null ? "" : al.get(i).get("partDisposition")) + "]]></typ:partDisposition>"
                    + "<typ:partMarkUp><![CDATA[" + (al.get(i).get("partMarkUp") == null ? "" : al.get(i).get("partMarkUp")) + "]]></typ:partMarkUp>"
                    + "<typ:partAmount><![CDATA[" + (al.get(i).get("partAmount") == null ? "" : al.get(i).get("partAmount")) + "]]></typ:partAmount>"
                    + "<typ:partSerialNumber><![CDATA[" + (al.get(i).get("partSerialNumber") == null ? "" : al.get(i).get("partSerialNumber")) + "]]></typ:partSerialNumber>"
                    + "</typ:claimPart>");
        }

        return claimPartsRequest.toString();
    }

}
