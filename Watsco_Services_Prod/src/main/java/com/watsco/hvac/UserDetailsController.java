package com.watsco.hvac;

import java.io.InputStream;
import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.json.JSONException;
import org.json.JSONObject;

import com.watsco.hvac.DBConnection.UserDetailsAPIConnection;
import com.watsco.hvac.common.CommonMethods;
import com.watsco.hvac.utils.JSONExtractor;
import com.watsco.hvac.utils.StringUtils;
import com.watsco.hvac.utils.URLGenerationUtility;
import org.apache.log4j.Logger;

@Path("/UserDetailsService")
public class UserDetailsController {

    private static final Logger logger = Logger.getLogger(UserDetailsController.class);
    String serverUrl = "";
    UserDetailsAPIConnection userImpl = new UserDetailsAPIConnection();
    JSONExtractor extractor = new JSONExtractor();

//	http://localhost:8081/watsco-v4-services/watsco/UserDetailsService/userdetails?buName=CARRIER%20ENTERPRISE&userId=99810&udid=f31d553a4c137dca
    @POST
    @Path("/userdetails")
    public Response getUserDetails(@QueryParam("buName") String buName,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid, @Context UriInfo uriInfo,
            InputStream inStream
    ) throws JSONException {
        JSONObject jSONObject = extractor.getJsonObject(inStream);

        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("POST", "UserDetailsService", "userdetails", uriInfo, jSONObject.toString());

        String response = "";

        response = userImpl.getUserDetailsImpl(userId, udid, buName, jSONObject);
        return Response.status(200).entity(response).header("Content-Type", "application/json;charset=UTF-8").build();
    }

//	http://localhost:8081/watsco-v4-services/watsco/UserDetailsService/userBranchUpdate?installId=1&userId=99810&udid=f31d553a4c137dca&reportId=47&buWid=4&branchNo=500081
    @GET
    @Path("/userBranchUpdate")
    public Response getUserDetails(@QueryParam("installId") int installId,
            @QueryParam("userId") int userId,
            @QueryParam("udid") String udid,
            @QueryParam("reportId") int reportId,
            @QueryParam("buWid") int buWid,
            @QueryParam("branchNo") String branchNo,
            @QueryParam("zipCode") String zipCode,
            @Context UriInfo uriInfo
    ) throws JSONException {
        URLGenerationUtility utility = new URLGenerationUtility();
        utility.printServiceUrl("GET", "UserDetailsService", "userBranchUpdate", uriInfo, "");

        String response = "";
        HashMap<Object, Object> keyValue = CommonMethods.removeNullValues(uriInfo);

        HashMap<String, String> prompts = new HashMap<String, String>();
        response = userImpl.getUserBranchUpdateImpl(installId, userId, udid, reportId, keyValue);

        return Response.status(200).entity(response).build();
    }
}
